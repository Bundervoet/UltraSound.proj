%% 2736D SYSTEM SETUP
%
% This script is used to generate a dictionary of Point Spread Functions (PSF) in order to linearly
% represent RAW channel RFdata obtained from the Cyst Setup.
%
% Created by Shaun Bundervoet on 26 MAY 16.
% Latest version 22 JUN 16.
%
%% PATH SETUP
%
simulationPath  = '/Volumes/WORK_HDD/MATLAB/Files/[S] Simulations/UltraSound/160420/2730E' ;
workingPath     = '/Volumes/WORK_HDD/MATLAB/Files/[S] Simulations/UltraSound/160420/2736D' ;
%
%% GENERATE PHANTOM
%
% Grid Size = 16 x 1 x Z ;
%
for tt = 1:3 ;
    UStransducer  = loadNow(simulationPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ;
    dx = UStransducer.elementPitch ; noX = 16 ; xStart = -(noX-1)/2 * dx ;
    dy = 1 ; noY = 1 ; yStart = 0 ;
    dz = UStransducer.pixelPitch('f0') ; zExtra = 7 - mod(floor((60/1000)/ dz)-1, 8) ;
    zStart = 30/1000 - floor(zExtra/2)*dz ; noZ = floor((60/1000)/ dz) + zExtra ;
    PHparameters = {'xStart', xStart, 'dx', dx, 'noX', noX, ...
        'yStart', yStart, 'dy', dy, 'noY', noY, ...
        'zStart', zStart, 'dz', dz, 'noZ', noZ} ;
    %
    PHdata = GridPhantom(['PhantomT', sprintf('%02d', tt)], PHparameters{:}) ;
    PHpath = {workingPath, 'Dictionary', 'Phantom'} ;
    PHdata.saveNow(PHpath) ;
end
%
%
%% SIMULATE ENERGY DENSITY DATA
%
for tt = 1:3 ;
    PHdata          = loadNow(workingPath, 'Dictionary', 'Phantom', ['PhantomT', sprintf('%02d', tt)]) ;
    UStransducer    = loadNow(simulationPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ; UStransducer.setupTransducer ;
    RFpath          = {workingPath, 'Dictionary', 'RadioFrequencyData', 'EDdata'} ;
    RFdata          = UStransducer.simulateEDline(['EDlineT', sprintf('%02d', tt)], PHdata, 0) ;
    RFdata.saveNow(RFpath) ;
    %
    RFdata          = UStransducer.simulateEDlineA(['EDlineT', sprintf('%02d', tt), ' -A'], PHdata, 0) ;
    RFdata.saveNow(RFpath) ;
end
%
%
%% SIMULATE RADIO FREQUENCY POINT (PSF) DATA
%
% Using the predefined UStransducer 'noS are simulated from the Cyst Phantom. The received
% signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% used during reception. These steps will be applied later during the beamforming stage.
RFparameters = {'rawSignal', true, 'cropSignal', true, 'sparseSignal', true, 'saveSparse', true} ;
for tt = 1:3 ;
    PHdata          = loadNow(workingPath, 'Dictionary', 'Phantom', ['PhantomT', sprintf('%02d', tt)]) ;
    UStransducer    = loadNow(simulationPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ; UStransducer.setupTransducer ;
    currentMaximum = 0 ;
    %
    batchSize = 2500 ;
    for bb = 1:PHdata.noBatch(batchSize) ;
        RFdata = UStransducer.simulateRFbatch(['RFpoints', 'B', sprintf('%02d', bb)], ...
            PHdata, 0, (1:batchSize) + (bb-1) * batchSize , RFparameters{:}) ;
        currentMaximum = max(max(abs(cell2mat(RFdata.signalDATA)))) ;
        %
        RFpath = {workingPath, 'Dictionary', 'RadioFrequencyData', ['RFbatchT', sprintf('%02d', tt)]} ;
        RFdata.saveNow('check', bb == 1, RFpath) ;
    end
    %
    if tt == 1
        USpath = {simulationPath, 'Simulation', 'Transducer'} ;
        UStransducer.set('grid2736D_AbsMaximum', currentMaximum) ;
        UStransducer.saveNow(USpath) ;
    end
end
%
%
%% SIMULATE RADIO FREQUENCY POINT (PSF) DATA (PROCESSED)
% %
% % Using the predefined UStransducer 'noS are simulated from the Cyst Phantom. The received
% % signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% % used during reception. These steps will be applied later during the beamforming stage.
% RFparameters = {'rawSignal', 4, 'cropSignal', true, 'sparseSignal', true, 'saveSparse', true} ;
% for tt = 1:3 ;
%     PHdata          = loadNow(workingPath, 'Dictionary', 'Phantom', ['PhantomT', sprintf('%02d', tt)]) ;
%     UStransducer    = loadNow(workingPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ; UStransducer.setupTransducer ;
%     UStransducer.noReceive = 32 ; currentMaximum = 0 ;
%     %
%     batchSize = 2500 ;
%     for bb = 1:PHdata.noBatch(batchSize) ;
%         RFdata = UStransducer.simulateRFbatch(['RFpoints', 'B', sprintf('%02d', bb)], ...
%             PHdata, 0, (1:batchSize) + (bb-1) * batchSize , RFparameters{:}) ;
%         currentMaximum = max(max(abs(cell2mat(RFdata.signalDATA)))) ;
%         %
%         RFdata.saveNow({workingPath, 'Dictionary', 'RadioFrequencyData', ['RFbatchT', sprintf('%02d', tt), ' -E']}, bb == 1) ;
%     end
%     %
% %     UStransducer.set('gridAbsMaximum', currentMaximum) ;
% %     UStransducer.saveNow({workingPath, 'Simulation', 'Transducer'}) ;
% end
%
%% DEFINE SAMPLING MASK
%
% First we create a full ROW MASK which is used as a template to subsample the recorded RFlines.
% Other sub ROW MASKES are deduced from this template.
for tt = 1:3 ; % 1:3 ;
    RFdata      = loadNow(simulationPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P01'], 'RFdata -N', 'RFline1.mat') ;
    RWmask      = SamplingMask('RWmask -C192') ;
    RWmask.import(RFdata, 'apodisation') ;
    RWmask.saveNow({workingPath, 'Dictionary', 'SamplingMask', 'RWmask', ['T', sprintf('%02d', tt)]}) ;
    %
    for cc = [128 64 32 16 8 4 2]
        RFdata      = loadNow(simulationPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P01'], ['RFdata -N -C', sprintf('%03d', cc)], 'RFline1.mat') ;
        RWpath      = {workingPath, 'Dictionary', 'SamplingMask', 'RWmask', ['T', sprintf('%02d', tt)]} ;
%         RFdata.beamform ;
        RWmask      = SamplingMask(['RWmask -C', sprintf('%03d', cc), ]) ;
        RWmask.import(RFdata, 'apodisation') ;
        RWmask.saveNow(RWpath) ;
    end
    %
    RFdata = loadNow(workingPath, 'Dictionary', 'RadioFrequencyData', 'EDdata', ['EDlineT', sprintf('%02d', tt)]) ;
    CNpath = {workingPath, 'Dictionary', 'SamplingMask', 'CNmask'} ;
    CNmask = SamplingMask(['CNmaskT', sprintf('%02d', tt)]) ;
    CNmask.import(RFdata, 'density') ;
    CNmask.saveNow(CNpath) ;
    %
    colMASK = RFdata.signalDATA ;
    maxMASK = max(colMASK(:)) ;
    filMASK = colMASK ;
    filMASK(colMASK < 55e-5 * maxMASK) = 0 ;
    RFdata.signalDATA = filMASK ;
    CNmask = SamplingMask(['CNmaskT', sprintf('%02d', tt), ' -F']) ;
    CNmask.import(RFdata, 'density', sum(filMASK(:) ~= 0)) ;
    CNmask.saveNow(CNpath) ;
end
%
%
%% CONSTRUCT DICTIONARY MATRIX
%
ff = {''} ;
for tt = 1 ; % 1:3 ;
    PHdata 	= loadNow(workingPath, 'Dictionary', 'Phantom', ['PhantomT', sprintf('%02d', tt)]) ;
    CNmask  = loadNow(workingPath, 'Dictionary', 'SamplingMask', 'CNmask', ['CNmaskT', sprintf('%02d', tt)]) ;
    UStransducer    = loadNow(simulationPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ;
    for cc = [32 16 8 4 2] ; % cc = [128 64 32 16 8 4 2] ;
%         RWmask = loadNow(workingPath, 'Dictionary', 'SamplingMask', 'RWmask', ['T', sprintf('%02d', tt)], ['RWmask -C', sprintf('%03d', cc)]) ;
%         SYmatrix = SystemMatrix(['DCmatrix -C', sprintf('%03d', cc)]) ;
%         RFpath = {workingPath, 'Dictionary', 'RadioFrequencyData', ['RFbatchT', sprintf('%02d', tt)]} ;       
%         SYmatrix.import(PHdata, RWmask, CNmask, RFpath, 'batchMode', true) ;
%         SYpath = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['DCdata', ff{:}]} ;
%         SYmatrix.saveNow(SYpath) ;
        %
        % NORMALISE RF DATA.
        SYmatrix = loadNow(workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['DCdata', ff{:}], ['DCmatrix -C', sprintf('%03d', cc)]) ;
        SYmatrix.normalise(UStransducer.get('grid2736D_AbsMaximum')) ;
        SYpath = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['DCdata', ff{:}, ' -N']} ;
        SYmatrix.saveNow(SYpath) ;
        %
        % PERFORM LOG-DECIBEL TRANSFORM
        SYmatrix = loadNow(workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['DCdata', ff{:}], ['DCmatrix -C', sprintf('%03d', cc)]) ;
        SYmatrix.decibel(UStransducer.get('globalAbsMaximumP01')) ;
        SYpath = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['DCdata', ff{:}, ' -L']} ;
        SYmatrix.saveNow(SYpath) ;
    end
end
%
%
%% CONSTRUCT DICTIONARY MATRIX (ENVELOPPED)
%
% for tt = 1:3 ;
%     PHdata 	= loadNow(workingPath, 'Dictionary', 'Phantom', ['PhantomT', sprintf('%02d', tt)]) ;
%     CNmask  = loadNow(workingPath, 'Dictionary', 'SamplingMask', 'CNmask', ['CNmaskT', sprintf('%02d', tt)]) ;
%     for cc = 32 ; % cc = [32 16 8 4 2] ; % cc = [128 64 32 16 8 4 2] ;
%         RWmask = loadNow(workingPath, 'Dictionary', 'SamplingMask', 'RWmask', ['T', sprintf('%02d', tt)], ['RWmask -C', sprintf('%03d', cc), ' -B']) ;
%         SYmatrix = SystemMatrix(['DCmatrix -C', sprintf('%03d', cc), ' -E']) ;
%         RFpath = {workingPath, 'Dictionary', 'RadioFrequencyData', ['RFbatchT', sprintf('%02d', tt), ' -E']} ;
%         SYmatrix.import(PHdata, RWmask, CNmask, RFpath, 'batchMode', true) ;
%         SYmatrix.saveNow({workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], 'DCdata'}) ;
%     end
% end
%
%
%% SAMPLE MATRIX 
%
% Specify type of matrix to sample.
ff = {''} ;
%
for nn = {' -N', ' -L'} ;
    for tt = 1 ; % tt = 1:3 ;
        for cc = [32 16] ;
            loadPath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['DCdata', ff{:}, nn{:}]} ;
            savePath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}]} ;
            DCmatrix    = loadNow(loadPath, ['DCmatrix -C', sprintf('%03d', cc)]) ;
            DCmatrix.set('name', 'Symatrix') ;
            DCmatrix.saveNow(savePath) ;
            %
            for mm = [1 2 4 8 16] * 1949 ;
                %
                loadPath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}]} ;
                savePath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}, ' -M']} ;
                SYmatrix = loadNow(loadPath, 'SYmatrix') ;
                SYmatrix.set('name', [SYmatrix.name, ' -M', num2str(mm)]) ;
                SYmatrix.noRows(mm) ;
                SYmatrix.saveNow(savePath) ;
                %
            end
            %
            for dd = [1 2] * 1949
                %
                loadPath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}]} ;
                savePath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}, ' -D']} ;
                SYmatrix = loadNow(loadPath, 'SYmatrix') ;
                SYmatrix.set('name', [SYmatrix.name, ' -D', num2str(dd)]) ;
                SYmatrix.noColumns(dd) ;
                SYmatrix.saveNow(savePath) ;
                %
                for mm = [1 2 4 8 16] * 1949 ;
                    %
                    loadPath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}, ' -D']} ;
                    savePath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}, ' -D -M']} ;
                    SYmatrix = loadNow(loadPath, ['SYmatrix -D', num2str(dd)]) ;
                    SYmatrix.set('name', [SYmatrix.name, ' -M', num2str(mm)]) ;
                    SYmatrix.noRows(mm) ;
                    SYmatrix.saveNow(savePath) ;
                    %
                end
            end
        end
    end
end
%
%
%% FULL MATRIX INVERSION
%
nn = {' -N'} ; ff = {''} ;
for tt = 1 ; % 1:3 ;
    for cc = 32 ;
        %
        loadPath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}]} ;
        savePath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), ff{:}, nn{:}, ' -I']} ;
        SYmatrix    = loadNow(loadPath, 'SYmatrix') ;
        SYmatrix.invert('inversion', 'over') ;
        SYmatrix.saveNow(savePath) ;
        %
%         loadPath    = {workingPath, 'Dictionary', 'DictionaryMatrix', ['T', sprintf('%02d', tt)], ['SYdata -C', sprintf('%03d', cc), nn{:}]} ;
%         SYmatrix    = loadNow(loadPath, 'SYmatrix') ;
%         SYmatrix.invert('inversion', 'pinv') ;
%         SYmatrix.saveNow(savePath) ;
        %
    end
end
%
disp('>> Finished Running 2736D SYSTEM SETUP 160526 <<') ;
%