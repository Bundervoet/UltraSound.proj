%% SPARSITY TEST v2 160602
%
% Created by Shaun Bundervoet on 2 JUN 16.
% Latest version 22 JUN 16.
%
% Standard Working folder ;
folder = '/Volumes/WORK_HDD/MATLAB/Files/[S] Simulations/UltraSound/160420' ;
%
%
%% SIMULATION SETUP 1
%
ff = {''} ; nn = {' -L'} ;
tt = 1 ; pp = 1 ; cc = 32 ; jj = 38 ; dd = 2 * 1949 ;
RFdata          = loadNow(folder, '2730E', 'Simulation', 'RadioFrequencyData', ['T',sprintf('%02d',tt),'P',sprintf('%02d',pp)],['RFdata', nn{:},' -C',sprintf('%03d',cc)],['RFline',num2str(jj)]) ;
signalFULL      = reshape(RFdata.signalDATA, [], 1) ;
RFdata.imageNow('h',100) ;
%
EDline          = loadNow('2736D', 'Dictionary', 'RadioFrequencyData', 'EDdata', 'EDlineT01') ;  
colDENS         = EDline.signalDATA ;
%
%% MATRIX SETUP TRIAL 1
%
nn = {' -N'} ;
DCdata_T1       = loadNow('2736D', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['DCdata', nn{:}], ['DCmatrix -C',sprintf('%03d',cc)]) ;
%
colMASK_T1      = DCdata_T1.colMASK.maskDATA  ;
colSIZE_T1      = DCdata_T1.colMASK.size ;
%
dicMTRX_T1      = DCdata_T1.getMatrixDATA ;
%
%
%% PROJECTION TRIAL 1
%
% SOLVE MODEL
dicMTRX_W1      = dicMTRX_T1 * diag(sqrt((1./colDENS(:)))) ;
dicMTRX_W1      = dicMTRX_W1 ./ (2*max(abs(dicMTRX_W1(:)))) ;
dicMTRX_I1      = (dicMTRX_W1' * dicMTRX_W1) \ dicMTRX_W1' ;
cffDATA_T1      = dicMTRX_I1 * signalFULL ;
%
% SETUP REDATA
REdata_T1 = setupSimulation160606(RFdata, DCdata_T1, dicMTRX_W1, cffDATA_T1) ;
%
% IMAGE SIGNAL/COEFF
imageSimulation160601(101, RFdata, REdata_T1)
%
%
disp('>> Finished Trial 1 <<') ;
%
%
%% MATRIX SETUP TRIAL 2
%
dd = 2 * 1949 ; nn = {' -N'} ;
DCdata_T2       = loadNow('2736D', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), nn{:},' -D'], ['SYmatrix -D',num2str(dd)]) ;
%
colMASK_T2      = DCdata_T2.colMASK.maskDATA  ;
colSIZE_T2      = DCdata_T2.colMASK.size ;
%
dicMTRX_T2      = DCdata_T2.matrixDATA ;
%
%
%% PROJECTION TRIAL 2
%
% SOLVE MODEL
dicMTRX_W2      = dicMTRX_T2 ./ (2*max(abs(dicMTRX_T2(:)))) ;
dicMTRX_I2      = (dicMTRX_W2' * dicMTRX_W2) \ dicMTRX_W2' ;
cffDATA_T2      = dicMTRX_I2 * signalFULL ;
%
% SETUP REDATA
REdata_T2 = setupSimulation160606(RFdata, DCdata_T2, dicMTRX_W2, cffDATA_T2) ;
%
% IMAGE SIGNAL/COEFF
imageSimulation160601(102, RFdata, REdata_T2)
%
%
disp('>> Finished Trial 2 <<') ;
% 
% 
%% MATRIX SETUP TRIAL 3
%
nn = {' -N'} ;
DCdata_T3       = loadNow('2736D', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['DCdata', nn{:}], ['DCmatrix -C',sprintf('%03d',cc)]) ;
%
colMASK_T3      = DCdata_T3.colMASK.maskDATA  ;
colSIZE_T3      = DCdata_T3.colMASK.size ;
%
dicMTRX_T3      = DCdata_T3.matrixDATA ;
%
%
%% PROJECTION TRIAL 3
%
% SOLVE MODEL
dicMTRX_I3      = (dicMTRX_T3' * dicMTRX_T3) \ dicMTRX_T3' ;
cffDATA_T3      = dicMTRX_I3 * signalFULL ;
%
% cffDATA_T3 = lsqnonneg(dicMTRX_T3, signalFULL) ; 
%
% SETUP REDATA
REdata_T3 = setupSimulation160606(RFdata, DCdata_T3, dicMTRX_T3, cffDATA_T3) ;
%
% IMAGE SIGNAL/COEFF
imageSimulation160601(103, RFdata, REdata_T3)
%
%
disp('>> Finished Trial 3 <<') ;
%
%
%% IMAGE
%
% imageSimulation160601(101, RFdata, REdata_T1)
% imageSimulation160601(102, RFdata, REdata_T2)
% imageSimulation160601(103, RFdata, REdata_T3)
%% PROJECTION TRIAL 3
% %
% % SOLVE MODEL
% h = 102 ; 
% REdataSIM2              = ReconstructionData([],RFdata) ; REdataSIM2.rowMASK = DCdataORI.rowMASK ; REdataSIM2.colMASK = DCdataORI.colMASK ; 
% DCmatrixWEI2            = DCmatrixORI * diag((1./colDENS(:))) ;
% DCmatrixWEI2            = DCmatrixWEI2 ./ (2*max(abs(DCmatrixWEI2(:)))) ;
% DCmatrixINV2            = inv(DCmatrixWEI2' * DCmatrixWEI2) * DCmatrixWEI2' ;
% coeffDATA               = DCmatrixINV2 * signalFULL ;
% REdataSIM2.coeffDATA([],coeffDATA) ;
% %
% % RECOVER SIGNAL
% REdataSIM2.signalDATA	= reshape(DCmatrixWEI2 * coeffDATA, [], cc) ;
% %
% % IMAGE SIGNAL/COEFF
% imageSimulation160601(h, RFdata, REdataSIM2)
% %
% %
% disp('>> Finished Trial 2 <<') ;
% %
% %% PROJECTION TRIAL 3
% %
% % SOLVE MODEL
% h = 103 ; 
% REdataSIM3              = ReconstructionData([],RFdata) ; REdataSIM3.rowMASK = DCdataORI.rowMASK ; REdataSIM3.colMASK = DCdataORI.colMASK ; 
% DCmatrixWEI3            = DCmatrixORI * diag((1./colDENS(:))) ;
% DCmatrixWEI3            = DCmatrixWEI3 ./ (2*max(abs(DCmatrixWEI3(:)))) ;
% DCmatrixINV3            = inv(DCmatrixWEI3' * DCmatrixWEI3) * DCmatrixWEI3' ;
% coeffDATA               = DCmatrixINV3 * signalFULL ;
% REdataSIM3.coeffDATA([],coeffDATA) ;
% %
% % RECOVER SIGNAL
% REdataSIM3.signalDATA	= reshape(DCmatrixWEI3 * coeffDATA, [], cc) ;
% %
% % IMAGE SIGNAL/COEFF
% imageSimulation160601(h, RFdata, REdataSIM3)
% %
% %
% disp('>> Finished Trial 3 <<') ;
% %
% %% PROJECTION TRIAL 4
% %
% % SOLVE MODEL
% h = 104 ; 
% REdataSIM4              = ReconstructionData([], RFdata, 'rowMASK', DCdataORI.rowMASK, 'colMASK', DCdataORI.colMASK) ; 
% DCmatrixWEI4            = DCmatrixORI * diag(sqrt((1./colDENS(:)))) ;
% DCmatrixWEI4            = DCmatrixWEI4 ./ (2*max(abs(DCmatrixWEI4(:)))) ;
% DCmatrixINV4            = inv(DCmatrixWEI4' * DCmatrixWEI4) * DCmatrixWEI4' ;
% coeffDATA               = DCmatrixINV4 * signalFULL ;
% REdataSIM4.coeffDATA([],coeffDATA) ;
% %
% % RECOVER SIGNAL
% REdataSIM4.signalDATA	= reshape(DCmatrixWEI4 * coeffDATA, [], cc) ;
% %
% % IMAGE SIGNAL/COEFF
% imageSimulation160601(h, RFdata, REdataSIM4)
% %
% %
% disp('>> Finished Trial 4 <<') ;
%
%% FFT TRANSFORM
%
% h = 102 ;
% REdata_FFT              = ReconstructionData([],RFdata) ; REdata_FFT.rowMASK = DCdataORI.rowMASK ; REdata_FFT.colMASK = DCdataORI.colMASK ; 
% coeffFFT                = fft(coeffIMG) ;
% REdata_FFT.imageNow('h',h+100,'data','coefficients') ;
% %
% figure(h+200) ; 
% hold on
% plot(sort(coeffFFT), 'LineWidth', 2, 'Color', RFdata.VUBcolors('groen')) ;
% title('FFT') ; xlabel('Coefficient') ; ylabel('Value') ;
% hold off
%
%
%% WAVELET TRANSFORM
%
% h = 103 ;
% REdata_FFT                   = ReconstructionData([],RFdata) ;
% REdata_FFT.coefficientDATA   = fft(REdata_S1.coefficientDATA) ;
% REdata_FFT.imageNow('h',h+100,'data','coefficients') ;
% %
% figure(h+200) ; 
% hold on
% plot(sort(REdata_FFT.coefficientDATA), 'LineWidth', 2, 'Color', RFdata.VUBcolors('groen')) ;
% title('FFT') ; xlabel('Coefficient') ; ylabel('Value') ;
% hold off
% 
% figure(3)
% coeffIMG = zeros(REdata_S1.colMASK.size) ;
% coeffIMG(REdata_S1.colMASK.maskDATA) = coeffDATA2 ;
% imagesc(coeffIMG(:,16:48).^2) ;
%
disp('>> Finished Running SPARSITY TEST v2 160527 <<') ;