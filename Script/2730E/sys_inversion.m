%% 2730E SYSTEM INV 160527
%
% This script is used to generate a dictionary of Point Spread Functions (PSF) in order to linearly
% represent RAW channel RFdata obtained from the Cyst Setup.
%
%
%% INVERT DICTIONARY MATRIX
%
ff = {''} ; nn = {' -N'} ;
for tt = 1:2
    for cc = 32 ;            
        %
        for dd = [1 2 4] * 1949
            %
            loadPath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), ff{:}, nn{:},' -D']} ;
            savePath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), ff{:}, nn{:},' -D -I']} ;
            SYmatrix    = loadNow(loadPath, ['SYmatrix -D', num2str(dd)]) ;
            SYmatrix.invert('inversion', 'over') ;
            SYmatrix.saveNow(savePath) ;
            %
            SYmatrix    = loadNow(loadPath, ['SYmatrix -D', num2str(dd)]) ;
            SYmatrix.invert('inversion', 'pinv') ;
            SYmatrix.saveNow(savePath) ;
            %
            for mm = [1 2 4 8 16] * 1949 * tt ;
                %
                loadPath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), ff{:}, nn{:},' -D -M']} ;
                savePath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), ff{:}, nn{:},' -D -M -I']} ;
                SYmatrix    = loadNow(loadPath, ['SYmatrix -D', num2str(dd),' -M', num2str(mm)]) ;
                if mm >= dd
                    SYmatrix.invert('inversion', 'over') ;
                else
                    SYmatrix.invert('inversion', 'under') ;
                end
                SYmatrix.saveNow(savePath) ;
                %
                SYmatrix    = loadNow(loadPath, ['SYmatrix -D', num2str(dd),' -M', num2str(mm)]) ;
                SYmatrix.invert('inversion', 'pinv') ;
                SYmatrix.saveNow(savePath) ;
            end
        end
    end
end
%
%
%% SAMPLE ENVELOPPE DICTIONARY MATRIX
%
% for tt = 1:3 ; % tt = 1:3 ;
%     for cc = [32 16 8 4 2] ; % cc = [128 64 32 16 8 4 2] ;
%         SYmatrix = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['DCmatrix -C',sprintf('%03d',cc)]) ;
%         SYmatrix.invert('inversion', 'over') ;
%         SYmatrix.saveNow({'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)]})        
%     end
% end
% %
% for tt = 1:3 ; % tt = 1:3 ;
%     for cc = [32 16 8 4 2] ; % cc = [128 64 32 16 8 4 2] ;
%         SYmatrix = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['DCmatrix -C',sprintf('%03d',cc)]) ;
%         SYmatrix.invert('inversion', 'pinv') ;
%         SYmatrix.saveNow({'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)]})        
%     end
% end
% %
% for tt = 1:3
%     for dd = [2 4] * 3897
%         for cc = 32 ;
%             SYmatrix = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], 'DCdata', ['DCmatrix -C',sprintf('%03d',cc),' -E']) ;
%             SYmatrix.set('name', ['SYmatrix -C',sprintf('%03d',cc),' -E -D', num2str(dd)]) ;
%             SYmatrix.noColumns(dd) ;
%             SYmatrix.saveNow({'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], 'SYdata'}) ;
%         end
%     end
% end

%
%
%% CONCATINATE WAVELET MATRIX 
%
% nn = {' -N'} ; xd = 7 ; yd = 4 ; w = 1 ;
% for tt = 1 ;
%     for cc = [32 16] ;
%         loadPath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['DCdata', nn{:}]} ;
%         DCmatrix    = loadNow(loadPath, ['DCmatrix -C',sprintf('%03d',cc)]) ;
%         DCmatrix.set('name', [DCmatrix.name,' -CDF(',num2str(xd),',',num2str(yd),',',num2str(w),')']) ;
%         DCmatrix.cdf97('xd', xd, 'yd', yd) ;
%         DCmatrix.saveNow(loadPath) ;
%     end
% end
%
%

%
%
%% CONSTRUCT REGULARIZED INVERSION 1
%
ff = {' -F'} ; nn = {' -N'} ;
for tt = 1 ; % 1:2 ;
    for cc = 32 ; % cc = [32 16 8 4 2] ;
        for dd = [1 2 4] * 1949 
            %
%             loadPath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['DCdata', nn{:}]} ;
%             savePath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), nn{:}]} ;
%             SYmatrix = loadNow(loadPath, ['DCmatrix -C',sprintf('%03d',cc)]) ;
%             SYmatrix.set('name', 'SYmatrix') ;
%             SYmatrix.saveNow(savePath) ;
%             %
%             loadPath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), nn{:}]} ;
%             savePath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), nn{:},' -D']} ;
%             SYmatrix = loadNow(loadPath, 'SYmatrix') ;
%             SYmatrix.set('name', [SYmatrix.name,' -D', num2str(dd)]) ;
%             SYmatrix.noColumns(dd) ;
%             SYmatrix.saveNow(savePath) ;
            %
            for mm = [1 2 4 8 16] * 1949 * tt ;
                loadPath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), ff{:}, nn{:},' -D']} ;
                savePath    = {'2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc), ff{:}, nn{:},' -D -M -I']} ;
                SYmatrix = loadNow(loadPath, ['SYmatrix -D', num2str(dd)]) ;
                SYmatrix.set('name', [SYmatrix.name,' -M', num2str(mm)]) ;
                SYmatrix.invert('inversion', 'M1', 'noMeasurements', mm) ;
                SYmatrix.saveNow(savePath) ;
                %
            end
        end
    end
end