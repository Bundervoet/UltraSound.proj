%% SPARSITY TEST 160527
%
%
%% SIMULATION SETUP
%
tt = 1 ; pp = 1 ; cc = 32 ; jj = 19 ; 
RFdata          = loadNow('2730E', 'Simulation', 'RadioFrequencyData', ['T',sprintf('%02d',tt),'P',sprintf('%02d',pp)],['RFdata -N -C',sprintf('%03d',cc)],['RFline',num2str(jj)]) ;
RFdata.imageNow('h',100) ;
%
%
%% MATRIX SETUP
%
DCdataORI       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N'], 'SYmatrix') ;
DCdataINV       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -I'], 'SYmatrix -O') ;
%
signalFULL      = reshape(RFdata.signalDATA, [], 1) ;
%
DCmatrixORI     = DCdataORI.matrixDATA ;
DCmatrixINV     = DCdataINV.matrixDATA ;
%
%
%% PROJECTION SETUP
%
% SOLVE MODEL
h = 101 ; 
REdata_S1        	= ReconstructionData([],RFdata) ; REdata_S1.rowMASK = DCdataORI.rowMASK ; REdata_S1.colMASK = DCdataORI.colMASK ; 
coeffDATA           = DCmatrixINV * signalFULL ;
% REdata_S1.coeffDATA(inv(DCmatrixORI' * DCmatrixORI) * DCmatrixORI' * signalFULL) ;
%
% FILTER COEFF
coeffFILT                   = zeros(size(coeffDATA)) ;
spread                      = abs(coeffDATA - mean(coeffDATA))/std(coeffDATA) ;
coeffFILT(spread < 1/20)    = coeffDATA(spread < 1/20) ;
REdata_S1.coeffDATA([],coeffFILT) ;
%
% RECOVER SIGNAL
REdata_S1.signalDATA        = reshape(DCmatrixORI * coeffFILT, [], cc) ;
% REdata_S1.coeffDATA([], reshape(coeffFILT, [], cc)) ;
%
% IMAGE SIGNAL/COEFF
REdata_S1.imageNow('h',h) ;
REdata_S1.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S1 = REdata_S1.PSNR(RFdata) ;
%
% PLOT SOLUTION
a  = 10 ;
z1 = [RFdata.minimum RFdata.maximum] ; o = [0 0] ; % o = zeros(DCdataORI.height,1) ;
z2 = [REdata_S1.minimum REdata_S1.maximum] ;
x1 = reshape(REdata_S1.signalDATA, [], 1) ;
y1 = signalFULL ; 
%
figure(200) ; %subplot(2,4,h-100) ;
hold on
plot(z2, z2, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1, y1, a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['REF - PSNR: ',num2str(REdata_S1.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
figure(h+200) ; 
plot(sort(abs(REdata_S1.coeffDATA), 'descend'), 'LineWidth', 2, 'Color', RFdata.VUBcolors('groen')) ;
title('REF') ; xlabel('Coefficient') ; ylabel('Value') ;
%
%
%% FFT TRANSFORM
%
h = 102 ;
REdata_FFT                   = ReconstructionData([],RFdata) ;
REdata_FFT.coefficientDATA   = fft(REdata_S1.coefficientDATA) ;
REdata_FFT.imageNow('h',h+100,'data','coefficients') ;
%
figure(h+200) ; 
hold on
plot(sort(REdata_FFT.coefficientDATA), 'LineWidth', 2, 'Color', RFdata.VUBcolors('groen')) ;
title('FFT') ; xlabel('Coefficient') ; ylabel('Value') ;
hold off
%
%
%% WAVELET TRANSFORM
%
% h = 103 ;
% REdata_FFT                   = ReconstructionData([],RFdata) ;
% REdata_FFT.coefficientDATA   = fft(REdata_S1.coefficientDATA) ;
% REdata_FFT.imageNow('h',h+100,'data','coefficients') ;
% %
% figure(h+200) ; 
% hold on
% plot(sort(REdata_FFT.coefficientDATA), 'LineWidth', 2, 'Color', RFdata.VUBcolors('groen')) ;
% title('FFT') ; xlabel('Coefficient') ; ylabel('Value') ;
% hold off

