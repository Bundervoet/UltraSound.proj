%% SIMULATION SETUP
%
tt = 1 ; pp = 1 ; cc = 32 ; jj = 19 ; 
RFdata          = loadNow('2730E', 'Simulation', 'RadioFrequencyData', ['T',sprintf('%02d',tt),'P',sprintf('%02d',pp)],['RFdata -N -C',sprintf('%03d',cc)],['RFline',num2str(jj)]) ;
RFdata.imageNow('h',100) ;
%
%
%% MATRIX SETUP
%
dd = 1949 * 2 ; mm = 1949 * 2 ;
DCdataORI       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -D'], ['SYmatrix -D',num2str(dd)]) ;
DCdataINV       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -D -I'], ['SYmatrix -D',num2str(dd),' -R']) ;
SYdataORI       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -D -M'], ['SYmatrix -D',num2str(dd),' -M',num2str(mm)]) ;
SYdataINV       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -D -M -I'], ['SYmatrix -D',num2str(dd),' -M',num2str(mm),' -R']) ;
%
SAmask          = SYdataORI.rowMASK.maskDATA(:) ;
signalFULL      = reshape(RFdata.signalDATA, [], 1) ;
signalDATA      = RFdata.signalDATA(SAmask) ;    % signalDATA_S1 = sqrt(numel(NsignalDATA_S1)) * signalDATA_S1 ;
signalNEGA      = RFdata.signalDATA(~SAmask) ;   % NsignalDATA_S1 = sqrt(NsignalDATA_S1) * NsignalDATA_S1 ;
%
DCmatrixORI     = DCdataORI.matrixDATA ;
DCmatrixINV     = DCdataINV.matrixDATA ;
SYmatrixORI     = SYdataORI.matrixDATA ;
SYmatrixINV     = SYdataINV.matrixDATA ;
SYmatrixNEG     = DCmatrixORI(~SAmask, :) ;
%
SAdata          = RFdata.copy ;          
SAdata.signalDATA           = zeros(RFdata.size) ;
SAdata.signalDATA(SAmask)   = RFdata.signalDATA(SAmask) ;
SAdata.imageNow('h',99) ;
%
%
%% SIMULATION 1
%
% SOLVE MODEL
h = 101 ; 
REdata_S1       = ReconstructionData([],RFdata) ; REdata_S1.rowMASK = SYdataORI.rowMASK ; REdata_S1.colMASK = SYdataORI.colMASK ; 
REdata_S1.coefficientDATA   = inv(DCmatrixORI' * DCmatrixORI) * DCmatrixORI' * signalFULL ;
REdata_S1.signalDATA        = reshape(DCmatrixORI * REdata_S1.coefficientDATA, [], 32) ;
REdata_S1.imageNow('h',h) ;
REdata_S1.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S1 = REdata_S1.PSNR(RFdata) ;
%
% PLOT SOLUTION
a  = 10 ;
z1  = [RFdata.minimum RFdata.maximum] ; o = [0 0] ; % o = zeros(DCdataORI.height,1) ;
z2  = [REdata_S1.minimum REdata_S1.maximum] ;
x1 = REdata_S1.signalDATA(~SAmask) ;
y1 = signalNEGA ;
x2 = REdata_S1.signalDATA(SAmask) ;
y2 = signalDATA ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z2, z2, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['REF - PSNR: ',num2str(REdata_S1.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
%
%% SIMULATION 2
%
h = 102 ;
REdata_S2       = ReconstructionData([],RFdata) ; REdata_S2.rowMASK = SYdataORI.rowMASK ; REdata_S2.colMASK = SYdataORI.colMASK ; 
REdata_S2.coefficientDATA   = SYmatrixINV * signalDATA ;
y_S2            = DCmatrixORI * REdata_S2.coefficientDATA ;
% y_S2(abs(y_S2) > 0.2488) = 0 ;
REdata_S2.signalDATA        = reshape(y_S2, [], 32) ;
REdata_S2.imageNow('h',h) ;
REdata_S2.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S2 = REdata_S2.PSNR(RFdata) ;
%
% PLOT SOLUTION
z2 = [REdata_S2.minimum REdata_S2.maximum] ;
x1 = REdata_S2.signalDATA(~SAmask) ;
x2 = REdata_S2.signalDATA(SAmask) ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z1, z1, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['ORI - PSNR: ',num2str(REdata_S2.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
%
%% SIMULATION 3
%
% SOLVE MODEL
h = 103 ;
REdata_S3       = ReconstructionData([],RFdata) ; REdata_S3.rowMASK = SYdataORI.rowMASK ; REdata_S3.colMASK = SYdataORI.colMASK ;
solveMATRIX_S3  = inv(SYmatrixORI' * SYmatrixORI) * SYmatrixORI';
REdata_S3.coefficientDATA   = solveMATRIX_S3 * signalDATA ;
y_S3            = DCmatrixORI * REdata_S3.coefficientDATA ;
% y_S3(abs(y_S3) > 0.2488) = 0 ;
REdata_S3.signalDATA        = reshape(y_S3, [], 32) ;
REdata_S3.imageNow('h',h) ;
REdata_S3.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S3 = REdata_S3.PSNR(RFdata) ;
%
% PLOT SOLUTION
z2 = [REdata_S3.minimum REdata_S3.maximum] ;
x1 = REdata_S3.signalDATA(~SAmask) ;
x2 = REdata_S3.signalDATA(SAmask) ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z2, z2, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['LS - PSNR: ',num2str(REdata_S3.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
%
%% SIMULATION 4
%
% ii = 0 ; PSNR_S3 = zeros(11,1) ;
jj = 200000 ;
% for jj = [200000 210000 220000 230000 240000 250000 260000 270000 280000 290000 62368^2]
% ii = ii + 1 ;
%
h = 104 ;
REdata_S4       = ReconstructionData([],RFdata) ; REdata_S4.rowMASK = SYdataORI.rowMASK ; REdata_S4.colMASK = SYdataORI.colMASK ;
solveMATRIX_S4  = inv(SYmatrixORI' * SYmatrixORI + sqrt(1/jj) * eye(SYdataORI.width)) * SYmatrixORI' ;
REdata_S4.coefficientDATA   = solveMATRIX_S4 * signalDATA ;
REdata_S4.signalDATA        = reshape(DCmatrixORI * REdata_S4.coefficientDATA, [], 32) ;
REdata_S4.imageNow('h',h) ;
REdata_S4.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S4 = REdata_S4.PSNR(RFdata) ;
%
% PLOT SOLUTION
z2 = [REdata_S4.minimum REdata_S4.maximum] ;
x1 = REdata_S4.signalDATA(~SAmask) ;
x2 = REdata_S4.signalDATA(SAmask) ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z2, z2, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['REG COEFF L2 - PSNR: ',num2str(REdata_S4.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
% end
%
%
%% SIMULATION 5
%
% ii = 0 ; PSNR_S4 = zeros(11,1) ;
% for jj = [1 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000]
%     ii = ii + 1 ;
h = 105 ;
REdata_S5       = ReconstructionData([],RFdata) ; REdata_S5.rowMASK = SYdataORI.rowMASK ; REdata_S5.colMASK = SYdataORI.colMASK ;
solveMATRIX_S5  = inv(SYmatrixORI' * SYmatrixORI + sqrt(1/SYdataORI.height) * SYmatrixNEG'* SYmatrixNEG) * SYmatrixORI' ;
REdata_S5.coefficientDATA   = solveMATRIX_S5 * signalDATA ;
REdata_S5.signalDATA        = reshape(DCmatrixORI * REdata_S5.coefficientDATA, [], 32) ;
REdata_S5.imageNow('h',h) ;
REdata_S5.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S5 = REdata_S5.PSNR(RFdata) ;
%
% PLOT SOLUTION
z2 = [REdata_S5.minimum REdata_S5.maximum] ;
x1 = REdata_S5.signalDATA(~SAmask) ;
x2 = REdata_S5.signalDATA(SAmask) ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z2, z2, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['REG NEG L2 - PSNR: ',num2str(REdata_S5.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
% end
%
%
%% SIMULATION 6
%
h = 106 ;
REdata_S6       = ReconstructionData([],RFdata) ; REdata_S6.rowMASK = SYdataORI.rowMASK ; REdata_S6.colMASK = SYdataORI.colMASK ;
solveMATRIX_S6  = inv(DCmatrixORI' * DCmatrixORI) * SYmatrixORI' ;
REdata_S6.coefficientDATA 	= solveMATRIX_S6 * signalDATA ;
REdata_S6.signalDATA        = reshape(DCmatrixORI * REdata_S6.coefficientDATA, [], 32) ;
REdata_S6.imageNow('h',h) ;
REdata_S6.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S6 = REdata_S6.PSNR(RFdata) ;
%
% PLOT SOLUTION
z2 = [REdata_S6.minimum REdata_S6.maximum] ;
x1 = REdata_S6.signalDATA(~SAmask) ;
x2 = REdata_S6.signalDATA(SAmask) ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z1, z1, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['REG OUT L2 - PSNR: ',num2str(REdata_S6.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
%
%% SIMULATION 7
%
h = 107 ;
REdata_S7       = ReconstructionData([],RFdata) ; REdata_S7.rowMASK = SYdataORI.rowMASK ; REdata_S7.colMASK = SYdataORI.colMASK ;
solveMATRIX_S7  = inv(SYmatrixORI' * SYmatrixORI + 3/8*sqrt(1/3898)*SYmatrixNEG'*SYmatrixNEG + 2/8*sqrt(1/(3898)) * eye(SYdataORI.width)) * SYmatrixORI' ;
REdata_S7.coefficientDATA   = solveMATRIX_S7 * signalDATA ;
REdata_S7.signalDATA        = reshape(DCmatrixORI * REdata_S7.coefficientDATA, [], 32) ;
REdata_S7.imageNow('h',h) ;
REdata_S7.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S7 = REdata_S7.PSNR(RFdata) ;
%
% PLOT SOLUTION
z2 = [REdata_S7.minimum REdata_S7.maximum] ;
x1 = REdata_S7.signalDATA(~SAmask) ;
x2 = REdata_S7.signalDATA(SAmask) ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z2, z2, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['REG MIX L2 - PSNR: ',num2str(REdata_S7.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
%
%
%% SIMULATION 8
%
dd = 1949 * 4 ;
% DCdataCDF       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -CDF(1949,32,1)'], 'SYmatrix') ;
% SYdataCDF       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -CDF(1949,32,1) -M'], ['SYmatrix -M',num2str(mm)]) ;
DCdataCDF       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -CDF(4,1,1)'], 'SYmatrix') ;
SYdataCDF       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -CDF(4,1,1) -M'], ['SYmatrix -M',num2str(mm)]) ;
% DCdataCDF       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -CDF(4,1,1) -D'], ['SYmatrix -D',num2str(dd)]) ;
% SYdataCDF       = loadNow('2730E', 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['SYdata -C',sprintf('%03d',cc),' -N -CDF(4,1,1) -D -M'], ['SYmatrix -D',num2str(dd),' -M',num2str(mm)]) ;
%
SAmask2         = SYdataCDF.rowMASK.maskDATA(:) ;
SYmatrixCDF     = SYdataCDF.matrixDATA ;
DCmatrixCDF     = DCdataCDF.matrixDATA ;
NEmatrixCDF     = DCmatrixCDF(~SAmask2, :) ;
%
% SOparameters = {'solver', 'irls', 'inversion', 'pinv', 'base', 'cdf97', 'noIterations', 50, ...
%     'sparsity', 0, 'epsilon', 5e-3, 'steps', {} };
% USsolver    = Solver('USsolver01', SOparameters{:}) ;
% REdata_S8   = USsolver.reconstruct([], DCdataCDF, SYdataCDF, [], RFdata) ;
%
h = 108 ;
REdata_S8       = ReconstructionData([],RFdata) ; REdata_S8.rowMASK = SYdataCDF.rowMASK ; REdata_S8.colMASK = SYdataCDF.colMASK ;
% x_S8            = l1_ls(SYmatrixCDF, signalDATA,1/3898, 1e-10, 1) ;
REdata_S8.coefficientDATA   = lasso(SYmatrixCDF, signalDATA, 'Lambda', 1e-6) ;
% solveMATRIX_S8  = inv(SYmatrixCDF' * SYmatrixCDF + sqrt(1/jj) * eye(SYdataCDF.width)) * SYmatrixCDF' ;
% REdata_S8.coefficientDATA   = solveMATRIX_S8 * signalDATA ;
REdata_S8.signalDATA        = reshape(DCmatrixCDF * REdata_S8.coefficientDATA, [], 32) ;
REdata_S8.imageNow('h',h) ;
REdata_S8.imageNow('h',h+100,'data','coefficients') ;
%
% SAVE PSNR
PSNR_S8 = REdata_S8.PSNR(RFdata) ;
%
% PLOT SOLUTION
z2 = [REdata_S8.minimum REdata_S8.maximum] ;
s8DATA = REdata_S8.matrixDATA ;
x1 = s8DATA(~SAmask) ;
x2 = s8DATA(SAmask) ;
%
figure(200) ; subplot(2,4,h-100) ;
hold on
plot(z2, z2, '-', 'LineWidth', 2, 'Color', RFdata.VUBcolors('grijs')) ;
plot(z1, o , '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
plot(o , z2, '-', 'LineWidth', 1, 'Color', RFdata.VUBcolors('lichtgrijs')) ;
scatter(x1,y1,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('donkerrood'),...
    'LineWidth',1) ;
scatter(x2,y2,a,'MarkerEdgeColor', RFdata.VUBcolors('bruin'),...
    'MarkerFaceColor',RFdata.VUBcolors('groen'),...
    'LineWidth',1) ;
title(['REG COEFF L1 - PSNR: ',num2str(REdata_S8.PSNR(RFdata))]) ; xlabel('fitted value') ; ylabel('real value') ;
hold off
