%% SPARSITY TEST v2 160527
%
%
%% PATH SETUP
%
simulationPath  = '/Volumes/WORK_HDD/MATLAB/Files/[S] Simulations/UltraSound/160420/2730E' ;
workingPath     = '/Volumes/WORK_HDD/MATLAB/Files/[S] Simulations/UltraSound/160420/2736D' ;
%
%
%% SIMULATION SETUP
%
ff = {''} ; nn = {' -L'} ;
tt = 1 ; pp = 1 ; cc = 32 ; jj = 23 ; dd = 2 * 1949 ;
RFdata          = loadNow(simulationPath, 'Simulation', 'RadioFrequencyData', ['T',sprintf('%02d',tt),'P',sprintf('%02d',pp)],['RFdata', nn{:},' -C',sprintf('%03d',cc)],['RFline',num2str(jj)]) ;
signalFULL      = reshape(RFdata.signalDATA, [], 1) ;
RFdata.imageNow('h',100) ;
%
%
%% MATRIX SETUP
%
nn = {' -L'} ;
DCdataORI       = loadNow(workingPath, 'Dictionary', 'DictionaryMatrix', ['T',sprintf('%02d',tt)], ['DCdata', nn{:}], ['DCmatrix -C',sprintf('%03d',cc)]) ;
EDline          = loadNow(workingPath, 'Dictionary', 'RadioFrequencyData', 'EDdata', 'EDlineT01') ;  
%
colMASK         = DCdataORI.colMASK.maskDATA  ;
colSIZE         = DCdataORI.colMASK.size ;
colDENS         = EDline.signalDATA ;
DCmatrixORI     = DCdataORI.matrixDATA ;
maxABS          = DCdataORI.absMaximum ;
% DCmatrixINV     = DCdataINV.matrixDATA ;
%
%
%% PROJECTION TRIAL 1
%
% SOLVE MODEL
h = 110 ; 
REdataSIM1               = ReconstructionData([], RFdata, 'rowMASK', DCdataORI.rowMASK, 'colMASK', DCdataORI.colMASK)  ; 
% DCmatrixWEI1            = DCmatrixORI ; 
% DCmatrixWEI1            = DCmatrixWEI1 ./ (2*max(abs(DCmatrixWEI1(:)))) ;
DCdataINV1              = inv(DCdataORI' * DCdataORI) * DCdataORI' ;
coeffDATA               = DCdataINV1 .* signalFULL ;
REdataSIM1.setCoeffDATA(coeffDATA) ;
%
% RECOVER SIGNAL
REdataSIM1.setMatrixDATA(reshape(DCmatrixORI * coeffDATA, [], cc)) ;
%
% IMAGE SIGNAL/COEFF
imageSimulation160601(h, RFdata, REdataSIM1)
%
disp('>> Finished Trial 1.0 <<') ;
%
% %% PROJECTION TRIAL 1.1
% %
% % SOLVE MODEL
% h = 111 ; 
% REdataSIM11               = ReconstructionData([],RFdata) ; REdataSIM11.rowMASK = DCdataORI.rowMASK ; REdataSIM11.colMASK = DCdataORI.colMASK ; 
% % DCmatrixWEI1            = DCmatrixORI ; 
% % DCmatrixWEI1            = DCmatrixWEI1 ./ (2*max(abs(DCmatrixWEI1(:)))) ;
% % DCmatrixINV1            = inv(DCmatrixWEI1' * DCmatrixWEI1) * DCmatrixWEI1' ;
% % coeffDATA               = DCmatrixINV1 * signalFULL ;
% coeffDATA11               = double(coeffDATA .* reshape(colDENS,[],1)) ;
% % coeffDATA11               = coeffDATA11./(2*max(abs(coeffDATA11))) ;
% REdataSIM11.coeffDATA([],coeffDATA11) ;
% %
% % RECOVER SIGNAL
% REdataSIM11.signalDATA	= reshape(DCmatrixORI * coeffDATA, [], cc) ;
% %
% % IMAGE SIGNAL/COEFF
% imageSimulation160601(h, RFdata, REdataSIM11)
% %
% %
% disp('>> Finished Trial 1.1 <<') ;
% %
% %
% %% PROJECTION TRIAL 2
% %
% % SOLVE MODEL
% h = 120 ; 
% REdataSIM2              = ReconstructionData([],RFdata) ; REdataSIM2.rowMASK = DCdataORI.rowMASK ; REdataSIM2.colMASK = DCdataORI.colMASK ; 
% DCmatrixWEI2            = DCmatrixORI * diag((1./colDENS(:))) ;
% DCmatrixWEI2            = DCmatrixWEI2 ./ (2*max(abs(DCmatrixWEI2(:)))) ;
% DCmatrixINV2            = inv(DCmatrixWEI2' * DCmatrixWEI2) * DCmatrixWEI2' ;
% coeffDATA               = DCmatrixINV2 * signalFULL ;
% REdataSIM2.coeffDATA([],coeffDATA) ;
% %
% % RECOVER SIGNAL
% REdataSIM2.signalDATA	= reshape(DCmatrixWEI2 * coeffDATA, [], cc) ;
% %
% % IMAGE SIGNAL/COEFF
% imageSimulation160601(h, RFdata, REdataSIM2)
% %
% %
% disp('>> Finished Trial 2 <<') ;
% %
% %% PROJECTION TRIAL 3
% %
% % SOLVE MODEL
% h = 130 ; 
% REdataSIM3              = ReconstructionData([],RFdata) ; REdataSIM3.rowMASK = DCdataORI.rowMASK ; REdataSIM3.colMASK = DCdataORI.colMASK ; 
% DCmatrixWEI3            = DCmatrixORI * diag((1./colDENS(:)).^2) ;
% DCmatrixWEI3            = DCmatrixWEI3 ./ (2*max(abs(DCmatrixWEI3(:)))) ;
% DCmatrixINV3            = inv(DCmatrixWEI3' * DCmatrixWEI3) * DCmatrixWEI3' ;
% coeffDATA               = DCmatrixINV3 * signalFULL ;
% REdataSIM3.coeffDATA([],coeffDATA) ;
% %
% % RECOVER SIGNAL
% REdataSIM3.signalDATA	= reshape(DCmatrixWEI3 * coeffDATA, [], cc) ;
% %
% % IMAGE SIGNAL/COEFF
% imageSimulation160601(h, RFdata, REdataSIM3)
% %
% %
% disp('>> Finished Trial 3 <<') ;
% %
% %% PROJECTION TRIAL 4
% %
% % SOLVE MODEL
% h = 140 ; 
% REdataSIM4              = ReconstructionData([], RFdata, 'rowMASK', DCdataORI.rowMASK, 'colMASK', DCdataORI.colMASK) ; 
% DCmatrixWEI4            = DCmatrixORI * diag(sqrt((1./colDENS(:)))) ;
% DCmatrixWEI4            = DCmatrixWEI4 ./ (2*max(abs(DCmatrixWEI4(:)))) ;
% DCmatrixINV4            = inv(DCmatrixWEI4' * DCmatrixWEI4) * DCmatrixWEI4' ;
% coeffDATA               = DCmatrixINV4 * signalFULL ;
% REdataSIM4.coeffDATA([],coeffDATA) ;
% %
% % RECOVER SIGNAL
% REdataSIM4.signalDATA	= reshape(DCmatrixWEI4 * coeffDATA, [], cc) ;
% %
% % IMAGE SIGNAL/COEFF
% imageSimulation160601(h, RFdata, REdataSIM4)
% %
% %
% disp('>> Finished Trial 4 <<') ;
%
%% FFT TRANSFORM
%
% h = 102 ;
% REdata_FFT              = ReconstructionData([],RFdata) ; REdata_FFT.rowMASK = DCdataORI.rowMASK ; REdata_FFT.colMASK = DCdataORI.colMASK ; 
% coeffFFT                = fft(coeffIMG) ;
% REdata_FFT.imageNow('h',h+100,'data','coefficients') ;
% %
% figure(h+200) ; 
% hold on
% plot(sort(coeffFFT), 'LineWidth', 2, 'Color', RFdata.VUBcolors('groen')) ;
% title('FFT') ; xlabel('Coefficient') ; ylabel('Value') ;
% hold off
%
%
%% WAVELET TRANSFORM
%
% h = 103 ;
% REdata_FFT                   = ReconstructionData([],RFdata) ;
% REdata_FFT.coefficientDATA   = fft(REdata_S1.coefficientDATA) ;
% REdata_FFT.imageNow('h',h+100,'data','coefficients') ;
% %
% figure(h+200) ; 
% hold on
% plot(sort(REdata_FFT.coefficientDATA), 'LineWidth', 2, 'Color', RFdata.VUBcolors('groen')) ;
% title('FFT') ; xlabel('Coefficient') ; ylabel('Value') ;
% hold off
% 
% figure(3)
% coeffIMG = zeros(REdata_S1.colMASK.size) ;
% coeffIMG(REdata_S1.colMASK.maskDATA) = coeffDATA2 ;
% imagesc(coeffIMG(:,16:48).^2) ;
%
disp('>> Finished Running SPARSITY TEST v2 160527 <<') ;