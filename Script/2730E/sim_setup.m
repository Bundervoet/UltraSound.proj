%% 2730E SIMULATION SETUP
%
% This script follows the full UltraSound (Simulation) Pipeline in order to create Bmode images from
% a Cyst Phantom containing 100 000 and 250 000 scattering regions.
%
% Created by Shaun Bundervoet on 6 JUN 16.
% Latest version 23 JUN 16.
%
%% PATH SETUP
%
workingPath = '/Volumes/WORK_HDD/MATLAB/Files/[S] Simulations/UltraSound/160420/2730E' ;
%
%% GENERATE PHANTOM
%
% First a Cyst Phantom is created by uniformly filling a box of 50mm x 10mm x 60 mm with 100 000
% scatterers (3D-points) having Gaussian drawn amplitudes. Afterwards 5 high-intensity cilinders, 
% resp. low-intensity, are defined inwhich the amplitude of the scatterers is increase, resp.
% lowered. These cilinders model the Cyst structures within the tissue.
PHpath = {workingPath, 'Simulation', 'Phantom'} ;
noScatterers = 100000 ;
PHdata01 = CystPhantom(['PH0x', dec2hex(noScatterers)], noScatterers) ;
PHdata01.saveNow(PHpath) 
%
PHpath = {workingPath, 'Simulation', 'Phantom'} ;
noScatterers = 250000 ;
PHdata02 = CystPhantom(['PH0x', dec2hex(noScatterers)], noScatterers) ;
PHdata02.saveNow(PHpath) 
%
%
%% SETUP TRANSDUCERS
%
% In this section a UltraSound transducer is defined from which RAW channel RFdata will be gathered.
% This transducer constist of 192 piezo-electric elements, 32 of which are used to create the high
% frequency acoustic pulse. In this setup all elements are used to receive echoes.
USpath = {workingPath, 'Simulation', 'Transducer'} ;
USparameters = {'noElements', 192, 'elementWidth', 4.4000e-04, 'elementHeight', 5/1000, ...
    'elementKerf', 0.05/1000, 'f0', 3.5e6, 'fs', 25e6, 'noTransmit', 32, 'noReceive', 192, ...
    'focusPoint', [0 0 70]/1000, 'focusZones', (30:20:200)'/1000, 'zFocus', 60/1000, ...
    'elevFocus', 0/1000, 'receiveApodisation', false, 'receiveFocus', true, ...
    'transmitApodisation', true, 'transmitFocus', true, 'apodisationWindow', 'hanning' } ;
UStransducer01 = UltraSoundTransducer('UStransducer01', USparameters{:}) ;
UStransducer01.saveNow(USpath) ;
%
USpath = {workingPath, 'Simulation', 'Transducer'} ;
USparameters = {'noElements', 192, 'elementWidth', 4.4000e-04, 'elementHeight', 5/1000, ...
    'elementKerf', 0.05/1000, 'f0', 5e6, 'fs', 50e6, 'noTransmit', 32, 'noReceive', 192, ...
    'focusPoint', [0 0 70]/1000, 'focusZones', (30:20:200)'/1000, 'zFocus', 60/1000, ...
    'elevFocus', 0/1000, 'receiveApodisation', false, 'receiveFocus', true, ...
    'transmitApodisation', true, 'transmitFocus', true, 'apodisationWindow', 'hanning' } ;
UStransducer02 = UltraSoundTransducer('UStransducer02', USparameters{:}) ;
UStransducer02.saveNow(USpath) ;
%
USpath = {workingPath, 'Simulation', 'Transducer'} ;
USparameters = {'noElements', 192, 'elementWidth', 4.4000e-04, 'elementHeight', 5/1000, ...
    'elementKerf', 0.05/1000, 'f0', 7e6, 'fs', 100e6, 'noTransmit', 32, 'noReceive', 192, ...
    'focusPoint', [0 0 70]/1000, 'focusZones', (30:20:200)'/1000, 'zFocus', 60/1000, ...
    'elevFocus', 0/1000, 'receiveApodisation', false, 'receiveFocus', true, ...
    'transmitApodisation', true, 'transmitFocus', true, 'apodisationWindow', 'hanning' } ;
UStransducer03 = UltraSoundTransducer('UStransducer03', USparameters{:}) ;
UStransducer03.saveNow(USpath) ;
%
%
%% SIMULATE RADIO FREQUENCY DATA T01P01
%
% Using the predefined UStransducer 50 RFlines are simulated from the Cyst Phantom. The received
% signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% used during reception. These steps will be applied later during the beamforming stage.
% LAOD DEPENDENT OBJECTS.
UStransducer01  = loadNow(workingPath, 'Simulation', 'Transducer', 'UStransducer01.mat') ;
PHdata01        = loadNow(workingPath, 'Simulation', 'Phantom', 'PH0x186A0.mat') ;
% 
RFpath = {workingPath, 'Simulation', 'RadioFrequencyData', 'T01P01', 'RFdata'} ;
RFparameters = {'rawSignal', true, 'cropSignal', true, 'sparseSignal', false, 'saveSparse', false} ;
currentMaximum = 0 ;
UStransducer01.setupTransducer ;
for jj = 1:50 
    RFdata = UStransducer01.simulateRFline(['RFline', num2str(jj)], PHdata01, jj, RFparameters{:}) ;
    currentMaximum = max(currentMaximum, RFdata.absMaximum) ;
    RFdata.saveNow(RFpath) ;
end
USpath = {workingPath, 'Simulation', 'Transducer'} ;
UStransducer01.set('globalAbsMaximumP01', currentMaximum) ;
UStransducer01.saveNow(USpath) ;
%
%
%% SIMULATE RADIO FREQUENCY DATA T01P02
%
% Using the predefined UStransducer 50 RFlines are simulated from the Cyst Phantom. The received
% signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% used during reception. These steps will be applied later during the beamforming stage.
% LAOD DEPENDENT OBJECTS.
UStransducer01  = loadNow(workingPath, 'Simulation', 'Transducer', 'UStransducer01.mat') ;
PHdata02        = loadNow(workingPath, 'Simulation', 'Phantom', 'PH0x3D090.mat') ;
%
RFpath = {workingPath, 'Simulation', 'RadioFrequencyData', 'T01P02', 'RFdata'} ;
RFparameters = {'rawSignal', true, 'cropSignal', true, 'sparseSignal', false, 'saveSparse', false} ;
currentMaximum = 0 ;
UStransducer01.setupTransducer ;
for jj = 1:50
    RFdata = UStransducer01.simulateRFline(['RFline', num2str(jj)], PHdata02, jj, RFparameters{:}) ;
    currentMaximum = max(currentMaximum, RFdata.absMaximum) ;
    RFdata.saveNow(RFpath) ;
end
USpath = {workingPath, 'Simulation', 'Transducer'} ;
UStransducer01.set('globalAbsMaximumP02', currentMaximum) ;
UStransducer01.saveNow(USpath) ;
%
%
%% SIMULATE RADIO FREQUENCY DATA T02P01
%
% Using the predefined UStransducer 50 RFlines are simulated from the Cyst Phantom. The received
% signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% used during reception. These steps will be applied later during the beamforming stage.
% LAOD DEPENDENT OBJECTS.
UStransducer02  = loadNow(workingPath, 'Simulation', 'Transducer', 'UStransducer02.mat') ;
PHdata01        = loadNow(workingPath, 'Simulation', 'Phantom', 'PH0x186A0.mat') ;
%
RFpath = {workingPath, 'Simulation', 'RadioFrequencyData', 'T02P01', 'RFdata'} ;
RFparameters = {'rawSignal', true, 'cropSignal', true, 'sparseSignal', false, 'saveSparse', false} ;
currentMaximum = 0 ;
UStransducer02.setupTransducer ;
for jj = 1:50
    RFdata = UStransducer02.simulateRFline(['RFline', num2str(jj)], PHdata01, jj, RFparameters{:}) ;
    currentMaximum = max(currentMaximum, RFdata.absMaximum) ;
    RFdata.saveNow(RFpath) ;
end
USpath = {workingPath, 'Simulation', 'Transducer'} ;
UStransducer02.set('globalAbsMaximumP01', currentMaximum) ;
UStransducer02.saveNow(USpath) ;
%
%
%% SIMULATE RADIO FREQUENCY DATA T02P02
%
% Using the predefined UStransducer 50 RFlines are simulated from the Cyst Phantom. The received
% signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% used during reception. These steps will be applied later during the beamforming stage.
% LAOD DEPENDENT OBJECTS.
UStransducer02  = loadNow(workingPath, 'Simulation', 'Transducer', 'UStransducer02.mat') ;
PHdata02        = loadNow(workingPath, 'Simulation', 'Phantom', 'PH0x3D090.mat') ;
%
RFpath = {workingPath, 'Simulation', 'RadioFrequencyData', 'T02P02', 'RFdata'} ;
RFparameters = {'rawSignal', true, 'cropSignal', true, 'sparseSignal', false, 'saveSparse', false} ;
currentMaximum = 0 ;
UStransducer02.setupTransducer ;
for jj = 1:50
    RFdata = UStransducer02.simulateRFline(['RFline', num2str(jj)], PHdata02, jj, RFparameters{:}) ;
    currentMaximum = max(currentMaximum, RFdata.absMaximum) ;
    RFdata.saveNow(RFpath) ;
end
USpath = {workingPath, 'Simulation', 'Transducer'} ;
UStransducer02.set('globalAbsMaximumP02', currentMaximum) ;
UStransducer02.saveNow(USpath) ;
%
%
%% SIMULATE RADIO FREQUENCY DATA T03P01
%
% Using the predefined UStransducer 50 RFlines are simulated from the Cyst Phantom. The received
% signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% used during reception. These steps will be applied later during the beamforming stage.
% LAOD DEPENDENT OBJECTS.
UStransducer03  = loadNow(workingPath, 'Simulation', 'Transducer', 'UStransducer03.mat') ;
PHdata01        = loadNow(workingPath, 'Simulation', 'Phantom', 'PH0x186A0.mat') ;
%
RFpath = {workingPath, 'Simulation', 'RadioFrequencyData', 'T03P01', 'RFdata'} ;
RFparameters = {'rawSignal', true, 'cropSignal', true, 'sparseSignal', false, 'saveSparse', false} ;
currentMaximum = 0 ;
UStransducer03.setupTransducer ;
for jj = 46:-1:1
    RFdata = UStransducer03.simulateRFline(['RFline', num2str(jj)], PHdata01, jj, RFparameters{:}) ;
    currentMaximum = max(currentMaximum, RFdata.absMaximum) ;
    RFdata.saveNow(RFpath) ;
end
USpath = {workingPath, 'Simulation', 'Transducer'} ;
UStransducer03.set('globalAbsMaximumP01', currentMaximum) ;
UStransducer03.saveNow(USpath) ;
%
%
%% SIMULATE RADIO FREQUENCY DATA T03P02
%
% Using the predefined UStransducer 50 RFlines are simulated from the Cyst Phantom. The received
% signal is cropped to match the depth dimensions of the Phantom. No apodisation or windowing is
% used during reception. These steps will be applied later during the beamforming stage.
% LAOD DEPENDENT OBJECTS.
UStransducer03  = loadNow(workingPath, 'Simulation', 'Transducer', 'UStransducer03.mat') ;
PHdata03        = loadNow(workingPath, 'Simulation', 'Phantom', 'PH0x3D090.mat') ;
%
RFpath = {workingPath, 'Simulation', 'RadioFrequencyData', 'T03P02', 'RFdata'} ;
RFparameters = {'rawSignal', true, 'cropSignal', true, 'sparseSignal', false, 'saveSparse', false} ;
currentMaximum = 0 ;
UStransducer03.setupTransducer ;
for jj = 1:50
    RFdata = UStransducer03.simulateRFline(['RFline', num2str(jj)], PHdata02, jj, RFparameters{:}) ;
    currentMaximum = max(currentMaximum, RFdata.absMaximum) ;
    RFdata.saveNow(RFpath) ;
end
USpath = {workingPath, 'Simulation', 'Transducer'} ;
UStransducer03.set('globalAbsMaximumP02', currentMaximum) ;
UStransducer03.saveNow(USpath) ;

%
%
%% NORMALISE RADIO FREQUENCY DATA
%
% When all RAW channel RFdata is receives (i.e. simulated using FIELD II), the RFlines are
% normalised and the most imporated Channels are kepts using the trim-function.
for tt = 1:3 ;
    for pp = 1:2 ;
        UStransducer  = loadNow(workingPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ;
        for jj = 1:50
            RFdata      = loadNow(workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata', ['RFline', num2str(jj)]) ;
            RFdata.normalise(UStransducer.get(['globalAbsMaximumP', sprintf('%02d', pp)])) ;
            RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata -N')
            %
            % Trim RFDATA.
            for cc = [128 64 32 16 8 4 2]
                RFdata = loadNow(workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata -N', ['RFline', num2str(jj)]) ;
                %
                RFdata.trim(cc) ;
                RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -N -C', sprintf('%03d', cc)]) ;
                %
                if tt > 1
                    RFdata.trim(2*(tt-1), false) ;
                    RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -N -C', sprintf('%03d', cc)], 'F25Mhz') ;
                end
            end
        end
    end
end
%
%
%% PERFORM A DECIBEL QUANTIZATION OF THE  RADIO FREQUENCY DATA
%
% When all RAW channel RFdata is receives (i.e. simulated using FIELD II), the RFlines are
% normalised and the most imporated Channels are kepts using the trim-function.
for tt = 1:3 ;
    for pp = 1:2 ;
        UStransducer  = loadNow(workingPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ;
        for jj = 1:50
            RFdata      = loadNow(workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata', ['RFline', num2str(jj)]) ;
            RFdata.decibelQuantization(UStransducer.get(['globalAbsMaximumP', sprintf('%02d', pp)])) ;
            RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata -Q')
            %
            % Trim RFDATA.
            for cc = [128 64 32 16 8 4 2]
                RFdata = loadNow(workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata -Q', ['RFline', num2str(jj)]) ;
                %
                RFdata.trim(cc) ;
                RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -Q -C', sprintf('%03d', cc)]) ;
                %
                if tt > 1
                    RFdata.trim(2*(tt-1), false) ;
                    RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -Q -C', sprintf('%03d', cc)], 'F25Mhz') ;
                end
            end
        end
    end
end
%
%
%% LOG DECIBEL TRANSFORM RADIO FREQUENCY DATA
%
% When all RAW channel RFdata is receives (i.e. simulated using FIELD II), the RFlines are
% normalised and the most imporated Channels are kepts using the trim-function.
for tt = 1:3 ;
    for pp = 1:2 ;
        UStransducer  = loadNow(workingPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ;
        for jj = 1:50
            RFdata      = loadNow(workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata', ['RFline', num2str(jj)]) ;
            RFdata.decibel(UStransducer.get(['globalAbsMaximumP', sprintf('%02d', pp)])) ;
            RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata -L') ;
            %
            % Trim RFDATA.
            for cc = [128 64 32 16 8 4 2]
                RFdata = loadNow(workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata -L', ['RFline', num2str(jj)]) ;
                %
                RFdata.trim(cc) ;
                RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -L -C', sprintf('%03d', cc)]) ;
                %
                if tt > 1
                    RFdata.trim(2*(tt-1), false) ;
                    RFdata.saveNow('check', jj ==1, workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -L -C', sprintf('%03d', cc)], 'F25Mhz') ;
                end
            end
        end
    end
end
%
%
%% CREATE IMAGES
%
% In this last section a BrightnessMode Image is created from the RAW channel RFdata. This is
% achieved by beamforming the data along the channels before applying enveloppe-detection and
% log-compression. The resulting images are stored using the writeNow-function.
BMparameters = {'beamformer', 'special', 'contrast', 'dark', 'interpolation', 1, 'decimation', 1, 'apodisation', true} ;
for tt = 1:3 ;
    UStransducer  = loadNow(workingPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ;
    for pp = 1:2 ;
        for cc = [128 64 32 16 8 4 2]
            BMpath  = {workingPath, 'Simulation', 'BrightnessModeImage', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)]} ;
            BIpath  = {workingPath, 'Simulation', 'Images', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)]} ;
            BMimage = BrightnessModeImage(['BMimage -N -C', sprintf('%03d', cc)], BMparameters{:}) ;
            RFpath  = {workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -N -C', sprintf('%03d', cc)]} ;
            BMimage.import(UStransducer, RFpath) ;
            BMimage.process
            BMimage.saveNow(BMpath) ;
            BMimage.writeNow(['BMimage -N -C', sprintf('%03d', cc)], BIpath) ;
            %
            % Extra decimated image.
            if tt > 1
                BMpath  = {workingPath, 'Simulation', 'BrightnessModeImage', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'F25Mhz'} ;
                BIpath  = {workingPath, 'Simulation', 'Images', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'F25Mhz'} ;
                BMimage = BrightnessModeImage(['BMimage -N -C', sprintf('%03d', cc)], BMparameters{:}) ;
                RFpath  = {workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -N -C', sprintf('%03d', cc)], 'F25Mhz'} ;
                BMimage.import(UStransducer, RFpath) ;
                BMimage.process
                BMimage.saveNow(BMpath) ;
                BMimage.writeNow(['BMimage -N -C', sprintf('%03d', cc)], BIpath) ;
                %
                BMpath  = {workingPath, 'Simulation', 'BrightnessModeImage', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'D25Mhz'} ;
                BIpath  = {workingPath, 'Simulation', 'Images', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'D25Mhz'} ;
                BMimage = BrightnessModeImage(['BMimage -N -C', sprintf('%03d', cc)], BMparameters{:} ) ;
                BMimage.set('decimation', 2 * (tt-1)) ;
                RFpath  = {workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], ['RFdata -N -C', sprintf('%03d', cc)]} ;
                BMimage.import(UStransducer, RFpath) ;
                BMimage.process
                BMimage.saveNow(BMpath) ;
                BMimage.writeNow(['BMimage -N -C', sprintf('%03d', cc)], BIpath) ;
            end
        end
    end
end
%
%
%% CREATE IMAGES
%
% In this last section a BrightnessMode Image is created from the RAW channel RFdata. This is
% achieved by beamforming the data along the channels before applying enveloppe-detection and
% log-compression. The resulting images are stored using the writeNow-function.
BMpath = {workingPath, 'Cyst', 'Simulation', 'BrightnessModeImage'} ;
IMpath = {workingPath, 'Cyst', 'Simulation', 'Images'} ;
BMparameters = {'beamformer', 'special', 'contrast', 'dark', 'interpolation', 1, 'decimation', 10, 'apodisation', true} ;
for ii = [128 64 32 16 8 4 2]
    RFpath = {'Cyst', 'Simulation', 'RadioFrequencyData', ['RFdata -N -C', num2str(ii)]} ;
    BMimage = BrightnessModeImage(['BMline -N -C', num2str(ii)], BMparameters{:}) ;
    UStransducer = BMimage.loadNow({'Cyst', 'Simulation', 'Transducer'}, 'UStransducer01') ;
    BMimage.import(UStransducer, RFpath) ;
    BMimage.process
    BMimage.saveNow(BMpath) ;
    BMimage.writeNow(['BMline -N -C', num2str(ii)], IMpath) ;
end
%
%
%
%% AUXILLIARY CODE
%
% Re-evaluate Globale Maximum. 
%
% for tt = 3 ;
%     for pp = 2 ;
%         currentMax      = 0 ;
%         UStransducer  = loadNow(workingPath, 'Simulation', 'Transducer', ['UStransducer', sprintf('%02d', tt)]) ;
%         for jj = 1: 50
%             RFdata      = loadNow(workingPath, 'Simulation', 'RadioFrequencyData', ['T', sprintf('%02d', tt), 'P', sprintf('%02d', pp)], 'RFdata', ['RFline', num2str(jj)]) ;
%             currentMax  = max(currentMax, RFdata.absMaximum) ;
%         end
%         UStransducer.set(['globalAbsMaximumP', sprintf('%02d', pp)], currentMax) ;
%         UStransducer.saveNow({workingPath, 'Simulation', 'Transducer'}) ;
%     end
% end
%
% A = [] ;
% B = [] ;
% for jj = 1:50
%     load(['Cyst/Simulation/RadioFrequencyData/RFdata -N/RFline', num2str(jj), '.mat']) ;
%     A(end+1) = RFdata.relativeFocus.x ;
%     %
%     load(['Cyst/Simulation/RadioFrequencyData/RFdata -N -C128/RFline', num2str(jj), '.mat']) ;
%     B(end+1) = RFdata.relativeFocus.x ;
% end
% %
% %
% %% ADAPT SAVED DATA
% for ii = [128 64 32 16 8 4 2]
%     for jj = 1:50
%         load(['Cyst/Simulation/RadioFrequencyData/RFdata -N -C', num2str(ii), '/RFline', num2str(jj), '.mat']) ;
%         RFdata.setVar('relativeFocus', UStransducer.activeFocusPoint(jj, (UStransducer.noElementsRightOfReceiveWindow - UStransducer.noElementsLeftOfReceiveWindow)*UStransducer.pixelPitch)) ;
%         RFpathC = {'Cyst', 'Simulation', 'RadioFrequencyData', ['RFdata -N -C', num2str(ii)]} ;
%         RFdata.saveNow(RFpathC) ;
%     end
% end
% %
% for jj = 1:50
%     load(['Cyst/Simulation/RadioFrequencyData/RFdata/RFline', num2str(jj), '.mat']) ;
%         RFdata.setVar('lineWidth', UStransducer.lineWidth) ;
%         RFdata.saveNow(RFpath) ;
% end