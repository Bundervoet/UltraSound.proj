function import(self, RFdata, scheme, noSamples, SApatern)

% Automatically select variables when insufficient parameters are provided to the function.
if nargin < 4
    noSamples   = RFdata.noPixels ;
elseif isempty(noSamples)
    noSamples   = RFdata.noPixels ;
end
if nargin < 3
    scheme      = 'density' ;
end
if nargin < 5
    SApaterns   = 1:RFdata.noPixels ;
end

% Copy RFdata's pixelSize to the mask.
self.set('pixelSize', RFdata.get('pixelSize')) ;

% Extract relevant variables.
width   = RFdata.get('width') ;
height  = RFdata.get('height') ;

% Define a density map using the signalDATA in RFdata and a specific scheme procedure.
switch lower(scheme)
    case {'apodisation', 'apo', 'apodization'}  % Apodisation map.
        do  = true ;
        self.densityDATA    = kron(hanning(width)',ones(height,1)) ;
        
    case {'random', 'uniform'}                  % Uniform density.
        do  = true ;
        self.densityDATA    = ones([height width]) ;
        
    case 'density'                              % Density map given by the user.
        do  = true ;
        self.densityDATA    = RFdata.getMatrixDATA() ;

    case {'apodiseddensity', 'apodensity'}      % Apodisation applied to Density.
        do  = true ;
        self.densityDATA    = kron(hanning(width)',ones(height,1)) .*  RFdata.getMatrixDATA ;
               
    case 'patern'                               % CURRENTLY NOT IMPLEMENTED CORRECTLY!
        do = false ;   
        rng(self.seed) ;
        SAunique        = unique(uint32(reshape(SApatern, 1, [])), 'stable') ;
        SAindex         = randi([1 length(SAunique)]) ;
        SAshifted       = SAunique([SAindex:end, 1:(SAindex-1)]) ;
        self.indexDATA  = [SAshifted; zeros(self.noPixels - length(SAshifted),1)] ;       
        
%         line = 1  ;
%         if ((line+1) * noSamples) < length(self.density) ;
%             self.indices = int32(self.density((sample_line * noSamples) : 1 : ((line+1)*noSamples))) ;
%         else
%             start = datasample(1:(length(density) - ceil(.2 * prod(self.densitySize))),1) ;
%             self.indices = int32(self.density(start:1:(start + noSamples))) ;
%         end
%         do  = false ;
        
    case {'fixeddensity', 'rfenergy', 'edline'} % Fixed sorting using densityDATA.
        do  = false ;
        self.densityDATA    = RFdata.getMatrixDATA() ; 
        [~,self.indexDATA]  = sort(self.densityData(:), 'descend')  ;
        
    case {'edlinecdf'}                          % Fixed sorting using CDF densityDATA.
        do = false ;
        self.densityDATA    = RFdata.getMatrixDATA() ; 
        
        xd = 4 ; yd = 1 ; w = 2 ;
        cdfDensity          = regular_97_lp(self.densityDATA, xd, yd) ;
        weightingMap        = coeffs_97_c(size(cdfDensity), xd, yd ,w) ;
        colDensity          = weightingMap .* cdfDensity  ;
        [~,self.indexDATA]  = sort(colDensity(:),'descend')  ;

    case {'edlinebdct'}                         % Fixed sorting according to BDCT density.
        do = false ;
        self.densityDATA    = RFdata.getMatrixDATA() ;
        
        xd = 4 ; yd = 8 ;
        bdctDensity         = dct_weightmap(self.density, self.no_dictionary, xd, yd) ;
        colDensity          = s2_dct(self.density, xd, yd ,bdctDensity) ;
        [~,self.indexDATA]  = sort(colDensity(:),'descend')  ;
        
    case 'full'                                 % Used all Samples.
        do = false ;
        self.densityDATA    = ones(RFdata.size) ;
        self.indexDATA      = reshape(1:RFdata.noPixels, RFdata.size) ;        
        
    otherwise
        errorStruct.message     = ['Structure of scheme "', scheme ,'" not recognised.'];
        errorStruct.identifier  = 'SamplingMask:import:PropertyNotRecognised';
        error(errorStruct) ;
end

% Sample using the prediscribed sampling density.
rng(self.seed) ;
if do
    samplingIndices = find(self.densityDATA(:)) ; inActiveIndices = find(~self.densityDATA(:)) ;
    samplingDensity = self.densityDATA(samplingIndices) ;
    activeIndices   = datasample(samplingIndices, length(samplingIndices), 'Replace', false, 'Weights' , samplingDensity(:)) ;
    self.set('indexDATA', [activeIndices; inActiveIndices]) ;
end

maskDATA            = false(self.size) ;
maskDATA(self.indexDATA(1:noSamples))  = true ;
self.set('maskDATA', maskDATA) ;
end