%% RESAMPLE

% Resamples method redraws the indices from the densityDATA. The density definition remains
% unchanged while the mask is reset to 100%.

% Created by Shaun Bundervoet on 07 MAR 17.
% Latest version 07 MAR 17.

function reSample(self, seed, noSamples, method)

if nargin > 1
    self.seed = seed ; 
end
if nargin < 3
    noSamples = self.noMeasurements ;
elseif isempty(noSamples)
    noSamples = self.noMeasurements ;
end
if nargin < 4
    method = 'regular' ;
end
% Sample using the prediscribed sampling density.
rng(self.seed) ;

switch method
    case {'regular', 'apodisation', 'apo', 'apodization', 'random', 'uniform', 'density'}
        if do
            samplingIndices = find(self.densityDATA(:)) ; inActiveIndices = find(~self.densityDATA(:)) ;
            samplingDensity = self.densityDATA(samplingIndices) ;
            activeIndices   = datasample(samplingIndices, length(samplingIndices), 'Replace', false, 'Weights' , samplingDensity(:)) ;
            self.set('indexDATA', [activeIndices; inActiveIndices]) ;
        end
    case {'shift', 'patern'}
        SApositive      = self.indexDATA(self.indexDATA ~= 0) ; 
        SAunique        = unique(uint32(SApositive), 'stable') ;
        SAindex         = randi([1 length(SAunique)]) ;
        SAshifted       = SAunique([SAindex:end, 1:(SAindex-1)]) ;
        self.indexDATA  = [SAshifted, zeros(1,self.noPixels - length(SAshifted))] ;        
end

maskDATA            = false(self.size) ;
maskDATA(self.indexDATA(1:noSamples))  = true ;
self.set('maskDATA', maskDATA) ;
end