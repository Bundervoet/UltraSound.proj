% SAMPLINGMASK

% The SamplingMask class provides all functionally in order to sample both the columns aswell as the
% rows of a System Matrix. This includes defining density and sampling from this density. Moreover
% since all data-indices are stored it is easy to quickly up/down-sample further at other rates
% without calculation new sample location.
%   - Sampling Rows comes down to downsampling the pixels in RFdata.
%   - Samping Columns comes down to downsampling the list coeffcients which can be used in the
%   represeantion.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 24 OKT 16.

classdef SamplingMask < DataMatrix
    
    
    %% PROPERTIES
    properties % DATA PROPERTIES
        maskDATA@logical                                                                            % Which indices are used.
        indexDATA@uint32                                                                            % Ordered list of all indices determining the order in which they should be sampled.
        densityDATA@double                                                                          % Density matrix from which the indexlist is drawn.
    end
    properties % DATA PROPERTIES
        seed@double = 1 ;                                                                           % Random seed for repreducability.
    end
    
    
    %% METHODS
    methods % SETUP METHOD
        
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a instance and accessing
        % / changing the state of its properties.
        % ===
        function SaM = SamplingMask(name, varargin)
            SaM@DataMatrix(name, varargin{:}) ;
        end
        function output = type(SaM)
            output = 'SAmask' ;
        end
        
        
        %% DATA METHODS.
        function output = getMatrixDATA(SaM)
            output = SaM.densityDATA ;
        end
        function setMatrixDATA(SaM, input)
            SaM.densityDATA    = input ;
        end
        function setOne(SaM, input, value)
            % ---
            % Similar to setOne in other classes this method provides functionality for addapting a
            % variable in RadioFrequencyData. Take special care in adapting non-double properties.
            % ---
            switch lower(input)
                case 'nomeasurements'
                    SaM.noMeasurements(value) ;
                case 'psmeasurements'
                    SaM.psMeasurements(value) ;
                case 'nosamples'
                    SaM.noSamples(value) ;
                case 'pssamples'
                    SaM.psSamples(value) ;
                case {'indexdata', 'maskdata'}
                    inputClass = class(SaM.(input))  ;
                    cmd = ['SaM.(input) = ', inputClass, '(value) ;'] ; eval(cmd) ;
                otherwise
                    setDATA(SaM, input, value) ;
            end
        end
        function output = getOne(SaM, input)
            switch lower(input)
                case 'nomeasurements'
                    output = SaM.noMeasurements() ;
                case 'psmeasurements'
                    output = SaM.psMeasurements() ;
                case 'nosamples'
                    output = SaM.noSamples() ;
                case 'pssamples'
                    output = SaM.psSamples() ;
                case {'activeindices', 'activeindex'}
                    output = SaM.activeIndex() ;
                case {'indexdata', 'maskdata'}
                    output = double(SaM.(input)) ;
                otherwise
                    output = getDATA(SaM, input) ;
            end
        end
        
        
        %% SIZE METHODS
        function output = noMeasurements(SaM, amount)
            if nargin == 2
                SaM.maskDATA       = false(SaM.size) ;
                SaM.maskDATA(SaM.activeIndex(amount)) = true ;
            end
            output = sum(SaM.maskDATA(:)) ;
        end
        function output = pcMeasurements(SaM, percentage)
            if nargin == 2
                noInput = floor((percentage/100) * SaM.noPixels) ;
                SaM.maskDATA       = false(SaM.size) ;
                SaM.maskDATA(SaM.activeIndex(noInput))  = true ;
            end
            output = round(sum(SaM.maskDATA(:))/SaM.noPixels*100) ;
        end
        function output = pmMeasurements(SaM, percentage)
            if nargin == 2
                noInput = floor((percentage/1000) * SaM.noPixels) ;
                SaM.maskDATA       = false(SaM.size) ;
                SaM.maskDATA(SaM.activeIndex(noInput))  = true ;
            end
            output = round(sum(SaM.maskDATA(:))/SaM.noPixels*1000) ;
        end
        function output = noSamples(SaM, input)
            if nargin == 2
                output = SaM.noMeasurements(input) ;
            else
                output = SaM.noMeasurements ;
            end
        end
        function output = pcSamples(SaM, input)
            if nargin == 2
                output = SaM.pcMeasurements(input) ;
            else
                output = SaM.pcMeasurements ;
            end
        end
        function output = pmSamples(SaM, input)
            if nargin == 2
                output = SaM.pmMeasurements(input) ;
            else
                output = SaM.pmMeasurements ;
            end
        end
        
        %         function output = activeIndices(SaM, input)
        %             output = sort(SaM.indexDATA(1:SaM.noMeasurements))' ;
        %             % ALTERNATIVE
        %             %             output = find(SAmask.maskDATA == 1) ;
        %             if nargin == 2
        %                 output = output(input) ;
        %             end
        %         end
        function output = activeMask(SaM, full)
            if nargin < 2
                full = true ;
            end
            if full
                output = false(SaM.size) ;
                output(SaM.densityDATA(:) ~= 0) = true ; % inActiveIndex = find(~SaM.densityDATA(:)) ;
            else
                output = true(sum(SaM.densityDATA(:) ~= 0),1) ;    
            end     
        end        
        function output = noActive(SaM)
            output = sum(SaM.activeMask(:)) ;
        end
        function output = getMaskDATA(SaM, full)
            if nargin < 2
                full = true ;
            end
            if full
                output = SaM.maskDATA ;
            else
                output = SaM.maskDATA(SaM.densityDATA(:) ~= 0) ;
            end
        end
        function output = activeIndex(SaM, limit, input)
            if nargin < 2
                limit = SaM.noMeasurements ;
            end
            if isempty(limit)
                limit = SaM.noMeasurements ;
            end
            [~ , I]       = sort(SaM.indexDATA) ;
            activeMask    = SaM.activeMask ;
            unSortedMask  = activeMask(I) ;
            activeIndex   = SaM.indexDATA(unSortedMask) ;
            output        = sort(activeIndex(1: min(limit, end))) ;
            if nargin == 3
                output = output(input) ;
            end
        end
        function output = fullMASK(SaM)
            output = true(SaM.size) ;
        end
        function output = outputMASK(SaM, input)
            if nargin < 2
                input = SaM.noPixels ;
            end
            output       = false(SaM.size) ;
            output(SaM.indexDATA(1:input))  = true ;
        end
        function trim(SaM, channels)
            noFirstColumns = floor((SaM.width - channels)/2) ;
            SaM.density = SaM.density(:, (1: channels) + noFirstColumns) ;
        end
        
        %% DATA METHODS
        function turnOff(SaM, varargin)
            % This function is used to "permentantly" discard a sample index in the mask. This can
            % occur when a vector accomponied with the index is small or zero.
            for jj = 1:(nargin-1)
                points                      = varargin{jj} ;
                for pp = points
                    % Move index to back of indexDATA list.
                    index                   = find(SaM.indexDATA == pp, 1, 'last') ;
                    %                     indexTEMP               = zeros(SaM.noPixels) ;
                    %                     indexTEMP(1:(end-1))    = SaM.indexDATA(setdiff(1:SaM.noPixels, index)) ;
                    indexTEMP               = SaM.indexDATA(setdiff(1:SaM.noPixels, index)) ;
                    indexTEMP(end+1)        = SaM.indexDATA(index) ; %#ok<AGROW>
                    SaM.indexDATA          = indexTEMP ;
                    
                    % Set matching pixel-value in mask to false.
                    SaM.maskDATA(pp)       = false ;
                    
                    % Set matching pixel-value in density to zero.
                    SaM.densityDATA(pp)    = 0 ;
                end
            end
        end
        
        
        %% IMAGING METHODS
        function [outputDATA, outputINFO, par] = imageDATA(SaM, par)
            % Imaging variables. From these variables the correct distance measurements
            % are displayed in the figure window. Moreover they help to preserver the
            % correct aspect ratios.
            switch lower(par.data)
                case {'density'}
                    par.setPar('scale', 4) ;
                    inputMatrix             = SaM.get('densityDATA') ;
                    outputINFO              = 'densityDATA' ;
                case {'mask'}
                    inputMatrix             = SaM.get('maskDATA') ;
                    outputINFO              = 'maskDATA' ;
                otherwise
                    errorStruct.message     = ['Input Property "', lower(par.data) ,'" not recognised.'];
                    errorStruct.identifier  = 'ReconstructionData:imageData:InputNotRecognised';
                    error(errorStruct) ;
            end
            outputDATA    = imresize(inputMatrix, [SaM.height (SaM.width * SaM.pixelWidth / SaM.pixelHeight)] * par.scale, 'nearest') ;
            
            par.setPar('minimum', min(inputMatrix(:))) ;
            par.setPar('maximum', max(inputMatrix(:))) ;
        end
    end
end