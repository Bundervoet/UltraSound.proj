%% REMASK
%
% The reMask method allows the user to overlay the current Sampling Mask with a New Sampling Mask
% Pattern in order to take subMasks. The Indices and Density definition remains unchanged.
%
function relativeMASK = reMask(self, SAmask)
%
%% ADDAPT MASK
% Current Mask
maskTEMP = self.maskDATA ;
%
% Submask the current maskDATA ;
self.maskDATA = self.maskDATA .* SAmask.maskDATA ;
%
% Find the relative Indices of the submask ;
relativeMASK = self.maskDATA(maskTEMP) ; 
%
end