%% GENERAL SYSTEM MATRIX

% This generalized method determins the result of either $y = S*x$ where $S = Rs * (Psi * Cs) *
% Phi^{-1}$ is the system matrix in a linear system of equations or $x = S' * y$ where
% $S' = Phi * (Cs^T * Psi^T) * Rs^T$ is the (complex) transpose of that system matrix. Both
% calculations are performed in either Matrix mode when a precalculated version of $S$ is stored in
% systemDATA, otherwise a fast implementation of the internal sparsifying matrix is used for large
% scale inverions.

% Based on code provided by Stijn Bettens on 13 okt. 2016
% Current version 20 Okt 2016.

function outputVector = A(SysM, inputVector, forwardMode, dictionaryMode)

% Infer missing input arguments.
if nargin < 3
    forwardMode = true ;                                                                            % If False use (complex) transpose.
end
if nargin < 4
    dictionaryMode = 0 ;                                                                        % If True use full orginigal matrix.
end

% forwardMode * Phi + (1 - forwardMode) * Phi'
sensingMatrix = getSystemDATA(SysM, forwardMode, (dictionaryMode == 1)) ;

if (forwardMode == 1)
    % Calculate S = Rs * (Psi * Cs) * Phi^{-1} which is de system matrix used such that
    % y = S * x ;
    
    if ~isempty(sensingMatrix)
        % Apply X = Cs^T * x
        xCoeff = zeros(SysM.colMASK.size) ;
        xCoeff(SysM.colMASK.getMaskDATA) = inputVector ;
    else
        % Apply X = x
        xCoeff = reshape(inputVector, SysM.colMASK.size) ;
    end
    
    % Apply F = Phi^{-1} * X ;
    switch SysM.systemFUNC
        case {'identity', 'id'}
            data = xCoeff ;
        case {'fft', 'fft2', 'dct', 'dct2'}
            cmd = ['data = sqrt(SysM.colMASK.noPixels) * i',SysM.systemFUNC,'(xCoeff) ;'] ; eval(cmd) ;
        case {'cdf97'}
            xd = 4 ; yd = 2 ; w = 1;
            data = regular_97_c(xCoeff, -xd, -yd, w) ;
        otherwise
            errorStruct.message = ['Unrecognised function "', SysM.systemFUNC ,'" during system creation.'];
            errorStruct.identifier = 'SystemMatrix:S:UnrecognisedFunction';
            error(errorStruct) ;
    end
    
    if ~isempty(sensingMatrix) && (dictionaryMode == 0)
        % Apply y = Rs * Psi * Cs * F ;
        outputVector = sensingMatrix * data(SysM.colMASK.getMaskDATA) ;
    elseif isempty(sensingMatrix) && (dictionaryMode == 0)
        % Apply y = Rs * F ;
        outputVector = data(SysM.rowMASK.getMaskDATA) ;
    elseif ~isempty(sensingMatrix) && (dictionaryMode == 1)
        % Apply D = Psi * Cs * F ;
        outputVector = reshape(sensingMatrix * data(SysM.colMASK.getMaskDATA),...
            SysM.rowMASK.size) ;
    elseif ~isempty(sensingMatrix) && (dictionaryMode == 2)
        % Apply Y = Cs * F ;
        outputVector = reshape(data(SysM.colMASK.getMaskDATA), SysM.colMASK.size) ;
    elseif isempty(sensingMatrix) && (dictionaryMode ~= 0)
        % Apply D = F ;
        outputVector = reshape(data, SysM.rowMASK.size) ;
    else
        % Apply y = Rs * F ;
        outputVector = data(SysM.rowMASK.getMaskDATA) ;
    end
    
else
    % Calculate S' = Phi * (Cs^T * Psi^T) * Rs^T which is used to calculate
    % argmin_x \| x \|_2 such that y = S * x ;
    
    if ~isempty(sensingMatrix) && (dictionaryMode == 0)
        % Apply F = (Cs^T * Psi^T) * Rs^T * y
        data = zeros(SysM.colMASK.size) ;
        data(SysM.colMASK.getMaskDATA) = sensingMatrix * inputVector ;
    elseif isempty(sensingMatrix) && (dictionaryMode == 0)
        % Apply F = Rs^T * y ;
        data = zeros(SysM.rowMASK.size) ;
        data(SysM.rowMASK.getMaskDATA) = inputVector ;
    elseif ~isempty(sensingMatrix) && (dictionaryMode == 1)
        % Apply F = (Cs^T * Psi^T) * y
        data = zeros(SysM.colMASK.size) ;
        data(SysM.colMASK.getMaskDATA) = sensingMatrix * inputVector ;
    elseif ~isempty(sensingMatrix) && (dictionaryMode == 2)
        % Apply F = Cs^T * D ;
        data = zeros(SysM.colMASK.size) ;
        data(SysM.colMASK.getMaskDATA) = inputVector ;
    elseif isempty(sensingMatrix) && (dictionaryMode ~= 0)
        % Apply F = D ;
        data = reshape(inputVector, SysM.rowMASK.size) ;
    else
        % Apply F = Rs^T * D ;
        data = zeros(SysM.rowMASK.size) ;
        data(SysM.rowMASK.getMaskDATA) = inputVector ;
    end
    
    % Apply X = Phi * F ;
    switch SysM.systemFUNC
        case {'identity', 'id'}
            xCoeff = data ;
        case {'fft', 'fft2', 'dct', 'dct2'}
            cmd = ['xCoeff = 1/sqrt(SysM.colMASK.noPixels) * ',SysM.systemFUNC,'(data) ;'] ; eval(cmd) ;
        case {'cdf97'}
            xd = 4 ; yd = 2 ; w = 1;
            xCoeff = regular_97_c(data, xd, yd, w) ;
        otherwise
            errorStruct.message = ['Unrecognised function "', SysM.systemFUNC ,'" during system creation.'];
            errorStruct.identifier = 'SystemMatrix:St:UnrecognisedFunction';
            error(errorStruct) ;
    end
    
    if ~isempty(sensingMatrix)
        % Apply X = Cs * x
        xCoeff =  xCoeff(SysM.colMASK.getMaskDATA)  ;
    end
    
    % Apply x = X ;
    if ~dictionaryMode
        outputVector = xCoeff(:) ;
    end
end
end

