% BDCT MULTIPLIERS
%
% This function multiply the matrix W associated with the
% bdct transform with the dictionary D; i.e. D * W.
%
function bdct(self, varargin)
%
%% SETUP VARIABLES
%
% Display Progress
%                    '12345678901234567890123456789012345678901234567890'
disp([self.name,'::','Initializing Blockwise Discrete Cosine blockwise -']) ;
%
% Initialize function variables
xd = 4 ; yd = 8 ;
blockSize       = 500 ;
%
for jj = 1:2:length(varargin)
    if isvarname(varargin{jj}) 
        cmd = [varargin{jj}, ' = varargin{jj+1} ;'] ; eval(cmd) ;
    else
        errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'];
        errorStruct.identifier = 'DataType:import:InvalidFieldName';
        error(errorStruct) ;
    end
end


%% ROW-WISE BLOCK CONCATINATION WITCH BLOCK DCT

if numel(self.colMASK.size) == 2
    
    if issparse(self.matrixDATA)
        bdctMatrix = sparse([]) ;
    else
        bdctMatrix = [] ;
    end
    
    for jj = 1:blockSize:self.height
        
        % Display progress.
        dispStruct.message = ['Applying Block-DCT Transform to block ', num2str(jj),'.'];
        dispStruct.identifier = 'Dictionary:bdct:ApplyingBDCT';
        self.displayProgress(dispStruct) ;
        %
        indices     = jj:min((jj+blockSize-1),self.noRows) ;
        %
        dictionary3Dmatrix = s3_dct(reshape(self.fullColumnMatrix(indices)', ...
            self.colMASK.height , self.colMASK.width, []),...
            xd, yd) ;
        %
        dictionary2Dmatrix = reshape(permute(dictionary3Dmatrix, [3 1 2]), [], self.colMASK.numel) ;
        bdctMatrix = cat(1,bdctMatrix, dictionary2Dmatrix(:, self.colMASK.activeIndices)) ;
    end
    
    self.matrixDATA	= bdctMatrix ;
    
else
    errorStruct.message = 'Matrix not suited for concatination with Block-DCT matrix' ;
    errorStruct.identifier = 'SystemMatrix:bdct:InproperMatrix';
    error(errorStruct) ;
end
end