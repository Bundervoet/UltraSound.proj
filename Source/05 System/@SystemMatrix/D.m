%% SYSTEM MATRIX

% This function calculates D = * D^{-1} * Cs^T which is de dictionary matrix used such that 
% f = D * x ;

% Based on code provided by Stijn Bettens on 13 okt. 2016
% Current version 17 Okt 2016.

function data = D(SysM, coeff)

% Apply Transpose of Column Sampling.
xCoeff = zeros(SysM.colMASK.size) ;
xCoeff(SysM.colMASK.maskDATA) = coeff;

% Apply Inverse of Sparsifying Base.
switch SysM.systemFUNC 
    case 'identity'
        data = xCoeff ;
    case 'fft'
        data = ifft(xCoeff) ; % 1/sqrt(System.rowMASK.noPixels) * ifft(xCoeff) ; % sqrt(System.rowMASK.noPixels) * ifft(xCoeff) ;
    case 'fft2'
        data = ifft2(xCoeff) ; 
    case 'dct'
        data = idct(xCoeff) ; % 1/sqrt(System.rowMASK.noPixels) * idct(xCoeff) ; % sqrt(System.rowMASK.noPixels) * idct(xCoeff) ;
    case 'dct2'
        data = idct2(xCoeff) ; 
    otherwise
        errorStruct.message = ['Unrecognised function "', SysM.systemFUNC ,'" during system creation.'];
        errorStruct.identifier = 'SystemMatrix:S:UnrecognisedFunction';
        error(errorStruct) ;
end
end

