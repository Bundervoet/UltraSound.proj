function import(SyM, PHdata, RWmask, CNmask, RFpath, varargin)


%% FUNCTION VARIABLES.
batchMode           = false ;
batchSize           = 5000 ;
verboseRate         = 50 ;
globalAbsMax        = 1.1354e-21 ; % = 1e-26 ;
globalNorm          = 0 ; 
currentAbsMax       = .5 ; 
useWeightDensity    = false ;
envelopeDetect      = false ;
saveSparse          = true ;
densityMap          = [] ; 

for jj = 1:2:length(varargin)
    if isvarname(varargin{jj}) 
        cmd = [varargin{jj}, ' = varargin{jj+1} ;'] ; eval(cmd) ;
    else
        errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'];
        errorStruct.identifier = 'DataType:import:InvalidFieldName';
        error(errorStruct) ;
    end
end

%% SETUP VARIABLES
if batchMode
    % Display Progress
    %   '12345678901234567890123456789012345678901234567890'
    disp([timeStamp, SyM.name,'::', ...
        'Importing Batch of RadioFrequency Scatter Points -',...
        '::B',sprintf('%02d',1)]) ; %#ok<*UNRCH>
    
    % Load first batch of RFpoints.
    RFbatch = loader(RFpath, ['RFpoints','_bch',sprintf('%02d',1)]) ;
    batchSize   = RFbatch.batchSize ;
end
D                       = PHdata.digits ;
SyM.rowMASK             = RWmask ;
SyM.colMASK             = CNmask ;
SyM.pixelSize           = SyM.rowMASK.pixelSize ; 

% Initialize systemMatrix 
if saveSparse
    dictionaryMatrix    = sparse([]) ;
    partialMatrix       = zeros(SyM.rowMASK.noSamples, batchSize) ;
else
    dictionaryMatrix    = zeros(SyM.rowMASK.noSamples, SyM.colMASK.noSamples) ;
end


%% ITERATIVELY ADD ELEMENTS

% Intiate Counters
counters                = struct('all', 0, 'add', 0, 'mod', 0, 'batch', 1) ;
currentBatchUpperBound  = batchSize ;
activeColumnIndices     = SyM.colMASK.activeIndex ; 

for jj = activeColumnIndices'
    
    counters.all        = counters.all + 1 ;
    I                   = PHdata.location(jj) ;
    
    % Increment batchLimit when exceeded. When in Batch Mode also import new batch of PSF's.
    if jj > currentBatchUpperBound
           
        % Set new upperbound.
        currentBatchUpperBound      = currentBatchUpperBound + batchSize ;
        
        if batchMode
            counters.batch  = counters.batch + 1 ;
            % Display Progress
            %   '12345678901234567890123456789012345678901234567890'
            disp([timeStamp, SyM.name,'::', ...
                'Importing Batch of RadioFrequency Scatter Points -',...
                '::B',sprintf('%02d',counters.batch)]) ;
            
            % Load RFbatch
            RFbatch     = loader(RFpath, ['RFpoints','_bch',sprintf('%02d',counters.batch)]) ;
        end
    end
    
    % Import New RadioFrequency Point Scatterer and trim channels according to the rowMASK.
    % Afterwords Sample the RFpoint and reshape the samples into a column vector.
    if batchMode
        RFdata    	= RFbatch.extract(['RFpoint','X',sprintf(D.x,I.x),'Y',sprintf(D.y,I.y),'Z',sprintf(D.z,I.z)], mod(jj - 1, batchSize) +1) ;
    else
        RFdata      = loader(RFpath, ['RFpoint','X',sprintf('%02d',I.x),'Y',sprintf('%01d',I.y),'Z',sprintf('%03d',I.z)]) ;
    end
    RFdata.trim(SyM.rowMASK.width) ;
    
    % Further parsify RFpoint with respect to a specified fraction of a given upper bound.
    if RFdata.matrixINFO.isNormalized
        upperBound = .5 ;
    else
        upperBound = globalAbsMax ;
    end  
    
    % Reshape RFdata into a vector-sized dictionary element.
    dictionaryElement = reshape(RFdata.signalDATA(SyM.rowMASK.maskDATA), [], 1) ;
    
    if SyM.sparseSignal.sparsify
            dictionaryElement(abs(dictionaryElement) < SyM.sparseFactor * upperBound ) = 0 ;
    end   
    if SyM.sparseSignal.rejectSmall
        if globalNorm ~= 0
            if norm(dictionaryElement(:), 2) < SyM.sparseFactor * globalNorm
                dictionaryElement = dictionaryElement .* 0 ;
            end
        else
            if  max(abs(dictionaryElement(:))) < (SyM.sparseFactor * upperBound)
                dictionaryElement = dictionaryElement .* 0 ;
            end
        end
    end
    
    % Apply envelope detection to the individual dictionary elements.
    if envelopeDetect
        RFdata.setMatrixDATA(abs(hilbert(RFdata.getMatrixDATA))) ;
    end  
    
    % Normalise dictionary elements.
    switch useWeightDensity       
        case {1, 'wDy', ' -wDy', '-wDy'}
            dictionaryElement   = dictionaryElement ./ SyM.colMASK.densityDATA(jj) ;
            if counters.all == 1 ; SyM.add('name', ' -wDy') ; end
        case {2, 'wL1', ' -wL1', '-wL1'}
            dictionaryElement   = dictionaryElement ./ norm(dictionaryElement, 1) ; 
            if counters.all == 1 ; SyM.add('name', ' -wL1') ; end
        case {3, 'wL2', ' -wL2', '-wL2'}
            dictionaryElement   = dictionaryElement ./ norm(dictionaryElement, 2) ;
            densityMap(end+1)   = norm(dictionaryElement, 2) ;
            if counters.all == 1 ; SyM.add('name', ' -wL2') ; end
        case {3.5, 'wE2', ' -wE2', '-wE2'}
            dictionaryElement   = dictionaryElement ./ (norm(dictionaryElement, 2).^2) ;
            densityMap(end+1)   = norm(dictionaryElement, 2) ;
            if counters.all == 1 ; SyM.add('name', ' -wE2') ; end
        case {4, 'LwL2', ' -LwL2', '-LwL2'}
            dictionaryElement   = 20*log10(abs(dictionaryElement ./ globalAbsMax) + 1e-4) + 80 ;
            dictionaryElement   = dictionaryElement ./ norm(dictionaryElement, 2) ;
            if counters.all == 1 ; SyM.add('name', ' -wLn2') ; end
        case {5, 'aL2', ' -aL2', '-aL2'} 
            dictionaryElement   = abs(dictionaryElement) ./ norm(dictionaryElement, 2) ;
            if counters.all == 1 ; SyM.add('name', ' -aL2') ; end
        case {6, 'nd', ' -nd', '-nd', true}
            dictionaryElement   = dictionaryElement ./ (2 * globalAbsMax) ;
            if counters.all == 1 ; SyM.add('name', ' -nd') ; end
        case {7, 'nDy', ' -nDy', '-nDy'}
            if counters.all == 1 ; SyM.add('name', ' -nDy') ; currentAbsMax = 0 ; end
            dictionaryElement   = dictionaryElement ./ SyM.colMASK.densityDATA(jj) ;
            currentAbsMax       = max(currentAbsMax, max(abs(dictionaryElement(:)))) ;         
        otherwise % case {0, false}
            % DO NOTHING.
    end
    
    % Check wether or not to add element to dictionaryMatrix.
    nonZeroCriteria = (sum(abs(dictionaryElement(:))) > 0) ;                                        % TRUE: dictionaryElement is nonZero.       FALSE: dictionaryElement is zero.
    maskCriteria    = SyM.colMASK.densityDATA(jj) ~= 0 ;                                            % TRUE: mask index is strictly positive.    FALSE: mask index is zero.
    keepCriteria    = ~SyM.sparseSignal.doReject  ;                                                 % TRUE: when no elements are rejected.      FALSE: When dictionary elements can be rejected.
    addElement      = (maskCriteria && nonZeroCriteria) || keepCriteria ;
    if saveSparse
        if addElement                              % Only add nonzero elements.
            % Increment second counter. This counter keeps track of how many elements
            % there are actually added. This is used to crop intialized dictionary of
            % all non-used columns.
            counters.add            = counters.add + 1 ;
            
            % Display Progress.
            if mod(counters.all, verboseRate) == 1
                %   '12345678901234567890123456789012345678901234567890'
                disp([timeStamp, SyM.name,'::', ...
                    'Adding Dictionary Element to Partial Matrix ------',...
                    '::I',sprintf(D.i,jj), ...
                    '::X',sprintf(D.x,I.x), ...
                    'Y',sprintf(D.y,I.y), ...
                    'Z',sprintf(D.z,I.z), ...
                    '::P',sprintf('%03d',floor(counters.all/SyM.colMASK.noMeasurements*100))]) ;
            end
            
            % Determine partial counter progress.
            counters.mod            = counters.mod + 1 ; 
            
            % Add element to partial dictionary.
            partialMatrix(:, counters.mod) = dictionaryElement ;
            
        else
            % Reset counters.add to zero the 
                      
            % Remember that this element was not added to the system Matrix.
            SyM.colMASK.turnOff(jj) ; % self.colMASK.maskDATA(jj) = false ;
            
            % Display Progress.
            %   '12345678901234567890123456789012345678901234567890'
            disp([timeStamp, SyM.name,'::', ...
                'Not adding Dictionary Element to Partial Matrix --',...
                '::I',sprintf(D.i,jj), ...
                '::X',sprintf(D.x,I.x),...
                'Y',sprintf(D.y,I.y),...
                'Z',sprintf(D.z,I.z),...
                '::P',sprintf('%03d',floor(counters.all/numel(activeColumnIndices)*100))]) ;
        end
        
        % Add partial dictionary to full dictionary.
        if jj == currentBatchUpperBound || jj >= activeColumnIndices(end)
            
            % Display Progress.
            %   '12345678901234567890123456789012345678901234567890'
            disp([timeStamp, SyM.name,'::', ...
                'Adding Partial Matrix to Full Dictionary Matrix --',...
                '::I',sprintf(D.i,jj), ...
                '::X',sprintf(D.x,I.x),...
                'Y',sprintf(D.y,I.y),...
                'Z',sprintf(D.z,I.z),...
                '::P',sprintf('%03d',floor(counters.all/SyM.colMASK.noMeasurements*100))]) ;
            
            % Add full partial matrix to sparse dictionary matrix.
            dictionaryMatrix    = [dictionaryMatrix, partialMatrix(:, 1:counters.mod)] ; %#ok<AGROW>
            %             partialMatrix       = zeros(self.rowMASK.noSamples, partialSaveAmount) ;
            %
            % Reset counters.mod to zero to allow refilling of Partial Matrix.
            counters.mod = 0 ;
        end
        
    else
        
        if addElement
            % Increment second counter. This counter keeps track of how many elements
            % there are actually added. This is used to crop intialized dictionary of
            % all non-used columns.
            counters.add            = counters.add +1 ;
            
            dictionaryMatrix(:, counters.add) = dictionaryElement ; %#ok<*SPRIX>
        else
            % Remember that this element was not added to the system Matrix.
            SyM.colMASK.maskDATA(SyM.colMASK.activeIndices(counters.all)) = false ;
            
            % Display Progress.
            %   '12345678901234567890123456789012345678901234567890'
            disp([timeStamp, SyM.name,'::', ...
                'Not adding Dictionary Element to Dictionary Matrix',...
                '::I',sprintf(D.i,jj), ...
                '::X',sprintf(D.x,I.x),...
                'Y',sprintf(D.y,I.y),...
                'Z',sprintf(D.z,I.z),...
                '::P',sprintf('%03d',floor(counters.all/SyM.colMASK.noMeasurements*100))]) ;
        end
    end
end

% Add learned weightmap to dictionaryMatrix.
switch useWeightDensity
    case {3, 'wL2', ' -wL2', '-wL2',...
            3.5, 'wE2', ' -wE2', '-wE2'}
        SyM.colMASK.import(RadioFrequencyData('noName', 'signalDATA', reshape(densityMap, PHdata.rearrangement2D))) ;
end
    
% Save Dictionary Matrix to SystemMatrix object.
SyM.setMatrixDATA(dictionaryMatrix(:, 1:counters.add) ./ (2 * currentAbsMax)) ;

end