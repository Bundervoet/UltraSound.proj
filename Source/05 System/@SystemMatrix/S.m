%% SYSTEM MATRIX

% This generalized method determins the result of either $y = S*x$ where $S = Rs * (Psi * Cs) *
% Phi^{-1}$ is the system matrix in a linear system of equations or $x = S' * y$ where
% $S' = Phi * (Cs^T * Psi^T) * Rs^T$ is the (complex) transpose of that system matrix. Both
% calculations are performed in either Matrix mode when a precalculated version of $S$ is stored in
% systemDATA, otherwise a fast implementation of the internal sparsifying matrix is used for large
% scale inverions.

% Based on code provided by Stijn Bettens on 13 okt. 2016
% Current version 23 Nov 2016.

function outputVector = S(SyM, inputVector, backwardMode, sensingType, multiplyFactor, systemFUNC)

% Infer missing input arguments.
if nargin < 3
    backwardMode    = false ;                                                                            % If False use (complex) transpose.
end
if nargin < 4
    sensingMatrix   = [] ;                                                                          % If False use (complex) transpose.
else
    sensingMatrix   = SyM.getMatrixDATA(sensingType) ;
end
if nargin < 5 
    multiplyFactor  = 1 ;
end
if nargin < 6 
    systemFUNC = SyM.systemFUNC ;
end

% Add columnwise alternating positive negative mask to facilitate coefficient spasification.
if ~isempty(strfind(systemFUNC, 'pN*'))
    posNegMask = repmat([1; -1], SyM.colMASK.height/2, SyM.colMASK.width) ;
    systemFUNC = systemFUNC(4:end) ;
else
    posNegMask = 1 ;
end
    
if (backwardMode == 0)
    % Calculate S = Rs * (Psi * Cs) * Phi^{-1} which is de system matrix used such that
    % y = S * x ;
    
    if ~isempty(sensingMatrix)
        % Apply X = Cs^T * x
        xCoeff = zeros(SyM.colMASK.size) ;
        xCoeff(SyM.colMASK.getMaskDATA) = inputVector ;
    else
        % Apply X = x
        xCoeff = reshape(inputVector, SyM.colMASK.size) ;
    end
    
    % Apply F = Phi^{-1} * X ;
    switch systemFUNC
        case {'identity', 'id'}
            data = posNegMask .* xCoeff ;
        case {'fft', 'fft2', 'dct', 'dct2'}
            cmd = ['data = sqrt(SyM.colMASK.noPixels) * posNegMask .* i',systemFUNC,'(xCoeff) ;'] ; eval(cmd) ;
        case {'regular_97_c',}
            xd = 4 ; yd = 2 ; w = 1;
            data = posNegMask .* regular_97_c(xCoeff, -xd, -yd, w) ;
        case {'regular_97_wt_be', 'regular_97_wt_be:inv', 'regular_97_wt_be:tp'}
            xd = 3 ;
            data = posNegMask .* regular_97_wt_be(xCoeff, -xd, false) ;
        otherwise
            errorStruct.message = ['Unrecognised function "', SyM.systemFUNC ,'" during system creation.'];
            errorStruct.identifier = 'SystemMatrix:S:UnrecognisedFunction';
            error(errorStruct) ;
    end
    
    if ~isempty(sensingMatrix)
        % Apply y = Rs * (Psi * Cs) * F ;
        outputVector = multiplyFactor * sensingMatrix * data(SyM.colMASK.getMaskDATA) ;
    else
        % Apply y = Rs * F ;
        outputVector = data(SyM.rowMASK.getMaskDATA) ;
    end
        
else
    % Calculate S' = Phi * (Cs^T * Psi^T) * Rs^T which is used to calculate
    % argmin_x \| x \|_2 such that y = S * x ;
    
    if ~isempty(sensingMatrix)
        % Apply F = (Cs^T * Psi^T) * Rs^T * y
        data = zeros(SyM.colMASK.size) ;
%         size(sensingMatrix) 
%         size(inputVector)
        data(SyM.colMASK.getMaskDATA) = multiplyFactor * sensingMatrix * inputVector ;       
    else
        % Apply F = Rs^T * D ;
        data = zeros(SyM.rowMASK.size) ;
        data(SyM.rowMASK.getMaskDATA) = inputVector ;
    end    
    
    % Apply X = Phi * F ;
    switch systemFUNC
        case {'identity', 'id'}
            xCoeff = posNegMask .* data ;
        case {'fft', 'fft2', 'dct', 'dct2'}
            cmd = ['xCoeff = 1/sqrt(SyM.colMASK.noPixels) * ',systemFUNC,'(posNegMask  .* data) ;'] ; eval(cmd) ;
        case {'regular_97_c'}
            xd = 4 ; yd = 2 ; w = 1;
            xCoeff = regular_97_c(posNegMask .* data, xd, yd, w) ;
        case {'regular_97_wt_be:inv'}
            xd = 3 ;
            xCoeff = regular_97_wt_be(posNegMask .* data, xd, false) ;
        case {'regular_97_wt_be', 'regular_97_wt_be:tp'}
            xd = 3 ;
            xCoeff = regular_97_wt_be(posNegMask .* data, -xd, true) ;
        otherwise
            errorStruct.message = ['Unrecognised function "', SyM.systemFUNC ,'" during system creation.'];
            errorStruct.identifier = 'SystemMatrix:St:UnrecognisedFunction';
            error(errorStruct) ;
    end
    
    if ~isempty(sensingMatrix)
        % Apply X = Cs * x
        outputVector = reshape(xCoeff(SyM.colMASK.getMaskDATA), [], 1)  ;
    else
        outputVector = xCoeff(:) ;
    end
end
end