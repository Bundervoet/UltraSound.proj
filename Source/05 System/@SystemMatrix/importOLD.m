function importOLD(self, PHdata, RWmask, CNmask, RFpath)
%
%% SETUP VARIABLES
%
RFdata = self.loadNow(RFpath, ['RFpoint','X',sprintf('%02d',I.x),'Y',sprintf('%01d',I.y),'Z',sprintf('%03d',I.z)]) ;
%
partialSaveAmount   = 5000 ;
verboseRate         = 50 ;
%
self.rowMASK = RWmask ;
self.colMASK = CNmask ;
%
if self.saveSparse
    dictionaryMatrix        = sparse([]) ;
    partialMatrix           = zeros(self.rowMASK.noSamples, partialSaveAmount) ;
else
    dictionaryMatrix        = zeros(self.rowMASK.noSamples, self.colMASK.noSamples) ;
end
%
%
%% ITERATIVELY ADD ELEMENTS
%
% Intiate Counter
counters                    = struct('all', 0, 'add', 0, 'mod', 0) ;
D                           = PHdata.digits ;
%
for jj = self.colMASK.activeIndices
    %
    counters.all            = counters.all + 1 ;
    %
    % Load RFdata.
    I = PHdata.location(jj) ;
    RFdata = self.loadNow(RFpath, ['RFpoint','X',sprintf('%02d',I.x),'Y',sprintf('%01d',I.y),'Z',sprintf('%03d',I.z)]) ;
    RFdata.trim(self.rowMASK.width) ;
    %
    dictionaryElement   = reshape(RFdata.signalDATA(self.rowMASK.maskDATA), [], 1) ;
    %
    switch self.sparseSignal
        % 0 = No Sparsify + Add All
        % 1 = Do Sparsify + Add All
        % 2 = Do Sparsify + Add Non Zero
        % 3 = No Sparsify + Add Non Zero
        case {0, 3}
            % DO NOTHING (YET).
        case {1, 2}
            % Sparcify dictionary_element if specified. All pixels having value lower
            % then x% of the recorded maximal value in all of the RFdata will be set to
            % 0. This process improves load/save performance at the cost of discriptive
            % efficiency.
            if RFdata.normalised
                dictionaryElement(abs(dictionaryElement) < self.sparseFactor * .5 ) = 0 ;
            else
                dictionaryElement(abs(dictionaryElement) < self.sparseFactor * .5 * 9e-22 ) = 0 ;
            end
    end
    %
    if self.saveSparse
        %
        % Check wether or not to add element to dictionaryMatrix.
        addElement = sum(dictionaryElement(:)) ~= 0 || self.sparseSignal < 2  ;
        %
        if addElement                              % Only add nonzero elements.
            % Increment second counter. This counter keeps track of how many elements
            % there are actually added. This is used to crop intialized dictionary of
            % all non-used columns.
            counters.add            = counters.add + 1 ;
            %
            % Display Progress.
            if mod(counters.all,verboseRate) == 1
                %                    '12345678901234567890123456789012345678901234567890'
                disp([self.name,'::','Adding Dictionary Element to Partial Matrix ------',...
                    '::I',sprintf(D.i,jj), ...
                    '::X',sprintf(D.x,I.x),...
                    'Y',sprintf(D.y,I.y),...
                    'Z',sprintf(D.z,I.z),...
                    '::P',sprintf('%03d',floor(counters.all/self.colMASK.noMeasurements*100))]) ;
            end
            %
            % Determine partial counter progress.
            counters.mod = mod(counters.add - 1, partialSaveAmount) +1 ;
            %
            % Add element to partial dictionary.
            partialMatrix(:, counters.mod) = dictionaryElement ;
            %
        else
            % Remember that this element was not added to the system Matrix.
            self.colMASK.maskDATA(self.colMASK.activeIndices(counters.all)) = false ;
            %
            % Display Progress.
            %                    '12345678901234567890123456789012345678901234567890'
            disp([self.name,'::','Not adding Dictionary Element to Partial Matrix --',...
                '::I',sprintf(D.i,jj), ...
                '::X',sprintf(D.x,I.x),...
                'Y',sprintf(D.y,I.y),...
                'Z',sprintf(D.z,I.z),...
                '::P',sprintf('%03d',floor(counters.all/self.colMASK.noMeasurements*100))]) ;
            
            %
        end
        %
        % Add partial dictionary to full dictionary.
        if ((mod(counters.add, partialSaveAmount) == 0) && addElement) || counters.all == numel(self.colMASK.activeIndices) ;
            % Display Progress.
            %                    '12345678901234567890123456789012345678901234567890'
            disp([self.name,'::','Adding Partial Matrix to Full Dictionary Matrix --',...
                '::I',sprintf(D.i,jj), ...
                '::X',sprintf(D.x,I.x),...
                'Y',sprintf(D.y,I.y),...
                'Z',sprintf(D.z,I.z),...
                '::P',sprintf('%03d',floor(counters.all/self.colMASK.noMeasurements*100))]) ;
%             
            % Add full partial matrix to sparse dictionary matrix.
            dictionaryMatrix = [dictionaryMatrix, partialMatrix(:, 1:counters.mod)] ; %#ok<*AGROW>
        end
        %
    else
        %
        if addElement
            % Increment second counter. This counter keeps track of how many elements
            % there are actually added. This is used to crop intialized dictionary of
            % all non-used columns.
            counters.add            = counters.add +1 ;
            %
            dictionaryMatrix(:, counters.add) = dictionaryElement ; %#ok<*SPRIX>
            %
        else
            % Remember that this element was not added to the system Matrix.
            self.colMASK.maskDATA(self.colMASK.activeIndices(counters.all)) = false ;
            %
            % Display Progress.
            %                    '12345678901234567890123456789012345678901234567890'
            disp([self.name,'::','Not adding Dictionary Element to Dictionary Matrix',...
                '::I',sprintf(D.i,jj), ...
                '::X',sprintf(D.x,I.x),...
                'Y',sprintf(D.y,I.y),...
                'Z',sprintf(D.z,I.z),...
                '::P',sprintf('%03d',floor(counters.all/self.colMASK.noMeasurements*100))]) ;
        end
    end
    %
end
%
self.matrixDATA = dictionaryMatrix(:, 1:counters.add) ;
%
end