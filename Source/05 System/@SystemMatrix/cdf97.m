% CDF 9/7 MULTIPLIERS

% This function multiply the matrix W associated with the
% cdf97 transform with the dictionary D; i.e. D * W.

function cdf97(self, varargin)


%% SETUP VARIABLES

% Display Progress
%                    '12345678901234567890123456789012345678901234567890'
disp([self.name,'::','Initializing CDF 97 blockwise transform ----------']) ;

% Initialize function variables
xd = 4 ; yd = 2 ; w = 1 ;
blockSize       = 500 ;

for jj = 1:2:length(varargin)
    if isvarname(varargin{jj})
        cmd = [varargin{jj}, ' = varargin{jj+1} ;'] ; eval(cmd) ;
    else
        errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'];
        errorStruct.identifier = 'DataType:import:InvalidFieldName';
        error(errorStruct) ;
    end
end


%% ROW-WISE BLOCK CONCATINATION WITCH WAVELET MATRIX.

if numel(self.colMASK.size) == 2
    
    if issparse(self.getMatrixDATA)
        cdf97Matrix = sparse([]) ;
    else
        cdf97Matrix = [] ;
    end
    
    for jj = 1:blockSize:self.height
        
        % Display progress.
        %                    '12345678901234567890123456789012345678901234567890'
        disp([self.name,'::','Applying CDF 97 Transform to block of rows -------',...
            '::B',sprintf('%06d',jj)]) ;
%         self.displayProgress(dispStruct) ;
        
        indices     = jj:min((jj+blockSize-1), self.noRows) ;
        %
        dictionary3Dmatrix = regular_97_c_3d(reshape(self.fullColumnMatrix(indices)', ...
            self.colMASK.height , self.colMASK.width, []),...
            xd, yd, w) ;
        
        dictionary2Dmatrix = reshape(permute(dictionary3Dmatrix, [3 1 2]), [], self.colMASK.noPixels) ;
%         cdf97Matrix = cat(1,cdf97Matrix, dictionary2Dmatrix(:, self.colMASK.activeIndices)) ;
        cdf97Matrix = cat(1,cdf97Matrix, dictionary2Dmatrix) ;
    end
    
    self.setMatrixDATA(cdf97Matrix) ;
    self.systemFUNC = 'cdf97' ; 
    
    % ADAPT SAMPLING DENSITY ANS RESET MASK ;
    self.colMASK.maskDATA       = true(self.colMASK.size) ;
    cdfDensity                  = regular_97_lp(self.colMASK.densityDATA, xd, yd) ;
    weightingMap                = coeffs_97_c(size(cdfDensity), xd, yd ,w) ;
    self.colMASK.densityDATA    = weightingMap .* cdfDensity  ;
    
    [~, colIndexDATA]           = sort(self.colMASK.densityDATA(:),'descend') ;
    self.colMASK.indexDATA      = uint32(colIndexDATA) ;
else
    errorStruct.message     = 'Matrix not suited for concatination with Wavelet matrix' ;
    errorStruct.identifier  = 'SystemMatrix:cdf97:InproperMatrix';
    error(errorStruct) ;
end
%
end