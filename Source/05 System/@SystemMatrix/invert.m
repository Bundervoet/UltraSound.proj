%% INVERT

% This function inverts the central MxN systemDATA matrix according to a prediscribed inversion method. 
% The end result is alway a NxM matrices with it's row- and columnsMASK's flipped.
% Note that this inversion is more broad then the overloaden inv method.

function invert(SyM, varargin)


%% SETUP VARIABLES
% Initialize function variables
inversion       = 'pinv' ;
type            = MaT('sensing', false) ;
noMeasurements  = SyM.noRows ;
regularMatrix   = [] ; 
tol             = uint8(100) ;

for jj = 1:2:length(varargin)
    if isvarname(varargin{jj})
        cmd = [varargin{jj}, ' = varargin{jj+1} ;'] ; eval(cmd) ;
    else
        errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'];
        errorStruct.identifier = 'DataType:import:InvalidFieldName';
        error(errorStruct) ;
    end
end

% Display Progress
%   '12345678901234567890123456789012345678901234567890'
disp([timeString, SyM.name,'::',...
    'Initializing inversion of System Matrix ----------',...
    '::Type = ', type.strT, '::Method = ', inversion]) ;

%% CORE INVERSION PROCESS.
systemMatrix    = SyM.getMatrixDATA(type) ;

% Invert dictionary using pseudo inverse.
switch lower(inversion)
    case {'pinv', 'p'}
%         SyM.setMatrixDATA(pinv(full(systemMatrix)),                             [type.strT, '_pinv']) ;
        SyM.setMatrixDATA(pinv(full(systemMatrix)),                                 [type.strT, '_pinv']) ;
    case {'inv', 't'}
        SyM.setMatrixDATA(inv(full(systemMatrix)),                                  [type.strT, '_inv']) ;
    case {'over', 'proj', 'o'}
        SyM.setMatrixDATA((systemMatrix' * systemMatrix) \ systemMatrix',           [type.strT, '_over']) ;
    case {'invover', 'invproj', 'to'}
        SyM.setMatrixDATA(inv(systemMatrix' * systemMatrix) * systemMatrix',        [type.strT, '_iOver']) ;
    case {'pinvover', 'pinvproj', 'po'}
        SyM.setMatrixDATA(pinv(systemMatrix' * systemMatrix) * systemMatrix',       [type.strT, '_pOver']) ;
    case {'pinveover', 'pinveproj', 'peo'}
        SyM.setMatrixDATA(PINVe(systemMatrix' * systemMatrix, tol) * systemMatrix', [type.strT, '_PeOver']) ;        
    case {'regularover', 'ro'}
        SyM.setMatrixDATA(systemMatrix' * regularMatrix,                            [type.strT, '_rOver']) ;
    case {'under', 'u'}
        SyM.setMatrixDATA(systemMatrix' / (systemMatrix * systemMatrix'),           [type.strT, '_under']) ;
    case {'invunder', 'tu'}
        SyM.setMatrixDATA(systemMatrix' * inv(systemMatrix * systemMatrix'),        [type.strT, '_iUnder']) ; 
    case {'pinvunder', 'pu'}
        SyM.setMatrixDATA(systemMatrix' * pinv(systemMatrix * systemMatrix'),       [type.strT, '_pUnder']) ; 
    case {'regularunder', 'ru'}       
        SyM.setMatrixDATA(regularMatrix * systemMatrix',                            [type.strT, '_rUnder']) ;
    case {'method1', 'm1', 'ri1'}
        maskDATA        = SyM.rowMASK.outputMASK(noMeasurements) ;
        fullMATRIX      = SyM.getMatrixDATA(MaT('dictionary', false)) ;
        trueMATRIX      = fullMATRIX(maskDATA,:) ;
        falseMATRIX     = fullMATRIX(~maskDATA,:) ;
        SyM.setMatrixDATA((transpose(trueMATRIX) * trueMATRIX + 1/sqrt(noMeasurements) ...
            * transpose(falseMATRIX) * falseMATRIX) \ transpose(trueMATRIX),        'dictionary_rInv') ;
    case {'core', 'c'}
        maskDATA        = SyM.rowMASK.outputMASK(noMeasurements) ;
        trueMATRIX      = SyM.matrixDATA.system(maskDATA,:) ;
        falseMATRIX     = SyM.matrixDATA.system(~maskDATA,:) ;
        SyM.setMatrixDATA(transpose(trueMATRIX) * trueMATRIX + 1/sqrt(noMeasurements) * ...
            transpose(falseMATRIX) * falseMATRIX,                                   [type.strT, '_core']) ;               
end

% Display Progress
%   '12345678901234567890123456789012345678901234567890'
disp([timeString, SyM.name,'::', ...
    'Finished inversion of System Matrix --------------',...
    '::Type = ', type.strT, '::Method = ', inversion]) ;

% Switch colMASK and rowMASK.
% rowMASK = self.rowMASK ; colMASK = self.colMASK ;
% self.rowMASK = colMASK ; self.colMASK = rowMASK ;
end