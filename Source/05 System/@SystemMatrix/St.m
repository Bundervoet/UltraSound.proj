%% TRANSPOSE OF SYSTEM MATRIX

% This function calculates ST = Cs * D * Rs^T which is used to calculate 
% argmin_x \| x \|_2 such that y = S * x ;

% Based on code provided by Stijn Bettens on 13 okt. 2016
% Current version 13 Okt 2016.

function coefficients = St(SysM, measurements)

% Apply Transpose of Column Sampling.
data = zeros(SysM.rowMASK.size) ;
data(SysM.rowMASK.maskDATA) = measurements;

% Apply Inverse of Sparsifying Base.
switch SysM.systemFUNC 
    case 'identity'
        xCoeff = data ;
    case 'fft'
        xCoeff = fft(data) ; % 1/sqrt(System.rowMASK.noSamples) * fft(data) ; % 1/sqrt(System.rowMASK.noPixels) * fft(data) ;
    case 'fft2'
        xCoeff = fft2(data) ;
    case 'dct'
        xCoeff = dct(data) ; % 1/sqrt(System.rowMASK.noSamples) * dct(data) ; % 1/sqrt(System.rowMASK.noPixels) * dct(data) ;
    case 'dct2'
        xCoeff = dct2(data) ; 
    otherwise
        errorStruct.message = ['Unrecognised function "', SysM.systemFUNC ,'" during system creation.'];
        errorStruct.identifier = 'SystemMatrix:St:UnrecognisedFunction';
        error(errorStruct) ;
end

% Apply Row Sampling.
coefficients = xCoeff(SysM.colMASK.maskDATA) ;
end

