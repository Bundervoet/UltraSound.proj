% SIMULATE RFDATA
%
% This function normalises the dictionary according to a fixed maximal value. This action is
% performed on the full dictionary and is not a columns-wise operation.
function sample(self, samplingMask)
%
if samplingMask.isRow
    %
    % Display Progress
    dispStruct.message      = 'Applying Row Mask to Dictionary Matrix.';
    dispStruct.identifier   = 'Dictionary:mask:ApplyingRowMask';
    self.displayProgress(dispStruct) ;
    %
    currentMask             = samplingMask.mask(self.mask.row) ;
    self.DCmatrix           = self.DCmatrix(currentMask, :) ;
    self.mask.row           = self.mask.row & samplingMask.mask ;
    self.extension.rowMask  = samplingMask.extension.rowMask ;
elseif samplingMask.isColumn
    %
    % Display Progress
    dispStruct.message      = 'Applying Column Mask to Dictionary Matrix.';
    dispStruct.identifier   = 'Dictionary:mask:ApplyingColumnMask';
    self.displayProgress(dispStruct) ;
    %
    currentMask             = samplingMask.mask(self.mask.column) ; 
    self.DCmatrix           = self.DCmatrix(:, currentMask) ;
    self.mask.column        = self.mask.column & samplingMask.mask ;
    self.extension.columnMask = samplingMask.extension.columnMask ;
%     self.addExtension(' -CM') ;
end
%
end