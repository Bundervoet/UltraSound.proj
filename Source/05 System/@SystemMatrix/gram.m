%% GRAM

% This function determines the Gram matrix of all correlation between dictionary elements.

function gram(SyM, type)

%% SETUP VARIABLES
% % Initialize function variables
% textINDX    = strfind(self.name, 'matrix') ;
% if ~isempty(textINDX)
%     name    = [self.name(1:(textINDX-1)), 'gram', self.name((textINDX+6):end)] ;
% else
%     name    = self.name ;
% end
if nargin < 2
    type = MaT('sensing', false)  ;
else
    type = MaT(type) ;
end

% Display Progress
if SyM.verbose
    %   '12345678901234567890123456789012345678901234567890'
    disp([timeString, SyM.name,'::', ...
        'Initializing correlation of System elements ------', ...
        '::Type = ',type.strT]) ;
end

%% CORRELATION PROCESS
systemMatrix =  SyM.getMatrixDATA(type) ;
            
SyM.setMatrixDATA(systemMatrix' * systemMatrix, [type.strT, '_Gram']) ;

% Display Progress
if SyM.verbose
    %   '12345678901234567890123456789012345678901234567890'
    disp([timeString, SyM.name,'::', ....
        'Finished correlating System elements -------------', ...
        '::Type = ',type.strT]) ;
end

% if forwardMode
%     SyM.rowMASK = SyM.colMASK ;
% else
%     SyM.colMASK = SyM.rowMASK ; %#ok<UNRCH>
% end
end