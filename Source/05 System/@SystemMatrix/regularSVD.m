%% REGULAR SVD

% This regularises the imported matrix by setting the lowest singular values to zero.

function regularSVD(SyM, type, tol)

%% SETUP VARIABLES
% % Initialize function variables
if nargin < 2
    type = MaT('sensing', false)  ;
else
    type = MaT(type) ;
end

% Display Progress
if SyM.verbose
    %   '12345678901234567890123456789012345678901234567890'
    disp([timeString, SyM.name,'::', ...
        'Determining Singular Value Decomposition ---------', ...
        '::Type = ',type.strT]) ;
end

[U,S,V] = svd(full(SyM.getMatrixDATA(type)), 'econ');
s = diag(S);

if nargin < 3
    tol = max(s) / 100 ;
elseif isa(tol, 'uint8') || isa(tol, 'uint16') || isa(tol, 'uint32')
    tol = max(s) / double(tol) ;
end

%% SVD PROCESS
r1          = sum(s > tol)+1 ;
V(:,r1:end) = [] ;
U(:,r1:end) = [] ;
s(r1:end)   = [] ;

SyM.setMatrixDATA((U *(s.*V)), [type.strT, '_rSVD']) ;

% Display Progress
if SyM.verbose
    %   '12345678901234567890123456789012345678901234567890'
    disp([timeString, SyM.name,'::', ....
        'Finished regularizing System Matrix --------------', ...
        '::Type = ',type.strT]) ;
end
end