% SYSTEMMARTRIX

% The central component for applying LeastSquare regression or Compressive Sensing is choosing a
% representation of the data of intrest (in our case RFdata).
%       %% f = A * x %%
% For this project we represent RFdata linearly by a collection of PSF's which are point responses of
% an UStransducer exciting a single scatterPoint.
% This class thus consists of the methods necceassry to create a 2D matrix of these PSF's and
% applying multiplactions of this 2D matrix with other sparcifying matrices such as Wavelets,
% Fouriers, Block-DCT, CDF97, etc...

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 28 OKT 16.

classdef SystemMatrix < DataMatrix
    
    
    %% PROPERTIES
    properties % DATA PROPERTIES
        matrixDATA@struct               = struct('dictionary', []) ;                                    % Matrix inwhich the columns are PSF's in array-form.                                                                          % Inverse of systemMatrix
        rowMASK@SamplingMask ;                                                                      % Used to sample the rows of the associated SystemMatrix.
        colMASK@SamplingMask ;                                                                      % Used to sample the columns of the associated SystemMatrix.
    end
    properties % SPARSE PROPERTIES
        sparseSignal@SparsitySetting    = SparsitySetting.complete ;                                % Sparsify the Channel RFdata.
        sparseFactor@double             = 1e-3 ;                                                    % Percentage treshhold for setting small values set to zero.
    end
    properties % FUNCTION HANDLE PROPERTIES
        systemFUNC@char                 = 'identity' ;                                              % Generic Spasifying functions.
    end
    
    %% METHODS
    methods
        
        
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a instance and accessing
        % / changing the state of its properties.
        % ===
        function SysM = SystemMatrix(name, varargin)
            SysM@DataMatrix(name, varargin{:}) ;
        end
        function output = type(SysM)
            output = 'SYmatrix' ;
        end
        function setOne(SyM, input, value)
            % ---
            % Similar to setOne in other classes this method provides functionality for addapting a
            % variable in UltraSoundTransducer. Special care is taken when adapting the properties
            % inhereted by SoundWave as interparameter dependencies exist.
            % ---
            switch lower(input)
                case {'sparsesignal'}
                    if isa(value, 'char')
                        SyM.(input) = SparsitySetting.(value) ;
                    else
                        SyM.(input) = value ;
                    end
                otherwise
                    setDATA(SyM, input, value) ;
            end
        end
        
        
        %% DATA METHODS.      
        function output = getSensingMatrix(SyM, type)
            % ---
            % getSystemDATA : gives an output of the current systemMatrix taking in to account the
            % state of of both the rowMASK and the colMASK. If certain columns are inactive in the
            % colMASK these columns will not be added.
            % ---
                        
            % Initialize standard paramaters.
            if nargin < 2
                type = MaT('sensing', false)  ;
            else
                type = MaT(type) ; 
            end
           
            % Initialize function variables          
            if isfield(SyM.matrixDATA, type.strT)
                sensingMatrix = SyM.matrixDATA.(type.strT) ;
                type.T = false ;
            elseif isfield(SyM.matrixDATA, type.str)
                sensingMatrix = SyM.matrixDATA.(type.str) ;
            elseif isfield(SyM.matrixDATA, type.strDT)
                sensingMatrix = SyM.matrixDATA.(type.strDT)(...
                    SyM.rowMASK.getMaskDATA(false), ...
                    SyM.colMASK.getMaskDATA(false)) ;
                type.T = false ;
            elseif isfield(SyM.matrixDATA, type.strD)
                sensingMatrix = SyM.matrixDATA.(type.strD)(...
                    SyM.rowMASK.getMaskDATA(false), ...
                    SyM.colMASK.getMaskDATA(false)) ;
            else
                SyM.matrixDATA.dictionary(...
                    SyM.rowMASK.getMaskDATA(false), ...
                    SyM.colMASK.getMaskDATA(false)) 
            end
            
            if type.T
                output = sensingMatrix' ;
            else
                output = sensingMatrix ;
            end         
        end
        function output = getDictionaryMatrix(SyM, type)
            % ---
            % getDictionaryDATA : gives an output of the overal systemMatrix taking in to account the
            % original state of of both the rowMASK and the colMASK. If certain columns are inactive 
            % in the colMASK these columns will be zero. 
            % ---
            % Initialize standard paramaters.
            if nargin < 2
                type = MaT('dictionary', false) ;
            else
                type = MaT(type) ;
            end
            
%             dictionaryMatrix = zeros(SyM.rowMASK.noPixels, SyM.colMASK.noMeasurements) ;
%             dictionaryMatrix(...
%                 SyM.rowMASK.activeMask(true), ...
%                 SyM.colMASK.getMaskDATA(false)) ...
%                 = SyM.matrixDATA.(type.N)(...
%                 SyM.rowMASK.activeMask(false), ...
%                 SyM.colMASK.getMaskDATA(false)) ;
            dictionaryMatrix = SyM.matrixDATA.(type.N) ; 
            
            if type.T
                output = dictionaryMatrix' ;
            else
                output = dictionaryMatrix ;
            end
%             output = full(output) ;  
        end
        function output = getFullSensingMatrix(SyM, getFullCols, getFullRows)
            % ---
            % getFullSystemMatrix, returns the full matrix stored in systemDATA.
            % when getFullCols == true :
            %   - All inactive zero columns are added to the output.
            % when getFullRows == true :
            %   - All inactive zero rows from the original matrix are added to the output.
            % ---
            if nargin < 2
                getFullCols = true ;
            end
            if nargin < 3
                getFullRows = true ;
            end
            % Switch between different modes, add all Cols <> add all Rows
            if getFullCols && getFullRows
                rowMask = SyM.rowMASK.activeMask(true) ;
                colMask = SyM.colMASK.activeMask(true) ;
                output  = zeros(SyS.rowMASK.noPixel, SyS.colMASK.noPixel) ;
                output(rowMask, colMask)  = SyM.getMatrixDATA ;
            elseif getFullCols && ~getFullRows
                rowMask = SyM.rowMASK.activeMask(false) ;
                colMask = SyM.colMASK.activeMask(true) ;
                output  = zeros(SyS.rowMASK.noActive, SyS.colMASK.noPixel) ;
                output(rowMask, colMask)  = SyM.getMatrixDATA ;
            elseif getFullCols && ~getFullRows
                rowMask = SyM.rowMASK.activeMask(true) ;
                colMask = SyM.colMASK.activeMask(false) ;
                output  = zeros(SyS.rowMASK.noPixel, SyS.colMASK.noActive) ;
                output(rowMask, colMask)  = SyM.getMatrixDATA ;
            else
                rowMask = SyM.rowMASK.activeMask(false) ;
                colMask = SyM.colMASK.activeMask(false) ;
                output  = zeros(SyS.rowMASK.noActive, SyS.colMASK.noActive) ;
                output(rowMask, colMask)  = SyM.getMatrixDATA ;
            end
        end
        function output = getMatrixDATA(SyM, type)
            if nargin < 2
                type = MaT('sensing', false)  ;
            else
                type = MaT(type) ; 
            end
            if strfind(type.N, 'dictionary')
                output = SyM.getDictionaryMatrix(type) ;
            elseif strfind(type.N, 'sensing')
                output = SyM.getSensingMatrix(type) ;
            elseif strfind(type.N, 'empty')
                output = [] ;
            elseif strfind(type.N, 'one')
                output = 1 ;
            elseif strfind(type.N, 'zero')
                output = 0 ;
            else
                output = SyM.matrixDATA.(type.N) ;
                if type.T
                    output = output' ;
                end
            end
        end
        function setMatrixDATA(SyM, input, type)
            if nargin < 3
                type = MaT('dictionary', false) ;
            else
                type = MaT(type) ;
            end
            if isa(SyM.matrixDATA, 'struct')
                SyM.matrixDATA.(type.N) = input ;
            else
                SyM.matrixDATA = input ;
            end
        end
        
        %% SIZE METHODS
        function output = noRows(SysM, input)
            if nargin >= 2
                output = SysM.rowMASK.noMeasurements(input) ;
            else
                output = SysM.rowMASK.noMeasurements ;
            end
        end
        function output = pcRows(SysM, input)
            if nargin >= 2
                output = SysM.rowMASK.pcMeasurements(input) ;
            else
                output = SysM.rowMASK.pcMeasurements ;
            end
        end
        function output = pmRows(SysM, input)
            if nargin >= 2
                output = SysM.rowMASK.pmMeasurements(input) ;
            else
                output = SysM.rowMASK.pmMeasurements ;
            end
        end
        function output = noColumns(SysM, input)
            if nargin >= 2
                output = SysM.colMASK.noMeasurements(input) ;
            else
                output = SysM.colMASK.noMeasurements ;
            end
        end
        function output = pcColumns(SysM, input)
            if nargin >= 2
                output = SysM.colMASK.pcMeasurements(input) ;
            else
                output = SysM.colMASK.pcMeasurements ;
            end
        end
        function output = pmColumns(SysM, input)
            if nargin >= 2
                output = SysM.colMASK.pmMeasurements(input) ;
            else
                output = SysM.colMASK.pmMeasurements ;
            end
        end
        
        
        %% REBUILD METHODS.
        function SYmatrix = fullColumnMatrix(SysM, indices)
            if nargin  ==  2
                SYmatrix = zeros(length(indices), SysM.colMASK.noPixels) ;
                SYmatrix(:, SysM.colMASK.activeIndex) = SysM.systemDATA(indices,:) ;
            else
                SYmatrix = zeros(SysM.rowMASK.noMeasurements, SysM.colMASK.noPixels) ;
                SYmatrix(SysM.rowMASK.activeIndex, SysM.colMASK.activeIndex) = SysM.systemDATA ;
            end
        end
        
        
        %% OPERATION METHODS.
        function output = normalised(SysM)
            if SysM.absMaximum < 0.51 && SysM.absMaximum > 0.051
                output = true ;
            else
                output = false ;
            end
        end
        
        %% IMAGING METHODS.
        function [outputDATA, outputINFO, par] = imageDATA(SysM, par)
            % Imaging variables. From these variables the correct distance measurements
            % are displayed in the figure window. Moreover they help to preserver the
            % correct aspect ratios.
            
%             inputMaT = MaT('dictionary_Dual', false) ; 
%             inputMaT = par.get('type') ; 
            inputMaT = par.meta.type ;
            
            % Size input
            switch lower(par.data)
                case  {'element', 'dcelement', 'dictionary_element', 'point_spread_function', 'psf'}
                    
                    % Determine column mask index.
                    switch numel(par.index)
                        case 1
                            index = par.index ;
                        case 2
                            index = par.index(1) + (par.index(2)-1) * SysM.colMASK.height ;
                        otherwise
                            errorStruct.message = 'Input size of "data" not recognised.';
                            errorStruct.identifier = 'Dictionary:imageData:InputNotRecognised';
                            error(errorStruct) ;
                    end
                    
                    if SysM.colMASK.maskDATA(index) ~= 0
                        % Determine dictionary index taking into account unused elements.
                        %                         dictionaryIndex                         = sum(SysM.colMASK.maskDATA(1:index)) ;
                        %                         inputMatrix                             = SysM.systemDATA(:, dictionaryIndex) ;
                        %
                        inputMatrix                             = SysM.getMatrixDATA(inputMaT) ;
                        currentMatrix                           = zeros(SysM.rowMASK.size) ;
                        currentMatrix(SysM.rowMASK.getMaskDATA) = inputMatrix(:, index) ;
                        par.minimum = min(currentMatrix(:)) ; par.maximum = max(currentMatrix(:)) ; par.scale = 4 ;
                        
                        % Retain correct aspect ratio of dictionary element.
                        dataSize        = SysM.rowMASK.size ;
                        pixelHeight     = SysM.rowMASK.pixelHeight ;
                        pixelWidth      = SysM.rowMASK.pixelWidth ;
                        outputDATA      = imresize(full(currentMatrix), [dataSize(1) (dataSize(2) * pixelWidth / pixelHeight)] * par.scale, 'nearest') ;
                        outputINFO      = 'DCelement' ;
                        
                        if SysM.matrixINFO.isDecibel
                            par.set('dynamic', DynamicSetting.none) ;
                        elseif SysM.matrixINFO.isNormalized
                            par.set('dynamic', DynamicSetting.decibel) ;
                            par.set('minimum', -.5) ;
                            par.set('maximum', .5) ;
                        else
                            par.set('dynamic', DynamicSetting.decibel) ;
                        end
                        
                    else
                        warning('Grid Element Not Found In Dictionary') ;
                    end
                    
                case  {'amplitude_response_function', 'arf', 'frame'}  
                    % Determine column mask index.
                    switch numel(par.index)
                        case 1
                            index = par.index ;
                        case 2
                            index = par.index(1) + (par.index(2)-1) * SysM.rowMASK.height ;
                        otherwise
                            errorStruct.message = 'Input size of "data" not recognised.';
                            errorStruct.identifier = 'Dictionary:imageData:InputNotRecognised';
                            error(errorStruct) ;
                    end
                    
                    if SysM.rowMASK.maskDATA(index) ~= 0
                        % Determine dictionary index taking into account unused elements.
                        %                         dictionaryIndex                         = sum(SysM.colMASK.maskDATA(1:index)) ;
                        %                         inputMatrix                             = SysM.systemDATA(:, dictionaryIndex) ;
                        %
                        inputMatrix                             = SysM.getMatrixDATA(inputMaT) ;
                        currentMatrix                           = zeros(SysM.colMASK.size) ;
                        currentMatrix(SysM.colMASK.getMaskDATA) = inputMatrix(index, :) ;
                        par.minimum = min(currentMatrix(:)) ; par.maximum = max(currentMatrix(:)) ; par.scale = 4 ;
                        
                        % Retain correct aspect ratio of dictionary element.
                        dataSize        = SysM.colMASK.size ;
                        pixelHeight     = SysM.colMASK.pixelHeight ;
                        pixelWidth      = SysM.colMASK.pixelWidth ;
                        outputDATA      = imresize(full(currentMatrix), [dataSize(1) (dataSize(2) * pixelWidth / pixelHeight)] * par.scale, 'nearest') ;
                        outputINFO      = 'DCelement' ;
                        
                        if SysM.matrixINFO.isDecibel
                            par.set('dynamic', DynamicSetting.none) ;
                        elseif SysM.matrixINFO.isNormalized
                            par.set('dynamic', DynamicSetting.decibel) ;
                            par.set('minimum', -.5) ;
                            par.set('maximum', .5) ;
                        else
                            par.set('dynamic', DynamicSetting.decibel) ;
                        end
                        
                    else
                        warning('Grid Element Not Found In Dictionary') ;
                    end
                    
                case  {'correlation'}
                    % Determine column mask index.
                    switch numel(par.index)
                        case 1
                            index = par.index ;
                        case 2
                            index = par.index(1) + (par.index(2)-1) * SysM.colMASK.height ;
                        otherwise
                            errorStruct.message = 'Input size of "data" not recognised.';
                            errorStruct.identifier = 'Dictionary:imageData:InputNotRecognised';
                            error(errorStruct) ;
                    end
                    
                    if SysM.colMASK.maskDATA(index) ~= 0
                        % Determine dictionary index taking into account unused elements.
                        %                         dictionaryIndex                         = sum(SysM.colMASK.maskDATA(1:index)) ;
                        %                         inputMatrix                             = SysM.systemDATA(:, dictionaryIndex) ;
                        %
                        inputMatrix                             = SysM.getMatrixDATA('dictionary_Gram', false) ;
                        currentMatrix                           = zeros(SysM.colMASK.size) ;
                        currentMatrix(SysM.colMASK.getMaskDATA) = inputMatrix(:, index) ;
                        par.minimum = min(currentMatrix(:)) ; par.maximum = max(currentMatrix(:)) ; par.scale = 4 ;
                        
                        % Retain correct aspect ratio of dictionary element.
                        dataSize        = SysM.colMASK.size ;
                        pixelHeight     = SysM.colMASK.pixelHeight ;
                        pixelWidth      = SysM.colMASK.pixelWidth ;
                        outputDATA      = imresize(full(currentMatrix), [dataSize(1) (dataSize(2) * pixelWidth / pixelHeight)] * par.scale, 'nearest') ;
                        outputINFO      = 'DCcorrelation' ;
                        
                        if SysM.matrixINFO.isDecibel
                            par.set('dynamic', DynamicSetting.none) ;
                        elseif SysM.matrixINFO.isNormalized
                            par.set('dynamic', DynamicSetting.decibel) ;
                            par.set('minimum', -.5) ;
                            par.set('maximum', .5) ;
                        else
                            par.set('dynamic', DynamicSetting.decibel) ;
                            par.set('minimum', min(inputMatrix(:))) ;
                            par.set('maximum', max(inputMatrix(:))) ;
                        end
                        
                    else
                        warning('Grid Element Not Found In Dictionary') ;
                    end
                    
                case  {'invelement', 'inverse_element'}
                    % Determine row mask index.
                    switch numel(par.index)
                        case 1
                            index = par.index ;
                        case 2
                            index = par.index(1) + (par.index(2)-1) * SysM.rowMASK.height ;
                        otherwise
                            errorStruct.message = 'Input size of "data" not recognised.';
                            errorStruct.identifier = 'Dictionary:imageData:InputNotRecognised';
                            error(errorStruct) ;
                    end
                    
                    if SysM.rowMASK.maskDATA(index) ~= 0
                        % Determine dictionary row index taking into account unused pixels.
                        %                         dictionaryIndex                         = sum(SysM.rowMASK.maskDATA(1:index)) ;
                        inputMatrix                             = SysM.getSystemDATA(false, false, true) ;
                        currentMatrix                           = zeros(SysM.colMASK.size) ;
                        currentMatrix(SysM.colMASK.getMaskDATA) = inputMatrix(:, index) ;
                        par.minimum = min(currentMatrix(:)) ; par.maximum = max(currentMatrix(:)) ; par.scale = 4 ;
                        
                        % Retain correct aspect ratio of dictionary element.
                        dataSize        = SysM.colMASK.size ;
                        pixelHeight     = SysM.colMASK.pixelHeight ;
                        pixelWidth      = SysM.colMASK.pixelWidth ;
                        outputDATA      = imresize(full(currentMatrix), [dataSize(1) (dataSize(2) * pixelWidth / pixelHeight)] * par.scale, 'nearest') ;
                        outputINFO      = 'INVelement' ;
                        
                        par.setPar('minimum', min(inputMatrix(:))) ;
                        par.setPar('maximum', max(inputMatrix(:))) ;
                    else
                        warning('Grid Element Not Found In Dictionary') ;
                    end
                    
                case {'outputdensity'}
                    inputMatrix     = SysM.outputDensity ;
                    
                    dataSize        = SysM.rowSize ;
                    pixelHeight     = SysM.rowPixelHeight ;
                    pixelWidth      = SysM.rowPixelWidth ;
                    outputDATA      = imresize(full(inputMatrix), [dataSize(1) (dataSize(2) * pixelWidth / pixelHeight)] * par.scaling, 'nearest') ;
                    outputINFO      = 'DCdensity' ;
                    
                    par.setPar('minimum', min(inputMatrix(:))) ;
                    par.setPar('maximum', max(inputMatrix(:))) ;
                    
                case {'absmatrix'}
                    inputMatrix     = abs(SysM.DCmatrix) ;
                    
                    outputDATA      = full(inputMatrix) ;
                    outputINFO      = 'DCmatrix' ;
                    
                    par.setPar('minimum', min(inputMatrix(:))) ;
                    par.setPar('maximum', max(inputMatrix(:))) ;
            end
        end
    end
end