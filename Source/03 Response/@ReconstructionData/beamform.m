function  beamform(REdata, visualizer)

if nargin < 2
    visualizer = struct('beamformer', BeamformSetting.focusing, 'apodizer', ApodizationSetting.hanning, 'filter', false) ;
end

pixelSize       = REdata.get('pixelSize') ;
height          = REdata.get('height') ;
width           = REdata.get('width') ;
firstPixel      = REdata.get('firstPixel') ;
relativeFocus   = REdata.get('relativeFocus') ;

fieldNames = fields(REdata.signalDATA) ;
for ii = 1 : length(fieldNames)
    
    % Choose dataMatrix to beamform.
    inputDATA       = REdata.getMatrixDATA(fieldNames{ii}) ;
    
    switch visualizer.beamformer
        case BeamformSetting.center
            % Apodize RFline using the apodisation function. Note this is only usefull
            % when the RFline consist of Channel RFdata.
            if visualizer.apodisation
                REdata.apodise() ;
            end
            
            % Only use the central line from the Channel RFdata when "beamforming". No
            % averaging is done and only the recieved signal from one elements is
            % trasferred on to the image processor.
            inputDATA   = inputDATA(:,floor(width/2)) ;
            
            % Determine the low-frequency enveloppe signal of the high-frequency
            % beamformed RFline. This high-frequency in the Rfline originates from the
            % excitation pulse used by the transducer but contains no imporatant visual
            % information about the underlying phantom tissue.
            inputDATA  = abs(hilbert(inputDATA)) ;
            
        case BeamformSetting.summation
            
            % Apodize RFline using the apodisation function. Note this is only usefull
            % when the RFline consist of Channel RFdata.
            if visualizer.apodisation
                REdata.apodise() ;
            end
            
            % Average the information received by all active elements to increase the
            % Signal to Noise Ratio (SNR).
            inputDATA = sum(inputDATA,2) ;
            
            % Determine the low-frequency enveloppe signal of the high-frequency
            % beamformed RFline. This high-frequency in the Rfline originates from the
            % excitation pulse used by the transducer but contains no imporatant visual
            % information about the underlying phantom tissue.
            %         RFdata.signalDATA = abs(hilbert(RFdata.signalDATA));
            
        case BeamformSetting.focusing
            
            % Distance between two neighboring centers of elements on the probe.
            elementPitch        = pixelSize(2) ;
            
            % Full probe width.
            apertureWidth       = (width - 1) * elementPitch ;                                          % Width between center of first and last element on the probe.
            %         probeWidth = transducer.noElements * elementPitch ;                               % Extended width between outer edges of the probe + kerf.
            
            % Window width.
            %         windowWidth = transducer.noElements * elementPitch - transducer.elementKerf ;     % Full width between outer edges of the probe.
            
            % Location of probe elements.
            xp = linspace(-apertureWidth/2, apertureWidth/2, width) ;                                   % Centers of transducer elements.
            
            %         % First active element.
            %         [noElementsLeftOfReceiveWindow, ~] = activeWindow(excitationProtocol, transducer, x , 'receive') ;
            %         % Center of active elements.
            %         centerFirst = xp(noElementsLeftOfReceiveWindow + 1) ; centerLast = xp(noElementsLeftOfReceiveWindow + excitationProtocol.noReceive) ;
            %         %
            %         % x-coordinates of active elements.
            %         xl = linspace(centerFirst, centerLast, excitationProtocol.noReceive) ;
            
            % Initial depths of raw rf data.
            z = ((0:height-1) + firstPixel) * pixelSize(2) ;
            
            % Initial lateral and axial points of RFdata after meshgrid.
            [xi,zi] = meshgrid(xp,z) ;
            
            % Depths where to interpolate to do the focusing.
            zf = zi + (-relativeFocus.z + sqrt((xi - relativeFocus.x).^2 + relativeFocus.z.^2))/2 ;
            
            % Interpolation = Focusing.
            inputDATA = interp2(xi, zi, inputDATA, xi, zf, 'nearest') ;
            
            %         rfline = signal_focused ;
            %                         cmd    = ['save foc_lines/line',num2str(line),'.mat rfline'] ; eval(cmd) ;
            
            % Apodize RFline using the apodisation function. Note this is only usefull
            % when the RFline consist of Channel RFdata.
            
            if visualizer.apodizer.useApodization && ~REdata.matrixINFO.isApodized
                REdata.apodize(char(visualizer.apodizer)) ;
            end
            
            % Remove NAN.
            inputDATA(isnan(inputDATA)) = 0 ;
            %
            % k = round(indexProtocol.lineStatus.current * 97/192) ;
            %
            if visualizer.filter
                REdata.signalDATA = gaussianFilter(sum(REdata.signalDATA,2), excitationProtocol.fs, excitationProtocol.f0, 100, 0) ;
            else
                inputDATA = sum(inputDATA,2) ;
            end
            %
        case BeamformSetting.none
            % DO NOTHING
        otherwise
            errorStruct.message = ['Beamformer"', num2str(visualizer.beamform),'" not recognised.'];
            errorStruct.identifier = 'BrightnessModeImage:b:BeamformerNotRecognised';
            error(errorStruct) ;
    end
    %
    REdata.setMatrixDATA(inputDATA, fieldNames{ii}) ;
end
%
end
