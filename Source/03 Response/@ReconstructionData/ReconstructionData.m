% RECONSTRUCTIONDATA

% This class is an extension of RadioFrequencyData. An instance created by a Solver when
% reconstructing RFdata subsampled by a SystemMatrix. It's central property signalDATA contains the
% approximated signalDATA of the original RFdata. Moreover the property coeffDATA contains the
% coefficient of the signalDATA with respect to the SystemMatrix.
% Similar to RFdata the signalDATA and coeffDATA can be visualized using imager. As REdata <
% RFdata it can also be fed to an BrightnessModeImage in order to obtain an overal visualization of
% the structure contained in the original Phantom.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 22 JUN 16.

classdef ReconstructionData < RadioFrequencyData
    
    
    %% PROPERTIES
    properties % DATA PROPERTIES
        spatialDATA@struct      = struct('result', []) ;                                            % Reconstructed acoustic coefficients.
        rowMASK@SamplingMask    = SamplingMask('') ;                                                % Used to sample the rows of the associated SystemMatrix.
        colMASK@SamplingMask    = SamplingMask('') ;                                                % Used to sample the columns of the associated SystemMatrix.
    end
    
    
    %% METHODS
    methods
        
        
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of RadioFrequencyData and
        % accessing / changing the state of its properties.
        % ===
        function ReD = ReconstructionData(name, varargin)
            % ---
            % Set-up is mainly facilitated by the upperclass RadioFrequencyData and the set-method
            % which internally uses this class' version of setOne to acces and alter internal
            % properties. Multiple input types are supported including:
            % - list of property-names and associated values.
            %   ex. ReD = REdata(name, 'pixelSize', value1, 'relativeFocus', value2) ;
            % - other Response objects, possible accomponied with an additional list-of-properties.
            %   ex. ReD = REdata(name, RFdata, value1, 'relativeFocus', value2) ;
            % ---
            if nargin > 1
                % If more than one argument is provided, check wheter the input is a collection of
                % property-names with associated values or an other Response objects. In the later
                % case the constructor will act as a copy method.
                switch class(varargin{1})
                    case {'RadioFrequencyData'}
                        % Copy the RFdata into REdata and set signalDATA as reference.
                        features    = varargin{1}.features ;
                        featureVars = features(5:end) ;
                        data     	= {'signalDATA', struct('reference', features{4})} ;
                        inputVars   = varargin(2:end) ;
                        % Also copy name when not provided.
                        if isempty(name)
                            name = features{2} ;
                        end
                    case {'ReconstructionData'}
                        % Copy provided REdata into new REdata.
                        features    = varargin{1}.features ;
                        featureVars = features(11:end) ;
                        data        = features(3:10) ;
                        inputVars   = varargin(2:end) ;
                        % Also copy name when not provided.
                        if isempty(name)
                            name = features{2} ;
                        end
                    otherwise
                        featureVars = {} ;
                        inputVars   = varargin ;
                        data        = {} ;
                end
            else
                featureVars = {} ;
                data        = {} ;
                inputVars   = {} ;
            end
            
            % Setup superclass with possible variables associated to a given Response object.
            ReD@RadioFrequencyData(name, featureVars{:}) ;
            ReD.setMatrixDATA([], 'result') ;
            % Add/Change variables in de following order.
            % 1) Add data-properties.
            % 2) Change properties provided by addional list-of-properties input.
            ReD.set(data{:}, inputVars{:}) ;
        end
        function output = type(ReD)
            output = 'REdata' ;
        end
        
        
        %% DATA METHODS
        % ===
        % Pseudo-property methods allowing for easy and generic access (and adaptation) of the
        % central data property signalDATA.
        % ===
        function output = maskDATA(ReD)
            output = ReD.rowMask.maskDATA ;
        end
        function output = getMeasurementDATA(ReD, step)
            if nargin < 2
                step = 'reference' ;
            end
            if isa(ReD.signalDATA, 'struct')
                output = ReD.signalDATA.(step)(ReD.rowMASK.getMaskDATA) ;
            else
                output = ReD.signalDATA ;
            end
        end
        function output = getMatrixDATA(ReD, step)
            if nargin < 2
                step = 'result' ;
            end
            if isa(ReD.signalDATA, 'struct')
                output = ReD.signalDATA.(step)  ;
            else
                output = ReD.signalDATA ;
            end
        end
        function setMatrixDATA(ReD, input, step)
            if nargin < 3
                step = 'result' ;
            end
            if isa(ReD.signalDATA, 'struct')
                ReD.signalDATA.(step) = input ;
            else
                ReD.signalDATA = input ;
            end
        end
        function output = getSpatialDATA(ReD, step)
            if nargin < 2
                step = 'result' ;
            end
            if isa(ReD.spatialDATA, 'struct')
                output = ReD.spatialDATA.(step) ;
            else
                output = ReD.spatialDATA ;
            end
        end
        function setSpatialDATA(ReD, input, step)
            if nargin < 3
                step = 'result' ;
            end
            if isa(ReD.spatialDATA, 'struct')
                ReD.spatialDATA.(step) = input ;
            else
                ReD.spatialDATA = input ;
            end
        end
        function output = getCoeffDATA(ReD, step)
            if nargin < 2
                step = 'result' ;
            end
            if isa(ReD.spatialDATA, 'struct')
                output = ReD.spatialDATA.(step) ;
            else
                output = ReD.spatialDATA ;
            end
        end
        function setCoeffDATA(ReD, input, step)
            if nargin < 3
                step = 'result' ;
            end
            if isa(ReD.spatialDATA, 'struct')
                ReD.spatialDATA.(step) = input ;
            else
                ReD.spatialDATA = input ;
            end
        end
        
        function output = getMeasurementMATRIX(ReD, step)
            if nargin < 2
                step = 'reference' ;
            end
            output          = zeros(ReD.rowMASK.size) ;
            if isa(ReD.signalDATA, 'struct')
                output(ReD.rowMASK.maskDATA)    = ReD.signalDATA.(step)(ReD.rowMASK.getMaskDATA) ;
            else
                output(ReD.rowMASK.maskDATA)    = ReD.signalDATA ;
            end
        end
        function output = getSpatialMATRIX(ReD, step)
            if nargin < 2
                step = 'result' ;
            end
            output          = zeros(ReD.colMASK.size) ;
            if isa(ReD.spatialDATA, 'struct')
                output(ReD.colMASK.maskDATA)    = ReD.spatialDATA.(step)(ReD.colMASK.getMaskDATA) ;
            else
                output(ReD.colMASK.maskDATA)    = ReD.spatialDATA ;
            end
        end
        function output = getCoeffMATRIX(ReD, step)
            if nargin < 2
                step = 'result' ;
            end
            output          = zeros(ReD.colMASK.size) ;
            if isa(ReD.spatialDATA, 'struct')
                output(ReD.colMASK.maskDATA)    = ReD.spatialDATA.(step)(ReD.colMASK.getMaskDATA) ;
            else
                output(ReD.colMASK.maskDATA)    = ReD.spatialDATA ;
            end
        end
        
        
        %% IMAGING METHODS
        % ===
        % Internal object-specific method used by imageNow in order to collect and process imaging
        % data according to a tunable set of paramaters.
        % ===
        function [outputDATA, outputINFO, par] = imageDATA(ReD, par)
            % ---
            % This method collects the required data for imaging and processes it such that for
            % example the correct distance measurements are displayed in the figure window.
            % Alternatively the processing helps to preserver the correct aspect ratios and
            % transforms the data to the correct visual domain (dct, cdf,...).
            % ---
            
            % Depending on the desired visualisation (matrixDATA vs CoeffDATA) a specific prosessing
            % pipeline is followed.
            switch lower(par.data)
                case {'coefficients', 'coefficientdata', 'abscoefficients', 'abscoefficientdata' , 'spatial', 'spatialdata'}
                    % Extract visualisation data.
                    coeffDATA           = full(ReD.getSpatialDATA(par.meta.step)) ;
                    
                    % Adapt parameters to specific ouput.
                    par.set('scale', 4) ;
                    height              = ReD.colMASK.height ;
                    width               = ReD.colMASK.width ;
                    pixelHeight        	= ReD.colMASK.pixelHeight ;
                    pixelWidth         	= ReD.colMASK.pixelWidth  ;
                    switch lower(par.data) % Type Specific settings.
                        case {'coefficients', 'coefficientdata', 'spatial'}
                            par.set('minimum', min(coeffDATA(:))) ;
                            par.set('maximum', max(coeffDATA(:))) ;
                            inputDATA           = ReD.getSpatialMATRIX(par.meta.step) ;
                            
                            % Remeber which type of data is visualised.
                            outputINFO       	= ['coefficientDATA.',par.meta.step] ;
                        otherwise
                            par.set('minimum', 0) ;
                            par.set('maximum', max(abs(coeffDATA(:)))) ;
                            inputDATA           = zeros(ReD.colMASK.size) ;
                            inputDATA(ReD.colMASK.maskDATA)    = abs(coeffDATA) ;
                            
                            % Remeber which type of data is visualised.
                            outputINFO       	= ['absCoefficientDATA.',par.meta.step] ;
                    end
                    
                    % Transform the inputDATA into the required visualisation domain.
                    xd = par.meta.xd ; yd = par.meta.yd ; w = par.meta.w ;
                    switch lower(par.meta.space)
                        case {'cdf'}
                            inputDATA	= regular_97_c(inputDATA, xd, yd, w) ;
                        case {'icdf'}
                            inputDATA	= regular_97_c(inputDATA, -xd, -yd , w) ;
                        case {'bdct'}
                            inputDATA   = s2_dct(inputDATA, xd, yd) ;
                        case {'ibdct'}
                            inputDATA   = s2_idct(inputDATA, xd, yd) ;
                        case {'id'}
                            % DO NOTHING
                        otherwise
                            errorStruct.message = ['Parameter Space "', num2str(par.meta.space),'" is not recognised.'];
                            errorStruct.identifier = 'ReconstructionData:imageDATA:UnrecognisedSpace';
                            error(errorStruct) ;
                    end
                    
                case {'measurements', 'samples', 'signal', 'signaldata', 'reconstruction', 'reconstructiondata'}
                    
                    height              = ReD.rowMASK.height ;
                    width               = ReD.rowMASK.width ;
                    pixelHeight        	= ReD.rowMASK.pixelHeight ;
                    pixelWidth         	= ReD.rowMASK.pixelWidth  ;
                    switch lower(par.data) % Type Specific settings.
                        case {'measurements', 'samples'}
                            inputDATA          = ReD.getMeasurementMATRIX(par.meta.step) ;
                            %
                            % Remeber which type of data is visualised.
                            outputINFO       	= 'measurements' ;
                        otherwise
                            inputDATA           = ReD.getMatrixDATA(par.meta.step) ;
                            %                             inputDATA           = reshape(inputDATA, ReD.size) ;
                            
                            % Remeber which type of data is visualised.
                            outputINFO       	= 'signalDATA' ;
                    end
                    
                    % Transform the inputDATA into the required visualisation domain.
                    xd = par.meta.xd ; yd = par.meta.yd ; w = par.meta.w ;
                    switch lower(par.meta.space)
                        case {'cdf'}
                            inputDATA	= regular_97_c(inputDATA, xd, yd , w) ;
                        case {'icdf'}
                            inputDATA	= regular_97_c(inputDATA, -xd, -yd, w) ;
                        case {'bdct'}
                            inputDATA   = s2_dct(inputDATA, x, y) ;
                        case {'ibdct'}
                            inputDATA   = s2_idct(inputDATA, x, y) ;
                        case {'id'}
                            % DO NOTHING
                        otherwise
                            errorStruct.message = ['Parameter Space "', num2str(par.meta.space),'" is not recognised.'];
                            errorStruct.identifier = 'ReconstructionData:imageDATA:UnrecognisedSpace';
                            error(errorStruct) ;
                    end
                    %                     par.set('minimum', min(inputDATA(:))) ; par.set('maximum', max(inputDATA(:))) ;
                    
                    % Adapt imaging setup according to current state of signal.
                    if ReD.matrixINFO.isDecibel
                        par.set('dynamic', DynamicSetting.none) ;
                    elseif ReD.matrixINFO.isNormalized
                        par.set('dynamic', DynamicSetting.decibel) ;
                        par.set('minimum', -.5) ;
                        par.set('maximum', .5) ;
                    else
                        par.set('dynamic', DynamicSetting.decibel) ;
                        par.set('minimum', ReD.minimum) ;
                        par.set('maximum', ReD.maximum) ;
                    end
                otherwise
                    errorStruct.message = ['Parameter Data "', num2str(par.meta.space),'" is not recognised.'];
                    errorStruct.identifier = 'ReconstructionData:imageDATA:UnrecognisedSpace';
                    error(errorStruct) ;
            end
            
            outputDATA  = imresize(full(inputDATA), [height (width * pixelWidth / pixelHeight)] * par.scale, 'nearest') ;
            
        end
        % Validation Methods.
        function value = PSNR(RfD, cmpr, ref)
            if nargin < 3
                ref = 'reference';
            end
            if nargin < 2
                cmpr = 'result';
            end
            refDATA     = RfD.signalDATA.(cmpr) ;
            RfDDATA     = RfD.signalDATA.(ref) ;
            value       = psnr(RfDDATA, refDATA) ;
        end
        
        %% VALIDATION METHODS
        
        function value = Test(Red, vararagin)
            value = 1 ;
        end
    end
end
