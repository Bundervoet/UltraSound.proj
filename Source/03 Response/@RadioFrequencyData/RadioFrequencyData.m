% RADIOFREQUENCYDATA

% This is the central class used in this project containing the recorded acoustic response when an
% UltrasSoundTransducer exites the scatterPoints in a Phantom. The information-properties here are
% crucial when truthely visualizing this data using imageNow. 
% 1) Multiple types of RFdata exist including:
%       - Energy Maps (EDline)  : containing the total received amount of energy from an UStransducer by scatterPoints contained in a GridPhantom.
%       - RFlines               : The response received by a UStransducer after exciting a portion of a Phantom.
%       - RFpoints              : The responce received by a UStransducer after exciting one scatterPoint within a Phantom.
%       - Batches of RFlines/RFpoints ;
% 2) Each type also comes in multiple states:
%       - Normalised            : -0.5 < pixelValues < 0.5 
%       - Quantized             : class(pixelValues) = Integer
%       - Decibel               : 0 < 256/A + 20*log10((-0.5 < pixelValues < 0.5) + A) < 256 ;

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 31 AUG 16.

classdef RadioFrequencyData < DataMatrix
    
    %% PROPERTIES
    properties % DATA PROPERTIES
        signalDATA                          = [] ;                         % Recorded acoustic response.
    end
    properties % EXCITATION PROPERTIES
        lineWidth@double                    = [] ;                         % Width of area excited to create RFline.
        activeFocus@ThreeDimensionalPoint   = ThreeDimensionalPoint(0,0,0) ; % Location of focus relative to the activation window.
        transducerElements@struct           = struct('noElements', 0, 'noTransmit', 0, 'noReceive', 0) ;
    end
    
    %% METHODS
    methods

        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of RadioFrequencyData and 
        % accessing / changing the state of its properties.
        % ===
        function RfD = RadioFrequencyData(name, varargin)
            % ---
            % Set-up is completely facilitated by the upperclass DataType which internally uses this
            % class' version of setOne to acces and alter internal properties.
            % ---
            RfD@DataMatrix(name, varargin{:}) ;
        end
        
        % ===
        % Specialised version of variable handling functions.
        % ===
        function setOne(RfD, input, value)
            % ---
            % Similar to setOne in other classes this method provides functionality for addapting a
            % variable in RadioFrequencyData. Take special care in adapting non-double properties.
            % ---
            switch lower(input)
                case {'signaldata'}
                    RfD.setMatrixDATA(value) ;           
                case {'batchelement'}
                    RfD.signalDATA{end+1} = value ;
                case 'firstpixel'
                    RfD.matrixINFO.setLowerBound('row', value) ;
                case 'lastpixel'
                    RfD.matrixINFO.setUpperBound('row', value) ;
                    case 'noelements'
                    RfD.transducerElements.noElements = value;
                case 'notransmit'
                    RfD.transducerElements.noTransmit = value;
                case 'noreceive'
                    RfD.transducerElements.noReceive = value;
                case 'elementpitch'
                    RfD.pixelSize(2) = value;
                otherwise
                    setDATA(RfD, input, value) ;
            end
        end
        function output = getOne(RfD, input)
            switch lower(input)
                case {'signaldata'}
                    output = RfD.getMatrixDATA ;
                case 'firstpixel'
                    output = RfD.matrixINFO.getBounds('row',1) ;
                case 'lastpixel'
                    output = RfD.matrixINFO.getBounds('row',2) ;
                case 'relativefocus'
                    output = RfD.relativeFocus ;
                case 'noelements'
                    output = double(RfD.transducerElements.noElements) ;
                case 'notransmit'
                    output = double(RfD.transducerElements.noTransmit) ;
                case 'noreceive'
                    output = double(RfD.transducerElements.noReceive) ;
                case 'elementpitch'
                    output = RfD.pixelSize(2) ;
                otherwise
                    output = getDATA(RfD, input) ;
            end
        end
        function output = type(RfD)
            output = 'RFdata' ;
        end

        %% DATA METHODS.
        % ===
        % Pseudo-property methods allowing for easy and generic access (and adaptation) of the 
        % central data property signalDATA.
        % ===
        function output = getMatrixDATA(RfD)
            if RfD.isBatch
                if isa(RfD.signalDATA{end}, 'struct')
                    output = full(RfD.signalDATA{end}.result) ;
                else
                    output = full(RfD.signalDATA{end}) ;
                end
            elseif isa(RfD.signalDATA, 'struct')
                output = full(RfD.signalDATA.result) ;
            else
                output = full(RfD.signalDATA) ;
            end
        end
        function setMatrixDATA(RfD, input)
            if RfD.isBatch
                if isa(RfD.signalDATA{end}, 'struct')
                    RfD.signalDATA{end}.result = input ;
                else
                    RfD.signalDATA{end} = input ;
                end
            elseif isa(RfD.signalDATA, 'struct')
                RfD.signalDATA.result = input ;
            else
                RfD.signalDATA	= input ;
            end
        end
        
        
        %% VALUE METHODS.
        function output = globalMax(RfD)
            if RfD.isBatch
                currentOutput = 0 ;
                for jj = 1:RfD.batchSize
                    currentOutput = max([currentOutput, max(reshape(RfD.signalDATA{jj}, 1, []))]) ;
                end
                output = full(double(currentOutput)) ;
            else
                output = RfD.max ;
            end
        end
        function output = globalMaximum(RfD)
            if RfD.isBatch
                currentOutput = 0 ;
                for jj = 1:RfD.batchSize
                    currentOutput = max([currentOutput, max(reshape(RfD.signalDATA{jj}, 1, []))]) ;
                end
                output = full(double(currentOutput)) ;
            else
                output = RfD.maximum ;
            end
        end
        function output = globalAbsMaximum(RfD)
            if RfD.isBatch
                currentOutput = 0 ;
                for jj = 1:RfD.batchSize
                    currentOutput = max([currentOutput, max(abs(reshape(RfD.signalDATA{jj}, 1, [])))]) ;
                end
                output = full(double(currentOutput)) ;
            else
                output = RfD.absMaximum ;
            end
        end
        function output = globalMin(RfD)
            if RfD.isBatch
                currentOutput = 0 ;
                for jj = 1:RfD.batchSize
                    currentOutput = min([currentOutput, min(reshape(RfD.signalDATA{jj}, 1, []))]) ;
                end
                output = full(double(currentOutput)) ;
            else
                output = RfD.min ;
            end
        end
        function output = globalMinimum(RfD)
            if RfD.isBatch
                currentOutput = 0 ;
                for jj = 1:RfD.batchSize
                    currentOutput = min([currentOutput, min(reshape(RfD.signalDATA{jj}, 1, []))]) ;
                end
                output = full(double(currentOutput)) ;
            else
                output = RfD.minimum ;
            end
        end
        function output = globalNorm(RfD, type)
            if nargin < 2
                type = 2 ;
            end
            if RfD.isBatch
                currentOutput = 0 ;
                for jj = 1:RfD.batchSize
                    if type == 1
                        currentOutput = abs(currentOutput + norm(full(reshape(RfD.signalDATA{jj},1,[])), type)) ;
                    elseif type == 2
                        currentOutput = sqrt(currentOutput^2 + norm(full(reshape(RfD.signalDATA{jj},1,[])), type)^2) ;
                    else
                        currentOutput = max([currentOutput, norm(full(reshape(RfD.signalDATA{jj},1,[])), type)]) ;
                    end
                end
                output = full(double(currentOutput)) ;
            else
                output = RfD.norm(type) ;
            end
        end
        function output = relativeFocus(RfD)
            % Calculate relative position of FocusPoint.
            xSteps      = RfD.activeFocus.xCoordinate / RfD.pixelSize(2) ;
            xRelative   = (xSteps - round(xSteps)) * RfD.pixelSize(2) ;
            output      = ThreeDimensionalPoint(xRelative, RfD.activeFocus.yCoordinate, RfD.activeFocus.zCoordinate) ;
        end
        
        
        %% FORMAT METHODS.
        % ===
        % Like general DataMatrices, RadiofrequencyData multiple operations can be applied to the
        % matrixDATA and some can be detected using the statatics of the data. This is usefull when
        % automatically asigninging a visualisation technique.
        % ===
        function answer = isBeamformed(RfD)
            % ---
            % Checks wether the data has been beamformed. This is usually the case if matrixDATA
            % only contains one column.
            % ---
            if RfD.width == 1
                answer = true ;
            else
                answer = false ;
            end
        end
        function output = isBatch(RfD)
            output = isa(RfD.signalDATA, 'cell') ;
        end
        function output = batchSize(RfD)
            if RfD.isBatch
                output = numel(RfD.signalDATA) ;
            else
                output = 1 ;
            end
        end
        function output = extract(RfD, name, batchElement)
            % ---
            % if the matrixDATA consists of a batch of channel RFdata then this method allows the
            % user the extract a specific signalDATA element from the batch. This is mainly used
            % when constructing a dictionary from a batched DataMatrix.
            % ---
            if nargin < 3
                batchElement = 1 ;
            end
            if nargin < 2
                name = RfD.name ;
            elseif isempty(name)
                name = RfD.name ;
            end
            fieldVar = features(RfD) ;
            if RfD.isBatch
                output              = RadioFrequencyData(name, fieldVar{3:end}) ;
                output.signalDATA   = RfD.signalDATA{batchElement} ;
            else
                output	= [fieldVar, {'name',name}] ;
            end
        end              

        
        %% IMAGING METHODS
        % ===
        % Internal object-specific method used by imageNow in order to collect and process imaging
        % data according to a tunable set of paramaters. 
        % ===
        function [outputDATA, outputINFO, par] = imageDATA(RfD, par)
            % ---
            % This method collects the required data for imaging and processes it such that for
            % example the correct distance measurements are displayed in the figure window. 
            % Alternatively the processing helps to preserver the correct aspect ratios and
            % transforms the data to the correct visual domain (dct, cdf,...).
            % ---
            if RfD.isBatch
                outputDATA  = imresize(full(RfD.signalDATA{1}), [RfD.height (RfD.width * RfD.pixelWidth / RfD.pixelHeight)] * par.scale, 'nearest') ;
                outputINFO  = 'signalDATA' ;
            else
                outputDATA  = imresize(full(RfD.signalDATA), [RfD.height (RfD.width * RfD.pixelWidth / RfD.pixelHeight)] * par.scale, 'nearest') ;
                outputINFO  = 'signalDATA' ;
            end
%             par.minimum = min(outputDATA(:)) ; par.maximum = max(outputDATA(:)) ;
            
            if RfD.matrixINFO.isDecibel 
                par.set('dynamic', DynamicSetting.none) ;
            elseif RfD.matrixINFO.isNormalized 
                par.set('dynamic', DynamicSetting.decibel) ; 
                par.set('minimum', -.5) ;
                par.set('maximum', .5) ; 
            elseif RfD.matrixINFO.isAbsolute
                par.set('dynamic', DynamicSetting.decibel) ; 
                par.set('minimum', 0) ;
                par.set('maximum', RfD.maximum) ;
            else
                par.set('dynamic', DynamicSetting.decibel) ; 
                par.set('minimum', RfD.minimum) ;
                par.set('maximum', RfD.maximum) ; 
            end
        end
        function plotNow(RfD, h, type, lines)
            % ---
            % This methods is used to plot multiple channels of the channel RF data (matrixDATA). 
            % ---
            if nargin < 2
                h = 1 ;
            end
            if nargin < 3
                type = 'rflines' ;
            end
            if nargin < 4
                lines = 8:24 ;
            end
            [N, ~]   =   size(RfD.signalDATA) ;
            %
            figure(h)
            switch lower(type)
                case {'rflines', 'lines'}
                    for jj = 1 : length(lines)
                        % plot((0:N-1)/UStransducer.fs, RfD.signalDATA(:,lines(jj)) + jj * max(RfD.signalDATA(:)), 'Color', RfD.VUBcolors('grijs'), 'LineWidth', 1) ;
                        plot((0:N-1)/RfD.pixelHeight, RfD.signalDATA(:,lines(jj)) + jj * max(RfD.signalDATA(:)), 'Color', RfD.VUBcolors('grijs'), 'LineWidth', 1) ;
                        hold on
                    end
                case {'beamform', 'beamformed'}
                    for jj = 1 : length(lines)
                        plot((0:N-1)/RfD.pixelHeight, sum(RfD.signalDATA,2), 'Color', RfD.VUBcolors('bruin'), 'LineWidth', 1) ;
                        hold on
                        plot((0:N-1)/RfD.pixelHeight, abs(hilbert(sum(RfD.signalDATA,2))), 'Color', RfD.VUBcolors('groen'), 'LineWidth', 1) ;
                    end
            end
            axis off
        end
              
        
        %% CUTOFF METHODS.
        % === 
        % This section mostly contains auxilliary methods at some times used for small tasks during 
        % the project. 
        % ===
        function horizontalCutOff(RfD, topPixel, bottomPixel)
            if nargin < 2
                rowSum          = sum(abs(RfD.signalDATA),2) ;
                nonZeroRows     = find(rowSum > 0) ;
                firstNonZeroRow = nonZeroRows(1) ;
                lastNonZeroRow  = nonZeroRows(end) ;
                topPixel        = RfD.firstPixel + firstNonZeroRow - 1 ;
                bottomPixel     = RfD.firstPixel + lastNonZeroRow - 1 ;
            end
            topCutOfPixel       = topPixel - RfD.firstPixel + 1 ;
            bottomCutOfPixel    = bottomPixel - RfD.firstPixel +1 ;
            RfD.firstPixel     = topPixel ;
            RfD.lastPixel      = bottomPixel ;
            RfD.signalDATA     = RfD.signalDATA(topCutOfPixel:bottomCutOfPixel,:) ;
        end
        function horizontalRepair(RfD, topPixel, bottomPixel)
            topDifference       = RfD.firstPixel - topPixel ;
            bottomDifference    = bottomPixel - RfD.lastPixel ;
            RfD.firstPixel     = topPixel ;
            RfD.lastPixel      = bottomPixel ;
            width               = size(RfD.signalDATA, 2) ;
            RfD.signalDATA     = [zeros(topDifference, width); RfD.signalDATA; zeros(bottomDifference, width)] ;
        end               
        function scatterer = scatterPoint(RfD)
            scatterer = RfD.version.setupDetails.coordinate.metric ;
            %             if exist(RfD.version.setupDetails.coordinate.metric) %#ok<EXIST>
            %                 scatterer = RfD.version.setupDetails.coordinate.metric ;
            %             else
            %                 scatterer = ScatterPoint(0,0,0,0) ;
            %             end
        end
    end
end

