function  beamform(RfD, visualizer)

if nargin < 2
    visualizer = struct('beamformer', BeamformSetting.focusing, 'apodizer', ApodizationSetting.hanning, 'filter', false) ;
end

inputDATA       = RfD.get('matrixDATA') ;
pixelSize       = RfD.get('pixelSize') ;
height          = RfD.get('height') ;
width           = RfD.get('width') ;
firstPixel      = RfD.get('firstPixel') ;
relativeFocus   = RfD.get('relativeFocus') ;

switch visualizer.beamformer
    case BeamformSetting.center
        % Apodize RFline using the apodisation function. Note this is only usefull
        % when the RFline consist of Channel RFdata.
%         if visualizer.apodisation
%             RfD.apodise() ;
%         end
        if visualizer.apodizer.useApodization && ~RfD.matrixINFO.isApodized
            RfD.apodize(char(visualizer.apodizer)) ;
        end
        
        % Only use the central line from the Channel RFdata when "beamforming". No
        % averaging is done and only the recieved signal from one elements is
        % trasferred on to the image processor.
        inputDATA   = inputDATA(:,floor(width/2)) ;
        
        % Determine the low-frequency enveloppe signal of the high-frequency
        % beamformed RFline. This high-frequency in the Rfline originates from the
        % excitation pulse used by the transducer but contains no imporatant visual
        % information about the underlying phantom tissue.
        inputDATA  = abs(hilbert(inputDATA)) ;
        
    case BeamformSetting.summation
        
        if RfD.debug && true
            fprintf('Running summation beamformsetting \n') ;      
        end
        
        % Apodize RFline using the apodisation function. Note this is only usefull
        % when the RFline consist of Channel RFdata.
        if visualizer.apodizer.useApodization && ~RfD.matrixINFO.isApodized
            RfD.apodize(char(visualizer.apodizer)) ;
        end
        
        
        % Average the information received by all active elements to increase the
        % Signal to Noise Ratio (SNR).
        inputDATA = sum(inputDATA,2) ;
        
        % Determine the low-frequency enveloppe signal of the high-frequency
        % beamformed RFline. This high-frequency in the Rfline originates from the
        % excitation pulse used by the transducer but contains no imporatant visual
        % information about the underlying phantom tissue.
        %         RFdata.signalDATA = abs(hilbert(RFdata.signalDATA));
        
    case BeamformSetting.focusing
        
        %% PRE 170314 CODE
%                 % Distance between two neighboring centers of elements on the probe.
%         elementPitch        = pixelSize(2) ;
%         
%         % Full probe width.
%         apertureWidth       = (width - 1) * elementPitch ;                                          % Width between center of first and last element on the probe.
%         %         probeWidth = transducer.noElements * elementPitch ;                               % Extended width between outer edges of the probe + kerf.
%         
%         % Window width.
%         %         windowWidth = transducer.noElements * elementPitch - transducer.elementKerf ;     % Full width between outer edges of the probe.
%         
%         % Location of probe elements.
%         xp = linspace(-apertureWidth/2, apertureWidth/2, width) ;                                   % Centers of transducer elements.
        
        %% POST 170314 CODE
        % X - Coordinate of active focus point.
        x = RfD.activeFocus.xCoordinate ;
        
        % Find the amount of unused elements left and right of the receive window.
        [noElementsLeftOfWindow, ~] = RfD.activeWindow(x, 'receive', width)  ;
        
        % Distance between two neighboring centers of elements on the probe.
        elementPitch    = RfD.get('elementPitch') ;
        
        % Full probe width.
        probeWidth      = (RfD.get('noElements') - 1) * elementPitch ;
        fullProbe       = linspace(-probeWidth/2, probeWidth/2, RfD.get('noElements')) ;
        
        xp =  fullProbe((1:width) + noElementsLeftOfWindow) ;
        
        if RfD.debug && true
            disp(['xp [',num2str(xp),']']) ;        
        end
        %         % First active element.
        %         [noElementsLeftOfReceiveWindow, ~] = activeWindow(excitationProtocol, transducer, x , 'receive') ;
        %         % Center of active elements.
        %         centerFirst = xp(noElementsLeftOfReceiveWindow + 1) ; centerLast = xp(noElementsLeftOfReceiveWindow + excitationProtocol.noReceive) ;
        %         %
        %         % x-coordinates of active elements.
        %         xl = linspace(centerFirst, centerLast, excitationProtocol.noReceive) ;
        
        % Initial depths of raw rf data.
        z = ((0:height-1) + firstPixel) * pixelSize(1) ;
        
        % Initial lateral and axial points of RFdata after meshgrid.
        [xi,zi] = meshgrid(xp,z) ;
        
        % Depths where to interpolate to do the focusing.
        zf = zi + (-relativeFocus.z + sqrt((xi - relativeFocus.x).^2 + relativeFocus.z.^2))/2 ;
        
        % Visualize transformation grid (DEBUG)
        if RfD.debug && true
            fprintf('xi [%d,%d] \n',size(xi,1),size(xi,2)) ;
            fprintf('zi [%d,%d] \n',size(zi,1),size(zi,2)) ;
            fprintf('zf [%d,%d] \n',size(zf,1),size(zf,2)) ;
            figure(1)
            hold on
            scatter(reshape(xi, [],1), reshape(zi, [], 1), 1, 'red')
            scatter(reshape(xi, [],1), reshape(zf, [], 1), 1, 'blue')
        end
        
        % Interpolation = Focusing.
        inputDATA = interp2(xi, zi, inputDATA, xi, zf, 'nearest') ;
        
        %         rfline = signal_focused ;
        %                         cmd    = ['save foc_lines/line',num2str(line),'.mat rfline'] ; eval(cmd) ;
        
        % Apodize RFline using the apodisation function. Note this is only usefull
        % when the RFline consist of Channel RFdata.
        
        if visualizer.apodizer.useApodization && ~RfD.matrixINFO.isApodized
            RfD.apodize(char(visualizer.apodizer)) ;
        end
        
        % Remove NAN.
        inputDATA(isnan(inputDATA)) = 0 ;
        
        % k = round(indexProtocol.lineStatus.current * 97/192) ;
        
        if visualizer.filter
            RfD.signalDATA = gaussianFilter(sum(RfD.signalDATA,2), excitationProtocol.fs, excitationProtocol.f0, 100, 0) ;
        else
            inputDATA = sum(inputDATA,2) ;
        end
        
    case BeamformSetting.none
        % DO NOTHING
    otherwise
        errorStruct.message = ['Beamformer"', num2str(visualizer.beamform),'" not recognised.'];
        errorStruct.identifier = 'BrightnessModeImage:b:BeamformerNotRecognised';
        error(errorStruct) ;
end
%
RfD.set('matrixDATA', inputDATA) ;
%
end
