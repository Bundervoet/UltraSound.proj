function beamformBatch(RFdata, imager, ii)
%
inputDATA = RFdata.signalDATA{ii} ;
%
switch imager.beamformer
    case 'center'
        % Apodize RFline using the apodisation function. Note this is only usefull
        % when the RFline consist of Channel RFdata.
        if imager.apodisation
            RFdata.apodise ;
        end
        %
        % Only use the central line from the Channel RFdata when "beamforming". No
        % averaging is done and only the recieved signal from one elements is
        % trasferred on to the image processor.
        inputDATA   = inputDATA(:,floor(RFdata.width/2)) ;
        %
        % Determine the low-frequency enveloppe signal of the high-frequency
        % beamformed RFline. This high-frequency in the Rfline originates from the
        % excitation pulse used by the transducer but contains no imporatant visual
        % information about the underlying phantom tissue.
        inputDATA  = abs(hilbert(inputDATA)) ;
        %
    case 'original'
        %
        % Apodize RFline using the apodisation function. Note this is only usefull
        % when the RFline consist of Channel RFdata.
        if imager.apodisation
            RFdata.apodise ;
        end
        %
        % Average the information received by all active elements to increase the
        % Signal to Noise Ratio (SNR).
        inputDATA = sum(inputDATA,2) ;
        %
        % Determine the low-frequency enveloppe signal of the high-frequency
        % beamformed RFline. This high-frequency in the Rfline originates from the
        % excitation pulse used by the transducer but contains no imporatant visual
        % information about the underlying phantom tissue.
        %         RFdata.signalDATA = abs(hilbert(RFdata.signalDATA));
        %
    case 'special'
        %
        % Distance between two neighboring centers of elements on the probe.
        elementPitch    = RFdata.pixelSize(2) ;
        width           = size(inputDATA, 2) ;
        height          = size(inputDATA, 1) ;
        %
        % Full probe width.
        apertureWidth      = (width - 1) * elementPitch ;                                    % Width between center of first and last element on the probe.
        %         probeWidth = transducer.noElements * elementPitch ;                               % Extended width between outer edges of the probe + kerf.
        %
        % Window width.
        %         windowWidth = transducer.noElements * elementPitch - transducer.elementKerf ;     % Full width between outer edges of the probe.
        %
        % Location of probe elements.
        xp = linspace(-apertureWidth/2, apertureWidth/2, width) ;                            % Centers of transducer elements.
        %
        %         % First active element.
        %         [noElementsLeftOfReceiveWindow, ~] = activeWindow(excitationProtocol, transducer, x , 'receive') ;
        %         % Center of active elements.
        %         centerFirst = xp(noElementsLeftOfReceiveWindow + 1) ; centerLast = xp(noElementsLeftOfReceiveWindow + excitationProtocol.noReceive) ;
        %         %
        %         % x-coordinates of active elements.
        %         xl = linspace(centerFirst, centerLast, excitationProtocol.noReceive) ;
        %
        % Initial depths of raw rf data.
        z = ((0:height-1) + RFdata.firstPixel) * RFdata.pixelSize(2) ;
        %
        % Initial lateral and axial points of RFdata after meshgrid.
        [xi,zi] = meshgrid(xp,z) ;
        %
        % Depths where to interpolate to do the focusing.
        zf = zi + (-RFdata.relativeFocus.z + sqrt((xi - RFdata.relativeFocus.x).^2 + RFdata.relativeFocus.z.^2))/2 ;
        %
        % Interpolation = Focusing.
        inputDATA = interp2(xi, zi, inputDATA, xi, zf, 'nearest') ;
        %
        %         rfline = signal_focused ;
        %                         cmd    = ['save foc_lines/line',num2str(line),'.mat rfline'] ; eval(cmd) ;
        %
        % Apodize RFline using the apodisation function. Note this is only usefull
        % when the RFline consist of Channel RFdata.
        %
        if imager.apodisation
            inputDATA = apodisation(inputDATA) ;
        end
        %
        % Remove NAN.
        inputDATA(isnan(inputDATA)) = 0 ;
        %
        % k = round(indexProtocol.lineStatus.current * 97/192) ;
        %
        inputDATA = sum(inputDATA,2) ;
    case 'none'
        % DO NOTHING
    otherwise
        errorStruct.message = ['Beamformer"', num2str(imager.beamform),'" not recognised.'];
        errorStruct.identifier = 'BrightnessModeImage:b:BeamformerNotRecognised';
        error(errorStruct) ;
end
%
RFdata.signalDATA{ii} = inputDATA ;
%
    function signalOut = apodisation(signalIn) % ( INTERNAL FUNCTION )
        %
        n           = size(signalIn,1) ;
        m           = size(signalIn,2) ;
        %
        if m > 1
            %
            % Calculate apodisation window.
            receiveWindow = transpose(hanning(m)) ;
            apoR = kron(receiveWindow, ones(n,1)) ;
            %
            % Apply apodisation to inserted RFsignal.
            signalOut = apoR .* signalIn ;
            %
        end
    end
%
end
