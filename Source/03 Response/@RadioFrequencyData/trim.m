% TRIM RFSIGNAL
%
function output = trim(RfD, input, doColumns)


%% SETUP VARIABLES

% Initialize function variables
if nargin < 3
    doColumns = true ;
end
if ~isa(input, 'double')
    input = double(input) ;
end

%% SUBSAMPLE/TRIM Lines or Columns.

if doColumns % Select Columns
    if input <= RfD.width
        
        %% PRE 170314 CODE
        %         % X-coordinate of current focus point.
        %         focusPoint      = self.relativeFocus ;
        %         x               = focusPoint.xCoordinate ;
        %
        %         % Distance between two neighboring centers of elements on the probe.
        %         elementPitch    = self.pixelSize(2) ;
        %
        %         % Full probe width.
        %         apertureWidth   = (self.width - 1) * elementPitch ;                               % Width between center of first and last element on the probe.
        %
        %         noElementsLeftOfWindow_1 = round((x + apertureWidth/2)/elementPitch - input/2) ;
        %         noElementsLeftOfWindow = min(max(noElementsLeftOfWindow_1,0), self.width - input) ;
        %         noElementsRightOfWindow = self.width - noElementsLeftOfWindow -  input ;
        %
        %         %     disp(['LEFT:  ',num2str(noElementsLeftOfWindow)]) ;
        %         %     disp(['RIGHT: ',num2str(noElementsRightOfWindow)]) ;
        %
        %         indices = (1:input) + noElementsLeftOfWindow ;
        %         self.signalDATA = self.signalDATA(:, indices) ;
        %         self.set('relativeFocus', self.relativeFocus + ThreeDimensionalPoint(...
        %             (noElementsRightOfWindow - noElementsLeftOfWindow) * elementPitch/2, 0, 0)) ;
        %         self.matrixINFO.setCropped('col', indices(1), indices(end)) ;
        
        %% POST 170314 CODE
        
        % X - Coordinate of active focus point.
        x = RfD.activeFocus.xCoordinate ;
        
        % Find the amount of unused elements left and right of the receive window.
        [noElementsLeftOfWindow, noElementsRightOfWindow] = RfD.activeWindow(x, '', input) ;

        if RfD.debug
            disp(['LEFT:  ',num2str(noElementsLeftOfWindow)]) ;
            disp(['RIGHT: ',num2str(noElementsRightOfWindow)]) ;
        end    
        
        indices = (1:input) + noElementsLeftOfWindow ;
        RfD.signalDATA = RfD.signalDATA(:, indices) ;
        RfD.matrixINFO.setCropped('col', indices(1), indices(end)) ;
        RfD.transducerElements.noReceive = input ;
        
    else
        errorStruct.message = ['Number of Channels "', num2str(input),'" exceeds RAW data.'];
        errorStruct.identifier = 'RadioFrequencyData:trimDATA:NotEnoughChannelsAvailable';
        error(errorStruct) ;
    end
    
    output = RfD.width ;
    
else % Subsample Rows
    RfD.set('firstPixel', floor(RfD.get('firstPixel')/input)) ;
    RfD.set('lastPixel',  floor(RfD.get('lastPixel')/input)) ;
%     PS              = [self.pixelHeight*input self.pixelWidth] ;
    RfD.pixelSize  = [RfD.pixelHeight*input, RfD.pixelWidth] ;
    RfD.signalDATA = RfD.signalDATA(1:input:end,:) ;
    
    output = RfD.height ;
    
end
%
end

