function par = imager(self, varargin)
% ---
% Central imager used by all object visualizing pre-prosessed data. The main compononent include:
% - imagePARAM which reads varargin and adjust standardPARAM accordingly.
% - imageDATA which procesessed RAW input data such as matrixDATA and applies further adjustments to 
%   the output of imagePARAM. 
% - the imager utilizing imshow for on-screen visualization.
% ---

%% INITIALIZE IMAGER
% Load and addapt imaging Parameters.
par                     = ImageParameter(class(self), varargin{:}) ;

% Load correct input matrix.
[imageMatrix, ~, par]   = self.imageDATA(par) ;

%% IMSHOW OF INPUT MATRIX
h = figure(par.window) ;

imshow(par.transform(imageMatrix), [0, par.contrast * 2^8], 'Border', 'tight') ;

% Adjust axis and colormap for a clean image.
colormap(par.cmap)
axis tight
axis off
set(gca, 'Units', 'normalized', 'Position', [0 0 1 1]) ;
axis('image')
drawnow 
end
