% GRIDPHANTOM

% GridPhantom when initilized, generates an 3D grid of ScatterPoints with amplitude set to 1. The
% spacing between scatterers, the number of scatterers en theire relative location can be set in all
% direction x, y and z. 

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 11 AUG 16.

classdef GridPhantom < Phantom
    
    
    %% PROPERTIES
    properties % GRID specific properties.
        x@GridParameter = GridParameter() ;                                                         % Parameters for setting scatterers allong x-axis.
        y@GridParameter = GridParameter() ;                                                         % Parameters for setting scatterers allong y-axis.
        z@GridParameter = GridParameter() ;                                                         % Parameters for setting scatterers allong z-axis.
        indexation      = [] ;                                                                      % Relate Spatial Location to Index Number.
        localization    = [] ;                                                                      % Relate Scatter Index to Spatial Location.
    end
    
    
    %% METHODS
    methods

        %% GRID GENERATOR
        
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a GridPhantom and accessing
        % / changing the state of its properties. Unlike most classes setting up a GridPhantom will
        % result in the automatic creation of scatterPoints without the need of constructor object.
        % Also a summary of how the grid scatterers are ordered in the scatterDATA array is stored
        % in indexation and localization.
        % ===
        function GrP = GridPhantom(name, varargin)
            
            % Setup superclass.
            GrP@Phantom(name) ;
            
            pixelWidth  = 4.4000e-04 + 0.05/10000 ;
            pixelHeight = 1540 / (2 * 3.5e6) ;
            
            GrP.x = GridParameter(-32/2* pixelWidth, pixelWidth,  32) ;
            GrP.y = GridParameter(0                , 1/1000,      1) ;
            GrP.z = GridParameter(30/1000          , pixelHeight, floor((60/1000) / pixelHeight) + (8 - mod(floor((60/1000) / pixelHeight), 8) - 1)) ;
            
            GrP.set(varargin{:}) ;
            
            % Distance between phantom and transducer. [m]                                          % Number of coord for z-dimension. [INT]                               
            GrP.indexation     = permute(reshape(1:GrP.x.no*GrP.y.no*GrP.z.no,...
                GrP.z.no, GrP.x.no, GrP.y.no), [2 3 1]) ;                                        % Relate Spatial Location to Index Number.
            GrP.localization   = [...
                repmat(kron((1:GrP.x.no)', ones(GrP.z.no,1)),GrP.y.no,1),...
                kron((1:GrP.y.no)', ones(GrP.x.no*GrP.z.no,1)), ...
                repmat((1:GrP.z.no)', GrP.x.no*GrP.y.no,1)] ;                                     % Relate Scatter Index to Spatial Location.
            
            % Coordinate positions are determined by a starting point for all three dimensions followed by a
            % jump size and the number of jumps to take.
            xPos = GrP.x.start: GrP.x.jump: GrP.x.start + (GrP.x.no - 1) * GrP.x.jump ;        % X - Coordinate positions. [m]
            yPos = GrP.y.start: GrP.y.jump: GrP.y.start + (GrP.y.no - 1) * GrP.y.jump ;        % Y - Coordinate positions. [m]
            zPos = GrP.z.start: GrP.z.jump: GrP.z.start + (GrP.z.no - 1) * GrP.z.jump ;        % Z - Coordinate positions. [m]

            % The coordinates from the three dimensions are "mixed" into a full grid.
            %             [x, y, z] = meshgrid(x_pos,y_pos,z_pos) ;                                 % Full grid posistions.
            %             [z, x, y] = meshgrid(z_pos, x_pos, y_pos) ;                               % Full grid posistions.
            [xCoord, zCoord ,yCoord] = meshgrid(xPos, zPos, yPos) ;                                 % Full grid posistions.

            % the output given by meshgrid has to be written as a list to ease further use.
            scatterers = [reshape(xCoord,[],1), reshape(yCoord,[],1), reshape(zCoord,[],1)] ;

            % Set amplitudes to 1 for each posistion on the grid.
            amplitudes = ones(size(scatterers,1),1) ;                                               % Uniform scatterers.

            % Save Scatterers.
            GrP.coordinateToScatterPoint(scatterers(:,1), scatterers(:,2), scatterers(:,3), amplitudes) ;
        end
        
        % Specialised version of variable handling functions.
        function setOne(GrP, input, value)
            switch lower(input)
                case 'zdistance'
                    GrP.confinementBox.zDistance(value) ;
                case {'xzize', 'yzize', 'zzize'}
                    GrP.confinementBox.(input) = value ;
                case 'nox'
                    GrP.x.setPar('no', value) ;
                case 'xstart'
                    GrP.x.setPar('start', value) ;
                case 'dx'
                    GrP.x.setPar('jump', value) ;
                case 'noy'
                    GrP.y.setPar('no', value) ;
                case 'ystart'
                    GrP.y.setPar('start', value) ;
                case 'dy'
                    GrP.y.setPar('jump', value) ;
                case 'noz'
                    GrP.z.setPar('no', value) ;
                case 'zstart'
                    GrP.z.setPar('start', value) ;
                case 'dz'
                    GrP.z.setPar('jump', value) ;
                otherwise
                    if sum(strcmp(properties(GrP), input))
                        GrP.(input) = value ;
                    else
                        GrP.meta.(input) = value ;
                    end
            end
        end
        function output = getOne(GrP, input)
            switch lower(input)
                case {'noscatterers'}
                    output = GrP.noScatterers() ;
                case {'zdistance'}
                    output = GrP.confinementBox.zDistance() ;
                case {'xsize', 'ysize', 'zsize'}
                    output = GrP.confinementBox.(input) ;
                case 'nox'
                    output = GrP.x.no() ;
                case 'xstart'
                    output = GrP.x.start() ;
                case 'dx'
                    output = GrP.x.jump() ;
                case 'noy'
                    output = GrP.y.no() ;
                case 'ystart'
                    output = GrP.y.start() ;
                case 'dy'
                    output = GrP.y.jump() ;
                case 'noz'
                    output = GrP.z.no() ;
                case 'zstart'
                    output = GrP.z.start() ;
                case 'dz'
                    output = GrP.z.jump() ;
                otherwise
                    if sum(strcmp(properties(GrP), input))
                        output = GrP.(input) ;
                    elseif isfield(GrP.meta, input)
                        output = GrP.meta.(input) ;
                    else
                        errorStruct.message = ['Input argument "', num2str(input),'" is non existent variable.'];
                        errorStruct.identifier = 'Phantom:getVarInternal:NonExistentVariable';
                        error(errorStruct) ;
                    end
            end
        end

        %% SIZE METHODS.
        function output = dimensions(GrP, dim)
            % Number of components for each coordinate.
            output = size(GrP.indexation) ;
            if nargin > 1
                output = output(dim) ;
            end
        end
        function output = rearrangement2D(GrP)
            output = [GrP.dimensions(3) GrP.dimensions(1)] ;
        end
        function output = rearrangement3D(GrP)
            output = [GrP.dimensions(3) GrP.dimensions(1) GrP.dimensions(2)] ;
        end
        function output = pixelSize(GrP)
            output = [GrP.get('dz') GrP.get('dx') GrP.get('dy')] ; 
        end
        function output = index(GrP, varargin)
            if nargin == 2 && numel(varargin{1}) == 3
                output = GrP.indexation(varargin{1}(1), varargin{1}(2), varargin{1}(3)) ;
            elseif nargin == 2 && isa(varargin{1}, 'ThreeDimensionalPoint') 
                 output = GrP.indexation(varargin{1}.x, varargin{1}.y, varargin{1}.z) ;   
            elseif nargin == 4
                output = GrP.indexation(varargin{1}, varargin{2}, varargin{3}) ;
            else
                errorStruct.message = 'Input argument mismatch';
                errorStruct.identifier = 'Phantom:indexl:IncorrectInput';
                error(errorStruct) ;
            end
        end
        function output = location(GrP, input)
            locTEMP = GrP.localization(input,:) ;
            output = ThreeDimensionalPoint(locTEMP(1), locTEMP(2), locTEMP(3)) ;
        end
        function output = digits(GrP, input)
            output = struct(...
                'x',['%0',num2str(numel(num2str(GrP.dimensions(1)))),'d'], ...
                'y',['%0',num2str(numel(num2str(GrP.dimensions(2)))),'d'], ...
                'z',['%0',num2str(numel(num2str(GrP.dimensions(3)))),'d'],...
                'i',['%0',num2str(numel(num2str(GrP.noScatterers))),'d']) ;
            if nargin == 2
                output = output.(input) ;
            end
        end
        function shift(GrP, UsT, line)
            [scatterers, amplitudes] = GrP.scatterPointToCoordinate ;
            focusPoint  = UsT.activeFocusPoint(line) ;
            xLoc        = focusPoint.xCoordinate ;
            
            % Distance between two neighboring centers of elements on the probe.
            elementPitch    = UsT.get('elementPitch') ;                    
            xShift = round(xLoc / elementPitch) * elementPitch  ;
            scatterers(:,1) = scatterers(:,1) + repmat(xShift, size(scatterers,1), 1) ;
            
            % Set shifted scatterPoints.
            GrP.coordinateToScatterPoint(scatterers(:,1), scatterers(:,2), scatterers(:,3), amplitudes) ;
        end
        
        
        
        function [outputDATA, outputINFO,  par] = imageDATA(GrP, par)
            % Imaging variables. From these variables the correct distance measurements
            % are displayed in the figure window. Moreover they help to preserver the
            % correct aspect ratios.
            switch lower(par.data)
                case {'scatterers'}
                    [A, B]          = GrP.scatterPointToCoordinate ;
                    B = par.dynamic.transform(B, min(B(:)), max(B(:))) ;
                    B = min(max(B, par.minimum), par.maximum) ;
                    if ~isempty(par.density)
                        C               = reshape(par.density,[],1) ;
                        C = par.dynamic.transform(C, min(C(:)), max(C(:))) ;
                        C = min(max(C, par.minimum), par.maximum) ;
                    end
                    inputMatrix     = [A, B, C] ;
                    outputDATA     = inputMatrix(par.mask(:),:) ;
                    outputINFO      = 'scattereres' ;
                case {'slicedata', '2dslice', '2dslicedata'}
                    [~, B]      = GrP.scatterPointToCoordinate ;
                    dims        = GrP.rearrangement2D ;
                    inputDATA   = reshape(B, dims) ;
                    outputDATA  = imresize(inputDATA, [dims(1) (dims(2) * GrP.x.jump / GrP.z.jump)] * par.scale, 'nearest') ;
                    outputINFO  = 'sliceDATA' ;
                otherwise
                    errorStruct.message     = ['Input Property "', lower(par.data) ,'" not recognised.'];
                    errorStruct.identifier  = 'Cyst2DGridPhantom:imageDATA:InputNotRecognised';
                    error(errorStruct) ;
            end
            %             outputDATA    = inputMatrix ; % imresize(inputMatrix, [dataSize(1) (dataSize(2) * pixelWidth / pixelHeight)] * par.scaling, 'nearest') ;
        end
        
        
    end
end