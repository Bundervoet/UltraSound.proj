% POINTPHANTOM
%
% Simple class wrapping a 3D scatterpoint within a Phantom for increased functionality and use by
% other methods expection a Phantom as input.
%
% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 2 DEC 15.
%
classdef PointPhantom < Phantom
    %
    %% METHODS
    %
    methods
        %
        %% DEFINITION AND VARIABLE METHODS.
        %
        % ===
        % This section contains methods pertaining to the construction of a PointPhantom and accessing
        % / changing the state of its properties. PointPhantom allows the user to setup the
        % enclosed 3D scatterpoint as well as the imaginary box inwhich it is contained.
        % ===
        %
        function self = PointPhantom(name, x, y, z, a, varargin)
            %
            xSize = 50/1000 ; ySize = 60/1000 ; zSize = 60/1000 ; zDistance = 30/1000 ;
            for jj = 1:2:length(varargin)
                switch varargin{jj}
                    case {'x_size','xSize','x'}
                        xSize           = varargin{jj+1} ;
                    case {'y_size','ySize','y'}
                        ySize           = varargin{jj+1} ;
                    case {'z_size','zSize','z'}
                        zSize           = varargin{jj+1} ;
                    case {'zDistance'}
                        zDistance       = varargin{jj+1} ;
                end
            end
            
            % Setup superclass.
            self@Phantom(name) ;
            self.set('xSize', xSize, 'ySize', ySize, 'zSize', zSize, 'zDistance', zDistance) ;
            
            % Save Scatterers.
            self.coordinateToScatterPoint(x, y, z, a) ;
        end
        %
        function output = objectType(self)
            output = 'Phantom' ;
        end
        %
        function output = objectExtension(self)
            output = ' -Point' ;
        end
        %
        %
        %% IMAGING METHODS.
        %
        function outputParameters = imageSetup(self)
            mask                = true ;
            outputParameters = struct(...
                'dynamic', true, ...
                'scaling', 1, ...
                'contrast', 1, ...
                'minimum', 0, ...
                'maximum', 1, ...
                'window', 500, ...
                'cmap', parula(256), ...
                'format', 'png', ...
                'addedName', '', ...
                'data', 'scatterers', ...
                'type', '3D-N', ...
                'density', ones(self.noScatterers,1), ...
                'mask', mask , ...
                'size', 50) ;
        end
    end
end