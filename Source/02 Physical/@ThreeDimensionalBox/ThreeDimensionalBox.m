% THREEDIMENSIONALBOX
%
% This class is a pseudo specialisation of 3D points adding a vertical, horizontal and depth width
% to the point. Functionality includes calculating the enclosed volume and determining the distance
% to the center. 

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 8 AUG 16.

classdef ThreeDimensionalBox < handle

    %% PROPERTIES
    properties
        xSize@double                    = 50/1000 ;                                                                 % [double] Width in [m].
        ySize@double                    = 10/1000 ;                                                                 % [double] Transverse width in [m].
        zSize@double                    = 60/1000 ;                                                                 % [double] Height in [m].
        center@ThreeDimensionalPoint    = ThreeDimensionalPoint(0, 0, 60/1000) ;                                      % [ThreeDimensionalPoint] center of box in [m x3].
    end

    %% METHODS
    methods
        
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a 3Dbox and accessing
        % / changing the state of its properties. 
        % ===
        function self = ThreeDimensionalBox(varargin)
            % The odd entries of varargin are "control" entries determining how the subsequent entries
            % most be handled
            for jj = 1:2:length(varargin)
                switch varargin{jj}
                    case {'x_size','xSize','x'}
                        self.xSize = varargin{jj+1} ;
                    case {'y_size','ySize','y'}
                        self.ySize = varargin{jj+1} ;
                    case {'z_size','zSize','z'}
                        self.zSize = varargin{jj+1} ;
                    case {'center'}
                        self.center = varargin{jj+1} ;
                    case {'zDistance'}
                        self.zDistance(varargin{jj+1}) ;
                    otherwise
                        errorStruct.message = ['Input Property ', varargin{jj} ,'not found.'];
                        errorStruct.identifier = 'Box3D:PropertyNotFound';
                        error(errorStruct) ;
                end
                
                % Assign the input value to accomponied inputProperty.
%                 try
%                     switch varargin{jj}
%                         case 'center'
%                             self.center = ThreeDimensionalPoint(varargin{jj+1}) ;
%                         otherwise
%                             setPositiveValue(self,varargin{jj},varargin{jj+1}) ;
%                     end
%                 catch ME
%                     switch ME.identifier
%                         case 'setPositiveValue:InputPropertyNotFound'
%                             warning(['Input Property ', inputProperty ,'not found.']) ;
%                         case 'setPositiveValue:InputValueNotPositive'
%                             warning(['Value assigned to size property ',inputProperty,' must be strictly positive.']) ;
%                         otherwise
%                             rethrow(ME)
%                     end 
%                 end
            end
            %
        end
    end 
end
