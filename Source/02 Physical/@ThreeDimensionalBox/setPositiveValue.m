function setPositiveValue(self,property,value)
% The method assigns a strictly possitive value to a given property.
%
% Check if given value is strictly possitive.
if value > 0 ;
    % Check wether the given property is known.
    switch property
        case {'xSize', 'ySize', 'zSize', 'center'}
            cmd = ['self.',property,' = value ;'] ; eval(cmd) ;
        otherwise
            errorStruct.message = 'Input Property not found.';
            errorStruct.identifier = 'setPositiveValue:InputPropertyNotFound';
            error(errorStruct) ;
    end
else
    errorStruct.message = 'Input Value is not positive.';
    errorStruct.identifier = 'setPositiveValue:InputValueNotPositive';
    error(errorStruct) ;
end
end