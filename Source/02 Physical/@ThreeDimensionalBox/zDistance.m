function distance = zDistance(self, setDistance)
if nargin == 2
    self.center = ThreeDimensionalPoint(0,0,setDistance + self.zSize/2) ;
end
% zDistance naievely calculates the distance between the ThreeDimensionalBox and the origin.
distance = l2Norm(self.center) - self.zSize/2 ;
if distance < 0; distance = 0 ; end
end