% CYSTPHANTOM

% CystPhantom when initilized, generates an array of ScatterPoints. Spatially these point scatterers 
% have specified properties depending on theire location inside our outside a Cyst structure. 
% Five low-scattering cylinders and five high-scattering cylinders representing the ten cysts are
% created whithin the box of scatterers using this phantom setup.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 2 DEC 15.

classdef CystPhantom < Phantom

    %% PROPERTIES
    properties % CYST specific properties.
        noCysts    = 5 ;                                                                            % [int] Parameter setting number of Cyst. (currently unused).
        randomSeed = 3 ;                                                                            % [int] random seed for repreducibility.
    end

    %% METHODS
    methods

        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a CystPhantom and accessing
        % / changing the state of its properties. Unlike most classes setting up a CystPhantom will
        % result in the automatic creation of scatterPoints without the need of constructor object.
        % ===
        function self = CystPhantom(name, noScatterers, varargin)    
            randomSeed = 1 ;
            xSize = 50/1000 ; ySize = 10/1000 ; zSize = 60/1000 ; zDistance = 30/1000 ;
            for jj = 1:2:length(varargin)
                switch lower(varargin{jj})
                    case {'x_size','xsize','x'}
                        xSize           = varargin{jj+1} ;
                    case {'y_size','ysize','y'}
                        ySize           = varargin{jj+1} ;
                    case {'z_size','zsize','z'}
                        zSize           = varargin{jj+1} ;
                    case {'zdistance'}
                        zDistance       = varargin{jj+1} ; 
                    case {'randomseed', 'seed', 'setseed'}
                        randomSeed = varargin{jj+1} ;
                end
            end

            % Setup superclass.
            self@Phantom(name) ;
            self.set('xSize', xSize, 'ySize', ySize, 'zSize', zSize, 'zDistance', zDistance) ;

            % Due to the randomness of in this phantom a seed is set for reproducibility reasons.
            self.randomSeed = randomSeed; rng(self.randomSeed);                                     % Set Seed.
            
            % Scatters are uniformly placed within a confined box determined by the x-,y- and z-size parameters.
            % Note that x- and y-coordinates are centered around 0 whereas the z-coordinates are always positive
            % (with the extra addition of z_distance representing the space between the transducer and the
            % phantom.)
            self.drawUniformScatterers(noScatterers) ;
            [scatterCoordinates, ~]  = scatterPointToCoordinate(self) ;
            x = scatterCoordinates(:,1) ;
            y = scatterCoordinates(:,2) ;
            z = scatterCoordinates(:,3) ;
            
            % The amplitudes are selected from a standard normal distribution centered around 0. This results in
            % a uniform noisy background representing the surrounding tissue.
            amp = randn(noScatterers,1);                                                            % Amplitudes.
            
            % Create Cyst areas by setting the amplitudes of the scatterers inside a given radius to zero.
            xc = 10/1000;                                                                           % X - coord of cyst center. [m]
            zc = self.zDistance ;                                                                   % Z - coord of cyst center. [m]
            for s = [6 5 4 3 2]
                r  = s/2/1000;                                                                      % Radius of cyst. [m]
                zc = zc + 10/1000  ;                                                                % Z - coord of cyst center. [m]
                calc_cylinder(0) ;                                                                  % Set amplitudes inside to 0.
            end
            
            % Make the high scattering regions by increasing the amplitudes of scatterers inside a given radius
            % by a factor 10.
            xc = -5/1000 ;                                                                          % X - coord of cyst. [m]
            zc = 60/1000 + self.zDistance ;                                                         % Z - coord of cyst. [m]
            for s = [5 4 3 2 1]
                r  = s/2/1000;                                                                      % Radius of cyst. [m]
                zc = zc - 10/1000  ;                                                                % Z - coord of cyst. [m]
                calc_cylinder(10) ;                                                                 % Increase the amplitudes inside by a factor 10.
            end
            
            % Place the 5 high intensity point scatterers in the phantom. These represent very small structures
            % within the tissue.
            for jj = size(x,1)-5:size(x,1)
                x(jj)   = -15/1000 ;                                                                % X - coord of scatterer. [m]
                y(jj)   = 0 ;                                                                       % Y - coord of scatterer. [m]
                z(jj)   = self.zDistance + (10+5*10)/1000 + (jj-size(x,1))*10/1000 ;                % Z - coord of scatterer. [m]
                amp(jj) = 20 ;                                                                      % Amp of scatterer. [FLOAT]
            end                                                                                     % List of accompanying amplitudes.
            
            % INTERNAL FUNCTIONS
            % This function increases the amplitude of all scatteres inside the cylinder with radius r and
            % centered around the line (xc,-,zc) with a.
            function calc_cylinder(a)
                inside = ( ((x-xc).^2 + (z-zc).^2) < r^2) ;                                         % Indices of scatterers inside cylinder.
                amp = amp .* (1-inside) + a * amp .* inside;                                         % Increase amplitudes inside by a factor 'a'.
            end
            
            % Save Scatterers.
            self.coordinateToScatterPoint(x,y,z,amp) ;
        end
    end
end