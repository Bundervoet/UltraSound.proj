% CYSTPHANTOM

% CystPhantom when initilized, generates an array of ScatterPoints. Spatially these point scatterers 
% have specified properties depending on theire location inside our outside a Cyst structure. 
% Five low-scattering cylinders and five high-scattering cylinders representing the ten cysts are
% created whithin the box of scatterers using this phantom setup.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 2 DEC 15.

classdef Cyst2DGridPhantom < GridPhantom

    %% PROPERTIES
    properties % CYST specific properties.
        noCysts    = 5 ;                                                                            % [int] Parameter setting number of Cyst. (currently unused).
        randomSeed = 3 ;                                                                            % [int] random seed for repreducibility.
    end

    %% METHODS
    methods

        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a CystPhantom and accessing
        % / changing the state of its properties. Unlike most classes setting up a CystPhantom will
        % result in the automatic creation of scatterPoints without the need of constructor object.
        % ===
        function self = Cyst2DGridPhantom(name, UsT, varargin)    
            
            randomSeed = 1 ;
            xSize = 50/1000 ; ySize = 10/1000 ; zSize = 60/1000 ; zDistance = 30/1000 ;
            for jj = 1:2:length(varargin)
                switch lower(varargin{jj})
                    case {'x_size','xsize','x'}
                        xSize           = varargin{jj+1} ;
                    case {'y_size','ysize','y'}
                        ySize           = varargin{jj+1} ;
                    case {'z_size','zsize','z'}
                        zSize           = varargin{jj+1} ;
                    case {'zdistance'}
                        zDistance       = varargin{jj+1} ; 
                    case {'randomseed', 'seed', 'setseed'}
                        randomSeed = varargin{jj+1} ;
                end
            end

            % Setup superclass.
            self@GridPhantom(name) ;
            self.set('xSize', xSize, 'ySize', ySize, 'zSize', zSize, 'zDistance', zDistance) ;
            
             % Due to the randomness of in this phantom a seed is set for reproducibility reasons.
            self.randomSeed = randomSeed; rng(self.randomSeed);   
            
            % Determine Grid locations within image-sector.
            apertureWidth  = UsT.elementPitch*double((UsT.noElements-1)) ;
            elementCenters = linspace(-apertureWidth/2, apertureWidth/2, UsT.noElements) ;            
            start = sum(elementCenters < -25/1000) ;
            
            self.x = GridParameter(elementCenters(start),   UsT.elementPitch,       double(UsT.noElements-2*start+2)) ;
            self.y = GridParameter(0,                       1/1000,                 1) ;
            self.z = GridParameter(30/1000,                 UsT.pixelPitch('f0'),   272) ; % floor((60/1000) / pixelHeight) + (8 - mod(floor((60/1000) / pixelHeight), 8) - 1)) ;     
            
            self.set(varargin{:}) ;
            
            % Distance between phantom and transducer. [m]                                          % Number of coord for z-dimension. [INT]                               
            self.indexation     = permute(reshape(1:self.x.no*self.y.no*self.z.no,...
                self.z.no, self.x.no, self.y.no), [2 3 1]) ;                                        % Relate Spatial Location to Index Number.
            self.localization   = [...
                repmat(kron((1:self.x.no)', ones(self.z.no,1)),self.y.no,1),...
                kron((1:self.y.no)', ones(self.x.no*self.z.no,1)), ...
                repmat((1:self.z.no)', self.x.no*self.y.no,1)] ;                                     % Relate Scatter Index to Spatial Location.
            
            % Coordinate positions are determined by a starting point for all three dimensions followed by a
            % jump size and the number of jumps to take.
            xPos = self.x.start: self.x.jump: self.x.start + (self.x.no - 1) * self.x.jump ;        % X - Coordinate positions. [m]
            yPos = self.y.start: self.y.jump: self.y.start + (self.y.no - 1) * self.y.jump ;        % Y - Coordinate positions. [m]
            zPos = self.z.start: self.z.jump: self.z.start + (self.z.no - 1) * self.z.jump ;        % Z - Coordinate positions. [m]

            % The coordinates from the three dimensions are "mixed" into a full grid.
            %             [x, y, z] = meshgrid(x_pos,y_pos,z_pos) ;                                 % Full grid posistions.
            %             [z, x, y] = meshgrid(z_pos, x_pos, y_pos) ;                               % Full grid posistions.
            [x, z ,y] = meshgrid(xPos, zPos, yPos) ;                                 % Full grid posistions.

            x = reshape(x,[],1) ;
            y = reshape(y,[],1) ;
            z = reshape(z,[],1) ;
            % the output given by meshgrid has to be written as a list to ease further use.
            scatterers = [x, y, z] ;

            % Set amplitudes to 1 for each posistion on the grid.
            amp = randn(size(scatterers,1),1) ;                                                     % Random scatterers.

            if isempty(self.name)
                self.set('name', ['PH0x', dec2hex(length(amp))]) ;
            end
            % Create Cyst areas by setting the amplitudes of the scatterers inside a given radius to zero.
            xc = 10/1000;                                                                           % X - coord of cyst center. [m]
            zc = self.zDistance ;                                                                   % Z - coord of cyst center. [m]
            for s = [6 5 4 3 2]
                r  = s/2/1000;                                                                      % Radius of cyst. [m]
                zc = zc + 10/1000  ;                                                                % Z - coord of cyst center. [m]
                calc_cylinder(0) ;                                                                  % Set amplitudes inside to 0.
            end
            
            % Make the high scattering regions by increasing the amplitudes of scatterers inside a given radius
            % by a factor 10.
            xc = -5/1000 ;                                                                          % X - coord of cyst. [m]
            zc = 60/1000 + self.zDistance ;                                                         % Z - coord of cyst. [m]
            for s = [5 4 3 2 1]
                r  = s/2/1000;                                                                      % Radius of cyst. [m]
                zc = zc - 10/1000  ;                                                                % Z - coord of cyst. [m]
                calc_cylinder(10) ;                                                                 % Increase the amplitudes inside by a factor 10.
            end
            
            % Place the 5 high intensity point scatterers in the phantom. These represent very small structures
            % within the tissue.
            for jj = 1:5
                xloc  = sum(xPos < -15/1000) ;
                zloc  = sum(zPos < (self.zDistance + jj*10/1000)) ;                                 % Z - coord of scatterer. [m]
                amp(zloc + length(zPos)*(xloc-1)) = 20 ;                                                                      % Amp of scatterer. [FLOAT]
            end                                                                                     % List of accompanying amplitudes.
           
            % INTERNAL FUNCTIONS
            % This function increases the amplitude of all scatteres inside the cylinder with radius r and
            % centered around the line (xc,-,zc) with a.
            function calc_cylinder(a)
                inside = ( ((x-xc).^2 + (z-zc).^2) < r^2) ;                               % Indices of scatterers inside cylinder.
                amp = amp .* (1-inside) + a * amp .* inside;                                        % Increase amplitudes inside by a factor 'a'.
            end
            
            % Save Scatterers.
            self.coordinateToScatterPoint(x,y,z,amp) ;
        end       
        
    end
end