% SCATTERPOINT

% Specialist class of Three dimensional spacial points adding an acoustic impetence parameter
% (amplitude) to the 3D point.

classdef ScatterPoint < ThreeDimensionalPoint ;
    
    %% PROPERTIES
    properties
        % This parameters determines how much energy will be reflected by the scatterer.
        amplitude@double = 0 ;                                                                             % [double] Amplitude parameter.
    end
    
    % METHODS
    
    methods
        
        %% DEFINITION AND VARIABLE METHODS.
        
        % ===
        % This section contains methods pertaining to the construction of a3Dpoints and accessing
        % / changing the state of its properties. Define scatterPoints by giving an input of 1 
        % 4D-array, 4 Doubles or 4 parameters with accomponied Doubles.
        % ===
        
        function self = ScatterPoint(varargin)
            self@ThreeDimensionalPoint ;
            switch length(varargin)
                case 0 % No Input.
                    % DO NOTHING.
                case 1 % Input is a 4D array
                    tupel = varargin{1} ;
                    if length(tupel) == 4 ;
                        self.xCoordinate = tupel(1) ;
                        self.yCoordinate = tupel(2) ;
                        self.zCoordinate = tupel(3) ;
                        self.aCoordinate = tupel(4) ;
                    else
                        errorStruct.message = 'Single input array to constructor is not 4-Dimensional';
                        errorStruct.identifier = 'ScatterPoint:InputArrayNot4D';
                        error(errorStruct) ;
                    end
                    
                case 4 % Inputs are 4 Doubles.
                    self.xCoordinate = varargin{1} ;
                    self.yCoordinate = varargin{2} ;
                    self.zCoordinate = varargin{3} ;
                    self.amplitude = varargin{4} ;
                    
                case 8 % Inputs are 4 parameters accompanied by 3 Doubles.
                    for jj = 1:2:length(varargin)
                        switch varargin{jj}
                            case 'x'
                                self.xCoordinate = varargin{jj+1} ;
                            case 'y'
                                self.yCoordinate = varargin{jj+1} ;
                            case 'z'
                                self.zCoordinate = varargin{jj+1} ;
                            case 'a'
                                self.amplitude = varargin{jj+1} ;
                            otherwise
                                errorStruct.message = 'Input paramater not recongised';
                                errorStruct.identifier = 'ScatterPoint:InputParameterNotRecognised';
                                error(errorStruct) ;
                        end
                    end
                    
                otherwise
                    errorStruct.message = 'Input format not recognised.';
                    errorStruct.identifier = 'ScatterPoint:InputFormatNotRecognised';
                    error(errorStruct) ;
            end
        end
        %% OPERATOR OVERLOADING
        
        function u = plus(v,w)
            % u = v + w ;
            switch class(v)
                case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                    u = ScatterPoint(v + w.x, v + w.y , v + w.z, v + w.a) ;
                otherwise
                    switch class(w)
                        case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                            u = ScatterPoint(v.x + w, v.y + w , v.z + w, v.a + w) ;
                        otherwise
                            u = ScatterPoint(v.x + w.x, v.y + w.y , v.z + w.z, v.a + w.a) ;
                    end
            end
        end
        
        function u = times(k,v)
            % u = k .* v ;
            u = ScatterPoint(k*v.x, k*v.y , k*v.z, k*v.a) ;
        end
        
        function u = mtimes(v,w)
            % u = v * w ;
            switch class(v)
                case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                    u = ScatterPoint(v * w.x, v * w.y , v * w.z, v * w.a) ;
                otherwise
                    switch class(w)
                        case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                            u = ScatterPoint(v.x * w, v.y * w , v.z * w, v.a * w) ;
                        otherwise
                            u = ScatterPoint(v.x * w.x, v.y * w.y , v.z * w.z, v.a + w.a) ;
                    end
            end
        end
        
        
        %% OUTPUT METHODS.
        function output = print(self)
            output = ['X',num2str(self.x*1000),'[mm]_Y',num2str(self.y*1000),'[mm]_Z',num2str(self.z*1000),'[mm]_A',num2str(self.z*1000)] ;
        end
    end
end