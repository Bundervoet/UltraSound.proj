function dValue = l2Norm(self)
% This functions gives the l2-norm of the vector pointing from (0,0,0) to the ThreeDimensionalPoint.
dValue = norm([self.xCoordinate, self.yCoordinate, self.zCoordinate], 2) ;
end