function dValue = l1Distance(self,point3D)
% This function gives the l1-norm of the vector pointing from self to Point3D.
dValue = norm([self.x - point3D.x, self.y - point3D.y, self.z - point3D.z], 1) ;
end