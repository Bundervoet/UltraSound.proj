% THREEDIMENSIONALPOINT

% Three dimensional spacial points allowing for the vectorial functionality such as addition, vector
% multiplication, L2-distance, etc...

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 8 AUG 16.

classdef ThreeDimensionalPoint < handle
    
    %% PROPERTIES
    properties
        xCoordinate@double = 0 ;                                                                           % [double] x-coordinate of point with respect to origin en orho-basis in [m].
        yCoordinate@double = 0 ;                                                                           % [double] y-coordinate of point in [m].
        zCoordinate@double = 0 ;                                                                           % [double] z-coordinate of point in [m].
    end

    %% METHODS
    methods

        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a 3Dpoints and accessing
        % / changing the state of its properties. Define 3D point by giving an input of 1 3D-array, 
        % 3 Doubles or 3 parameters with accomponied Doubles.
        % ===
        function self = ThreeDimensionalPoint(varargin)
            switch length(varargin)
                case 0 % No input.
                    % DO NOTHING.
                case 1 % Input is a 3D array
                    tupel = varargin{1} ;
                    if length(tupel) == 3 ;
                        self.xCoordinate = tupel(1) ;
                        self.yCoordinate = tupel(2) ;
                        self.zCoordinate = tupel(3) ;
                    else
                        errorStruct.message = 'Single input array to constructor is not 3-Dimensional';
                        errorStruct.identifier = 'ThreeDimensionalPoint:InputArrayNot3D';
                        error(errorStruct) ;
                    end
                    
                case 3 % Inputs are 3 Doubles.
                    self.xCoordinate = varargin{1} ;
                    self.yCoordinate = varargin{2} ;
                    self.zCoordinate = varargin{3} ;
                    
                case 6 % Inputs are 3 parameters accompanied by 3 Doubles.
                    for jj = 1:2:length(varargin)
                        switch varargin{jj}
                            case 'x'
                                self.xCoordinate = varargin{jj+1} ;
                            case 'y'
                                self.yCoordinate = varargin{jj+1} ;
                            case 'z'
                                self.zCoordinate = varargin{jj+1} ;
                            otherwise
                                errorStruct.message = 'Input paramater not recongised';
                                errorStruct.identifier = 'ThreeDimensionalPoint:InputParameterNotRecognised';
                                error(errorStruct) ;
                        end
                    end
                    
                otherwise
                    errorStruct.message = 'Input format not recognised.';
                    errorStruct.identifier = 'ThreeDimensionalPoint:InputFormatNotRecognised';
                    error(errorStruct) ;
            end
        end

        %% RENAME COORDINATE PSEUDO-VARIABLES
        function output = x(self)
            output = self.xCoordinate ;
        end
        function output = y(self)
            output = self.yCoordinate ;
        end
        function output = z(self)
            output = self.zCoordinate ;
        end

        %% OPERATOR OVERLOADING
        function u = plus(v,w)
            % u = v + w ;
            switch class(v)
                case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                    u = ThreeDimensionalPoint(v + w.x, v + w.y , v + w.z) ;
                otherwise
                    switch class(w)
                        case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                            u = ThreeDimensionalPoint(v.x + w, v.y + w , v.z + w) ;
                        otherwise
                            u = ThreeDimensionalPoint(v.x + w.x, v.y + w.y , v.z + w.z) ;
                    end
            end
        end
        function u = times(k,v)
            % u = k .* v ;
            u = ThreeDimensionalPoint(k*v.x, k*v.y , k*v.z) ;
        end
        function u = mtimes(v,w)
            % u = v * w ;
            switch class(v)
                case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                    u = ThreeDimensionalPoint(v * w.x, v * w.y , v * w.z) ;
                otherwise
                    switch class(w)
                        case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                            u = ThreeDimensionalPoint(v.x * w, v.y * w , v.z * w) ;
                        otherwise
                            u = ThreeDimensionalPoint(v.x * w.x, v.y * w.y , v.z * w.z) ;
                    end
            end
        end

        %% OUTPUT METHODS.
        function output = print(self)
            output = ['X',num2str(self.x*1000),'[mm]_Y',num2str(self.y*1000),'[mm]_Z',num2str(self.z*1000),'[mm]'] ;
        end
    end
end