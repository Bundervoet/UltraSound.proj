function dValue = l2Distance(self,point3D)
% This function gives the l2-norm of the vector pointing from self to Point3D.
dValue = norm([self.x - point3D.x, self.y - point3D.y, self.z - point3D.z], 2) ;
end