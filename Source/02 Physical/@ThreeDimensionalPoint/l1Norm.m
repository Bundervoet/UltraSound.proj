function dValue = l1Norm(self)
% This functions gives the l1-norm of the vector pointing from (0,0,0) to the ThreeDimensionalPoint.
dValue = norm([self.xCoordinate, self.yCoordinate, self.zCoordinate], 1) ;
end
