function par = imagerScat(self, varargin)
% ---
% Specialised version of imageNow used by all object visualizing pre-prosessed data. The main 
% compononent still include:
% - imagePARAM which reads varargin and adjust standardPARAM accordingly.
% - imageDATA which procesessed RAW input data such as matrixDATA and applies further adjustments to 
%   the output of imagePARAM. 
% - the imager does not use imshow but scatter due to the nature of Phantom data.
% ---
%% INITIALIZE IMAGER
% Load and addapt imaging Parameters.
par                     = PhantomImageParameter(class(self), self.noScatterers(), varargin{:}) ;

% Load correct input matrix.
[imageMatrix, ~, par]   = self.imageDATA(par) ;


%% IMSHOW OF INPUT MATRIX

figure(par.window)
switch par.rendering
    case ScatterSetting.Amplitude2D
        s = scatter(imageMatrix(:,1), -imageMatrix(:,3), par.scale, imageMatrix(:,4), 'filled') ;
    case ScatterSetting.Density2D
        s = scatter(imageMatrix(:,1), -imageMatrix(:,3), par.scale, imageMatrix(:,5), 'filled') ;
    case ScatterSetting.Amplitude3D
        s = scatter3(imageMatrix(:,1), imageMatrix(:,2), -imageMatrix(:,3), par.scale, imageMatrix(:,4), 'filled') ;
    case ScatterSetting.Density3D
        s = scatter3(imageMatrix(:,1), imageMatrix(:,2), -imageMatrix(:,3), par.scale, imageMatrix(:,5), 'filled') ;
    case ScatterSetting.PartialAmplitude3D
        imageMatrix = imageMatrix(imageMatrix(:,2) > -0.01 & imageMatrix(:,2) < 0.01, :) ;
        s = scatter3(imageMatrix(:,1), imageMatrix(:,2), -imageMatrix(:,3), par.scale, imageMatrix(:,4), 'filled') ;
    case ScatterSetting.PartialDensity3D
        imageMatrix = imageMatrix(imageMatrix(:,2) > -0.01 & imageMatrix(:,2) < 0.01, :) ;
        s = scatter3(imageMatrix(:,1), imageMatrix(:,2), -imageMatrix(:,3), par.scale, imageMatrix(:,5), 'filled') ;       
end

% s.MarkerEdgeColor = [0 0 0] ;
% Adjust axis and colormap for a clean image.
colormap(par.cmap) ;
axis tight
axis off
set(gca, 'Units', 'normalized', 'Position', [0 0 1 1]) ;
axis('image') ;
drawnow ;

end
