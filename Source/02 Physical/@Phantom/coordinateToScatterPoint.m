% Export the list of scatterers to a 4D array of the coordinates.
function coordinateToScatterPoint(phantom, x, y, z, a)
%
% Test if input array are of same length.
if (length(x) == length(y)) && (length(y) == length(z)) && (length(z) == length(a))
    % Determine number of scatterers.
    noScatterers = length(x) ;
else
    errorStruct.message = 'Input arrays must be of same length.';
    errorStruct.identifier = 'coordinateToScatterers:LengthOfInputMismatch';
    error(errorStruct) ;
end
%
% ArrayOfScatterers = zeros(noScatterers,4) ;
for jj = 1 : noScatterers
    ArrayOfScatterers(jj) = ScatterPoint(x(jj),y(jj),z(jj),a(jj)) ; %#ok<AGROW>
end
%
phantom.set('scatterDATA', ArrayOfScatterers) ;
end