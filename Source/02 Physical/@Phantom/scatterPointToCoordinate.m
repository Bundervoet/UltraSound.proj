% Export the list of scatterers to a 4D array of the coordinates.
function [scatterCoordinates, scatterAmplitude] = scatterPointToCoordinate(self, points)
if nargin == 2 ;
    noScatterers    = numel(points) ;
else
    noScatterers    = self.noScatterers ;
    points          = 1 : noScatterers ;
end

% Preset coordinates.
xCoordinate         = zeros(noScatterers,1) ;
yCoordinate         = zeros(noScatterers,1) ;
zCoordinate         = zeros(noScatterers,1) ;
scatterAmplitude    = zeros(noScatterers,1) ;

% Read coordinates from list.
scatterers = self.get('scatterDATA') ;
for jj = 1:noScatterers 
    xCoordinate(jj)         = scatterers(points(jj)).xCoordinate ;
    yCoordinate(jj)         = scatterers(points(jj)).yCoordinate ;
    zCoordinate(jj)         = scatterers(points(jj)).zCoordinate ;
    scatterAmplitude(jj)    = scatterers(points(jj)).amplitude ;
end

scatterCoordinates = [xCoordinate, yCoordinate, zCoordinate] ;
end