function drawUniformScatterers(phantom, noScatterers)
% Uniformly fill ThreeDimensionalBox with ScatterPoints by calculating their coordinates.
x = (rand (noScatterers,1) - 0.5) * phantom.confinementBox.xSize + phantom.confinementBox.center.y ;                               % X - coordinates [m]
y = (rand (noScatterers,1) - 0.5) * phantom.confinementBox.ySize + phantom.confinementBox.center.x ;                               % Y - coordinates [m]
z = (rand (noScatterers,1) - 0.5) * phantom.confinementBox.zSize + phantom.confinementBox.center.z ;                               % Z - coordinates [m]

% Make a list of all ScatterPoints. 
for ii = 1:noScatterers
    listOfScatterers(ii) = ScatterPoint(x(ii),y(ii),z(ii),0) ;
end

% Save Scatterers.
phantom.set('scatterDATA', listOfScatterers) ;
end