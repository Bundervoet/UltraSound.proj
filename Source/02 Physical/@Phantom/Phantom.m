% PHANTOM

% A phantom consist of 3D ScatterPoints (scatterers) contained in a 3D box each linked to
% an acoustic impedance parameter (amplitudes) denoting how much the scatterer will reflect incoming
% pressure waves. ScatterPoints are stored as in array in scatterDATA. Examples of phantoms include:
%  - Point; which generates 1 scattering point.
%  - Grid;  which generates a 3D grid of point scatterers.
%  - Cyst;  which generates continous tissue containing $n$ high- and low intensity scatterers.

% Note;
%  - 1 - International standard measures are used for distance and other variables.
%  - 2 - phantom is a subclass of simulation and is thus able to save and load the bulk of it's
%  data to the HDD.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 8 AUG 16.

classdef Phantom < DataType
    
    % PROPERTIES 
    properties % DATA PROPERTIES
        confinementBox@ThreeDimensionalBox = ThreeDimensionalBox() ;
        scatterDATA@ScatterPoint ;                                                                                % List of [ScatterPoints] placed whithin the confimentBox.
    end
    
    % METHODS    
    methods % SETUP METHOD
        
        % DEFINITION AND VARIABLE METHODS.
        
        % This function constructs an instance of the object in a encompassing script, by defining
        % all it's relevant properties. All non-critical properties can be specified later on.
        function self = Phantom(name, varargin)  
            self@DataType(name, varargin{:}) ;            
        end
        
        % Specialised version of variable handling functions.
        function setOne(self, input, value)
            switch input
                case 'zDistance'
                    self.confinementBox.zDistance(value) ;
                case {'xSize', 'ySize', 'zSize'}
                    self.confinementBox.(input) = value ;
                otherwise
                    if sum(strcmp(properties(self), input))
                        self.(input) = value ;
                    else
                        self.meta.(input) = value ;
                    end
            end
        end
        function output = getOne(self, input)
            switch input
                case {'zDistance', 'xSize', 'ySize', 'zSize'}
                    output = self.confinementBox.(input) ;
                case {'dx', 'dy', 'dz'}
                    if isfield(self.meta, input)
                        output = self.meta.(input) ;
                    else
                        output = 0  ;
                    end
                otherwise
                    if sum(strcmp(properties(self), input))
                        output = self.(input) ;
                    elseif isfield(self.meta, input)
                        output = self.meta.(input) ;
                    else
                        errorStruct.message = ['Input argument "', num2str(input),'" is non existent variable.'];
                        errorStruct.identifier = 'Phantom:getVarInternal:NonExistentVariable';
                        error(errorStruct) ;
                    end
            end
        end
        function output = type(self)
            output = 'PHdata' ;
        end
        
        % DATA METHODS
        function output = zDistance(self)
            output = self.confinementBox.zDistance ; 
        end
        function output = batchInterval(self, size)
            output = 1:size:self.noScatterers ;
        end
        function output = inBatch(self, size, scat)
            interval    = self.batchInterval(size) ;
            output      = find(interval <= scat, 1, 'last' );
        end
        function output = noBatch(self, size)
            output = numel(self.batchInterval(size)) ;
        end
        function output = isGrid(self)
            output = isa(self, 'GridPhantom') ;
        end
        function output = isCyst(self)
            output = isa(self, 'CystPhantom') ;
        end
        function ouput = scatterCoordinates(self)
            [ouput, ~] = self.scatterPointToCoordinate ;
        end
        function output = scatterAmplitdues(self)
            [~, output] = self.scatterPointToCoordinate ;
        end
               
        % Imaging Methods
        function outputParameters = standardPARAM(self)
            mask            = false(self.noScatterers,1) ;
            mask(1:8:end)   = true ; 
            outputParameters = struct(...
                'dynamic', true, ...
                'scaling', 1, ...
                'contrast', 1, ...
                'minimum', 0, ...
                'maximum', 1, ...
                'window', 500, ...
                'cmap', parula(256), ...
                'format', 'png',...
                'check', true, ...
                'data', 'scatterers', ...
                'type', '3D-N', ...
                'density', ones(self.noScatterers,1), ...
                'mask', mask , ...
                'size', 25) ;
        end
        function output = pixelSize(self)
            output = [1 1 1] ; 
        end
        function [outputDATA, outputINFO,  par] = imageDATA(self, par)
            % Imaging variables. From these variables the correct distance measurements
            % are displayed in the figure window. Moreover they help to preserver the
            % correct aspect ratios.           
            switch lower(par.data)
                case {'scatterers'}           
                    [A, B]          = self.scatterPointToCoordinate ;
                    B = par.dynamic.transform(B, min(B(:)), max(B(:))) ;
                    B = min(max(B, par.minimum), par.maximum) ; 
                    if ~isempty(par.density)
                        C               = reshape(par.density,[],1) ;
                        C = par.dynamic.transform(C, min(C(:)), max(C(:))) ;
                        C = min(max(C, par.minimum), par.maximum) ; 
                    end
                    inputMatrix     = [A, B, C] ;
                    inputMatrix     = inputMatrix(par.mask(:),:) ;
                    outputINFO      = 'scattereres' ;
                otherwise
                    errorStruct.message     = ['Input Property "', lower(par.data) ,'" not recognised.'];
                    errorStruct.identifier  = 'Phantom:imageDATA:InputNotRecognised';
                    error(errorStruct) ;
            end
            outputDATA    = inputMatrix ; % imresize(inputMatrix, [dataSize(1) (dataSize(2) * pixelWidth / pixelHeight)] * par.scaling, 'nearest') ;
        end  
    end
end