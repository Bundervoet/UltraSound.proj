% BRIGHTNESSMODEIMAGE

% This class is used to import the full set of RFlines simulated by a UStranducer of a Phantom. 
% After processing a bmodeIMAGE is obtained which represent the structure contained in a slice of
% the Phantom. Again this data can be visualized using imageNow.
% NOTE : The vizualization pipeline tries to mimic the visualization process performed by a real
% UltraSound setup. This included beamforming of the RFdata and contrast compensation, etc...

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 22 AUG 16.

classdef ReconstructionImage < BrightnessModeImage
    
    
    %% PROPERTIES
    properties % OUTPUT FROM IMAGING PIPELINE
        importDATA       = struct('result', []) ;                                                   % Collected beamformed channel RF data.
        envelopeDATA     = struct('result', []) ;                                                   % Envelopped version of importDATA.
        logEnvelopeDATA  = struct('result', []) ;                                                   % Log transform version of envelopeDATA.
        logEnvelopeIMAGE = struct('result', []) ; ;                                                 % Aspect ratio correction of logEnvelopeDATA for visualization.
        bmodeIMAGE       = struct('result', []) ;                                                   % Interpolated version of logEnvelopeIMAGE.
    end
      
    %% METHODS   
    methods
       
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a instance and accessing
        % / changing the state of its properties.
        % ===
        function self = ReconstructionImage(name, varargin)
            self@BrightnessModeImage(name, varargin{:}) ;
        end

        function output = matrixDATA(self,input)
            if nargin == 2
                self.logEnvelopeDATA = input ;
            end
            output = self.logEnvelopeDATA ;
        end
        function setOne(self, input, value)
            % ---
            % Similar to setOne in other classes this method provides functionality for addapting a
            % variable in BrightnessModeImage. Take special care in adapting non-double properties.
            % ---
            switch lower(input)
                case 'beamformer'
                    if isa(value, 'char')
                        self.(input) = BeamformSetting.(value) ;
                    else
                        self.(input) = value ;
                    end
                case 'contraster'
                    if isa(value, 'char')
                        self.(input) = ContrastSetting.(value) ;
                    else
                        self.(input) = value ;
                    end
                case 'enveloper'
                    if isa(value, 'char')
                        self.(input) = EnvelopeSetting.(value) ;
                    else
                        self.(input) = value ;
                    end
                case 'apodizer'
                    if isa(value, 'char')
                        self.(input) = ApodizationSetting.(value) ;
                    else
                        self.(input) = value ;
                    end
                otherwise
                    setDATA(self, input, value) ;
            end
        end
        function output = getOne(self, input)
            switch lower(input)
                case {'imagesize', 'datasize'}
                    output = self.(input) ;
                otherwise
                    output = getDATA(self, input) ;
            end
        end       
        function output = type(self)
            output = 'BMimage' ;
        end
        
        
        %% DATA METHODS.
        % ===
        % Pseudo-property methods allowing for easy and generic access (and adaptation) of the 
        % central data property signalDATA.
        % ===
        function output = getMatrixDATA(self)
                output = self.logEnvelopeDATA ;
        end
        function setMatrixDATA(self, input)
                self.logEnvelopeDATA    = input ;
        end
        
        
        %% SIZE METHODS.
        
        function output = imageSize(self,dim)
            output = size(self.logEnvelopeIMAGE) ;
            if nargin == 2
                output = output(dim) ;
            end
        end
        function output = dataSize(self,dim)
            output = size(self.envelopeDATA) ;
            if nargin == 2
                output = output(dim) ;
            end
        end
        
        
        %% IMAGING METHODS
        function [outputDATA, outputINFO, par] = imageDATA(self, par)
            % ---
            % This method collects the required data for imaging and processes it such that for
            % example the correct distance measurements are displayed in the figure window. 
            % Alternatively the processing helps to preserver the correct aspect ratios and
            % transforms the data to the correct visual domain (dct, cdf,...).
            % ---
            switch lower(par.data)
                case {'bmode', 'bmodeimage'} % Since BmodeIMAGEs have been preprocessed no futher aspect ratio correction are required.
                    outputDATA  = double(imresize(self.bmodeIMAGE, [self.imageSize(1) self.imageSize(2)] * par.scale, 'nearest')) ;
                    outputINFO  = 'bmodeIMAGE' ;
                case {'logenvelopeimage'}    % Since LogenvelopeIMAGEs have been preprocessed no futher aspect ratio correction are required.
                    outputDATA  = double(imresize(self.logEnvelopeIMAGE, [self.imageSize(1) self.imageSize(2)] * par.scale, 'nearest')) ;
                    outputINFO  = 'logEnvelopeIMAGE' ;                  
                case {'logenvelopedecimateddata'}
                    outputDATA  = imresize(self.logEnvelopeDecimatedDATA, [self.dataSize(1) (self.dataSize(2)* self.pixelWidth / self.pixelHeight)] * par.scale, 'nearest') ;
                    outputINFO  = 'logEnvelopeDecimatedDATA' ;
                    par.minimum = self.minimum ;
                    par.maximum = self.maximum ;
                case {'logenvelopedata'}
                    outputDATA  = imresize(self.logEnvelopeDATA, [self.dataSize(1) (self.dataSize(2)* self.pixelWidth / self.pixelHeight)] * par.scale, 'nearest') ;
                    outputINFO  = 'logEnvelopeDATA' ;
                    par.minimum = self.minimum ;
                    par.maximum = self.maximum ;
                case {'envelopedata'}
                    outputDATA  = imresize(self.envelopeDATA, [self.dataSize(1) (self.dataSize(2)* self.pixelWidth / self.pixelHeight)] * par.scale, 'nearest') ;
                    outputINFO  = 'envelopeDATA' ;
                    par.minimum = self.minimum ;
                    par.maximum = self.maximum ;
                otherwise
                    errorStruct.message = ['Input Property "', lower(par.data) ,'" not recognised.'];
                    errorStruct.identifier = 'BrightnessModeImage:imageDATA:InputNotRecognised';
                    error(errorStruct) ;
            end
        end
       
    end
end