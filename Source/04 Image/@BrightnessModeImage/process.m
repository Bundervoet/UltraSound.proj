function process(BmI, UStransducer, RFpath)
% This paramater helps to detemine the number of digits to use when displaying amounts such as
% number of lines.
D = UStransducer.digits ;

for ii = 1:UStransducer.noLines
    
    % Load RFdata.
    RFdata = loader(RFpath,['RFline',num2str(ii)]) ;
    
    if ii == 1 && isa(RFdata, 'ReconstructionData')
        fieldNames = fields(RFdata.signalDATA) ;
        
        setupVar = cell(1, length(fieldNames)) ;
        for ff = 1 : length(fieldNames)
            setupVar{2*ff-1} = fieldNames{ff} ;
            setupVar{2*ff} = [] ;
        end
        BmI.importDATA       = struct(setupVar{:}) ;                      % Collected beamformed channel RF data.
        BmI.envelopeDATA     = struct(setupVar{:}) ;                      % Envelopped version of importDATA.
        BmI.logEnvelopeDATA  = struct(setupVar{:}) ;                      % Log transform version of envelopeDATA.
        BmI.logEnvelopeIMAGE = struct(setupVar{:}) ;                      % Aspect ratio correction of logEnvelopeDATA for visualization.
        BmI.bmodeIMAGE       = struct(setupVar{:}) ;                      % Interpolated version of logEnvelopeIMAGE.
    end
    
    % Display Progress.
    if BmI.verbose
    %                    '12345678901234567890123456789012345678901234567890'
    disp([timeStamp,' ', BmI.name,'::','Beamforming RFline and adding to imported data ---',...
        '::I',sprintf(D.i,ii), ...
        '::P',sprintf('%03d', floor(double(ii)/double(UStransducer.noLines)*100))]) ;
    end
    
    % Beamform channel RF data.
    RFdata.beamform(BmI) ;
    
    if isa(RFdata, 'ReconstructionData')     
        for ff = 1 : length(fieldNames)
            % Add Beamformed data to imported data.
            BmI.importDATA.(fieldNames{ff})(:, end+1) = RFdata.signalDATA.(fieldNames{ff}) ;
        end
    else
        % Add Beamformed data to imported data.
        BmI.importDATA(:, end+1) = RFdata.get('matrixDATA') ;
    end
end

% Infer Bmode-pixelSize from RFdata.
if isempty(BmI.pixelSize) || sum(BmI.pixelSize) == 0
    BmI.pixelSize = [RFdata.pixelSize(1) RFdata.lineWidth] ;
end

if isa(RFdata, 'ReconstructionData')
    BmI.setMatrixDATA(BmI.importDATA.result, 'result') ;
    
    for ff = 1 : length(fieldNames)
        % Detect enveloping signal in high frequency data. (Type of demodulation.)
        BmI.envelopeDATA.(fieldNames{ff}) = BmI.enveloper.detect(BmI.importDATA.(fieldNames{ff})) ;
        
        % Set normalization
        if ff == 1
            if BmI.matrixINFO.normalizationFactor == 0
                %         disp('1') ;
                normalizationFactor = max(abs(BmI.importDATA.reference(:))) ;
            else
                normalizationFactor = BmI.matrixINFO.normalizationFactor ;
            end
            BmI.matrixINFO.set('normalizationFactor', normalizationFactor) ;
        end
        % Perform a log-typ transformation to increase contrast of interesting tissue-features.
        if ~strcmp(char(BmI.contraster), char(RFdata.matrixINFO.transformed))
            BmI.logEnvelopeDATA.(fieldNames{ff}) = BmI.contraster.increaseContrast(BmI.envelopeDATA.(fieldNames{ff}), normalizationFactor) ;
        end
        
        % Adjust aspect ratios and quantize DATA to obtain a 8-bit Bmode image.
        BmI.bmodeIMAGE.(fieldNames{ff})        = uint8(imresize(BmI.logEnvelopeDATA.(fieldNames{ff}), [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)]))  ;
        BmI.logEnvelopeIMAGE.(fieldNames{ff})  = uint8(imresize(BmI.logEnvelopeDATA.(fieldNames{ff}), [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)], 'nearest')) ;
        % BmI.bmodeIMAGE        = uint8(mat2gray(imresize(BmI.logEnvelopeDATA, ...
        %     [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)]), [BmI.minimum BmI.maximum])*256)  ;
        % BmI.logEnvelopeIMAGE  = uint8(mat2gray(imresize(BmI.logEnvelopeDATA, ...
        %     [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)], 'nearest'), [BmI.minimum BmI.maximum])*256) ;
    end
else
    % Detect enveloping signal in high frequency data. (Type of demodulation.)
    BmI.set('envelopeDATA', BmI.enveloper.detect(BmI.get('importDATA'))) ;
    
    % Set normalization
    if BmI.matrixINFO.normalizationFactor == 0
        %         disp('1') ;
        normalizationFactor = max(abs(BmI.envelopeDATA(:))) ;
    else
        normalizationFactor = BmI.matrixINFO.normalizationFactor ;
    end
    BmI.matrixINFO.set('normalizationFactor', normalizationFactor) ;
    % Perform a log-typ transformation to increase contrast of interesting tissue-features.
    if ~strcmp(char(BmI.contraster), char(RFdata.matrixINFO.transformed))
        BmI.set('logEnvelopeDATA', BmI.contraster.increaseContrast(BmI.get('envelopeDATA'), normalizationFactor)) ;
    end
    
    % Adjust aspect ratios and quantize DATA to obtain a 8-bit Bmode image.
    BmI.bmodeIMAGE        = uint8(imresize(BmI.logEnvelopeDATA, [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)]))  ;
    BmI.logEnvelopeIMAGE  = uint8(imresize(BmI.logEnvelopeDATA, [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)], 'nearest')) ;
    % BmI.bmodeIMAGE        = uint8(mat2gray(imresize(BmI.logEnvelopeDATA, ...
    %     [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)]), [BmI.minimum BmI.maximum])*256)  ;
    % BmI.logEnvelopeIMAGE  = uint8(mat2gray(imresize(BmI.logEnvelopeDATA, ...
    %     [BmI.height (BmI.width * BmI.pixelWidth / BmI.pixelHeight)], 'nearest'), [BmI.minimum BmI.maximum])*256) ;
end
end