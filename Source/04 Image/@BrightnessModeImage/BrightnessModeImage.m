% BRIGHTNESSMODEIMAGE

% This class is used to import the full set of RFlines simulated by a UStranducer of a Phantom. 
% After processing a bmodeIMAGE is obtained which represent the structure contained in a slice of
% the Phantom. Again this data can be visualized using imageNow.
% NOTE : The vizualization pipeline tries to mimic the visualization process performed by a real
% UltraSound setup. This included beamforming of the RFdata and contrast compensation, etc...

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 22 AUG 16.

classdef BrightnessModeImage < DataMatrix
    
    
    %% PROPERTIES
    properties % OUTPUT FROM IMAGING PIPELINE
        importDATA ;                                                                                % Collected beamformed channel RF data.
        envelopeDATA ;                                                                              % Envelopped version of importDATA.
        logEnvelopeDATA ;                                                                           % Log transform version of envelopeDATA.
        logEnvelopeIMAGE ;                                                                          % Aspect ratio correction of logEnvelopeDATA for visualization.
        bmodeIMAGE ;                                                                                % Interpolated version of logEnvelopeIMAGE.
    end
    properties % IMAGING PIPELINE SETTINGS
        beamformer@BeamformSetting  = BeamformSetting.focusing ;                                    % Use centerline profile. ['center', 'summation', 'focussing']
        contraster@ContrastSetting  = ContrastSetting.decibel ;                                     % Determin setting used to increase contrasted in images.
        enveloper@EnvelopeSetting   = EnvelopeSetting.overall ;                                     % Setting used to detect envelope data.
        apodizer@ApodizationSetting = ApodizationSetting.hanning ;                                  % Apodize imported RFdata.
    end
    properties % ADDED PIPELINE SETTINGS.
        filter@logical              = false ;                                                       % Filter the Enveloppe detected signal.                                                         % Factor at which to normalize RFdata. 0 = No normalization.
    end
    
    
    %% METHODS   
    methods
       
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a instance and accessing
        % / changing the state of its properties.
        % ===
        function BmI = BrightnessModeImage(name, varargin)
            BmI@DataMatrix(name, varargin{:}) ;
        end

        function output = matrixDATA(BmI, input)
            if nargin == 2
                BmI.logEnvelopeDATA = input ;
            end
            output = BmI.logEnvelopeDATA ;
        end
        function setOne(BmI, input, value)
            % ---
            % Similar to setOne in other classes this method provides functionality for addapting a
            % variable in BrightnessModeImage. Take special care in adapting non-double properties.
            % ---
            switch lower(input)
                case 'beamformer'
                    if isa(value, 'char')
                        BmI.(input) = BeamformSetting.(value) ;
                    else
                        BmI.(input) = value ;
                    end
                case 'contraster'
                    if isa(value, 'char')
                        BmI.(input) = ContrastSetting.(value) ;
                    else
                        BmI.(input) = value ;
                    end
                case 'enveloper'
                    if isa(value, 'char')
                        BmI.(input) = EnvelopeSetting.(value) ;
                    else
                        BmI.(input) = value ;
                    end
                case 'apodizer'
                    if isa(value, 'char')
                        BmI.(input) = ApodizationSetting.(value) ;
                    else
                        BmI.(input) = value ;
                    end
                otherwise
                    setDATA(BmI, input, value) ;
            end
        end
        function output = getOne(BmI, input)
            switch lower(input)
                case {'imagesize', 'datasize'}
                    output = BmI.(input) ;
                otherwise
                    output = getDATA(BmI, input) ;
            end
        end       
        function output = type(BmI)
            output = 'BMimage' ;
        end
        
        
        %% DATA METHODS.
        % ===
        % Pseudo-property methods allowing for easy and generic access (and adaptation) of the 
        % central data property signalDATA.
        % ===
        function output = getMatrixDATA(BmI, step)
            if nargin < 2
                step = 'result' ;
            end
            if isa(BmI.logEnvelopeDATA, 'struct')
                output = BmI.logEnvelopeDATA.(step) ;
            else
                output = BmI.logEnvelopeDATA ;
            end
        end
        function setMatrixDATA(BmI, input, step)
            if nargin < 3
                step = 'result' ;
            end
            if isa(BmI.logEnvelopeDATA, 'struct')
                BmI.logEnvelopeDATA.(step) = input;
            else
                BmI.logEnvelopeDATA = input ;
            end
        end
        
        
        %% SIZE METHODS.
        
        function output = imageSize(BmI, dim)
            if isa(BmI.logEnvelopeDATA, 'struct')
                output = size(BmI.logEnvelopeIMAGE.result) ;
            else
                output = size(BmI.logEnvelopeIMAGE) ;
            end
            if nargin == 2
                output = output(dim) ;
            end
        end
        function output = dataSize(BmI,dim)
            if isa(BmI.envelopeDATA, 'struct')
                output = size(BmI.envelopeDATA.result) ;
            else
                output = size(BmI.envelopeDATA) ;
            end
            if nargin == 2
                output = output(dim) ;
            end
        end
        
        
        %% IMAGING METHODS
        function [outputDATA, outputINFO, par] = imageDATA(BmI, par)
            % ---
            % This method collects the required data for imaging and processes it such that for
            % example the correct distance measurements are displayed in the figure window. 
            % Alternatively the processing helps to preserver the correct aspect ratios and
            % transforms the data to the correct visual domain (dct, cdf,...).
            % ---
            
            switch lower(par.data)
                case {'bmode', 'bmodeimage'} % Since BmodeIMAGEs have been preprocessed no futher aspect ratio correction are required.
                    if isa(BmI.bmodeIMAGE, 'struct')
                        inputDATA = BmI.bmodeIMAGE.(par.meta.step) ;
                    else
                        inputDATA = BmI.bmodeIMAGE ;
                    end
                    outputDATA  = double(imresize(inputDATA, [BmI.imageSize(1) BmI.imageSize(2)] * par.scale, 'nearest')) ;
                    outputINFO  = 'bmodeIMAGE' ;
                case {'logenvelopeimage'}    % Since LogenvelopeIMAGEs have been preprocessed no futher aspect ratio correction are required.
                    if isa(BmI.logEnvelopeIMAGE, 'struct')
                        inputDATA = BmI.logEnvelopeIMAGE.(par.meta.step) ;
                    else
                        inputDATA = BmI.logEnvelopeIMAGE ;
                    end
                    outputDATA  = double(imresize(inputDATA, [BmI.imageSize(1) BmI.imageSize(2)] * par.scale, 'nearest')) ;
                    outputINFO  = 'logEnvelopeIMAGE' ;                  
                case {'logenvelopedecimateddata'}
                    outputDATA  = imresize(BmI.logEnvelopeDecimatedDATA, [BmI.dataSize(1) (BmI.dataSize(2)* BmI.pixelWidth / BmI.pixelHeight)] * par.scale, 'nearest') ;
                    outputINFO  = 'logEnvelopeDecimatedDATA' ;
                    par.minimum = BmI.minimum ;
                    par.maximum = BmI.maximum ;
                case {'logenvelopedata'}
                    if isa(BmI.logEnvelopeDATA, 'struct')
                        inputDATA = BmI.logEnvelopeDATA.(par.meta.step) ;
                    else
                        inputDATA = BmI.logEnvelopeDATA ;
                    end
                    outputDATA  = imresize(inputDATA, [BmI.dataSize(1) (BmI.dataSize(2)* BmI.pixelWidth / BmI.pixelHeight)] * par.scale, 'nearest') ;
                    outputINFO  = 'logEnvelopeDATA' ;
                    par.minimum = BmI.minimum ;
                    par.maximum = BmI.maximum ;
                case {'envelopedata'}
                    if isa(BmI.envelopeDATA, 'struct')
                        inputDATA = BmI.envelopeDATA.(par.meta.step) ;
                    else
                        inputDATA = BmI.envelopeDATA ;
                    end
                    outputDATA  = imresize(inputDATA, [BmI.dataSize(1) (BmI.dataSize(2)* BmI.pixelWidth / BmI.pixelHeight)] * par.scale, 'nearest') ;
                    outputINFO  = 'envelopeDATA' ;
                    par.minimum = BmI.minimum ;
                    par.maximum = BmI.maximum ;
                otherwise
                    errorStruct.message = ['Input Property "', lower(par.data) ,'" not recognised.'];
                    errorStruct.identifier = 'BrightnessModeImage:imageDATA:InputNotRecognised';
                    error(errorStruct) ;
            end
        end
       
    end
end