% SAVENOW 

% This function simply allows to save an object at a given path. It takes into account the class of
% the object to determine which save-version of MATLAB will be used.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 22 JUN 16.

function savePath = saver(self, varargin) 
% ---
% saveNOW can be used on data object in the following two ways :
%   - ['folder1/folder2/[dataObj.name].mat'] = dataObj.saveNow({'folder1','folder2'}) ;
%   - ['folder1/folder2/[dataObj.name].mat'] = dataObj.saveNow('folder1/folder2') ;
%   - ['folder1/folder2/[dataObj.name].mat'] = dataObj.saveNow('folder1''folder2') ;
%   - ['folder1/folder2/[dataObj.name].mat'] = dataObj.saveNow(check,galse, 'folder1', 'folder2') ;
% The following fixed save settings are used by saveNow when calling save:
%   - .mat file is given by dataObj.name
%   - internal variable stored in [dataObj.name].mat is named dataObj.type 
% ---

check       = true ;
% Check input for pressence of saving paramter check and relative. If present there values are
% checked and the input is removed from varargin.
if nargin > 2 ;
    for jj = 1:2:length(varargin)
        if isa(varargin{jj},'char' ) || isa(varargin{jj}, 'scalar') ;
            switch lower(varargin{jj})
%                 case {'relativepath', 'relative', 'rp'}
%                     relative    = varargin{jj+1} ;
                case {'check', 'c'}
                    check       = varargin{jj+1} ;
                otherwise
                    varargin = varargin(jj:end) ;
                    break ;
            end
        else
            varargin = varargin(jj:end) ;
            break ;
        end
    end
end
% (Since 23 JUN 16). Multiple input types are supported for high user flexibility. Similar to
% loadNow one can input cells, chars or a list of folders (less used). 
if length(varargin) == 1
    input = varargin{1} ;
    if isa(input, 'cell')   
        path = fullfile(input{:}) ;
    elseif isa(input, 'char')
        path = input ; 
    else
        % Throw an error if the given input is not a cell or char-string.
        errorStruct.message = ['Input class "', class(input),'" not recognised by loadNow.'] ;
        errorStruct.identifier = 'saveNow:InputClassNotRecognised';
        error(errorStruct) ;
    end
else
    input   = varargin(:) ; 
    if isa(input, 'cell') ;
        path = fullfile(input{:}) ;
    elseif isa(input, 'char')
        path = input ;
    else
        % Throw an error if the given input is not a cell or char-string.
        errorStruct.message = ['Input class "', class(input),'" not recognised by loadNow.'] ;
        errorStruct.identifier = 'saveNow:InputClassNotRecognised';
        error(errorStruct) ;
    end
end

% Ensure that path is a char.
if isa(path, 'cell') ;
    path = fullfile(path{:}) ;
end

% NOTE THAT :
% 1) save can NOT be used to store data in a folder which does not exists.
% 2) if a folder allready exists one can not "recreate" it using mkdir. (without producing
% warnings.)
% So, if the specified save-folder does not exists it has to be created first. (But only if it does 
% not exists.) Note that checking whether or not the folder exists takes time and is not desired when 
% a large batch of files needs to be stored for we which the user knows the folder allready exists.
if check
    if ~exist(path, 'file')
        mkdir(path) ;
    end
end

% Use MATLAB's internal function save to store object on HDD.
internalName = self.type ;
cmd = [internalName, '= self ;'] ; eval(cmd)  ;
savePath = fullfile(path, self.name) ;
save(savePath, internalName, self.version) ;
end