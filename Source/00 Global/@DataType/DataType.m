% DATATYPE

% - DataType is a global type generalising all features required to save and load an object to the 
% hdd. 
% - Morover DataType facilitates the basic building blocks in order to visualize specific intrinsic
% information contant in the Data object.
%
% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 8 AUG.

classdef DataType < handle
    
    %% PROPERTIES
    properties % General Properties
        name@char       = 'Data' ;                                                                  % [char string] Object name when saving.
        meta@struct     = struct() ;                                                                % [struct] Addionaly added properties. Ex. globalMaximum.
    end
    properties  % Visualization Properties
        verbose@logical = true ;
        debug@logical   = false ;
    end
    
    %% METHODS
    methods % SETUP METHOD
        
        %% BASE CONSTRUCTOR.
        % ===
        % This section contains methods pertaining to the construction of a dataObj and accessing / 
        % changing the state of its properties. 
        % ===
        
        function self = DataType(name, varargin)
            % ---
            % Basic constructor for an instance of DataType. The most essential propertie required
            % to save an object is a name. Therefor the object's name allways has to be given.
            % ---
            
            % Set Name for future reference.
            self.name   = name ;
            
            % Set given properties with non standard-values.
            self.set(varargin{:}) ;
        end
        %      
        function set(self, varargin)
            % ---
            % This global objec-class indepentent method allows users to easily define the state of 
            % any property. This is facilited trough the object-class specific setOne method. This 
            % method will check if the given property-name is part of the object. If not a new 
            % property is created within meta. 
            % Using the set methode multiple properties can be defined at the same time whereas
            % setOne can only change one property at once and is ment for internal use.
            % ex: 
            %           dataObj.set('name', someName, 'globalMax', 19e88) ;
            % ---
            for jj = 1:2:length(varargin)
                if isvarname(varargin{jj})
                    % Object-class specific internal method for defining the state of a property.
                    self.setOne(varargin{jj}, varargin{jj+1}) ;
                else
                    % Throw an error if the given name does not fullfile the requirements of a
                    % variable name for MATLAB.
                    switch class(varargin{jj})
                        case {'char', 'cell'}
                            errorStruct.message = ['Input argument "', varargin{jj},'" is not a valid field name.'] ;
                        otherwise
                            errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'] ;
                    end
                    errorStruct.identifier = 'DataType:set:InvalidFieldName';
                    error(errorStruct) ;
                end
            end
        end
        function setOne(self, input, value) 
            % ---
            % setOne is a class-specific setter mainly using the setDATA method. Other subclasses
            % are able to add specific properties or pseudo-properties to its list of fetchable
            % data.
            % ---
            setDATA(self, input, value) ;            
        end
        function setDATA(self, input, value)
            if sum(strcmp(properties(self), input))
                self.(input) = value ;
            else
                self.meta.(input) = value ;                                                 % unrecgonised properties are added to meta.
            end
        end
        function output = getOne(self, input)
            % ---
            % getOne is a class-specific getter mainly using the getDATA method. Other subclasses
            % are able to add specific properties or pseudo-properties to its list of adaptable
            % data.
            % ---
            output = getDATA(self, input) ;           
        end
        function output = getDATA(self, input)
            if sum(strcmp(properties(self), input))
                output = self.(input) ;
            elseif isfield(self.meta, input)
                output = self.meta.(input) ;
            else
                errorStruct.message = ['Input argument "', num2str(input),'" is non existent variable.'] ;
                errorStruct.identifier = 'DataType:getDATA:NonExistentVariable' ;
                error(errorStruct) ;
            end
        end
        function varargout = get(self, varargin)
            % This global objec-class indepentent method allows users to easily access the state of 
            % any property. This is facilited trough the object-class specific getOne method. This 
            % method will check if the given property-name is either part of the object or is stored
            % within the meta property.
            % Using the get methode multiple properties can be accessed at the same time whereas
            % getOne can only access one property at once and is ment for internal use.
            % ex: 
            %       [someName, 19e88] = dataObj.get('name', 'globalMax') ;
            varargout = cell(size(varargin)) ;
            for jj = 1:length(varargin)
                if isvarname(varargin{jj})
                    % Object-class specific internal method for accessing the state of a property.
                    varargout{jj} = self.getOne(varargin{jj}) ;
                else
                    % Throw an error if the given name does not fullfile the requirements of a
                    % variable name for MATLAB.
                    switch class(varargin{jj})
                        case 'char'
                            errorStruct.message = ['Input argument "', varargin{jj},'" is not a valid field name.'] ;
                        otherwise
                            errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'] ;
                    end
                    errorStruct.identifier = 'DataType:get:InvalidFieldName';
                    error(errorStruct) ;
                end
            end
        end          
        function add(self, varargin)
            % ---
            % This global objec-class indepentent method allows users to easily add information to a 
            % specified object. This is however highly property specific. For instance generaly add
            % applied to the name-property will result in a concatination of the current name with
            % new associated extension. Again this functionality is facilited by the object-class 
            % specific addOne method.
            % ex: 
            %       dataObj.set('name', someName, 'globalMax', 19e88) ;
            % ---
            for jj = 1:2:length(varargin)
                % The functionality for "name" is readily implemented and can not be adapted by
                % sub-classes.
                if isvarname(varargin{jj})
                    switch varargin{jj}
                        case 'name'
                            self.name = [self.name, varargin{jj+1}] ;
                        otherwise
                            self.addOne(varargin{jj}, varargin{jj+1}) ;
                    end
                else
                    % Throw an error if the given name does not fullfile the requirements of a
                    % variable name for MATLAB.
                    switch class(varargin{jj})
                        case 'char'
                            errorStruct.message = ['Input argument "', varargin{jj},'" is not a valid field name.'] ;
                        otherwise
                            errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'] ;
                    end
                    errorStruct.identifier = 'DataType:add:InvalidFieldName';
                    error(errorStruct) ;
                end
            end
        end
        function addOne(self, input, value)
             % ---
            % This object-class depentend method is internally used by add information to the given 
            % "input" property. Standard this method acts the same as setOne but implimention can 
            % differ in subclasse
            % ---
            if sum(strcmp(properties(self), input))
                self.(input) = value ;
            else
                self.meta.(input) = value ;
            end
        end
        
        function fieldVar = features(self)
            % ---
            % features extracts all property names of an object and there associated values. The
            % name-property is placed in front for easy access (or exclusion) by other functions
            % such as copy. 
            % ex:
            %       {'name', someName', 'meta', someStruct, ...} = dataObj.features ;
            % ---

            % Derive the property names of this object. Setup cell output.
            fieldNames  = fieldnames(self) ;
            fieldVar    = cell(1, 2*length(fieldNames)) ;

            % First property is always name.
            fieldVar{1} = 'name' ;
            fieldVar{2} = self.name ;
            ii = 3 ; 

            % Add all other properties and there state execept for name.
            for jj = 1:length(fieldNames) 
                if ~sum(strcmp(fieldNames{jj}, {'name'})) 
                    fieldVar{ii} = fieldNames{jj} ;
                    fieldVar{ii+1} = self.(fieldNames{jj}) ;
                    ii = ii + 2 ;
                end
            end
        end
        function output = isProperty(self, input)
            % ---
            % This simple methods checks wether a given string corresponds to the name of a
            % property.
            % ---
            fieldNames  = fieldnames(self) ; 
            output      = sum(strcmp(input, fieldNames)) ;
        end
        function MTdata = copy(self) %#ok<STOUT>
            % --- 
            % All dataType object belong to the handle superclass allowing for methods to adapt
            % properties without having to output a new object e.g:
            % dataObj.set('name', someName, 'globalMax', 19e88) ;
            % instead of 
            %       dataObj = dataObj.set('name', someName, 'globalMax', 19e88) ;
            % Allthough this usage is very natural it does pose some issues. For example when
            % overloading the + operator when would like the usage:
            % dataObj_New = dataObj_1 + dataObj_2.
            % Internally the creation of dataObj_New is made easier by copiying dataObj_1 and
            % adapting its parameters.
            % ---
            fieldVar            = features(self) ; %#ok<NASGU>
            cmd = ['MTdata = ',class(self),'(fieldVar{2},fieldVar{3:end}) ;'] ; eval(cmd) ;
        end

        %% SAVE METHODS.
        % ===
        % This section contains methods mainly used when saving / loading dataObj to or from the
        % HDD. 
        % ===
        
        function output = dataClass(self) 
            % ---
            % Allow the usage of a method dataObj.dataClass returning the specific subclass dataObj
            % belongs to.
            % ---
            output = class(self) ;
        end
        function output = dataType(self)
            % ---
            % Allow the usage of a method dataObj.dataType (analoge to dataClass) returning the 
            % specific subtype dataObj belongs to. Types are mainly used when saving objects and
            % determines the internal MATLAB-name given to object.
            % ex :
            %       dataObj.saveNow(path, fileName) ; 
            % saves dataObj in '\path\fileName.mat' where the name of the dataObj saved is 
            % dataObj.dataType. As a result, when loading '\path\fileName.mat' using
            %       load('\path\fileName.mat') 
            % a variable is added to the workspace which is called dataObj.dataType.
            % ---
            output = self.type ;
        end
        function output = type(self)
            % --- OLD USAGE.
            % Allow the usage of a method dataObj.type returning the specific subtype dataObj 
            % belongs to. Types are mainly used when saving objects
            % ---
            output = 'DTdata' ;
        end
        function output = version(self)
            % ---
            % This method is again mainly used by saveNow. It detemines which saving mode matlab
            % should use depending on the subclass of dataObj. This is mostly imporatant when saving
            % large systemMatrices which have a matrixDATA property containing over 4Gb of data.
            % ---
            switch lower(class(self))
                case {'dictionary', 'systemmatrix'}
                    output = '-v7.3' ;
                case {'radiofrequencydata'}
                    if self.isBatch
                        output = '-v7.3' ;
                    else
                        output = '-v7' ;
                    end
                otherwise
                    output = '-v7' ;
            end
        end       
        function displayProgress(self, displayStruct)
            % --- OLD CODE.
            % DataTypes have to ability to display progress of certain internal methods applied to 
            % them.
            % ---
            displayStruct.identifier = [self.fileType, ' ', self.fileExtension,':', displayStruct.identifier] ;
            disp(displayStruct) ;
        end
        
  
        %% Imaging Methods.
        
        function [outputDATA, outputINFO, outputPARM] = imageDATA(self, ~)
            % ---
            % Dummy generalised method adapted by each specific subclass outputting data ready to be
            % used by imageNow. Typically pixelSize and contrast mapping of RAW matrixDATA is
            % adjusted for before usefull imageDATA is obtained. Morever the imageging parameters
            % are adjusted according to the type of RAW input data.
            % ---
            outputDATA = zeros(2000,2000) ;
            outputINFO = self.name ;
            outputPARM = ImageParameter() ;
        end
    end
end