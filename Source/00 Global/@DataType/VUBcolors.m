% The VUBcolors methods allows to image lines, dots, ... using the VUB color scheme.
function output = VUBcolors(self, colorType, outputType)
if nargin < 3
    outputType = 'rgb' ;
end
switch lower(colorType)
    case {'pantone021', 'es', 'oranje'}
        col = [255 102 0  ] / 255 ;
    case {'pantone286', 'blauw'}
        col = [0   51  153] / 255 ;
    case {'pantone6', 'zwart'}
        col = [0   0   0  ] / 255 ;
        
    case {'pantone398', 'groen', 'green'}
        col = [171 178 2  ] / 255 ;
    case {'pantone418', 'donkerbruin', 'bruin'}
        col = [95  96  74 ] / 255 ;
    case {'pantone416', 'grijs', 'grey'}
        col = [135 136 127] / 255 ;
    case {'pantone7531', 'lichtbruin'}
        col = [127 115 88 ] / 255 ;
    case {'pantone576', 'we', 'donkergroen'}
        col = [80  120 17 ] / 255 ;
    case {'pantone2955', 'lk', 'donkerblauw'}
        col = [6   56  104] / 255 ;
    case {'pantone7427', 'rg', 'donkerrood'}
        col = [180 0   32 ] / 255 ;
    case {'pantone3135', 'pe', 'lichtblauw'}
        col = [1   142 159] / 255 ;
    case {'pantone423', 'tw', 'lichtgrijs'}
        col = [143 143 143] / 255 ;
    case {'pantone485', 'gf', 'lichtrood'}
        col = [255 13  0  ] / 255 ;
    case {'pantone117', 'lw', 'geel'}
        col = [217 178 0  ] / 255 ;
    otherwise
        errorStruct.message = ['Input Color "', colorType ,'" not recognised.'];
        errorStruct.identifier = 'DataType:VUBcolors:ColorNotRecognised';
        error(errorStruct) ;
end
switch lower(outputType)
    case 'rgb'
        output      = col ;
    case 'hex'
        colArray    = dec2hex(col * 255) ;
        output      = [colArray(1,:), colArray(2,:), colArray(3,:)] ;
    case 'cmyk'
        output      = rgb2cmyk(col) ;
    otherwise
        errorStruct.message = ['Output Type "', outputType ,'" not recognised.'];
        errorStruct.identifier = 'DataType:VUBcolors:OutputTypeNotRecognised';
        error(errorStruct) ;
end
%
end