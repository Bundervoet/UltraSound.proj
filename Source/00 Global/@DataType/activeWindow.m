function [noElementsLeftOfWindow, noElementsRightOfWindow] = activeWindow(Obj, x , transmission, width)
if isa(Obj, 'RadioFrequencyData') || isa(Obj, 'UltraSoundTransducer')
    if nargin < 2
        x = 0 ;
    end
    if nargin < 3
        transmission = 'receive' ;
    end
    if nargin < 4
        switch transmission
            case {0, 'receive', 'reception'}
                width = Obj.get('noReceive') ;
            case {1, 'transmit', 'transmission'}
                width = Obj.get('noTransmit') ;
        end
    end
    
    % Distance between two neighboring centers of elements on the probe.
    elementPitch    = Obj.get('elementPitch') ;
    
    % Full probe width.
    noElements      = Obj.get('noElements') ;
    
    %     probeWidth      = (noElements - 1) * elementPitch ;
    %     noElementsLeftOfWindow_unbound = round((x + probeWidth/2)/elementPitch - width/2) ;
    
    %   "round(x/elementPitch) + noElements/2" Determines how many elements are left from the
    %   central focus line.
    %   " - width/2 substracts the amount needed for the current window"
    noElementsLeftOfWindow_unbound = round(x/elementPitch) + noElements/2 - width/2 ;
    noElementsLeftOfWindow  = min(max(noElementsLeftOfWindow_unbound,0), noElements - width) ;
    noElementsRightOfWindow = noElements - noElementsLeftOfWindow - width ;                         % Number of elements after active window. [INT:double]
    
else
    noElementsLeftOfWindow = 0 ;
    noElementsRightOfWindow = 0 ;
end