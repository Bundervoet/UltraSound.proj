%% WRITE NOW

% Method facilitating the export of matrixDATA to image files on HDD.

function writer(self, name, path, varargin)

%% INITIALIZE IMAGER

if nargin < 3
    path = [] ;
end
if isempty(name)
    name = self.name ;
end

% Load and addapt imaging Parameters.
par                     = ImageParameter(class(self), varargin{:}) ;

% Load correct input image.
[imageMatrix, ~, par]   = self.imageDATA(par) ;

%% MAT2GRAY OF INPUT MATRIX

exportData = round(par.transform(imageMatrix)) ;
% exportData = round(mat2gray(par.transform(imageMatrix), [0, par.contrast * 2^8])*256) ;

if isa(path, 'cell') 
    path = fullfile(path{:}) ;
end

if par.check
    if ~exist(path, 'file')
        mkdir(path) ;
    end
end

savePath = fullfile(path, [name '.', par.format]) ;
imwrite(uint8(exportData), par.cmap, savePath) ;
end
