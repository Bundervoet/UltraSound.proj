function output = mtimes(first, second)
% ---
% Extend * to the usage of dataMatrixP = dataMatrix1 * dataMatrix2 ;
% ---
switch class(first)
    case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
        switch class(second)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                output = builtin('mtimes', first, second) ;
            otherwise
                output = builtin('mtimes', double(first), second.getMatrixDATA()) ;
        end
    otherwise
        switch class(second)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                output = builtin('mtimes', first.getMatrixDATA(), double(second)) ;
            otherwise
                output = builtin('mtimes', first.getMatrixDATA(), second.getMatrixDATA()) ;
        end
end
end