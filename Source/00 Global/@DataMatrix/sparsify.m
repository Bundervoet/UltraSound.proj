function output = sparsify(self, sparsifyingFactor)

% Try to use a global normalizationFactor. If not supplied use self determined local factor. 
if nargin < 2
    if self.matrixINFO.sparsityFactor == 0
        sparsifyingFactor = 1e-3 ;
    else
        sparsifyingFactor = self.matrixINFO.sparsityFactor ;
    end
elseif sparsifyingFactor == 0
    sparsifyingFactor = 1e-3 ; 
else
    sparsifyingFactor = 1 ;
end

% Retrieve signalDATA
inputDATA = self.getMatrixDATA ;

% Find indices of small pixel values
indices = abs(inputDATA) < (sparsifyingFactor * self.absMaximum) ;

% Set selected indices to zero.
inputDATA(abs(inputDATA) < (sparsifyingFactor * self.absMaximum)) = 0 ;

% Store sparsified Matrix. 
self.setMatrixDATA(inputDATA) ;
self.matrixINFO.setSparsified(sparsifyingFactor) ; 

% Output amount of indices set to zero.
output = sum(indices(:)) ;
end