
function output = ldivide(first, second)
% ---
% Extend .\ to the usage of dataMatrixP = dataMatrix1 .\ dataMatrix2 ;
% ---
switch class(first)
    case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
        % B = a .\ dataMatrix2 ;
        output = builtin('ldivide', first, second.get('matrixDATA')) ;
    otherwise
        switch class(second)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                % dataMatrixR = dataMatrix1 .\ a ;
                output = first.copy ;
                output.setMatrixDATA(builtin('ldivide', first.get('matrixDATA'), second)) ;
            otherwise
                % dataMatrixR = dataMatrix1 .\ dataMatrix2 ;
                output = second.copy ;
                output.setMatrixDATA(builtin('ldivide', first.get('matrixDATA'), second.get('matrixDATA'))) ;
        end
end
end