% APODIZE SIGNAL

% Function used to apodize the columns of 
function apodize(self, window) % ( INTERNAL FUNCTION )
%
if nargin < 2 
    window = 'hanning' ;
end
n           = self.size(1) ;
m           = self.size(2) ;

if m > 1
    % Calculate apodisation window.
    cmd = ['receiveWindow = transpose(',window,'(m)) ;'] ;
    eval(cmd)                                                                                       % Apodisation window.
    apoR = kron(receiveWindow, ones(n,1)) ;
    
    % Apply apodisation to inserted RFsignal.
    self.setMatrixDATA(apoR .* self.getMatrixDATA) ;
    
    % Remember that signal is apodized
    self.matrixINFO.setApodized(window) ; 
end
end