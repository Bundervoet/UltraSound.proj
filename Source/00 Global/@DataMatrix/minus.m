function output = minus(self, input)
% ---
% Extend - to the usage of dataMatrixP = dataMatrix1 - dataMatrix2 ;
% ---
switch class(self)
    case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
        % Allow the usage of B = A - dataMatrix1 ;
        output = builtin('minus', self, input.get('matrixDATA')) ;
    otherwise
        output = self.copy ;
        switch class(input)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                % dataMatrixP = dataMatrix1 - A ;
                output.setMatrixDATA(builtin('minus', self.get('matrixDATA'), input)) ;
            otherwise
                % dataMatrixP = dataMatrix1 - dataMatrix2 ;
                output.setMatrixDATA(builtin('minus', self.get('matrixDATA'), input.get('matrixDATA'))) ;
        end
end
%
end