% DATAMATRIX

% - DataMatrix is a global class generalising all objects which are made up of:
%   * a central property containing the bulk of important data, typically refered to as the
%   matrixDATA property.
%   * secundary properties which acts a meta-data for the central data-property. These values can
%   pertain to the simulation settings used to create the dataObj and other properties such as the
%   relative pixel size of the matrix stored in matrixDATA.
% NOTE: Allthough the principal data-property is refered to as matrixDATA this exact property
% genearlly does not exist.
% ex. for RadioFrequencyData the data-property is called signalDATA. It can however be accessed
% using dataObj.getMatrixDATA within functions and dataObj.matrixDATA in scipts.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 8 AUG.

classdef DataMatrix < DataType

    %% PROPERTIES
    properties % properties allways associated with matrixDATA
        matrixINFO@MatrixInfo   = MatrixInfo() ;                                                   % Initialize Status of signalDATA.
        pixelSize@double       	= [0 0] ;                                                           % Size of pixel in reality in [m].
    end
  
    %% METHODS
    methods % SETUP METHOD
    
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a dataMatrix and accessing
        % / changing the state of its properties. Moreover because dataMatrices are in escence
        % matrices all operators and reference tools are overloaded as well.
        % ===
        function DaM = DataMatrix(name, varargin)
            % ---
            % Set-up is completely facilitated by the upperclass DataType which internally uses this
            % class' version of setOne to acces and alter internal properties.
            % ---          
            DaM@DataType(name, varargin{:})             
        end          
        function output = type(DaM)
            output = 'DTmatrix' ;
        end
        function setOne(DaM, input, value) 
            % ---
            % setOne is a class-specific setter mainly using the setDATA method. Other subclasses
            % are able to add specific properties or pseudo-properties to its list of fetchable
            % data.
            % ---
            setDATA(DaM, input, value) ;            
        end
        function setDATA(DaM, input, value)
            % ---
            % DataMatrix is able to set the ambigious properties matrixDATA and coeffDATA 
            % (if they exist). For this the method relies on the class specific method setMatrixDATA 
            % and setCoeffDATA. Unrecognised properties are added to the meta-list.
            % ex. dataMatrix.setOne('pixelsize', [1 0]) : dataMatrix.setOne('matrixDATA', [..]) ;
            % ---
            switch lower(input)
                case 'matrixdata'
                    DaM.setMatrixDATA(value) ;                                                     % class-specific internal method.
                case 'coeffdata'
                    DaM.setCoeffDATA(value) ;                                                      % class-specific internal method.
                case 'normalizationfactor'
                    DaM.matrixINFO.normalizationFactor = value ; 
                otherwise
                    if sum(strcmp(properties(DaM), input))
                        DaM.(input) = value ;
                    else
                        DaM.meta.(input) = value ;                                                 % unrecgonised properties are added to meta.
                    end
            end  
        end
        function output = getOne(DaM, input) 
            % ---
            % getOne is a class-specific getter mainly using the getDATA method. Other subclasses
            % are able to add specific properties or pseudo-properties to its list of adaptable
            % data.
            % ---
            output = getDATA(DaM, input) ;           
        end
        function output = getDATA(DaM, input)
            % ---
            % Like in setMatrix, DataMatrix allows the user access the ambigious properties matrixDATA
            % and coeffDATA (if they exist). For this the method relies on the class specific method
            % setMatrixDATA and setCoeffDATA. Unrecognised properties are accessed through the meta-list.
            % ex. [1 0] = dataMatrix.getOne('pixelsize') : [..] = dataMatrix.getOne('matrixDATA') ;
            % ---
            switch lower(input)
                case 'matrixdata'
                    output = DaM.getMatrixDATA ;
                case 'coeffdata'
                    output = DaM.getCoeffDATA ;
                case 'normalizationfactor'
                    output = DaM.matrixINFO.normalizationFactor ;
                case {'max', 'maximum', 'min', 'minimum', 'absmaximum', 'absminimum', 'mean', ...
                        'mode', 'std', 'var', 'size', 'numel', 'nopixels', 'height', 'norows', ...
                        'width', 'nocolumns', 'pixelheight', 'pixelwidth'}
                    output = DaM.(input) ;
                otherwise
                    if sum(strcmp(properties(DaM), input))
                        output = DaM.(input) ;
                    elseif isfield(DaM.meta, input)
                        output = DaM.meta.(input) ;
                    else
                        errorStruct.message = ['Input argument "', num2str(input),'" is non existent variable.'] ;
                        errorStruct.identifier = 'DataMatrix:getDATA:NonExistentVariable' ;
                        error(errorStruct) ;
                    end
            end         
        end    
        
        
        %% VALUE METHODS
        % ===
        % In this section we write some general functions able to access some pseudo-information
        % contained in the values of the central matrixDATA such as the maximal pixel value etc..
        % ===
        function output = getMatrixDATA(DaM)
            output = zeros(0,0) ;
        end
        function setMatrixDATA(~, ~)
            % The DataMatrix is an abstract class generalising functionality over a wide-spread of
            % subclasses. Each of these subclasses include a central-DATA property. Since DataMatrix
            % is an abstract generalization it does not contain a concrete matrixDATA property. Note
            % that this central property has a different name for each subclass e.g:
            %   - RadioFrequencyData    : signalDATA ;
            %   - BrightnessModeImage   : logEnveloppeDATA ;
            %   - SystemMatrix          : matrixDATA ; 
        end
        function absMatrix = getOriginalDATA(DaM)
            % Return to original Matrix Data (upto absolute value).
            absMatrix = DaM.matrixINFO.transformed.invertTransformation(DaM.getMatrixDATA, DaM.matrixINFO.normalizationFactor) ;
        end
        function output = max(DaM)
            output = DaM.maximum ;
        end
        function output = maximum(DaM)
            output = full(double(max(reshape(DaM.getMatrixDATA,1,[])))) ;
        end
        function output = absMaximum(DaM)
            output = full(double(max(abs(reshape(DaM.getMatrixDATA,1,[]))))) ;
        end
        function output = min(DaM)
            output = DaM.minimum ;
        end
        function output = minimum(DaM)
            output = full(double(min(reshape(DaM.getMatrixDATA,1,[])))) ;
        end
        function output = absMinimum(DaM)
            output = full(double(min(abs(reshape(DaM.getMatrixDATA,1,[]))))) ;
        end
        function output = norm(DaM, type)
            if nargin < 2
                type = 2 ;
            end
            output = norm(full(reshape(DaM.getMatrixDATA,1,[])), type) ;
        end
        
        %% STATISTICAL METHODS        
        % ===
        % In this section contains some "overloaded" varsion of statistical operators applicable on
        % the matrixDATA.
        % ===             
        function output = mean(DaM)
            output = full(double(mean(reshape(DaM.getMatrixDATA,1,[])))) ;
        end
        function output = mode(DaM)
            output = full(double(mode(reshape(DaM.getMatrixDATA,1,[])))) ;
        end
        function output = quantile(DaM, input)
            output = full(double(quantile(reshape(DaM.getMatrixDATA,1,[]), input))) ;
        end
        function output = std(DaM)
            output = full(double(std(reshape(DaM.getMatrixDATA,1,[])))) ;
        end
        function output = var(DaM)
            output = full(double(var(reshape(DaM.getMatrixDATA,1,[])))) ;
        end
        
        
        %% STATE QUERRY METHODS      
        % ===
        % Previously the state of the MatrixData was monitored using suffisticated statiscal
        % measures. Now For computational ease these states are stored in a MatrixState setting and
        % can simply be called by querry methods.
        % ===
        function answer = isApodized(state)
            answer = DaM.matrixINFO.apodized.isApodized ;            
        end
        function answer = isTransformed(state)
            answer = DaM.matrixINFO.transformed.isTransformed ;            
        end
        function answer = isRAW(state)
            answer = (DaM.matrixINFO.transformed == TransformationState.RAW) ;
        end
        function answer = isNormalized(state)
            answer = (DaM.matrixINFO.transformed == TransformationState.normalized) ;
        end
        function answer = isDecibel(state)
            answer = (DaM.matrixINFO.transformed == TransformationState.decibel) ;
        end
                
        %% SIZE METHODS        
        % ===
        % In this section we write some general functions able to access some pseudo-information
        % contained in the shape of the matrixDATA such as the maximal pixel value etc..
        % ===
        function output = size(DaM, dim)
            output = full(size(DaM.getMatrixDATA)) ;
            
            if nargin == 2
                output = output(dim) ;
            end
        end
        function output = numel(DaM)
            output = 1 ;
        end
        function output = noPixels(DaM)
            output = full(numel(DaM.getMatrixDATA)) ;
        end
        function output = height(DaM)
            output = full(size(DaM.getMatrixDATA,1)) ;
        end
        function output = noRows(DaM)
            output = full(size(DaM.getMatrixDATA, 1)) ;
        end
        function output = width(DaM)
            output = full(size(DaM.getMatrixDATA,2)) ;
        end
        function output = noColumns(DaM)
            output = full(size(DaM.getMatrixDATA, 2)) ;
        end
        
        
        %% PIXEL METHODS.        
        % ===
        % The following methods allow for extra functionality when quering dataMatrix for it's pixel
        % size. Pixelsize denotes the physical dimensions represented by 1 pixel in the matrixDATA.
        % ===     
        function output = pixelHeight(DaM)
            output = DaM.pixelSize(1) ;
        end
        function output = pixelWidth(DaM)
            output = DaM.pixelSize(2) ;
        end
    end
end