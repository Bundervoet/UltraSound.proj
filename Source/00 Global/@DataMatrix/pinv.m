function output = pinv(self)
% ---
% Extend inv to the usage of dataMatrixP = pinv(dataMatrix1) ;
% NOTE that is colMASK and rowMASK exist these will be switched to account for the
% transposition of columns and rows.
% ---
output = self.copy ;
output.setMatrixDATA(pinv(self.get('matrixDATA'))) ;

% add colMASK and rowMASK to output.
features = self.features ; features = features(1:2:end) ;
if sum(strcmp('rowMASK', features)) && sum(strcmp('colMASK', features)) 
    output.rowMASK = self.colMASK ; output.colMASK = self.rowMASK ;
end
end