function output = mrdivide(first, second)
% ---
% Extend / to the usage of dataMatrixP = dataMatrix1 ./ dataMatrix2 ;
% ---
switch class(first)
    case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
        % B = a / dataMatrix2 ;
        output = builtin('mrdivide', first, second.get('matrixDATA')) ;
    otherwise
        output = first.copy ;
        switch class(second)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                % dataMatrixR = dataMatrix1 / a ;
                output.setMatrixDATA(builtin('mrdivide', first.get('matrixDATA'), second)) ;
            otherwise
                % dataMatrixR = dataMatrix1 / dataMatrix2 ;
                output.setMatrixDATA(builtin('mrdivide', first.get('matrixDATA'), second.get('matrixDATA'))) ;
        end
end
end
