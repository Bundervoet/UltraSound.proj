function output = power(self, input)
% ---
% Extend .^ to the usage of dataMatrixP = dataMatrix1 .^ dataMatrix2 ;
% ---
switch class(self)
    %                 case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
    %                     % B = a .^ dataMatrix2 ;
    %                     output = builtin('power', self, input.matrixDATA) ;
    otherwise
        output = self.copy ;
        switch class(input)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                % dataMatrixR = dataMatrix1 .^ a ;
                output.setMatrixDATA(builtin('power', self.get('matrixDATA'), input)) ;
                %                         otherwise
                %                             % dataMatrixR = dataMatrix1 .^ dataMatrix2 ;
                %                             output.setMatrixDATA(builtin('power', self.matrixDATA, input.matrixDATA)) ;
        end
end
end