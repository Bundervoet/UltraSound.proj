function output = plus(first, second)
% ---
% This method allows the usage of dataMatrixP = dataMatrix1 + dataMatrix2 ; by accesing
% the central matrixDATA property. Note that, since dataMatrices are subclasses of the
% handle-class a new dataMatrixP has to be created or in this case copied from
% dataMatrix1 in order for the object to be selfsustaining, meaning that it's properties
% would nog change when changing the properties of another object!
% ---
switch class(first)
    case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
        % Allow the usage of B = A + dataMatrix1 ;
        output = builtin('plus', first, second.get('matrixDATA')) ;
    otherwise
        output = first.copy ;
        switch class(second)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                % Allow the usage of dataMatrixP = dataMatrix1 + A ;
                output.setMatrixDATA(builtin('plus', first.get('matrixDATA'), second)) ;
            otherwise
                % Allow the usage of dataMatrixP = dataMatrix1 + dataMatrix2 ;
                output.setMatrixDATA(builtin('plus', first.get('matrixDATA'), second.get('matrixDATA'))) ;
        end
end
end