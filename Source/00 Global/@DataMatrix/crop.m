function output = crop(self, type, lowerBound, upperBound) % ( INTERNAL FUNCTION )

% Auto select type if not provided
if nargin < 2
    type = 'row' ;
end

% Try to use a global Bounds. If not supplied use self determined local factor. 
if nargin < 3
    if sum(self.matrixINFO.bounds.(type)(:)) == 0
        bounds = [1 self.height] ;
    else
        bounds = self.matrixINFO.bounds.(type) ;
    end
elseif nargin < 4 && length(lowerBound) > 1 && sum(lowerBound(:)) > 0
    bounds = lowerBound ;
else   
    bounds = [lowerBound, upperBound] ;
end

% Crop Signal
switch type
    case 'row'
        inputDATA    = [self.getMatrixDATA ; zeros(bounds(2) - self.height, self.width)] ;
        self.setMatrixDATA(inputDATA(bounds(1) : bounds(2), :)) ;
    case 'col'
        inputDATA    = [self.getMatrixDATA, zeros(self.height, bounds(2) - self.width)] ;
        self.setMatrixDATA(inputDATA(:, bounds(1) : bounds(2))) ;
end

self.matrixINFO.setCropped(type, bounds) ;
output = bounds(2) - self.height ;
end