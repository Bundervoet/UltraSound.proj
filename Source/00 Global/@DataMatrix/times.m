function output = times(self, input)
% ---
% Extend .* to the usage of dataMatrixP = dataMatrix1 .* dataMatrix2 ;
% NOTE: output is always [double matrix].
% ---
switch class(self)
    case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
        % B = a .* dataMatrix1 ;
        output = builtin('mtimes', self, input.get('matrixDATA')) ;
    otherwise
        switch class(input)
            case {'double', 'single', 'uint8', 'uint16', 'uint32', 'uint64'}
                % dataMatrixP = dataMatrix1 .* a ;
                output = builtin('mtimes', self.get('matrixDATA'), input) ;
            otherwise
                % dataMatrixP = dataMatrix1 .* dataMatrix2 ;
                % NOTE: it is unlikely that either dataMatrix1 or dataMatrix2 consists
                % of just a double and not a vector/matrix...
                output = builtin('mtimes', self.get('matrixDATA'), input.get('matrixDATA'))  ;
        end
end
end