%% PUT DATA IN DECIBEL SCALE.

% This function transforms the data samples in a datamatrix to the log decibel scale. This ensures
% better feature recognitian in some situation. 
% NOTE that transformations are not stacked, i.e. when a previous transformation has been performed
% the current transformation will try to undo the previous changes before continuing itself. 
function output = transform(self, type, normalizationFactor)
% Auto select type if not provided
if nargin < 2
    type = 'normalized' ;
end

% Try to use a global normalizationFactor. If not supplied use self determined local factor. 
if nargin < 3
    if self.matrixINFO.normalizationFactor == 0
%         disp('1') ;
        normalizationFactor = self.absMaximum ;
    else
%         disp('2') ; 
        normalizationFactor = self.matrixINFO.normalizationFactor ;
    end
elseif normalizationFactor == 0
%     disp('3') ;
    normalizationFactor = self.absMaximum ;
end

% Return to absolute version of original Matrix.
absMatrix                   = self.getOriginalDATA ;

% Transform to desired interval and contrast.
self.matrixINFO.transformed = TransformationState.(type) ;
transformedMatrix           = self.matrixINFO.transformed.forwardTransformation(absMatrix, normalizationFactor) ;

% Return interval bound.
output = [min(transformedMatrix (:)) max(transformedMatrix (:))] ;
self.set('matrixDATA', transformedMatrix) ;
self.matrixINFO.set(type, normalizationFactor) ;
end

