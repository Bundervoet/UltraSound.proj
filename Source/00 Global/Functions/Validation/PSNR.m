% PEAK SIGNAL TO NOISE RATIO.

% Simple Validation Function

% Created by Shaun Bundervoet on 07 MAR 17.
% Latest version 07 MAR 17.
function result = PSNR(comp, ref, diff)
ref = double(ref); comp = double(comp);
if nargin < 3
    diff = (max(ref(:))-min(ref(:))).^2 ;
end
result = 10*log10(diff/MSE(comp, ref)) ;
end