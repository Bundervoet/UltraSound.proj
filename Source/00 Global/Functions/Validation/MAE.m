% MEAN ABSOLUTE ERROR

% Simple Validation Function

% Created by Shaun Bundervoet on 07 MAR 17.
% Latest version 07 MAR 17.

function result = MAE(comp, ref)
ref = double(ref); comp = double(comp) ;
result = sum(abs(ref(:)-comp(:))) / numel(ref) ;
end