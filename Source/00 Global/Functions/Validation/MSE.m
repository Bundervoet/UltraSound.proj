% MEAN SQUARED ERROR

% Simple Validation Function

% Created by Shaun Bundervoet on 07 MAR 17.
% Latest version 07 MAR 17.

function result = MSE(comp, ref)
ref = double(ref); comp = double(comp) ;
result = sum((ref(:)-comp(:)).^2) / numel(ref) ;
end