% PEAK SIGNAL TO NOISE RATIO.

% Simple Validation Function

% Created by Shaun Bundervoet on 07 MAR 17.
% Latest version 07 MAR 17.

function result = PSNR2(comp, ref, diff)
ref = double(ref); comp = double(comp); 
if nargin < 3
    switch class(ref)
        case 'uint8'
            diff = 2^8 - 1 ;
        case 'uint16'
            diff = 2^16 - 1;
        case 'uint32'
            diff = 2^32 - 1;
        case 'uint64'
            diff = 2^64 - 1;
        otherwise
    diff = (max(ref(:))-min(ref(:))).^2 ;
    end
end
result = 10*log10(diff/MSE(comp, ref)) ;
end