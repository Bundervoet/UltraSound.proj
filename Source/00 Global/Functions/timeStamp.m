function output = timeStamp
output = ['[',datestr(datetime('now'),'HH:MM:SS.FFF'), '] '] ;
end