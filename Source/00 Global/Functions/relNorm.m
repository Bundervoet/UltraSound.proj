function value = relNorm(approx,ref)
% value = 10 * log10((max(ref(:))^2)/MSE(approx(:),ref(:))) ;
% value = 10 * log10((128^2)/MSE(approx,ref)) ;

% n       = numel(ref) ;
% msesqr  = 1/n * sqrt(norm(approx - ref,2)) ;
% msesqr  = 1/n * sum((approx(:) - ref(:)).^2) ;

denominator = norm(approx(:) - ref(:), 2) ;
nominator   = norm(ref(:), 2) ;
value   = denominator/nominator ;
end