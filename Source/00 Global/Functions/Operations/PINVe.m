% Thresholded Moorse-Penrose PseudoInverse

function X = PINVe(A, tol)

[U,S,V] = svd(A, 'econ');
s = diag(S);
if nargin < 2
    tol = max(s) / 100 ;
elseif isa(tol, 'uint8') || isa(tol, 'uint16') || isa(tol, 'uint32')
    tol = max(s) / double(tol) ;
end
r1          = sum(s > tol)+1;
V(:,r1:end) = [];
U(:,r1:end) = [];
s(r1:end)   = [];
s           = 1./s(:);
X           = (V.*s.')*U' ;
end