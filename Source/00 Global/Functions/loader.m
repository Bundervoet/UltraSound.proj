% LOADNOW

% Enable the user to load an object AND attach a choosen variablename to the loaded object. When
% using the build-in load from MATLAB this naming will result in a structure-object with the given
% name containing the loaded object, which is not desirable.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 23 JUN 16.

function output = loader(varargin)
% ---
% Compared to load this functions allows the user:
%   - To use a wide arrange of methods to input a path including:
%           O1 loadNow({'folder1/folder2/file'}) ; 
%           O2 loadNow({'folder1/folder2/file.mat'}) ; 
%           A1 loadNow({'folder1','folder2','file'}) ; 
%           A2 loadNow({'folder1','folder2','file.mat'}) ; 
%           B1 loadNow('folder1/folder2/file') ; 
%           B2 loadNow('folder1/folder2/file.mat') ; 
%           C1 loadNow('folder1/folder2','file') ; 
%           C2 loadNow('folder1/folder2','file.mat') ; 
%           D1 loadNow('folder1','folder2','file') ; 
%           D2 loadNow('folder1','folder2','file.mat') ; 
%   - Choose the name of the output variable in the MATLAB Workspace using:
%           * varName = loadNow({'folder1','folder2','file'}) ; 
% This method then addapts the input to an input recgoniseabel by load.
% ---

% Check the variety of input methods:
% 1) according to the number of inputs ;
% 2) the class of the input ;
% 3) whether or not the end contains .mat ;
if nargin == 1                                      % Type == A. or B.
    input = varargin{1} ;
    name = '' ;
    if isa(input, 'cell')                            
        if length(input) == 1                       % Type == O.
            path = '' ;
            name = input{1} ;
        else                                        % Type == A.
            path = fullfile(input{1:(end-1)}) ;
            name = input{end} ;
        end
    elseif isa(input, 'char')                       % Type == B.
        path = '' ;
        name = input ; 
    else
        % Throw an error if the given input is not a cell or char-string.
        errorStruct.message = ['Input class "', class(input),'" not recognised by loadNow.'] ;
        errorStruct.identifier = 'loadNow:InputClassNotRecognised';
        error(errorStruct) ;
    end
else                                                % Type == C. or D.
    input   = varargin(1:(end-1)) ; 
    name    = varargin{end} ;
    if isa(input, 'cell') 
        path = fullfile(input{:}) ;
    elseif isa(input, 'char')
        path = input ;
    else
        % Throw an error if the given input is not a cell or char-string.
        errorStruct.message = ['Input class "', class(input),'" not recognised by loadNow.'] ;
        errorStruct.identifier = 'loadNow:InputClassNotRecognised';
        error(errorStruct) ;
    end
end
% check wether .mat has allready been added
if isempty(strfind(name,'.mat'))                  % Type == .1
    name = [name,'.mat'] ;
end
% Create a char clas path. (With builtin extra garentee that cells are avoided.
switch class(path)
    case 'cell'
        directory    = fullfile(path{:}, name) ;
    case 'char'
        directory    = fullfile(path, name) ;
end
% directory
% Test if output data for the specified data type can be found on the HDD.
% directory 
verification = exist(directory, 'file') ;
if verification
    % Attach loaded data to output.
    data    = load(directory) ;
    names   = fieldnames(data) ;
    output  = data.(names{1}) ;
else
    % Trow an error if the data can not be located on the HDD.
    errorStruct.message = ['Input data "', name,'" can not be located on disk'] ;
    errorStruct.identifier = 'loadNow:InputNotOnHDD';
    error(errorStruct) ;
end
end


