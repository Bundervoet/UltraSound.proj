% SOUNDWAVE

% Soundwave generalises the  excitation wave used by an UlraSound Transducer. As such it adjusts for 
% alterations made to one parameter by adapting the related parameters.

% Created by Shaun Bundervoet. 
% Latest version 8 AUG 16.
classdef SoundWave < handle

    %% PROPERTIES
    properties % Physical constants 
        f0 = 3.5e6 ;                                                                                % [double] Transducer center frequency. [Hz]
        fs = 25e6 ;                                                                                 % [double] Sampling frequency. [Hz]
        c  = 1540 ;                                                                                 % [double] Speed of sound. [m/s]
        lambda ;                                                                                    % [double] Wavelength [m]
    end

    %% METHODS
    methods

        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a instance and accessing
        % / changing the state of its properties.
        % ===
        function self = SoundWave(f0, fs, c, lambda)
            % ---
            % Unlike most constructor methods used in this project Soundwave requires it's input to
            % be given in a specific order, depending on there importance/specificity. 
            % ---
            if nargin > 0
                if ~isempty(f0)
                    self.f0 = f0 ;                                                                      % Transducer center frequency. [Hz]
                end
            end
            if nargin > 1
                if ~isempty(fs)
                    self.fs = fs ;                                                                      % Sampling frequency. [Hz]
                end
            end
            if nargin > 2
                if ~isempty(c)
                    self.c  = c ;                                                                       % Speed of sound. [m/s]
                end
            end
            if nargin > 3
                if ~isempty(lambda)
                    self.lambda = lambda ;                                                              % Wavelength [m]
                end
            else
                self.lambda = self.c / self.f0 ;
            end
        end

        
        %% SET/GET Methods
        % ===
        % Because of the depencies between wave variables, specialised set methods need to be
        % provided for each parameter wich automatically adapts all other parameters to the new
        % situation.
        % ===
        function output = setFrequency(self, f0)
            
            if nargin == 2
                self.f0 = f0 ;
            end
            self.lambda = self.c / self.f0 ;
            output = self.f0 ;
        end
        function output = setSpeed(self, c)
            if nargin == 2
                self.c = c ;
            end
            self.lambda = self.c / self.f0 ;
            output = self.c ;
        end
        function output = setLambda(self, lambda)
            if nargin == 2
                self.lambda = lambda ;
            end
            self.f0 = self.c / self.lambda ;
            output = self.f0 ;
        end
        function output = setSampling(self, fs)
            if nargin == 2
                self.fs = fs ;
            else
                output = self.fs ;
            end
        end
        
        
        %% Convert METHODS
        % ===
        % These methods allow to convert distance into sample distance or time travelled, and
        % vice-versa.
        % ===
        function output = distance2time(self, distance)
            output = distance / self.c ;
        end
        function output = time2distance(self, time)
            output = time * self.c ;
        end
        function output = sample2time(self, sample)
            output = sample / (2 * self.fs) ;
        end
        function output = time2sample(self, time)
            output = floor(time * 2* self.fs) ;
        end
        function output = distance2sample(self, distance)
            % output = distance * self.fs * 2 / self.c ;
            output = floor(self.time2sample(self.distance2time(distance))) ;             
        end
        function output = sample2distance(self, sample)
            % output = sample * self.c / (2 * self.fs) ;
            output = self.time2distance(self.sample2time(sample)) ; 
        end
        
        
        %% OUTPUT METHODS
        function output = sampledWave(self, length)
            % ---
            % With this function a sampled sin wave version of the SoundWave can be outputted. 
            % ---
            if nargin < 2
                length = 2 ;
            end
            output = sin(2 * pi * self.f0 * (0: 1/self.fs: (length/self.f0))) ;
        end
    end  
end