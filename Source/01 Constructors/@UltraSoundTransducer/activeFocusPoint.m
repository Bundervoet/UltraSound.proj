function output = activeFocusPoint(self, line, shift, focusPoint)
if nargin < 3
    shift = 0 ;
elseif isempty(shift)
    shift = 0 ;
end
if nargin < 4
    focusPoint = [] ;
end
if isempty(focusPoint)
    if line == 0
        focusPoint = ThreeDimensionalPoint(0, 0, self.zFocus);                                       % Centerline. [m]
    else
        x = (line - 1) * self.get('lineWidth') - self.get('imageWidth')/2 ;                             % Centerline. [m]
        focusPoint = ThreeDimensionalPoint(x, 0, self.zFocus);
    end
end
output = focusPoint + ThreeDimensionalPoint(shift, 0, 0) ;
end