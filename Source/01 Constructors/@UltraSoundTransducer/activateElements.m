% ACTIVATE ELEMENTS

% Designate which elements need to be activated. Together with the focus point this will determine the
% direction of the acoustic beam. The simulator will calculate in what sequence to active the
% elements to obtain the desire direction for the acoustic beam. The apodisation is also applied here.
function focusPoint = activateElements(self, line, focusPoint)

% DETERMINE X-COORD OF FOCUS POINT. (= IMAGING LINE.)

% The imaging lines need to be evenly distributed within the field of view. The x-coordinate of the
% beams focus point for each imaging line.
if nargin < 2
    line = 0 ;
elseif isempty(line)
    line = 0 ;
end
if nargin < 3
    focusPoint = [] ;
end

focusPoint  = activeFocusPoint(self, line, [], focusPoint) ;
x           = focusPoint.xCoordinate ;

% DETERMINE Z-COORD(S) OF FOCUS POINT(S). (= IMAGING DEPTH.)

% Set the transmit focus for this diretion with the proper reference point. (FIELD II)
switch self.transmitFocus 
    case FocusSetting.no % Don't specify the focus depth.
        xdc_center_focus(self.transmitHandleFIELDII, [x 0 0]);                                      % Designate center of focus to Transmitter
    case FocusSetting.yes % Use fix focus point z_focus.
        xdc_center_focus(self.transmitHandleFIELDII, [x 0 0]);                                      % Designate center of focus to Transmitter
        xdc_focus(self.transmitHandleFIELDII, 0, [x 0 self.zFocus]) ;                               % Designate focus point to Tranmitter
    otherwise
        error('!! Dynamic focusing not yet implemented !!') ;
end

% SET TRANSMIT APERTURE
self.setAperture(x, 'transmit') ;
xdc_apodization(self.transmitHandleFIELDII, 0, self.transmitAperture);                              % Apply apodisation to Transmitter.

% DETERMINE Z-COORD(S) OF FOCUS POINT(S). (= IMAGING DEPTH.)
% Set the receive focus for this diretion with the proper
% reference point. (FIELD II)
switch self.receiveFocus 
    case FocusSetting.no        % Don't specify the focus depth.
        xdc_center_focus(self.receiveHandleFIELDII, [x 0 0]) ;                                      % Designate center of focus to receiver
    case FocusSetting.yes       % Use fix focus point z_focus.
        xdc_center_focus(self.receiveHandleFIELDII, [x 0 0]) ;                                      % Designate center of focus to receiver
        xdc_focus(self.receiveHandleFIELDII, 0, [x 0 self.zFocus]);                                 % Designate focus point to receiver
    case FocusSetting.multi     % Use multiple focus_zones.
        xdc_center_focus (self.receiveHandleFIELDII, [x 0 0]) ;                                     % Designate center of focus to receiver
        xdc_focus (receiveHandleFIELDII, self.focusTimes, ...
            [x * ones(self.noFocus,1), zeros(self.noFocus,1), ...
            self.focusZones]) ;    
    otherwise
        error('!! Dynamic focusing not yet implemented !!') % Designate multiple focus zones to receiver
end

% SET RECEIVE APERTURE
self.setAperture(x, 'receive') ;
xdc_apodization(self.receiveHandleFIELDII, 0, self.receiveAperture);                                %  Apply apodisation to Receiver.
end