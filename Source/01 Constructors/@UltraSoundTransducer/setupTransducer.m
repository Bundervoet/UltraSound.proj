% SETUP TRANSDUCER

% Define a transducer in FIELD II by passing along the relevant physical properties.
function setupTransducer(USTransducer)

field_init                                                                                          % Initalize FIELD II enviroment.

% Set the sampling frequency used by the system.(FIELD II)
set_sampling(USTransducer.get('fs')) ;                                                              % Set sampling frequency. [Hz]

% Define prefered transducer for emission. (FIELD II)
if USTransducer.get('elevFocus') == 0 
    USTransducer.transmitHandleFIELDII = xdc_linear_array(USTransducer.get('noElements'), USTransducer.get('elementWidth'), ...
        USTransducer.get('elementHeight'), USTransducer.get('elementKerf'), 1, 10, USTransducer.get('focusPoint')) ;                             % Define Transmit Array.
else
    USTransducer.transmitHandleFIELDII = xdc_focused_array(USTransducer.get('noElements'), USTransducer.get('elementWidth'), ...
        USTransducer.get('elementHeight'), USTransducer.get('elementKerf'), USTransducer.get('elevFocus'), 1, 10, ...
        USTransducer.get('focusPoint')) ;                                                           % Define Focused Transmit Array.
end
% NOTE: The 6th variable notes the number of subdivisions of an
% transducer element in the x-direction.
%       The 7th variable notes the number of subdivisions of an
%       transducer element in the y-direction.

%  Set the impulse response and excitation of the xmit
%  aperture. (FIELD II)
impulseResponse = USTransducer.excitationWave.sampledWave() ;                                       % Sinusoidal impuls response.
impulseResponse = impulseResponse.*hanning(max(size(impulseResponse)))' ;                           % apodisation of the impluse response over time.
xdc_impulse(USTransducer.transmitHandleFIELDII, impulseResponse) ;                                  % Set impuls response to tranmitter.

excitationWave = USTransducer.excitationWave.sampledWave() ;                                        % Determin which pulse will excite the piezoelectric crystals.
xdc_excitation(USTransducer.transmitHandleFIELDII, excitationWave) ;                                % Set excitation to transmitter.

% Define prefered transducer for receive. (FIELD II)
if USTransducer.get('elevFocus') == 0 
    USTransducer.receiveHandleFIELDII = xdc_linear_array(USTransducer.get('noElements'), USTransducer.get('elementWidth'), ...
        USTransducer.get('elementHeight'), USTransducer.get('elementKerf'), 1, 10, USTransducer.get('focusPoint'));                              % Define receive Array.
else
    USTransducer.receiveHandleFIELDII = xdc_focused_array(USTransducer.get('noElements'), USTransducer.get('elementWidth'), ...
        USTransducer.get('elementHeight'), USTransducer.get('elementKerf'), USTransducer.get('elevFocus'), 1, 10, ...
        USTransducer.get('focusPoint')) ;                                                           % Define Focused receive Array.
end
% NOTE: The 6th variable notes the number of subdivisions of an
% transducer element in the x-direction.
%       The 7th variable notes the number of subdivisions of an
%       transducer element in the y-direction.

% Set the impulse response for the receive aperture. (FIELD II)
xdc_impulse (USTransducer.receiveHandleFIELDII, impulseResponse) ;  
% Set impuls to receiver.

% INITIALIZE APERTURES
USTransducer.setAperture(0, 'transmit', true) ;
USTransducer.setAperture(0, 'receive', true) ;
end
