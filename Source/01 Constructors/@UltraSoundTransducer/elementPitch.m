function output = elementPitch(transducer, input)
if nargin > 1
    self.set('elmentWidth', input - transducer.get('elementKerf')) ;
end
output = transducer.get('elementWidth') + transducer.get('elementKerf') ;
end