function RFdata = simulateBatch(UStransducer, name, PHdata, line, points, focusPoint, varargin)

% Initialize variable parameters of method.
if nargin < 4 
    line = 0 ;
end
if nargin < 5 
    points = 0 ;
elseif isempty(points)
    points = 0 ; 
end
if nargin < 6
    focusPoint = [] ;
end
% Define RadioFrequencyData.
RFdata = RadioFrequencyData(name, varargin{:}) ;
RFdata.transducerElements = struct('noElements', UsT.noElements, 'noTransmit', UsT.noTransmit, 'noReceive', UsT.noReceive) ;

% Load Phantom variables.
zDistance   = PHdata.get('zDistance') ;
zSize       = PHdata.get('zSize') ;
noScat      = PHdata.get('noScatterers') ;

% Make sure all input scatter indices exist.
first = min(noScat, points(1)) ;
last  = min(noScat, points(end)) ;
points = first:last ;

% RFdata.signalDATA = cell(1, numel(points)) ;
metricCoordinate  = zeros(1, numel(points)) ;
gridCoordinate	  = cell(1, numel(points)) ;

% Determine how the elements should be activated to image the specified line.
focusPoint = UStransducer.activateElements(line, focusPoint) ;

% First Relevant Sample
if isempty(RFdata.get('firstPixel')) || (RFdata.get('firstPixel') == 0)
    RFdata.set('firstPixel', ...
        UStransducer.excitationWave.distance2sample(zDistance)) ;                                           % first pixel of phantom. [uint16]
end
% Last Relevant Sample
if isempty(RFdata.get('lastPixel')) || (RFdata.get('lastPixel') == 0)
    RFdata.set('lastPixel', ...
        UStransducer.excitationWave.distance2sample(zDistance + zSize)) ;                                   % last pixel of phantom. [uint16]
end

% Determine Pixel Properties.
elementPitch = UStransducer.get('elementPitch') ;
pixelPitch   = UStransducer.pixelPitch('fs') ;
RFdata.set('lineWidth', UStransducer.get('lineWidth')) ;                                                    % Copy lineWidth property.

% Save activeFocusPoint
RFdata.set('activeFocus', focusPoint) ;

if UStransducer.outputSignal.simulateEnergy
    RFdata.set('pixelSize', [PHdata.get('dz') PHdata.get('dx') PHdata.get('dy')]) ;
else
    RFdata.set('pixelSize', [pixelPitch elementPitch]) ;
end

% Export proper scatter data.
[scatterCoordinates, scatterAmplitude]  = scatterPointToCoordinate(PHdata, points) ;

% Load variables.
firstPixel  = RFdata.get('firstPixel') ;
lastPixel   = RFdata.get('lastPixel') ;
noTransmit  = UStransducer.get('noTransmit') ;
noReceive   = UStransducer.get('noReceive') ;
fs          = UStransducer.get('fs') ;

for ii = 1:numel(points)
    jj = points(ii) ;
    
    % Display Progress.
    I = PHdata.location(jj) ; D = PHdata.digits ;
    if mod(jj, 50) == 1
        %   '12345678901234567890123456789012345678901234567890'
        disp([timeString, UStransducer.name,'::', ...
            'Simulating RadioFrequencyData of Scatter Point ---',...
            '::I',sprintf(D.i,jj),...
            '::X',sprintf(D.x,I.x),'Y',sprintf(D.y,I.y),'Z',sprintf(D.z,I.z),...
            '::P',sprintf('%03d',floor(jj/noScat*100))]) ;
    end
    
    if isa(PHdata, 'GridPhantom')
        noX = PHdata.dimensions(1) ; noZ = PHdata.dimensions(3) ; % noY = PHdata.dimension(2) ;
        metricCoordinate(ii)	= scatterCoordinates(ii) ;
        gridCoordinate{ii}      = ThreeDimensionalPoint(mod(floor((jj-1)/noZ)+1, noX), floor((jj-1)/(noX*noZ))+1, mod(jj, noZ)) ;
    else
        metricCoordinate(ii)    = scatterCoordinates(ii) ;
    end
   
    switch UStransducer.outputSignal
        % NOTE: The setup will only start recording when the first element start
        % receiving echoes. Zeros are added to the start of the received signal to
        % compensate for the delay in recording. Moreover all information received
        % by the non-active elements will be discarded. (Crop in de x-direction.)
        case OutputSetting.nothing
            RFdata.set('batchElement', zeros(lastPixel - firstPixel, noReceive)) ;
        case OutputSetting.beamformed
            % Calculate Tx/Rx field.
            [rf_data, tstart] = calc_scat(UStransducer.transmitHandleFIELDII, UStransducer.receiveHandleFIELDI, ...
                scatterCoordinates(ii,:), scatterAmplitude(ii)) ;
            % Add zeros to start of signal to accommodate for delay in recording.
            RFdata.set('batchElement', [zeros(round(tstart * fs) - 25,1) ; rf_data ]) ;
            
        case OutputSetting.channel
            % Calculate Tx/Rx field.
            [rf_data, tstart] = calc_scat_multi(UStransducer.transmitHandleFIELDII, UStransducer.receiveHandleFIELDII, ...
                scatterCoordinates(ii,:), scatterAmplitude(ii)) ;
            % Add zeros to start of signal to accommodate for delay in recording. Crop
            % all "zero" data received by the nonactive elements.
            RFdata.set('batchElement', [zeros(round(tstart * fs) - 25, noReceive); ...
                rf_data(:,(UStransducer.noElementsLeftOfReceiveWindow() + (1 : noReceive)))]) ;
            
        case OutputSetting.synthetic
            % Calculate Tx/Rx field. Each transmitting element will one-by-one generate
            % an acoustic pulse while each receiving element will seperatly listen to
            % the echo response. This creates a response containing no_receive *
            % no_transmit columns.
            [rf_data, tstart] = calc_scat_all(UStransducer.transmitHandleFIELDII, slef.receiveHandleFIELDII, ...
                scatterCoordinates(ii,:), scatterAmplitude(ii), 1) ;
            
            % The echo response received by each elements as result of all different
            % transmitting element emitting indivually is summed to create the create
            % the echo respons at each element as a result of all transmitting elements
            % being activated at the same time.
            for rr = 1 : UStransducer.noReceive
                data(:,rr) = sum(rf_data(:, rr : noTransmit : (noTransmit * noReceive)), 2) ; %#ok<AGROW>
            end
            
            % Add zeros to start of signal to accommodate for delay in recording. Crop
            % all "zero" data received by the nonactive elements.
            RFdata.set('batchElement', [zeros(round(tstart * fs) - 25, noReceive); ...
                data( : , (UStransducer.noElementsLeftOfTransmitWindow() + (1 : noReceive)))]) ;
            %                     signal = data( : , (pre_receive + (1:self.no_receive))) ;
        case OutputSetting.energy
            % Calculate Energy.
            RFenergy = sum(calc_hp(UStransducer.transmitHandleFIELDII, scatterCoordinates(ii,:)).^2,1) ;
            
        case OutputSetting.absolute
            RFenergy = sum(abs(calc_hp(UStransducer.transmitHandleFIELDII, scatterCoordinates(ii,:))),1) ;
            
        otherwise
            errorStruct.message = ['rawSignal property ', char(RFdata.outputSignal) ,'not recognised.'];
            errorStruct.identifier = 'RadioFrequencyData:simulatePoint:PopertyNotRecognised:switch1';
            error(errorStruct) ;
    end
    
    if UStransducer.outputSignal.simulateEnergy
        switch class(PHdata)
            case 'GridPhantom'
                RFdata.set('batchElement', reshape(RFenergy, PHdata.rearrangement3D)) ;
            case 'GridCDF97Phantom'
                % NOT IMPLEMENTED YET
            case 'CystPhantom'
                RFdata.set('batchElement', RFenergy)  ;
            otherwise
                errorStruct.message     = ['Invalid Phantom ', class(PHdata),' when simulating energy output! Phantom must of type -Grid!'] ;
                errorStruct.identifier  = 'RadioFrequencyData:simulateBatch:InvalidPhantom';
                error(errorStruct) ;
        end
    else
        % Crop signal in the z-direction to coincide with the phantoms depth
        % dimensions.
        if UStransducer.cropSignal 
            RFdata.crop ;
        end
        
        % Sparcify signal by setting all pixels having a value lower then 1% of the
        % signals maximal value to zero. (Local)
        if UStransducer.sparseSignal.sparsify
            RFdata.sparsify ;
        end
        
        if UStransducer.saveSparse
            RFdata.set('signalDATA', sparse(double(RFdata.get('signalDATA')))) ;
        end
    end
end

if isa(PHdata, 'GridPhantom')
    RFdata.set('metricCoordinate', metricCoordinate, 'gridCoordinate', gridCoordinate) ;
else
    RFdata.set('metricCoordinate', metricCoordinate) ;
end

end