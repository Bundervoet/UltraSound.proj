function RFdata = simulateSynthetic(UStransducer, name, PHdata, points, varargin)

% Initialize variable parameters of method.
if nargin < 4
    points = 0 ;
elseif isempty(points)
    points = 0 ; 
endif isa(points, 'char')
    varargin = [{points}, varargin]  ;   
end

% Define RadioFrequencyData.
RFdata = RadioFrequencyData(name, varargin{:}) ;

% Load Phantom variables.
zDistance   = PHdata.get('zDistance') ;
zSize       = PHdata.get('zSize') ;

% First Relevant Sample
if isempty(RFdata.get('firstPixel')) || (RFdata.get('firstPixel') == 0)
    RFdata.set('firstPixel', ...
        UStransducer.excitationWave.distance2sample(zDistance)) ;                                           % first pixel of phantom. [uint16]
end
% Last Relevant Sample
if isempty(RFdata.get('lastPixel')) || (RFdata.get('lastPixel') == 0)
    RFdata.set('lastPixel', ...
        UStransducer.excitationWave.distance2sample(zDistance + zSize)) ;                                   % last pixel of phantom. [uint16]
end

% Determine Pixel Properties.
elementPitch = UStransducer.get('elementPitch') ;
pixelPitch   = UStransducer.pixelPitch('fs') ;
RFdata.set('lineWidth', UStransducer.get('lineWidth')) ;                                                    % Copy lineWidth property.
if UStransducer.outputSignal.simulateEnergy
    RFdata.set('pixelSize', [PHdata.get('dz') PHdata.get('dx') PHdata.get('dy')]) ;
else
    RFdata.set('pixelSize', [pixelPitch elementPitch]) ;
end

% Import proper scatter data.
if prod(points == 0) 
    % Simple import when simulator is not in point mode.
    [scatterCoordinates, scatterAmplitude]  = PHdata.scatterPointToCoordinate ;
else
    % Expanded import for pointwise visualization. Mainly used when creating Point Spread Function.
    [scatterCoordinates, scatterAmplitude]  = scatterPointToCoordinate(PHdata, points) ;
    if isa(PHdata, 'GridPhantom')
        noX = PHdata.dimensions(1) ; noZ = PHdata.dimensions(3) ; % noY = PHdata.dimension(2) ;
        metricCoordinate    = PHdata.scatterDATA(point) ;
        gridCoordinate      = ThreeDimensionalPoint(mod(floor((point-1)/noZ)+1, noX), floor((point-1)/(noX*noZ))+1, mod(point, noZ)) ;
        RFdata.set('metricCoordinate', metricCoordinate, 'gridCoordinate', gridCoordinate) ;
    else
        metricCoordinate    = PHdata.scatterDATA ;
        RFdata.set('metricCoordinate', metricCoordinate) ;
    end
end

% Simulate the echo response from the phantom to the emitted pulse by the
% self.
% - signal_calculate = 0 : Simulate Beamformed response.
% - signal.calculate = 1 : Simulate Channel RFdata.
% - signal.calculate = 2 : Simulate Synthetic aperture response.

% Load variables.
firstPixel  = RFdata.get('firstPixel') ;
lastPixel   = RFdata.get('lastPixel') ;
noTransmit  = UStransducer.get('activeTransmit') ;
noReceive   = UStransducer.get('activeReceive') ;
noElements  = UStransducer.get('noElements') ;
fs          = UStransducer.get('fs') ;

switch UStransducer.outputSignal        
    case OutputSetting.plainWave
        % Calculate Tx/Rx field. In this setting each element will simultaniously generate an
        % acoustic pulse creating an unfocused plain wave front. This specific wave front is
        % facilitated by manipulating the synthetic aperture method in field II.
        [rf_data, tstart] = calc_scat_all(UStransducer.transmitHandleFIELDII, UStransducer.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;
        
        % The echo response received by each elements as result of all different transmitting 
        % element emitting indivually is summed to create the echo respons at each element as a 
        % result of all transmitting elements being activated at the same time.
        for rr = UStransducer.receiveWindow
            data(:,rr) = sum(rf_data(:, rr : noTransmit : (noTransmit * noReceive)), 2) ; %#ok<AGROW>
        end
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25, noReceive); ...
            data( : , (UStransducer.noElementsLeftOfTransmitWindow() + (1 : noReceive)))]) ;
        %                     signal = data( : , (pre_receive + (1:self.no_receive))) ;
     
    case OutputSetting.synthetic_vAll
        % Calculate Tx/Rx field. Each transmitting element will one-by-one generate
        % an acoustic pulse while each receiving element will seperatly listen to
        % the echo response. This creates a response containing no_receive *
        % no_transmit columns.
        [rf_data, tstart] = calc_scat_all(UStransducer.transmitHandleFIELDII, UStransducer.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;       
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25, noElements); rf_data]) ;
        %                     signal = data( : , (pre_receive + (1:self.no_receive))) ;
        
    case OutputSetting.synthetic_v001
        % Calculate Tx/Rx field. Each transmitting element will one-by-one generate
        % an acoustic pulse while each receiving element will seperatly listen to
        % the echo response. This creates a response containing no_receive *
        % no_transmit columns.
        [rf_data, tstart] = calc_scat_all(UStransducer.transmitHandleFIELDII, UStransducer.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;
        
        % The echo response received by all element as a result of individual element activation is
        % summed to create a synthetic aperture response.
        data = zeros(noTransmit, noReceive, size(rf_data,1)) ;
        for tt = UStransducer.transmitWindow
            for rr = UStransducer.receiveWindow
                data(tt, rr, :) = rf_data(:, rr + (tt - 1) * noReceive) ;
            end
        end
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', cat(3, zeros(noTransmit, noReceive, round(tstart * fs) - 25), data)) ;
        
    case OutputSetting.synthetic_v002
        % Calculate Tx/Rx field. Each transmitting element will one-by-one generate
        % an acoustic pulse while each receiving element will seperatly listen to
        % the echo response. This creates a response containing no_receive *
        % no_transmit columns.
        [rf_data, tstart] = calc_scat_all(UStransducer.transmitHandleFIELDII, UStransducer.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;
        
        % The echo response received by all element as a result of individual element activation is
        % summed to create a synthetic aperture response.
        for tt = UStransducer.transmitWindow
            data(:,tt) = sum(rf_data(:, ((tt-1)*noReceive + 1) : (tt * noReceive)), 2) ; %#ok<AGROW>
        end
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25, noReceive); ...
            data( : , (UStransducer.noElementsLeftOfTransmitWindow() + (1 : noReceive)))]) ;
        %                     signal = data( : , (pre_receive + (1:self.no_receive))) ;
        
    case OutputSetting.energy
        % Calculate Energy.
        RFenergy = sum(calc_hp(UStransducer.transmitHandleFIELDII, scatterCoordinates).^2,1) ;
        
    case OutputSetting.absolute
        % Calculate Energy.
        RFenergy = sum(abs(calc_hp(UStransducer.transmitHandleFIELDII, scatterCoordinates)),1) ;
        
    otherwise
        errorStruct.message = ['rawSignal property ', char(RFdata.outputSignal) ,'not recognised.'];
        errorStruct.identifier = 'RadioFrequencyData:simulateLine:PopertyNotRecognised:switch1';
        error(errorStruct) ;
end

if RFdata.outputSignal.simulateEnergy
    switch class(PHdata)
        case 'GridPhantom'
            RFdata.set('signalDATA', reshape(RFenergy, PHdata.rearrangement3D)) ;
        case 'GridCDF97Phantom'
            % NOT IMPLEMENTED YET
        case 'CystPhantom'
            RFdata.set('signalDATA', RFenergy) ;
        otherwise
            errorStruct.message     = ['Invalid Phantom ', class(PHdata),' when simulating energy output! Phantom must of type -Grid!'] ;
            errorStruct.identifier  = 'RadioFrequencyData:simulateLine:InvalidPhantom';
            error(errorStruct) ;
    end
else
    % Crop signal in the z-direction to coincide with the phantoms depth
    % dimensions.
    if UStransducer.cropSignal 
        RFdata.crop ;
    end
    
    % Sparcify signal by setting all pixels having a value lower then 1% of the
    % signals maximal value to zero. (Local)
    if UStransducer.sparseSignal.sparsify 
        RFdata.sparsify(UStransducer.sparseFactor) ;
    end
    
    if UStransducer.saveSparse 
        RFdata.set('signalDATA', sparse(RFdata.get('signalDATA'))) ;
    end
end
end