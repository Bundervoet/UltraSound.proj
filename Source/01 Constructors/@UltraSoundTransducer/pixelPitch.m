function output = pixelPitch(transducer, type, input)
if nargin < 2
    type = 'f0' ;
end
if nargin > 2
    transducer.set(type, transducer.get('c') / (2 * input)) ;
end
switch type
    case 'f0'
        output = transducer.get('c') / (2 * transducer.get('f0')) ;
    case 'fs'
        output = transducer.get('c') / (2 * transducer.get('fs')) ;
    otherwise
        errorStruct.message = ['Frequency input "', num2str(type),'" is not recognised.'];
        errorStruct.identifier = 'Phantom:getVarInternal:NoSuchFrequency';
        error(errorStruct) ;
end
end