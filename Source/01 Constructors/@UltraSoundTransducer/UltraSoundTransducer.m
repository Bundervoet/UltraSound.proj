% ULTRASOUNDTRANSDUCER

% The UltraSoundTransducer object is used to generate RadioFrequencyData from a Phantom. Multiple
% types of RFdata can be generated such as
%   - Energy Maps (EDline)      using simulateEDline ;
%   - RFlines                   using simulateRFline ;
%   - Point responses           using simulateRFpoint ;
%   - Batch of RFlines/RFpoints using simulateRFbatch ;
% All response calculations are facilitated using the FIELD II simulator.

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 8 AUG 16.

classdef UltraSoundTransducer < DataType
    
    %% PROPERTIES
    properties  % PHYSICAL PROPERTIES
        % Acoustic Wave
        excitationWave@SoundWave    = SoundWave() ;
        
        % Transducer element variables.
        noElements@uint16           = uint16(192) ;                                                 % Number of physical elements.
        elementWidth@double         = 4.4000e-04 ;                                                  % Width of transducer element. [m]
        elementHeight@double        = 5/1000 ;                                                      % Height of transducer element. [m]
        elementKerf@double          = 0.05/1000 ;                                                   % Kerf : Distance between transducer elements. [m]
    end
    properties % ACQUISITION PROPERTIES
        noTransmit@uint16           = uint16(64) ;                                                  % Number of Transmitting elements.
        noReceive@uint16            = uint16(64) ;                                                  % Number of Recieving elements.
        
        % Focus point variables
        focusPoint@double           = [0 0 70]/1000 ;                                               % Initial electronic focus. [m]
        focusZones@double           = (30:20:200)'/1000 ;                                           % Focal Zones for reception in [m].
        zFocus@double               = 60/1000 ;                                                     % Transmit focus in [m].
        elevFocus@double            = 0/1000 ;                                                      % Elevation focus using a Focussed array in [m].
        
        % Imaging variables.
        noLines@uint16              = uint16(50) ;                                                  % Number of imaging lines.
        imageWidth@double           = 40/1000 ;                                                     % Imaging sector in [m].
        
        % receive aperture properties. (FIELD II)
        receiveApodization@ApodizationSetting   = ApodizationSetting.hanning ;                      % Set receive apodization [0 = none, 1 = hanning, 2 = hamming] ;
        receiveFocus@FocusSetting               = FocusSetting.yes ;                                % Set receive Focus. [0 = NO, 1 = YES, 2 = MULTI] ;
        
        % Transmit aperture properties. (FIELD II)
        transmitApodization@ApodizationSetting	= ApodizationSetting.hanning ;                      % Set Transmit apodization. [0 = none, 1 = hanning, 2 = hamming] ;
        transmitFocus@FocusSetting            	= FocusSetting.yes ;                                % Set Transmit Focus [0 = NO, 1 = YES, 3 = DYNAMIC] ;
    end
    properties % CURRENT STATUS
        transmitAperture@double ;                                                                   % Window of element currently active.
        transmitHandleFIELDII ;                                                                     % FIELD II component.
        
        receiveAperture@double ;                                                                    % Window of element currently active.
        receiveHandleFIELDII ;                                                                      % FIELD II component.
    end
    properties % SIGNAL OUTPUT PROPERTIES
        outputSignal@OutputSetting          = OutputSetting.channel ;                               % Use Channel RFdata. [beamforming, channel, synthetic, energy, absolute]
        sparseSignal@SparsitySetting        = SparsitySetting.complete ;                            % Sparsify the Channel RFdata.
        sparseFactor@double                 = 1e-3 ;                                                % Percentage treshhold for setting small values to zero.
        saveSparse@logical                  = false ;                                               % Save Channel RFdata as sparse matrix.
        cropSignal@logical                  = true ;                                                % Use Cropped Channel RFdata.
    end
    
    %% METHODS
    methods
        
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a instance and accessing
        % / changing the state of its properties.
        % ===
        function self = UltraSoundTransducer(name, varargin)
            % ---
            % First The upperclass SoundWave is initiated using standard parameters. Afterwards
            % set-up is facilitated by the upperclass DataType which internally uses this
            % class' version of setOne to acces and alter internal properties.
            % ---
            self@DataType(name, varargin{:}) ;
        end
        
        % ===
        % Specialised version of variable handling functions.
        % ===
        function setOne(self, input, value)
            % ---
            % Similar to setOne in other classes this method provides functionality for addapting a
            % variable in UltraSoundTransducer. Special care is taken when adapting the properties
            % inhereted by SoundWave as interparameter dependencies exist.
            % ---
            switch lower(input)
                case {'frequency', 'f0'}
                    self.excitationWave.setFrequency(value) ;
                case {'speed', 'c'}
                    self.excitationWave.setSpeed(value) ;
                case {'lambda'}
                    self.excitationWave.setLambda(value) ;
                case {'sampling', 'fs'}
                    self.excitationWave.setSampling(value) ;
                case {'apodizationwindow'}
                    self.transmitWindow = value ;
                    self.receiveWindow  = value ;
                case 'linewidth'
                    self.lineWidth(value) ;
                case 'elementpitch'
                    self.elementPitch(value) ;
                case 'pixelpitch'
                    self.pixelPitch('f0', value) ;
                case {'transmitapodization', 'receiveapodization'}
                    if isa(value, 'char') 
                        self.(input) = ApodizationSetting.(value) ;
                    else
                        self.(input) = value ;
                    end
                case {'transmitfocus', 'receivefocus'}
                    if isa(value, 'char') 
                        self.(input) = FocusSetting.(value) ;
                    else
                        self.(input) = value ;
                    end
                case {'noelements', 'notransmit', 'noreceive', 'nolines'}
                    inputClass = class(self.(input)) ;
                    cmd = ['self.(input) = ', inputClass, '(value) ;'] ; eval(cmd) ;
                case {'outputsignal', 'sparsesignal'}
                    if isa(value, 'char')
                        enumClass       = class(self.(input)) ;
                        cmd = ['self.', input,' = ',enumClass,'.',value ,' ;'] ; eval(cmd) ;
                    else
                        self.(input)    = value ;
                    end
                otherwise
                    setDATA(self, input, value) ;
            end
        end
        function output = getOne(self, input)
            switch lower(input)
                case {'frequency', 'f0'}
                    output = self.excitationWave.setFrequency ;
                case {'speed', 'c'}
                    output = self.excitationWave.setSpeed ;
                case {'lambda'}
                    output = self.excitationWave.setLambda ;
                case {'sampling', 'fs'}
                    output = self.excitationWave.setSampling ;
                case 'linewidth'
                    output = self.lineWidth() ;
                case 'elementpitch'
                    output = self.elementPitch() ;
                case 'pixelpitch'
                    output = self.pixelPitch() ;
                case {'noelements', 'notransmit', 'noreceive', 'nolines', 'activetransmit', 'activereceive'}
                    output = double(self.(input)) ;
                otherwise
                    output = getDATA(self, input) ;
            end
        end
        function output = type(self)
            output = 'UStransducer' ;
        end
        
        %% PHYSICAL METHODS
        function output = lines(transducer)
            output = double(1:transducer.noLines) ;
        end
        function output = noElementsLeftOfTransmitWindow(transducer)
            output = find(transducer.transmitAperture, 1, 'first') - 1 ;
        end
        function output = noElementsRightOfTransmitWindow(transducer)
            output = transducer.get('noElements') - find(transducer.transmitAperture, 1, 'last') ;
        end
        function output = noElementsLeftOfReceiveWindow(transducer)
            output = find(transducer.receiveAperture, 1, 'first') - 1 ;
        end
        function output = noElementsRightOfReceiveWindow(transducer)
            output = transducer.get('noElements') - find(transducer.receiveAperture, 1, 'last') ;
        end
        function output = receiveWindow(transducer)
            output = find(transducer.receiveAperture) ;
        end
        function output = transmitWindow(transducer)
            output = find(transducer.transmitAperture) ;
        end
        function output = activeTransmit(transducer)
            output = length(transducer.transmitWindow) ;
        end
        function output = activeReceive(transducer)
            output = length(transducer.receiveWindow) ;
        end
        function output = digits(self, input)
            output = struct(...
                'i',['%0',num2str(numel(num2str(self.noLines))),'d']) ;
            if nargin == 2
                output = output.(input) ;
            end
        end
        
        %% ACQUISITION METHODS
        function fValue = noFocus(self)
            fValue = length(self.focusZones) ;
        end
        function fValue = focusTimes(self)
            % Determin focus times from variables.
            fValue = (self.get('focusZones') - 10/1000)/self.get('c') ;
        end
        
        %% PLOT METHODS
        function output = dimensions(transducer)
            % Use field II to retrieve transducer dimensions.
            Th = xdc_linear_array(transducer.noElements, transducer.elementWidth, ...
                transducer.elementHeight, transducer.elementKerf, 1, 10, [0 0 70]/1000) ;
            output = xdc_get(Th,'rect');
        end
        function plotNow(transducer, h , colors)
            data    = transducer.dimensions ;
            [~,M]   = size(data);
            if nargin < 3
                colors = data(5,:) ;
            end
            
            hold on
            figure(h) ;
            for i=1:M
                x = [data(11,i), data(20,i); data(14,i), data(17,i)] ;
                y = [data(12,i), data(21,i); data(15,i), data(18,i)] ;
                z = [data(13,i), data(22,i); data(16,i), data(19,i)] ;
                c = colors(i) * ones(2,2) ;
                %
                surf(x, y, z, c)               
            end
            
%             colormap(cool(256));
            colormap('parula') ; 
            axis tight
            axis off
            set(gca, 'Units', 'normalized', 'Position', [0 0 1 1]) ;
            axis('image') ;
            hold off
            drawnow ;
        end
    end
end
