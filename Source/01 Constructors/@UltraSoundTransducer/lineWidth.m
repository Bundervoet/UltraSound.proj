function output = lineWidth(transducer, input)
if nargin > 1;
    transducer.set('noLines', transducer.get('imageWidth') / input) ;
end
if sum(transducer.noLines ~= [0 1]) ;
    output = transducer.get('imageWidth') / (transducer.get('noLines') - 1) ;
else
    output = transducer.get('imageWidth') / (transducer.get('noLines')) ;
end
end