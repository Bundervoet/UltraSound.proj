function setAperture(transducer, line, type, setup)

% CHECK INPUT PARAMETERS
if nargin < 4
    setup = false ;
end

switch type
    case 'transmit'
        width = transducer.get('noTransmit') ;
    case 'receive'
        width = transducer.get('noReceive') ;
    otherwise
        errorStruct.message = ['Input argument "', type,'" is not a valid modality.'];
        errorStruct.identifier = 'UltrasoundTransducer:getAperture:InvalidInput';
        error(errorStruct) ;
end

% DETERMINE WHICH APODISATION WINDOW TO USE.

% If the use of a "fade-out" window is specified the system will use a hanning window
% apodisation to lower the importance of elements which are further away from the focus point.
if setup
    partialWindow = ones(1, width) ;
else
    cmd = ['partialWindow = transducer.',type ,'Apodization.apodizationArray(width) ;'] ; eval(cmd) ;
end

% ACTIVATE ELEMENTS IN SEQUENCE.

% Select elements to activate and set the apodisation for transmition. FIELD II will determin in
% what order to activate the elements.
[noElementsLeftOfWindow,noElementsRightOfWindow] = transducer.activeWindow(line, type);                % Number of elements after active window. [INT:double]
aperture = [zeros(1, noElementsLeftOfWindow), partialWindow, zeros(1, noElementsRightOfWindow)] ; %#ok<NASGU>

cmd = ['transducer.',type ,'Aperture = aperture ;'] ; eval(cmd) ;
end