function RFdata = simulateLine(UsT, name, PHdata, line, points, focusPoint, varargin)

% Initialize variable parameters of method.
if nargin < 4 
    line = 0 ;
end
if nargin < 5 
    points = 0 ;
elseif isempty(points)
    points = 0 ; 
end
if nargin < 6
    focusPoint = [] ;
end
if isa(line, 'char') 
    varargin = [{line, points, focusPoint}, varargin] ;   
elseif isa(points, 'char') 
    varargin = [{points, focusPoint}, varargin]  ;   
elseif isa(focusPoint, 'char')
    varargin = [{focusPoint}, varargin]  ; 
end

% Display Progress.
if UsT.verbose
    %   '12345678901234567890123456789012345678901234567890'
    disp([timeStamp,' ', UsT.name,'::',...
        'Simulating RadioFrequency Data -------------------', ...
        sprintf('::Points[%d,%d]', min(points), max(points)), ...
        sprintf('::Line%03d',line)]) ;
end

% Define RadioFrequencyData.
RFdata = RadioFrequencyData(name, varargin{:}) ;
RFdata.transducerElements = struct('noElements', UsT.noElements, 'noTransmit', UsT.noTransmit, 'noReceive', UsT.noReceive) ;

% Determine how the elements should be activated to image the specified line.
focusPoint = UsT.activateElements(line, focusPoint) ;

% Load Phantom variables.
zDistance   = PHdata.get('zDistance') ;
zSize       = PHdata.get('zSize') ;

% First Relevant Sample
if isempty(RFdata.get('firstPixel')) || (RFdata.get('firstPixel') == 0)
    RFdata.set('firstPixel', ...
        UsT.excitationWave.distance2sample(zDistance)) ;                                           % first pixel of phantom. [uint16]
end
% Last Relevant Sample
if isempty(RFdata.get('lastPixel')) || (RFdata.get('lastPixel') == 0) 
    RFdata.set('lastPixel', ...
        UsT.excitationWave.distance2sample(zDistance + zSize)) ;                                   % last pixel of phantom. [uint16]
end

% Determine Pixel Properties.
elementPitch = UsT.get('elementPitch') ;
pixelPitch   = UsT.pixelPitch('fs') ;
RFdata.set('lineWidth', UsT.get('lineWidth')) ;                                                    % Copy lineWidth property.
% RFdata.set('relativeFocus', UStransducer.activeFocusPoint(line, ...
%         (UStransducer.noElementsRightOfReceiveWindow() - UStransducer.noElementsLeftOfReceiveWindow()) * elementPitch), ...
%         focusPoint) ;

% Save activeFocusPoint
RFdata.set('activeFocus', focusPoint) ;

if UsT.outputSignal.simulateEnergy
    RFdata.set('pixelSize', [PHdata.get('dz') PHdata.get('dx') PHdata.get('dy')]) ;
else
    RFdata.set('pixelSize', [pixelPitch elementPitch]) ;
end

% Import proper scatter data.
if prod(points == 0) 
    % Simple import when simulator is not in point mode.
    [scatterCoordinates, scatterAmplitude]  = PHdata.scatterPointToCoordinate ;
else
    % Expanded import for pointwise visualization. Mainly used when creating Point Spread Function.
    [scatterCoordinates, scatterAmplitude]  = scatterPointToCoordinate(PHdata, points) ;
    if isa(PHdata, 'GridPhantom')
        noX = PHdata.dimensions(1) ; noZ = PHdata.dimensions(3) ; % noY = PHdata.dimension(2) ;
        metricCoordinate    = PHdata.scatterDATA(point) ;
        gridCoordinate      = ThreeDimensionalPoint(mod(floor((point-1)/noZ)+1, noX), floor((point-1)/(noX*noZ))+1, mod(point, noZ)) ;
        RFdata.set('metricCoordinate', metricCoordinate, 'gridCoordinate', gridCoordinate) ;
    else
        metricCoordinate    = PHdata.scatterDATA ;
        RFdata.set('metricCoordinate', metricCoordinate) ;
    end
end

% Simulate the echo response from the phantom to the emitted pulse by the
% self.
% - signal_calculate = 0 : Simulate Beamformed response.
% - signal.calculate = 1 : Simulate Channel RFdata.
% - signal.calculate = 2 : Simulate Synthetic aperture response.

% Load variables.
firstPixel  = RFdata.get('firstPixel') ;
lastPixel   = RFdata.get('lastPixel') ;
noTransmit  = UsT.get('noTransmit') ;
noReceive   = UsT.get('noReceive') ;
noElements  = UsT.get('noElements') ;
fs          = UsT.get('fs') ;
switch UsT.outputSignal
    % NOTE: The setup will only start recording when the first element start
    % receiving echoes. Zeros are added to the start of the received signal to
    % compensate for the delay in recording. Moreover all information received
    % by the non-active elements will be discarded. (Crop in de x-direction.)
    case OutputSetting.nothing
        RFdata.signalDATA = zeros(lastPixel - firstPixel, noReceive) ;
    case OutputSetting.beamformed
        % Calculate Tx/Rx field.
        [rf_data, tstart] = calc_scat(UsT.transmitHandleFIELDII, UsT.receiveHandleFIELDI, ...
            scatterCoordinates, scatterAmplitude) ;
        % Add zeros to start of signal to accommodate for delay in recording.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25,1) ; rf_data ]) ;
        
    case OutputSetting.channel
        % Calculate Tx/Rx field.
        [rf_data, tstart] = calc_scat_multi(UsT.transmitHandleFIELDII, UsT.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude) ;
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25, noReceive); ...
            rf_data(:,(UsT.noElementsLeftOfReceiveWindow() + (1 : noReceive)))]) ;
        
    case OutputSetting.plainWave
        % Calculate Tx/Rx field. In this setting each element will simultaniously generate an
        % acoustic pulse creating an unfocused plain wave front. This specific wave front is
        % facilitated by manipulating the synthetic aperture method in field II.
        [rf_data, tstart] = calc_scat_all(UsT.transmitHandleFIELDII, UsT.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;
        
        % The echo response received by each elements as result of all different transmitting 
        % element emitting indivually is summed to create the echo respons at each element as a 
        % result of all transmitting elements being activated at the same time.
        for rr = 1 : noReceive
            data(:,rr) = sum(rf_data(:, rr : noTransmit : (noTransmit * noReceive)), 2) ; %#ok<AGROW>
        end
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25, noReceive); ...
            data( : , (UsT.noElementsLeftOfTransmitWindow() + (1 : noReceive)))]) ;
        %                     signal = data( : , (pre_receive + (1:self.no_receive))) ;
     
    case OutputSetting.synthetic_vAll
        % Calculate Tx/Rx field. Each transmitting element will one-by-one generate
        % an acoustic pulse while each receiving element will seperatly listen to
        % the echo response. This creates a response containing no_receive *
        % no_transmit columns.
        [rf_data, tstart] = calc_scat_all(UsT.transmitHandleFIELDII, UsT.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;       
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25, noElements); rf_data]) ;
        %                     signal = data( : , (pre_receive + (1:self.no_receive))) ;
        
    case OutputSetting.synthetic_v001
        % Calculate Tx/Rx field. Each transmitting element will one-by-one generate
        % an acoustic pulse while each receiving element will seperatly listen to
        % the echo response. This creates a response containing no_receive *
        % no_transmit columns.
        [rf_data, tstart] = calc_scat_all(UsT.transmitHandleFIELDII, UsT.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;
        
        % The echo response received by all element as a result of individual element activation is
        % summed to create a synthetic aperture response.
        data = zeros(noTransmit, noReceive, size(rf_data,1)) ;
        for tt = 1 : noTransmit
            for rr = 1:noReceive
                data(tt, rr, :) = rf_data(:, rr + (tt - 1) * noReceive) ;
            end
        end
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', cat(3, zeros(noTransmit, noReceive, round(tstart * fs) - 25), data)) ;
        
    case OutputSetting.synthetic_v002
        % Calculate Tx/Rx field. Each transmitting element will one-by-one generate
        % an acoustic pulse while each receiving element will seperatly listen to
        % the echo response. This creates a response containing no_receive *
        % no_transmit columns.
        [rf_data, tstart] = calc_scat_all(UsT.transmitHandleFIELDII, UsT.receiveHandleFIELDII, ...
            scatterCoordinates, scatterAmplitude, 1) ;
        
        % The echo response received by all element as a result of individual element activation is
        % summed to create a synthetic aperture response.
        for tt = 1 : noTransmit
            data(:,tt) = sum(rf_data(:, ((tt-1)*noReceive + 1) : (tt * noReceive)), 2) ; %#ok<AGROW>
        end
        
        % Add zeros to start of signal to accommodate for delay in recording. Crop
        % all "zero" data received by the nonactive elements.
        RFdata.set('signalDATA', [zeros(round(tstart * fs) - 25, noReceive); ...
            data( : , (UsT.noElementsLeftOfTransmitWindow() + (1 : noReceive)))]) ;
        %                     signal = data( : , (pre_receive + (1:self.no_receive))) ;
        
    case OutputSetting.energy
        % Calculate Energy.
        RFenergy = sum(calc_hp(UsT.transmitHandleFIELDII, scatterCoordinates).^2,1) ;
        
    case OutputSetting.absolute
        % Calculate Energy.
        RFenergy = sum(abs(calc_hp(UsT.transmitHandleFIELDII, scatterCoordinates)),1) ;
        
    case OutputSetting.energyL2
        % Calculate Energy.
        RFenergy = sum(abs(calc_hp(UsT.transmitHandleFIELDII, scatterCoordinates)),1) ;
        
    otherwise
        errorStruct.message = ['rawSignal property ', char(RFdata.outputSignal) ,'not recognised.'];
        errorStruct.identifier = 'RadioFrequencyData:simulateLine:PopertyNotRecognised:switch1';
        error(errorStruct) ;
end

if UsT.outputSignal.simulateEnergy
    switch class(PHdata)
        case 'GridPhantom'
            RFdata.set('signalDATA', reshape(RFenergy, PHdata.rearrangement3D)) ;
        case 'GridCDF97Phantom'
            % NOT IMPLEMENTED YET
        case 'CystPhantom'
            RFdata.set('signalDATA', RFenergy) ;
        otherwise
            errorStruct.message     = ['Invalid Phantom ', class(PHdata),' when simulating energy output! Phantom must of type -Grid!'] ;
            errorStruct.identifier  = 'RadioFrequencyData:simulateLine:InvalidPhantom';
            error(errorStruct) ;
    end
else
    % Crop signal in the z-direction to coincide with the phantoms depth
    % dimensions.
    if UsT.cropSignal
        RFdata.crop ;
    end
    
    % Sparcify signal by setting all pixels having a value lower then 1% of the
    % signals maximal value to zero. (Local)
    if UsT.sparseSignal.sparsify
        RFdata.sparsify(UsT.sparseFactor) ;
    end
    
    if UsT.saveSparse
        RFdata.set('signalDATA', sparse(RFdata.get('signalDATA'))) ;
    end
end
end