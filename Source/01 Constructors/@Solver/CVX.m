function REdata = CVX(self, REdata, SYdata) %#ok<*STOUT>
%
% Get Dictionary Data ;
D = @(x) SYdata.A(x, true, true) ;
S = SYdata.systemDATA ;

% Setup Measurements.
measurements  	= REdata.getMeasurementDATA ; 
m               = SYdata.noRows ;

switch self.inversion
    case 'eq'                       % argmin_x \|x\|_1 s.t. y = D*x 
        cvx_begin
        variable RFcoeff(m) ;
        minimize(norm(RFcoeff, 1 )) ;
        subject to
        S * RFcoeff == measurements ; %#ok<EQEFF>
        cvx_end
        %
    case 'l2_eq'                    % argmin_x \|x\|_2 s.t. y = D*x 
        cvx_begin
        variable RFcoeff(m) ;
        minimize(norm(RFcoeff, 2 )) ;
        subject to
        SYdata.getMatrixDATA * RFcoeff == measurements ; %#ok<EQEFF>
        cvx_end
        %
    case 'qc'                       % argmin_x \|x\|_1 s.t. \|D*x - y \|_2 <= eps
        cvx_begin
        variable RFcoeff(m) ;
        minimize(norm( RFcoeff , 1 )) ;
        subject to
        norm(S * RFcoeff - measurements, 2) <= self.epsilon  ; %#ok<*VUNUS>
        cvx_end
        %
    case {'l2_qc', 'qc_l2'}         % argmin_x \|x\|_2 s.t. \|D*x - y \|_2 <= eps
        cvx_begin
        variable RFcoeff(m) ;
        minimize(norm( RFcoeff , 2 )) ;
        subject to
        norm(S * RFcoeff - measurements, 2) <= self.epsilon  ; %#ok<*VUNUS>
        cvx_end
        %
    case 'lc'                       % argmin_x \|x\|_1 s.t. \|D*x - y \|_1 <= eps
        cvx_begin
        variable RFcoeff(m) ;
        minimize(norm( RFcoeff , 1 )) ;
        subject to
        norm(S * RFcoeff - measurements, 1) <= self.epsilon  ; %#ok<*VUNUS>
        cvx_end
        %
    case 'psnr'                     % argmin_x \|x\|_1 s.t. PSNR(y,D*x) <= eps
        cvx_begin gp
        variable RFcoeff(m) ;
        minimize(norm( RFcoeff , 1 )) ;
        subject to
        psnr(S * RFcoeff,measurements) >= self.epsilon ;
        cvx_end
        %
    case {'l2_psnr', 'psnr_l2'}     % argmin_x \|x\|_� s.t. PSNR(y,D*x) <= eps
        cvx_begin gp
        variable RFcoeff(m) ;
        minimize(norm( RFcoeff , 2 )) ;
        subject to
        psnr(S * RFcoeff, measurements) >= self.epsilon ;
        cvx_end
        %
    case 'mse'
        cvx_begin
        variable RFcoeff(m) ;
        minimize(norm( RFcoeff , 1 )) ;
        subject to
        MSE(S * RFcoeff, measurements) <= self.epsilon ;
        cvx_end
        %
    case {'l2_mse', 'mse_l2'}
        cvx_begin
        variable RFcoeff(m) ;
        minimize(norm( RFcoeff , 2 )) ;
        subject to
        MSE(S * RFcoeff, measurements) <= self.epsilon ;
        cvx_end
        %
    otherwise
        errorStruct.message     = ['Inversion method "', num2str(inversion),'" not recognised.'] ;
        errorStruct.identifier  = 'Solver:CVX:methodNotRecognised';
        error(errorStruct) ;
end
%
% Reconstruct Data.
REdata.setCoeffDATA(RFcoeff) ;
REdata.setMatrixDATA(D(RFcoeff)) ;
%
end
