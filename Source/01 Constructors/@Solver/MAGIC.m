% L1 MAGIC SOLVER

% Use the l1-magic solver.
function REdata = MAGIC(self, REdata, SYdata)

if isempty(SYdata.sytemDATA) ; % LARGE SCALE MODE
    
    
    
    
% Setup Measurements.
measurements  	= RFdata.signalDATA(SYdata.rowMASK.maskDATA) ;

switch 2 % self.solver_inverse
    case {0,'fs'}
        RFcoeff = measurements \ SYdata.matrixDATA ;
        RFcoeff = RFcoeff' ;
    case {1,'bs'}
        RFcoeff = SYdata.getMatrixDATA \ measurements ;
    case {2,'pinv'}
        RFcoeff = SYdataInv.getMatrixDATA * measurements ;
end

% Solve using the quadratically constrained l1-magic solver.
%             RFcoeff = l1qc_logbarrier(RFcoeff, system_full, system_inverse, measurements, 5e-5) ;
noIterations    = self.get('noIterations') ;
epsilon         = self.get('epsilon') ;

switch self.inversion
    case {'l1eq', 'eq'}
        RFcoeff = l1eq_pd(RFcoeff, SYdata.getMatrixDATA, SYdataInv.getMatrixDATA, measurements, 5e-3, noIterations) ;
    case {'l1qc', 'qc'}
        RFcoeff = l1qc_logbarrier(RFcoeff, SYdata.getMatrixDATA, SYdataInv.getMatrixDATA, measurements, epsilon ) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, system, system_inverse, measurements, self.epsilon, 1e-3, numel(self.iterations), 1e-8, 500) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, system, system_inverse, measurements, self.epsilon, 1e-3, 50, 1e-8, 500) ;
    case {'l1qcN', 'qcN'}
        RFcoeff = l1qc_newton(RFcoeff, SYdata.getMatrixDATA, SYdataInv.getMatrixDATA, measurements, 5e-3, 1e-3, noIterations) ;
    case 'tveq'
        RFcoeff = tveq_logbarrier(RFcoeff, SYdata.getMatrixDATA, SYdataInv.getMatrixDATA, measurements, 1e-3, 5, 1e-8, noIterations) ;
    case 'tvqc'
        RFcoeff = tvqc_logbarrier(RFcoeff, SYdata.getMatrixDATA, SYdataInv.getMatrixDATA, measurements, 5e-3, 1e-3, 5, 1e-8, noIterations) ;
        errorStruct.message     = ['Inversion method "', num2str(inversion),'" not recognised.'] ;
        errorStruct.identifier  = 'Solver:MAGIC:methodNotRecognised';
        error(errorStruct) ;
end

% Reconstruct Data.
self.coefficientDATA.result	= RFcoeff ;
if ~isempty(DCdata)
    self.signalDATA.result  = reshape(DCdata.getMatrixDATA * self.coefficientDATA.result, RFdata.size) ;
end

end
