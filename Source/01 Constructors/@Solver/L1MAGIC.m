% L1 MAGIC SOLVER

% Use the l1-magic solver.
function REdata = L1MAGIC(self, REdata, SYdata, varargin)

% Initialize standard paramaters.
sensingType         = 'sensing' ;
sensingTranspose    = false ;
dictionaryType      = 'dictionary' ;
dictionaryTranspose = false ;

% Adapt standard paramaters to specified values.
for jj = 1:2:length(varargin)
    if isvarname(varargin{jj}) 
        cmd = [varargin{jj}, ' = varargin{jj+1} ;'] ; eval(cmd) ;
    else
        errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'];
        errorStruct.identifier = 'DataType:import:InvalidFieldName';
        error(errorStruct) ;
    end
end

systemMatrix     = SYdata.getMatrixDATA(sensingType,    sensingTranspose) ;
dictionaryMatrix = SYdata.getMatrixDATA(dictionaryType, dictionaryTranspose) ;

% Define handle version of the following system operators:
S       = @(x) SYdata.S(x, false, systemMatrix) ;                                                   % = Rs * (Psi * Cs) * Phi^{1} ; 
St      = @(y) SYdata.S(y, true,  systemMatrix) ;                                                   % = Phi * (Cs^T * Psi^T) * Rs^T

% Define handle version of the following operators:
D       = @(x) reshape(SYdata.S(x, false, dictionaryMatrix), SYdata.rowMASK.size) ;                 % = (Psi * Cs) * Phi^{1}
F       = @(x) reshape(SYdata.S(x, false, 1), SYdata.colMASK.size) ;                               % Phi^{1}

% Retreive Measurements.
measurements  	= REdata.getMeasurementDATA ; 

% Initial Estimate of Coefficient values. 
RFcoeff         = St(measurements) ;

% Save Initial Estimate.
REdata.setCoeffDATA(RFcoeff, 'initial') ;
REdata.setCoeffDATA(F(RFcoeff), 'tissue_init') ;
REdata.setMatrixDATA(D(RFcoeff), 'initial') ;

% Solve using the quadratically constrained l1-magic solver.
noIterations    = self.get('noIterations') ;

% Find a Value for epsilon.
epsilon         = self.get('epsilon') ;

% Reconstruct Coefficients.
switch self.inversion
    case {'l1eq', 'eq'}
        RFcoeff = l1eq_pd(RFcoeff, S, St, measurements , epsilon, noIterations) ;
    case {'l1qc', 'qc'}
%         RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, epsilon, 1e-3, 50, 1e-8, 500) ;
        RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, epsilon) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, 1e-1, 2, 50, 1e-8, 600) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, 5e-3, 1e-3) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, system, system_inverse, measurements, self.epsilon, 1e-3, numel(self.iterations), 1e-8, 500) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, system, system_inverse, measurements, self.epsilon, 1e-3, 50, 1e-8, 500) ;
    case {'l1qcN', 'qcN'}
        RFcoeff = l1qc_newton(RFcoeff, S, St, measurements, 5e-3, 1e-3, noIterations) ;
    case 'tveq'
        RFcoeff = tveq_logbarrier(RFcoeff, S, St, measurements, 1e-3, 5, 1e-8, noIterations) ;
    case 'tvqc'
        RFcoeff = tvqc_logbarrier(RFcoeff, S, St, measurements , 5e-3, 1e-3, 5, 1e-8, noIterations) ;
    otherwise
        errorStruct.message     = ['Inversion method "', num2str(inversion),'" not recognised.'] ;
        errorStruct.identifier  = 'Solver:MAGIC:methodNotRecognised';
        error(errorStruct) ;
end

% Reconstruct Data.
REdata.setCoeffDATA(RFcoeff) ;
REdata.setCoeffDATA(F(RFcoeff), 'tissue') ;
REdata.setMatrixDATA(D(RFcoeff)) ;
end
