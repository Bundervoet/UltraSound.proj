% ITERATIVE REWEIGHTED LEAST SQUARE SOLVER

% The IRLS algoritm progressivly searched a weighted L2-minimizer to y=Ax. By choosing the
% weights correctly the RFcoeff will converge to the L1-minimizer of the system. Note that
% L1 is sparsity promoting which implies that this sollution also has minimal L0-norm
function REdata = IRLS(self, REdata, DCdata, SYdata, RFdata)

%% SETUP VARIABLES
% Setup Measurements.
measurments     = RFdata.signalDATA(SYdata.rowMASK.maskDATA) ;

% Choose weight parameter.
m               = DCdata.get('height') ;
n               = DCdata.get('width') ;

% Initialize RFcoefficients.
coefficients    = ones(n,1) ;

% Set the regularization paremeter making sure that a devision by zero does
% not occur. This creates more stability at the cost of a added inaccurary.
% When the sparcity of the coefficients is know this parameter can be
% choosen to be more flexibale, addapting to each new solution.
if self.sparsity == 0
    x_eps = self.epsilon / m ;
else
    self.epsilon = 1 ;
    x_eps = self.epsilon ;
end

% Initialize the weights. As such the first iterations is just an
% L2-minimization.
x_weight = ones(n,1) ;

% Enable a figurebutton to interrupt the iterations when wanted.
H = figure(999);
H = uicontrol('Style', 'PushButton', ...
    'String', 'Break', ...
    'Callback', 'delete(gcbf)');
drawnow ;

iteration = 1 ;

%% IRLS ITERATIONS
% for iteration = 1:algorithmProtocol.noIterations ; % number of iterations
while (iteration <= self.get('noIterations')) && (ishandle(H))
    
    % ORIGNAL IRLS ALGORITM
    start = tic ;
    
    [coefficients, x_weight, x_eps] = self.IRLS_step(x_weight, x_eps, SYdata.getMatrixDATA, measurments) ;
    
    % Display Progress if convergence is reached and quit.
    if x_eps == 0 && self.epsilon ~= 0
        fprintf('::IRLS_step::convergence reached::\n') ;
        break ;
    end
    
    %% OUTPUT STATISTICS
    % Determine l1-norm of coefficients.
    x_norm  = sum(abs(coefficients(:))) ;
    
    % Determine elapsed time.
    elapsed = toc(start) ;
    
    % Reconstruct Data.
    if sum(strcmp(iteration,self.steps))
        structureLink                           = ['step',num2str(iteration)] ;
        REdata.coefficientDATA.(structureLink)  = coefficients ;
        if ~isempty(DCdata)
            REdata.signalDATA.(structureLink)   = reshape(DCdata.getMatrixDATA * self.coefficientDATA.(structureLink), RFdata.size) ;
        end
    end
    
    % Display statistics concerning current iteration.
    %                    '12345678901234567890123456789012345678901234567890'
    disp([self.name,'::','IRLS iteration', ...
        '::I',sprintf('%03d', iteration), ...
        '::L1 = ',sprintf('%e', x_norm), ...
        ' eps = ' ,sprintf('%e', x_eps), ...
        ' time = ',sprintf('%fs', elapsed), ...
        ]) ;
    
    iteration = iteration + 1 ;
    drawnow ;
    
end

% Reconstruct Data.
REdata.coefficientDATA.result     = coefficients ;
if ~isempty(DCdata)
    REdata.signalDATA.result      = reshape(DCdata.getMatrixDATA * REdata.coefficientDATA.result, RFdata.size) ;
end

end
