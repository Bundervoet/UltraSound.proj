function [coefficients, x_weight, x_eps] = IRLS_step(self, x_weight, x_eps, system, measurements)

tau         = 1/2 ;

% ByteSize(system)
m           = size(system, 1) ;
n           = size(system,2) ;

% Create a tensor matrix making the matrix multiplication with the diagonal
% weighting matrix faster. This trick increases the effiency at the cost
% of memory.
Dk = kron(x_weight, ones(1,m));

% Perform the reconstruction using the IRLS algorithm.
switch self.inversion
    case {0,'fs'}
        coefficients = Dk .* system' * (measurements \ (system * (Dk .* system')))' ;                % Use "Reversed Backslash" to solve (system * (Dk .* system'))^-1.
    case {1,'bs'}
        coefficients = Dk .* system' * ((system * (Dk .* system')) \ measurements) ;                 % Use the regular Backslash operation to solve (system * (Dk .* system'))^-1.
    case {2,'pinv'}
        coefficients = Dk .* system' * (pinv(full(system) * (Dk .* full(system)')) * measurements);              % Use Moore-Penrose Pseudoinverse to solve (system * (Dk .* system'))^-1.
    case 'pinv_gpu'
        coefficients = Dk .* system' * (pinv(full(system) * (Dk .* full(system)')) * measurements);              % Use Moore-Penrose Pseudoinverse to solve (system * (Dk .* system'))^-1.
end

% Recompute the value of the regularization parameter.
if self.sparsity ~= 0
    x_sort  = sort(abs(coefficients),'descend') ;
    x_eps   = min(x_eps,x_sort(floor(self.sparsity))/n) ;
end

% Determine new weighting used in the next iteration step.
x_weight    = (coefficients.^2 + x_eps^2).^(tau) ;
end