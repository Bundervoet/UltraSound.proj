% L2 SOLVER

% Solves the linear system of equations Ax = y. This methods finds the closest sollution in
% terms of \|Ax - y\|_2 having the least L2 norm argmin_x \|x\|_2.
function REdata = LEASTSQUARE(self, REdata, SYdata, varargin)
%
% Get Dictionary Data ;
D       = @(x) SYdata.A(x, true, true) ;

% Setup Measurements.
measurements  	= REdata.getMeasurementDATA ; 

if ~isempty(SYdata.inverseDATA)
    inversion = 'fix' ; 
else
    inversion = self.inversion ; 
end

% Reconstruct Coefficients.
switch inversion
    case {1,'fs'}
        RFcoeff = transpose(measurements \ SYdata.getSystemDATA) ;        
    case {2,'bs'}
        RFcoeff = SYdata.getSystemDATA \ measurements ;
    case {3, 'fit', 'reg', 'over', 'ls'}
        RFcoeff = (SYdata.getSystemDATA' * SYdata.getSystemDATA) \ SYdata.getSystemDATA' * measurements ;
    case {4, 'under'}
        %         RFcoeff = SYdata.getSystemDATA' * ((SYdata.getSystemDATA * SYdata.getSystemDATA') \ measurements ) ;
        SYdata.inverseDATA  = SYdata.getSystemDATA' * inv(SYdata.getSystemDATA * SYdata.getSystemDATA') ; %#ok<MINV>
        RFcoeff             = SYdata.inverseDATA * measurements ;
    case {5, 'pinv'}
        %         RFcoeff = pinv(SYdata.getSystemDATA) * measurements ;
        SYdata.inverseDATA  = pinv(SYdata.getSystemDATA) ; %#ok<MINV>
        RFcoeff             = SYdata.inverseDATA * measurements ;
    case {6, 'inv'}
        RFcoeff = inv(SYdata.getSystemDATA) * measurements ;
    case {0, 'fix'}
        RFcoeff = SYdata.inverseDATA * measurements ;
    otherwise
        errorStruct.message     = ['Inversion method "', num2str(inversion),'" not recognised.'] ;
        errorStruct.identifier  = 'Solver:LEASTSQUARE:methodNotRecognised';
        error(errorStruct) ;
end

% Reconstruct Data.
REdata.setCoeffDATA(RFcoeff) ;
REdata.setMatrixDATA(D(RFcoeff)) ;

end
