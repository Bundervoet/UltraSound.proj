% INVERSION SOLVER

% Solves the linear system of equations Ax = y. This methods finds the closest sollution in
% terms of \|Ax - y\|_2 having the least L2 norm argmin_x \|x\|_2.
function REdata = INVERSE(Sol, REdata, SYdata, varargin)

% Initialize standard paramaters.
sensingType         = MaT('sensing', false) ;
inverseType         = MaT('sensing', false) ; 
dictionaryType      = MaT('dictionary', false) ;
tissueType          = MaT('ones', false) ;

% Adapt standard paramaters to specified values.
for jj = 1:2:length(varargin)
    if isvarname(varargin{jj}) 
        cmd = [varargin{jj}, ' = varargin{jj+1} ;'] ; eval(cmd) ;
    else
        errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'];
        errorStruct.identifier = 'DataType:import:InvalidFieldName';
        error(errorStruct) ;
    end
end

% Define handle version of the following system operators:                                          % = Rs * (Psi * Cs) * Phi^{1} ; 
St      = @(y) SYdata.S(y, true,  MaT(sensingType.N, ~sensingType.T)) ;                             % = Phi * (Cs^T * Psi^T) * Rs^T

% Define handle version of the following operators:
D       = @(x) reshape(SYdata.S(x, false, dictionaryType),  SYdata.rowMASK.size) ;               % = (Psi * Cs) * Phi^{1}
F       = @(x) reshape(SYdata.S(x, false, tissueType),      SYdata.colMASK.size) ; 

% Retreive Measurements.
msrmnts = REdata.getMeasurementDATA ; 

% Initial Estimate of Coefficient values. 
RFcoeff = St(msrmnts) ;

% Save Initial Estimate.
REdata.setSpatialDATA(RFcoeff,    'coeff_init') ;
REdata.setSpatialDATA(F(RFcoeff), 'tissue_init') ;
REdata.setMatrixDATA(D(RFcoeff),  'init') ;

% Reconstruct Coefficients.
switch Sol.inversion
    case {1,'fs', ' -fs'}
        RFcoeff = transpose(msrmnts \ SYdata.getMatrixDATA(sensingType)) ;        
    case {2,'bs', ' -bs'}
        RFcoeff = SYdata.getMatrixDATA(sensingType) \ msrmnts ;
    case {3, 'fit', 'reg', 'ls', ' -fit', ' -reg', ' -ls'}
        RFcoeff = (SYdata.getMatrixDATA(sensingType)' * SYdata.getMatrixDATA(sensingType)) \ ...
            SYdata.getMatrixDATA(sensingType)' * msrmnts ;
    case {'pinv', 'under', 'over', ' -pinv', ' -under', ' -over'}
        SYdata.invert('type', sensingType, 'inversion', inversion) ;
        RFcoeff = SYdata.getMatrixDATA([sensingType.strT,'_', inversion], sensingTranspose) * msrmnts ;
    case {0, 'fix', ' -fix'}
        RFcoeff = SYdata.getMatrixDATA(inverseType) * msrmnts ;
    otherwise
        errorStruct.message     = ['Inversion method "', num2str(inversion),'" not recognised.'] ;
        errorStruct.identifier  = 'Solver:LEASTSQUARE:methodNotRecognised';
        error(errorStruct) ;
end

% Reconstruct Data.
REdata.setSpatialDATA(RFcoeff,   'coeff_result') ;
REdata.setSpatialDATA(F(RFcoeff), 'tissue_result') ;
REdata.setMatrixDATA(D(RFcoeff)) ;
end
