% LEAST SQUARE SOLVER

% Use the Least Square solver.
function REdata = LSQR(self, REdata, SYdata, varargin)

sensingType         = MaT('sensing', false) ;
dictionaryType      = MaT('dictionary', false) ;
tissueType          = MaT('ones', false) ;

% Adapt standard paramaters to specified values.
for jj = 1:2:length(varargin)
    if isvarname(varargin{jj}) 
        cmd = [varargin{jj}, ' = varargin{jj+1} ;'] ; eval(cmd) ;
    else
        errorStruct.message = ['Input argument "', num2str(varargin{jj}),'" is not a valid field name.'];
        errorStruct.identifier = 'DataType:import:InvalidFieldName';
        error(errorStruct) ;
    end
end

% Define handle version of the following operators:
D       = @(x) reshape(SYdata.S(x, false, dictionaryType),  SYdata.rowMASK.size) ;                      % = (Psi * Cs) * Phi^{1}
F       = @(x) reshape(SYdata.S(x, false, tissueType),      SYdata.colMASK.size) ;

% Define handle version of the following system operators:
S       = @(x, type) SYdata.S(x, strcmp(type, 'transp'), MaT(sensingType.N, strcmp(type, 'transp'))) ;	% = Rs * (Psi * Cs) * Phi^{1} 

% Retreive Measurements.
measurements  	= REdata.getMeasurementDATA ; 

% Initial Estimate of Coefficient values. 
RFcoeff         = S(measurements, 'transp') ;

% Save Initial Estimate.
REdata.setSpatialDATA(RFcoeff,    'coeff_init') ;
REdata.setSpatialDATA(F(RFcoeff), 'tissue_init') ;
REdata.setMatrixDATA(D(RFcoeff),  'init') ;

% Solve using the quadratically constrained l1-magic solver.
noIterations    = self.get('noIterations') ;

% Find a Value for epsilon.
epsilon         = self.get('epsilon') ;

% Reconstruct Coefficients.
RFcoeff = lsqr(S, measurements , epsilon, noIterations) ;

% Reconstruct Data.
REdata.setSpatialDATA(RFcoeff,   'coeff_result') ;
REdata.setSpatialDATA(F(RFcoeff), 'tissue_result') ;
REdata.setMatrixDATA(D(RFcoeff)) ;
end
