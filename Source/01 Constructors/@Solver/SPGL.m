% SPGL1

% Use the SPGL1 solver.
function REdata = SPGL(self, REdata, SYdata)

% Define handle version of the following operators:
D       = @(x) SYdata.A(x, true, 1) ;               % = (Psi * Cs) * Phi^{1}
F       = @(x) SYdata.A(x, true, 2) ;               % Phi^{1}

% Define handle version of the following system operators:
S       = @(x, mode) SYdata.A(x, (mode == 1), 0) ;  % = Rs * (Psi * Cs) * Phi^{1} ; 

% Retreive Measurements.
measurements  	= REdata.getMeasurementDATA ; 

% Initial Estimate of Coefficient values. 
RFcoeff         = St(measurements) ;

% Save Initial Estimate.
REdata.setCoeffDATA(RFcoeff, 'initial') ;
REdata.setCoeffDATA(F(RFcoeff), 'tissue_init') ;
REdata.setMatrixDATA(D(RFcoeff), 'initial') ;

% Solve using the quadratically constrained l1-magic solver.
% noIterations    = self.get('noIterations') ;

% Find a Value for epsilon.
epsilon         = self.get('epsilon') ;

% Reconstruct Coefficients.
RFcoeff = spgl1(S, measurements, 0, epsilon) ;

% Reconstruct Data.
REdata.setCoeffDATA(RFcoeff) ;
REdata.setCoeffDATA(F(RFcoeff), 'tissue') ;
REdata.setMatrixDATA(D(RFcoeff)) ;
end
