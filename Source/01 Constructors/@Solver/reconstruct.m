%% RECONSTRUCT CHANNEL RF DATA

% This method allows the solver to reconstruct the input RFdata using the given SystemMatrix.
% Therefor the RFdata is first subsampled according to the rowMASK in SystemMatrix before being
% reconstructed according the the methode/settings present in the solver properties. The solver
% returns a ReconstructionData which is a subclass of RadioFrequencyData which also contains the
% measurements onwhich the reconstruction is based.

% USAGE - Matrices
% REdata = Solver.reconstruct('name', DCdata, SYdata, OPT:SYIdata, RFdata) ;
%       - Handles
% REdata = Solver.reconstruct('name', DCdata, RFdata) ;

% Created by Shaun Bundervoet on 1 DEC 15.
% Latest version 17 Okt 16.

function REdata = reconstruct(solver, name, SYdata, RFdata, varargin)

% Initialize a Reconstruction Data Object.
REdata      = ReconstructionData(name, RFdata) ;

% Set Sampling Masks
REdata.set('rowMASK', SYdata.rowMASK) ;
REdata.set('colMASK', SYdata.colMASK) ;

switch solver.method
    % An L2 minimizer for the undetermined system.
    case SolverSetting.IRLS     % An L1 minimizer for the underdetermined system. (L0 Sparcity promoting!)
        REdata = solver.IRLS(REdata, DCdata, SYdata, RFdata) ;
        
    case SolverSetting.CVX
        REdata = solver.CVX(REdata, DCdata, SYdata, RFdata) ;
        
    case SolverSetting.L1MAGIC
        REdata = solver.L1MAGIC(REdata, SYdata, varargin{:}) ;
    case SolverSetting.SPGL
        REdata = solver.SPGL(REdata, SYdata, varargin{:}) ;
        
    case SolverSetting.LSQR
        REdata = solver.LSQR(REdata, SYdata, varargin{:}) ;
    case SolverSetting.inverse
        REdata = solver.INVERSE(REdata, SYdata, varargin{:}) ;
    otherwise
        
end
end
