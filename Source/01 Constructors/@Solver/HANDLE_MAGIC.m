% L1 MAGIC SOLVER

% Use the l1-magic solver.
function REdata = MAGIC(self, REdata, SYdata)

% Get Dictionary Data ;
D               = @(x) SYdata.A(x, true, true) ;
if isempty(SYdata.systemDATA) % LARGE SCALE MODE  
    % Define Handle Functions.
    S               = @(x) SYdata.A(x, true, false) ;
    St              = @(x) SYdata.A(x, false, true) ;
else % MATRIX MODE
    S = SYdata.systemDATA ;
    St = [] ;
end
% Setup Measurements.
measurements  	= REdata.getMeasurementDATA ; 

% Estimate for Coefficient values. 
RFcoeff         = St(measurements) ;

% Save L2 step
REdata.setCoeffDATA(RFcoeff, 'initial') ;
REdata.setMatrixDATA(D(RFcoeff), 'initial') ;

% Solve using the quadratically constrained l1-magic solver.
%             RFcoeff = l1qc_logbarrier(RFcoeff, system_full, system_inverse, measurements, 5e-5) ;
noIterations    = self.get('noIterations') ;

% Find a Value for epsilon.
epsilon         = self.get('epsilon') ;
% sigma = 1e-1 ; % 0.005;
% take epsilon a little bigger than sigma*sqrt(K)
% epsilon =  sigma*sqrt(DCdata.noRows)*sqrt(1 + 2*sqrt(2)/sqrt(DCdata.noRows)) ;

% Reconstruct Coefficients.

switch self.inversion
    case {'l1eq', 'eq'}
        RFcoeff = l1eq_pd(RFcoeff, S, St, measurements , epsilon, noIterations) ;
    case {'l1qc', 'qc'}
%         RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, epsilon, 1e-3, 50, 1e-8, 500) ;
        RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, epsilon) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, 1e-1, 2, 50, 1e-8, 600) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, S, St, measurements, 5e-3, 1e-3) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, system, system_inverse, measurements, self.epsilon, 1e-3, numel(self.iterations), 1e-8, 500) ;
%         RFcoeff = l1qc_logbarrier(RFcoeff, system, system_inverse, measurements, self.epsilon, 1e-3, 50, 1e-8, 500) ;
    case {'l1qcN', 'qcN'}
        RFcoeff = l1qc_newton(RFcoeff, S, St, measurements, 5e-3, 1e-3, noIterations) ;
    case 'tveq'
        RFcoeff = tveq_logbarrier(RFcoeff, S, St, measurements, 1e-3, 5, 1e-8, noIterations) ;
    case 'tvqc'
        RFcoeff = tvqc_logbarrier(RFcoeff, S, St, measurements , 5e-3, 1e-3, 5, 1e-8, noIterations) ;
        errorStruct.message     = ['Inversion method "', num2str(inversion),'" not recognised.'] ;
        errorStruct.identifier  = 'Solver:MAGIC:methodNotRecognised';
        error(errorStruct) ;
end

% Reconstruct Data.
REdata.setCoeffDATA(RFcoeff) ;
REdata.setMatrixDATA(D(RFcoeff)) ;

end
