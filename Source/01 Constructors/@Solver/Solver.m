% SOLVER

% Similar to UltraSoundTransducer, Solver is used to to transform one DataMatrix into an other. 
% Specifically UltraSoundData is sampled and afterwards reconstructed using a SystemMatrix and the
% reconstruct method to obtain ReconstructionData. Internally the following methods are used :
%   CVX             for L0 - reconstruction ;
%   IRLS and MAGIC  for L1 - reconstruction ;
%   LEASTSQUARE     for L2 - reconstruction ;

% Created by Shaun Bundervoet 1 DEC 16.
% Latest version 22 JUN 16.

classdef Solver < DataType
    
    
    %% PROPERTIES
    properties % SOLVER PROPERTIES       
        method@SolverSetting    = SolverSetting.none ;                                              % Specify internal solver method used. e.g. 'LSQR','IRLS','Full', ...
        inversion@char          = 'pinv' ;                                                          % Internal inversion methods. e.g. 'pinv', 'bs', ...
        base@char               = 'cdf97' ;                                                         % Sparcify base inwhich to represent signal. e.g. 'cdf97', 'bdct', ...
        noIterations@uint16     = uint16(50) ;                                                      % Number of iterations to use in iterative methods.
        sparsity@double         = 0                                                                 % Number of non-zero coefficients to expect. 0 = uknown.
        epsilon@double          = 5e-3 ;                                                            % Tuning parameter. 
        steps@struct            = struct() ;                                                        % Intermediate steps at which to save results. e.g. {'step1', 'result'} ;
    end

    
    %% METHODS
    methods
        
        
        %% DEFINITION AND VARIABLE METHODS.
        % ===
        % This section contains methods pertaining to the construction of a instance and accessing
        % / changing the state of its properties.
        % ===
        function self = Solver(name, varargin)
            self@DataType(name, varargin{:}) ;
        end
        function output = type(self)
            output = 'USsolver' ;
        end

        % ===
        % Specialised version of variable handling functions.
        % ===
        function setOne(self, input, value)
            % ---
            % Similar to setOne in other classes this method provides functionality for addapting a
            % variable in RadioFrequencyData. Take special care in adapting non-double properties.
            % ---
            switch lower(input)
                case {'noiterations'}
                    inputClass = class(self.(input))  ;
                    cmd = ['self.(input) = ', inputClass, '(value) ;'] ; eval(cmd) ;
                case {'method'}
                    if isa(value, 'char')
                        enumClass       = class(self.(input)) ;
                        cmd = ['self.', input,' = ',enumClass,'.',value ,' ;'] ; eval(cmd) ;
                    else
                        self.(input)    = value ;
                    end
                otherwise
                    setDATA(self, input, value) ;
            end
        end
        function output = getOne(self, input)
            switch lower(input)
                case {'noiterations'}
                    output = double(self.(input)) ;
                otherwise
                    output = getDATA(self, input) ;
            end
        end
    end
end