%% ULTRASOUND PROJECT
% 
% Created by Shaun Bundervoet 22 JUN 16.
% Latest version 22 JUN 16.
%
% =========
% This project is used to apply the Compressive Sensing framework to Ultrasound RF channel Data.
% Generally a sensed signal y is represented by x using a matrix A, i.e. y = A*x. Depending on the
% setup the goal is to find a suitable value for x. In the case of CS:
%   $$ x = \argmin_u \|u\|_0 subject to \|y - A*u\|_2 \leq \epsilon ; $$
% =========
%
%
%% Classes 	{
%%      Global  {
%				DataType  < handle ;
%               ---------
%               DataType is a global class generalising all features required to save and load an object to the 
%               HDD. Morover DataType facilitates the basic building blocks required in order to visualize specific 
%               intrinsic information contained in the Data object.
%               ---------
%
% 				DataMatrix <  DataType ;
%               ---------
%               DataMatrix is a global class generalising all objects which are made up of:
%                   - a central property containing the bulk of important data, typically refered to as the
%               matrixDATA property.
%                   - secundary properties which acts a information relating the central data-property. These values 
%               can pertain to the simulation settings used to create the dataObj and other properties such as 
%               the relative pixel size of the matrix stored in matrixDATA.
%               NOTE: Allthough the principal data-property is refered to as matrixDATA this exact property
%               genearlly does not exist.
%               ex. for RadioFrequencyData the data-property is called signalDATA. It can however be accessed
%               using dataObj.getMatrixDATA within functions. (and dataObj.matrixDATA in scipts.)
%               ---------
% 				}
%
%%      Constructors    {
%                       SoundWave < handle ;
%                       ---------
%                       Soundwave generalises the excitation wave used by an UlraSound Transducer. As such it adjusts for 
%                       alterations made to one parameter by adapting the related parameters.
%                       ---------
%
%                       UltraSoundTransducer < SoundWave & DataType ;
%                       ---------
%                       The UltraSoundTransducer object is used to generate RadioFrequencyData from a Phantom. Multiple
%                       types of RFdata can be generated such as
%                           - Energy Maps (EDline)      using simulateEDline ;
%                           - RFlines                   using simulateRFline ;
%                           - Point responses           using simulateRFpoint ;
%                           - Batch of RFlines/RFpoints using simulateRFbatch ;
%                       All response calculations are facilitated using the FIELD II simulator.
%                       ---------
%
%                       Solver < DataType ;
%                       ---------
%                       Similar to UltraSoundTransducer, Solver is used to to transform one DataMatrix into an other. 
%                       Specifically UltraSoundData is sampled and afterwards reconstructed using a SystemMatrix and the
%                       reconstruct method to obtain ReconstructionData. Internally the following methods are used :
%                           - CVX             for L0 - reconstruction ;
%                           - IRLS and MAGIC  for L1 - reconstruction ;
%                           - LEASTSQUARE     for L2 - reconstruction ;
%                       ---------
%                       }
%
%%      Physical    {
%                   ThreeDimensionalPoint < handle ;
%                   ---------
%                   Three dimensional spacial points allowing for the vectorial functionality such as addition, vector
%                   multiplication, L2-distance, etc...
%                   ---------
%
%                   ScatterPoint < ThreeDimensionalPoint ;
%                   ---------
%                   Specialist class of Three dimensional spacial points adding an acoustic impetence parameter
%                   (amplitude) to the 3D point.
%                   ---------
%
%                   ThreeDimensionalBox < handle ;
%                   ---------
%                   This class is a pseudo specialisation of 3D points adding a vertical, horizontal and depth width
%                   to the point. Functionality includes calculating the enclosed volume and determining the distance
%                   to the center. 
%                   ---------
%
%                   Phantom < ThreeDimensionalBox & DataType ;
%                   ---------
%                   A phantom consist of 3D ScatterPoints (scatterers) contained in a 3D box each linked to
%                   an acoustic impedance parameter (amplitudes) denoting how much the scatterer will reflect incoming
%                   pressure waves. ScatterPoints are stored as in array in scatterDATA. Examples of phantoms include:
%                       - Point; which generates 1 scattering point.
%                       - Grid;  which generates a 3D grid of point scatterers.
%                       - Cyst;  which generates continous tissue containing $n$ high- and low intensity scatterers.
%
%                   Note;
%                       - 1 - International standard measures are used for distance and other variables.
%                       - 2 - phantom is a subclass of simulation and is thus able to save and load the bulk of it's
%                   data to the HDD.
%                   ---------
%
%                   CystPhantom < Phantom :
%                   ---------
%                   CystPhantom when initilized, generates an array of ScatterPoints. Spatially these point scatterers 
%                   have specified properties depending on theire location inside our outside a Cyst structure. 
%                   Five low-scattering cylinders and five high-scattering cylinders representing the ten cysts are
%                   created whithin the box of scatterers using this phantom setup.
%                   ---------
%
%                   GridPhantom < Phantom :
%                   ---------
%                   GridPhantom when initilized, generates an 3D grid of ScatterPoints with amplitude set to 1. The
%                   spacing between scatterers, the number of scatterers en theire relative location can be set in all
%                   direction x, y and z. 
%                   ---------
%
%                   PointPhantom < Phantom :
%                   ---------
%                   Simple class wrapping a 3D scatterpoint within a Phantom for increased functionality and use by
%                   other methods expection a Phantom as input.
%                   --------- 
%                   }
%                   
%%      Response    {
%                   RadioFrequencyData < DataMatrix :
%                   ---------
%                   This is the central class used in this project containing the recorded acoustic response when an
%                   UltrasSoundTransducer exites the scatterPoints in a Phantom. The information-properties here are
%                   crucial when truthely visualizing this data using imageNow. 
%                   1) Multiple types of RFdata exist including:
%                       - Energy Maps (EDline)  : containing the total received amount of energy from an UStransducer by scatterPoints contained in a GridPhantom.
%                       - RFlines               : The response received by a UStransducer after exciting a portion of a Phantom.
%                       - RFpoints              : The responce received by a UStransducer after exciting one scatterPoint within a Phantom.
%                       - Batches of RFlines/RFpoints ;
%                   2) Each type also comes in multiple versions:
%                       - Normalised            : -0.5 < pixelValues < 0.5 
%                       - Quantized             : class(pixelValues) = Integer
%                       - Decibel               : 0 < 256/A + 20*log10((-0.5 < pixelValues < 0.5) + A) < 256 ;
%                   ---------
%
%                   ReconstructionData < RadioFrequencyData :
%                   ---------
%                   This class is an extension of RadioFrequencyData. An instance created by a Solver when 
%                   reconstructing RFdata subsampled by a SystemMatrix. It's central property signalDATA contains the
%                   approximated signalDATA of the original RFdata. Moreover the property coeffDATA contains the
%                   coefficient of the signalDATA with respect to the SystemMatrix. 
%                   Similar to RFdata the signalDATA and coeffDATA can be visualized using imageNow. As REdata <
%                   RFdata it can also be fed to an BrightnessModeImage in order to obtain overal visualization of the
%                   structure contained in the original Phantom.
%                   ---------
%                   }
%	
%%      Image	{
%               BrightnessModeImage < DataMatrix ;
%               ---------
%               This class is used to import the full set of RFlines simulated by a UStranducer of a Phantom. 
%               After processing a bmodeIMAGE is obtained which represent the structure contained in a slice of
%               the Phantom. Again this data can be visualized using imageNow.
%               NOTE : The vizualization pipeline tries to mimic the visualization process performed by a real
%               UltraSound setup. This included beamforming of the RFdata and contrast compensation, etc...
%               ---------
%				}
%
%%      System	{
%               SystemMatrix < DataMatrix ;
%               ---------
%               The central component for applying LeastSquare regression or Compressive Sensing is choosing a
%               representation of the data of intrest (in our case RFdata).
%                   %% f = A * x %%
%               For this project we represent RFdata linearly by a collection of PSF's which are point responses of
%               an UStransducer exciting a single scatterPoint.
%               This class thus consists of the methods necceassry to create a 2D matrix of these PSF's and
%               applying multiplactions of this 2D matrix with other sparcifying matrices such as Wavelets,
%               Fouriers, Block-DCT, CDF97, etc...
%               ---------
%
%               SamplingMask < DataMatrix ;
%               ---------
%               The SamplingMask class provides all functionally in order to sample both the columns aswell as the
%               rows of a System Matrix. This includes defining density and sampling from this density. Moreover
%               since all data-indices are stored it is easy to quickly up/down-sample further at other rates
%               without calculation new sample location.
%                   - Sampling Rows comes down to downsampling the pixels in RFdata.
%                   - Samping Columns comes down to downsampling the list coeffcients which can be used in the
%               representation.
%               ---------
%				}
% 			}
%