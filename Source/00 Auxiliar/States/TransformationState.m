%% TransformationState

% Setting determining how to detect the envelope of a dataset.

% Created by Shaun Bundervoet on 22 AUG 16.
% Latest version 22 AUG 16.

classdef TransformationState < uint16
    
    enumeration
        RAW         (0)
        normalized  (1)
        decibel     (2)
        logarithm   (3)
        unity       (4) 
        absolute    (5)
    end
    
    methods
        function answer = isTransformed(state)
            switch state
                case TransformationState.RAW
                    answer = false ;
                otherwise
                    answer = true ;
            end
        end
        
        function absoluteSignal = invertTransformation(state, inputSignal, normalizationFactor)
            switch state
                case {TransformationState.RAW, TransformationState.unity, TransformationState.absolute} 
                    absoluteSignal  = normalizationFactor * inputSignal ;
                case TransformationState.normalized
                    absoluteSignal  = (2 * normalizationFactor) * inputSignal ;
                case TransformationState.decibel
                    absoluteSignal  = normalizationFactor * ((1/20)*10^((80/(2^8 -1))*(self.getMatrixDATA) - 80) - 1e-4) ;
                case TransformationState.logarithm
                    absoluteSignal  = normalizationFactor * (exp(inputSignal) - .1) ;
                otherwise
                    errorStruct.message     = ['State "', state,'" not recognised.'] ;
                    errorStruct.identifier  = 'TransformationState:invertTransformation:StateNotRecognised';
                    error(errorStruct) ;
            end
        end
        function outputSignal = forwardTransformation(state, inputSignal, normalizationFactor)
            % Normalise the inputSignal to [0 1] interval.
            absoluteSignal = abs(inputSignal ./ normalizationFactor) ;
            switch state
                case {TransformationState.RAW, TransformationState.unity, TransformationState.absolute} 
                    outputSignal   = absoluteSignal ; 
                case TransformationState.normalized
                    outputSignal   = inputSignal ./ (2 * normalizationFactor) ;
                case TransformationState.decibel
                    outputSignal   = ((2^8 -1)/80)*(20*log10(absoluteSignal + 1e-4) + 80) ;
                case TransformationState.logarithm
                    outputSignal   = ((2^8-1)/9) * (log(absoluteSignal + exp(-9)) + 9) ;
%                     outputSignal   = log(absoluteSignal + .1) ;
                otherwise
                    errorStruct.message     = ['State "', state,'" not recognised.'] ;
                    errorStruct.identifier  = 'TransformationState:forwardTransformation:StateNotRecognised';
                    error(errorStruct) ;
            end
        end
    end
end