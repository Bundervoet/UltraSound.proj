%% MatrixInfo

% Overall class tracking all possible states of the central matrixDATA.

% Created by Shaun Bundervoet on 24 AUG 16.
% Latest version 27 OKT 16.

classdef MatrixInfo < handle
    
    properties
        transformed@TransformationState = TransformationState.RAW ;                                 % Current transform applied  to DataMatrix.
        normalizationFactor@double      = 1 ;                                                       % NormalizationFactor applied when transforming.
        apodized@ApodizationState       = ApodizationState.no ;                                     % Remember if DataMatrix is apodized.   
        window@char                     = 'none' ;                                                  % Window applied during apodizationg.
        sparsified@SparsityState        = SparsityState.no ;                                        % Remember if DataMatrix is apodized. 
        sparsityFactor@double           = 1 ;                                                       % SparsityFactor applied when sparsifying DataMatrix.
        cropped@CropState               = CropState.none ;                                          % Remember if signal has been cropped.
        bounds@struct                   = struct('row', uint16([0 0]), 'col', uint16([0 0])) ;      % If Matrix is cropped along a certain dimenions this parameter remebers the orignal position of the first and last pixel.
    end
    
    methods
        function self = MatrixInfo()
        end
            
        %% Status Questions
        function answer = isApodized(state)
            answer = state.apodized.isApodized ;            
        end
        function answer = isSparsified(state)
            answer = state.sparsified.isSparsified ;            
        end
        function answer = isCropped(state)
            answer = state.cropped.isCropped ;            
        end
        function answer = isTransformed(state)
            answer = state.transformed.isTransformed ;            
        end
        function answer = isNormalized(state)
            answer = (state.transformed == TransformationState.normalized) ;
        end
        function answer = isDecibel(state)
            answer = (state.transformed == TransformationState.decibel) ;
        end
        function answer = isAbsolute(state)
            answer = (state.transformed == TransformationState.absolute) ;
        end
        function setApodized(state, window)
            if nargin > 1
                state.window = window ; 
            end
            state.apodized = ApodizationState.yes ;
        end
        function setSparsified(state, sparsifyingFactor)
            if nargin > 1
                state.sparsityFactor = sparsifyingFactor ; 
            end
            state.sparsified = SparsityState.yes ;
        end
        function setCropped(state, type, lowerBound, upperBound)
            if nargin < 4
                state.setBounds(type, lowerBound) ;
            else
                state.setBounds(type, lowerBound, upperBound) ;
            end
            state.changeCropState(type) ; 
        end
        function setNormalized(state, normalizationFactor)
            if nargin > 1
                state.normalizationFactor = normalizationFactor ;
            end
            state.transformed = TransfromationState.normalized ;
        end
        function setDecibel(state, normalizationFactor)
            if nargin > 1
                state.normalizationFactor = normalizationFactor ;
            end
            state.transformed = TransfromationState.decibel ;
        end
        function setLogarithm(state, normalizationFactor)
            if nargin > 1
                state.normalizationFactor = normalizationFactor ;
            end
            state.transformed = TransfromationState.logarithm ;
        end
        function output = getBounds(state, type, bounds)
            if nargin < 2
                type = 'row' ;
            end
            if nargin < 3
                bounds = 1:2 ;
            end
            output = double(state.bounds.(type)(bounds)) ;
        end
        function setBounds(state, type, lowerBound, upperBound)
            if nargin < 2
                type = 'row' ;
            end
            if nargin > 3
                state.bounds.(type) = uint16([lowerBound, upperBound]) ;
            elseif nargin > 2 && length(lowerBound) > 1
                state.bounds.(type) = uint16(lowerBound) ; 
            end
        end
        function setLowerBound(state, type, bound) 
            state.bounds.(type)(1) = uint16(bound) ; 
        end
        function setUpperBound(state, type, bound) 
            state.bounds.(type)(2) = uint16(bound) ; 
        end
        function changeCropState(state, type)
            switch type
                case 'row'
                    switch state.cropped
                        case {CropState.none, CropState.row}
                            state.cropped = CropState.row ;
                        otherwise
                            state.cropped = CropState.both ;
                    end
                case {'col', 'column'}
                    switch state.cropped
                        case {CropState.none, CropState.col}
                            state.cropped = CropState.col ;
                        otherwise
                            state.cropped = CropState.both ;
                    end
                otherwise
                    errorStruct.message = ['Unrecognised Crop State "', type,'" during setting.'];
                    errorStruct.identifier = 'MatrixInfo:changeCropState:UnrecognisedState';
                    error(errorStruct) ;
                    
            end
        end
        function set(state, type, additionalInput)
            switch type
                case 'RAW'
                    state.transformed = TransformationState.RAW ;
                    if nargin > 2 
                        state.normalizationFactor = additionalInput ; 
                    end
                case 'normalized'
                    state.transformed = TransformationState.normalized ;
                    if nargin > 2 
                        state.normalizationFactor = additionalInput ; 
                    end
                case 'decibel'
                    state.transformed = TransformationState.decibel ;
                    if nargin > 2 
                        state.normalizationFactor = additionalInput ; 
                    end
                case 'logarithm'
                    state.transformed = TransformationState.logarithm ;
                    if nargin > 2 
                        state.normalizationFactor = additionalInput ; 
                    end
                case 'apodized'
                    state.apodized = ApodizationState.yes ;
                    if nargin > 2 
                        state.window = additionalInput ; 
                    end
                case 'sparsified'
                    state.sparsified = SparsityState.yes ;
                    if nargin > 2 
                        state.sparsityFactor = additionalInput ; 
                    end
                    
            end
            
        end
    end  
end