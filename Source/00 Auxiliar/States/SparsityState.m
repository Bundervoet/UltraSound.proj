%% SparsityState

% Setting determining how to detect the envelope of a dataset.

% Created by Shaun Bundervoet on 17 OKT 16.
% Latest version 16 OKT 16.

classdef SparsityState < logical
    
    enumeration
        no        (0)
        yes       (1)
    end
    
    methods
        function answer = isSparsified(state)
            switch state
                case SparsityState.no
                    answer = false ;
                otherwise
                    answer = true ; 
            end            
        end
    end  
end