%% ApodizationState

% Setting determining how to detect the envelope of a dataset.

% Created by Shaun Bundervoet on 22 AUG 16.
% Latest version 22 AUG 16.

classdef ApodizationState < logical
    
    enumeration
        no        (0)
        yes       (1)
    end
    
    methods
        function answer = isApodized(state)
            switch state
                case ApodizationState.no
                    answer = false ;
                otherwise
                    answer = true ; 
            end            
        end
    end  
end