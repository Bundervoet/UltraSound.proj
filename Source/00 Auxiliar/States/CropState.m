%% CropState

% State keeping track of signal cropping.

% Created by Shaun Bundervoet on 17 OKT 16.
% Latest version 17 OKT 16.

classdef CropState < uint16
    
    enumeration
        none      (0)
        row       (1)
        col       (2)
        both      (3)
    end
    
    methods
        function answer = isCropped(state)
            switch state
                case CropState.none
                    answer = false ;
                otherwise
                    answer = true ;
            end
        end
    end
end