%% DynamicSetting

% Setting determining how an image should be transformed before viewing, i.e. which dynamic range
% should be used.

% Created by Shaun Bundervoet on 24 AUG 16.
% Latest version 24 AUG 16.

classdef DynamicSetting < uint16
    
    enumeration
        none        (0) 
        plain       (1)
        decibel     (2)
        square      (3)
        absolute    (4)
    end
    
    methods      
        function outputSignal = transform(setting, inputSignal, lowerBound, upperBound)    
            % outputSignal should be forced to fall withing the [0 255] interval! 
            switch setting
                case DynamicSetting.none 
                    outputSignal   = inputSignal ;
                case DynamicSetting.plain
                    normalizationFactor = (upperBound - lowerBound) ;
                    outputSignal   = ((2^8 - 1)/normalizationFactor ) * (inputSignal - lowerBound) ;
%                     outputSignal   = ((2^8 - 1) * (inputSignal/normalizationFactor ) * (inputSignal - lowerBound) ;
                case DynamicSetting.decibel
                    normalizationFactor = max(abs(lowerBound), abs(upperBound)) ;
                    outputSignal   = ((2^8 -1)/80)*(20*log10(abs(inputSignal)/normalizationFactor + 1e-4) + 80) ;
                case DynamicSetting.square
                    normalizationFactor = max(lowerBound^2, upperBound^2) ;
                    outputSignal   = (2^8 - 1) * (inputSignal.^2 / normalizationFactor) ;
                case DynamicSetting.absolute
                    normalizationFactor = max(abs(lowerBound), abs(upperBound)) ;
                    outputSignal   = (2^8 - 1) * (abs(inputSignal) / normalizationFactor) ;
            end
        end
    end
end