%% SolverSetting

% List of various implemented solvers.

% Created by Shaun Bundervoet on 2 SEP 16.
% Latest version 2 SEP 16.

classdef SolverSetting < uint8
    
    %% STATES
    enumeration
        none            (0)
        inverse         (1)

        CVX             (2)
        IRLS            (3)
        
        LSQR            (4)
        SPGL            (5)
        L1MAGIC         (6)
    end
    
    %% METHODS
    methods
        function output = L2(setting)
            switch setting
                case SolverSetting.inverse, SolverSetting.LSQR
                    output = true ;
                otherwise
                    output = false ;
            end
        end
        function output = L1(setting)
            switch setting
                case {SolverSetting.SPGL, SolverSetting.L1MAGIC, SolverSetting.IRLS}
                    output = true ;
                otherwise
                    output = false ;
            end
        end
        function output = L0(setting)
            switch setting
                case {SolverSetting.CVX}
                    output = true ;
                otherwise
                    output = false ;
            end
        end
    end
end