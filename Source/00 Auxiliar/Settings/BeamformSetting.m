%% BeamformSetting

% Set which beamformer method should be used.

% Created by Shaun Bundervoet on 22 AUG 16.
% Latest version 22 AUG 16.

classdef BeamformSetting < uint8
    
    %% STATES
    enumeration
        center      (0)
        summation   (1)
        focusing    (2)
    end
    
    %% METHODS
    methods

    end
end