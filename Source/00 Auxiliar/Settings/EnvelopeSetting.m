%% EnvelopeSetting

% Setting determining how to detect the envelope of a dataset.

% Created by Shaun Bundervoet on 22 AUG 16.
% Latest version 22 AUG 16.

classdef EnvelopeSetting < uint8
    
    %% STATES
    enumeration
        none            (0)
        lineWise        (1)
        overall         (2)
    end
    
    %% METHODS
    methods
        function outputSignal = detect(setting, inputSignal)
            switch setting
                case EnvelopeSetting.lineWise
                    for jj = 1:size(inputSignal, 2)
                        outputSignal        = zeros(size(inputSignal)) ;
                        outputSignal(:,jj)  = abs(hilbert(inputSignal(:,jj))) ;
                    end
                case EnvelopeSetting.overall
                    % Determine the low-frequency enveloppe signal of the high-frequency
                    % beamformed RFline. This high-frequency in the Rfline originates from the
                    % excitation pulse used by the transducer but contains no imporatant visual
                    % information about the underlying phantom tissue.
                    outputSignal            = abs(hilbert(inputSignal)) ;
                otherwise 
                    outputSignal = inputSignal ;
            end
        end
    end
end