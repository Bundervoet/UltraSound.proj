%% OutputSetting

% Choose which data to simulate.

% Created by Shaun Bundervoet on 8 AUG 16.
% Latest version 8 AUG 16.

classdef OutputSetting < uint8
    
    %% STATES
    enumeration
        nothing         (0)
        beamformed      (1)
        channel         (2)
        plainWave       (3)
        energy          (4) 
        absolute        (5)
        synthetic_vAll  (6)
        synthetic_v001  (7)
        synthetic_v002  (7)
    end
    
    %% METHODS
    methods
        function output = simulateEnergy(setting)
            switch(setting)
                case {OutputSetting.energy, OutputSetting.absolute}
                    output = true ;
                otherwise
                    output = false ; 
            end
        end
    end
end