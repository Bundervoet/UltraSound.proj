%% ApodizationSetting

% List of various Tx/Rx windows supported

% Created by Shaun Bundervoet on 8 AUG 16.
% Latest version 8 AUG 16.

classdef ApodizationSetting < uint8
    
    %% STATES
    enumeration
        none        (0)
        hanning     (1)
        hamming     (2)
    end
    
    %% METHODS
    methods
        function output = useApodization(setting)
            switch setting
                case ApodizationSetting.none
                    output = false ;
                otherwise
                    output = true ;
            end
        end
        function outputWindow = apodizationArray(setting, width)
            switch setting
                case ApodizationSetting.none 
                    outputWindow = ones(1, width) ; 
                case ApodizationSetting.hanning
                    outputWindow = transpose(hanning(width)) ;
                case ApodizationSetting.hamming
                    outputWindow = transpose(hamming(width)) ;
            end
        end
    end
end