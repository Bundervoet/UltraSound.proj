%% SparsitySetting

% Choose how to sparsify data

% Created by Shaun Bundervoet on 30 AUG 16.
% Latest version 30 AUG 16.

classdef SparsitySetting < uint8
    
    %% STATES
    enumeration
        complete            (0)                                                                     % DO NOT sparsify   AND keep all signals.
        sparse              (1)                                                                     % Sparsify          AND keep all signals.
        completeRejectZero  (2)                                                                     % DO NOT sparsify   AND delete zero-signal.
        sparseRejectZero    (3)                                                                     % Sparsify          AND delete zero-signal.
        completeRejectSmall (4)                                                                     % DO NOT sparsify   AND delete small-signal.
        sparseRejectSmall   (5)                                                                     % Sparsify          AND delete small-signal.
    end
    
    %% METHODS
    methods
        function answer = sparsify(setting) 
            switch setting
                case {SparsitySetting.sparse, SparsitySetting.sparseRejectZero, SparsitySetting.sparseRejectSmall}
                    answer = true ;
                otherwise
                    answer = false ;
            end
        end
        function answer = rejectZero(setting) 
            switch setting
                case {SparsitySetting.completeRejectZero, SparsitySetting.sparseRejectZero}
                    answer = true ;
                otherwise
                    answer = false ;
            end
        end
        function answer = rejectSmall(setting)
            switch setting
                case {SparsitySetting.completeRejectSmall, SparsitySetting.sparseRejectSmall}
                    answer = true ;
                otherwise
                    answer = false ;
            end
        end
        function answer = doReject(setting)
            if setting.rejectZero() || setting.rejectSmall()
                answer = true ;
            else
                answer = false ;
            end
        end
    end
end
