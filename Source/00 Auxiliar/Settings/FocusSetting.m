%% FocusSetting

% Various settings provided when Tx/Rxfocussing 

% Created by Shaun Bundervoet on 8 AUG 16.
% Latest version 8 AUG 16.

classdef FocusSetting < uint8
    
    %% STATES
    enumeration
        no      (0)
        yes     (1)
        multi   (2)
        dynamic (3)
    end
    
    %% METHODS
    methods
        function output = useFocus(setting)
            switch(setting)
                case {FocusSetting.yes, FocusSetting.multi, FocusSetting.dynamic}
                    output = true ;
                otherwise
                    output = false ;
            end
        end
    end
end