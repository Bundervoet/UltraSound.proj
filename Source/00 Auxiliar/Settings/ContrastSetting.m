%% ContrastSetting

% Setting determining how to improve contrast.

% Created by Shaun Bundervoet on 22 AUG 16.
% Latest version 22 AUG 16.

classdef ContrastSetting < uint8
    
    %% STATES
    enumeration
        none            (0)
        decibel         (1)
        logarithm       (2)
    end
    
    %% METHODS
    methods
        function outputSignal = increaseContrast(setting, inputSignal, normalizationFactor)
            if nargin < 3
                normalizationFactor = max(abs(inputSignal(:))) ;
            elseif normalizationFactor == 0
                normalizationFactor = max(abs(inputSignal(:))) ;
            end
            
            % Normalize full signal to within [0 1] interval.
            normalizedSignal        = inputSignal ./ normalizationFactor ;
            
            switch setting
                case ContrastSetting.decibel
                    % This type is preferably used with non normalised RFlines.
                    % Normalize the data before initiating the upcomming steps which improve
                    % the gain of the enveloppe detected image. This normalization can be done
                    % using the env signals own maximum or by using a prediscribed maximum
                    % which helps ensure equal normalization over multiple similar bmode images
                    % ( for example when comparing multiple reconstruction ).                 
                    
                    % Perform a Db transform of the enveloppe image. Output signal in [0 255]
                    % inteval. 
                    outputSignal            = ((2^8-1)/80) * (20*log10(normalizedSignal + 1e-4) + 80) ;
                case ContrastSetting.logarithm
                    outputSignal            = ((2^8-1)/9) * (log(normalizedSignal + exp(-9)) + 9) ;
                otherwise
                    outputSignal = inputSignal ;
            end
        end
        
    end
end