%% ScatterSetting

% Setting determining which imaging type to use when visualizing scatter data.

% Created by Shaun Bundervoet on 30 AUG 16.
% Latest version 30 AUG 16.

classdef ScatterSetting < uint16
    
    %% SETTINGS
    enumeration
        Amplitude2D         (1)                                                                     % 2D rendering of scatterer location with amplitude.
        Density2D           (2)                                                                     % 2D rendering of scatterer location with density.                                                         
        Amplitude3D         (3)                                                                     % 3D rendering of scatterer location with amplitude. 
        Density3D           (4)                                                                     % 3D rendering of scatterer location with density. 
        PartialAmplitude3D  (5)                                                                     % Partial 3D rendering of scatterer location with amplitude. 
        PartialDensity3D    (6)                                                                     % Partial 3D rendering of scatterer location with density. 
    end
    
    
    %% METHODS
    methods      

    end
end