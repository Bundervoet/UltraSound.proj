%% PhantomImageParameter

% Class containing all imaging parameters when using imager.

% Created by Shaun Bundervoet on 30 AUG 16.
% Latest version 30 AUG 16.

classdef PhantomImageParameter < ImageParameter
    
    %% PROPERTIES
    properties
        rendering@ScatterSetting        = ScatterSetting.Amplitude2D ;                              % Control visualization technique.
        addedName@char                  = ''                                                        % Parameter used when storing visual.
        density@double                  = [] ;                                                      % Add Overlapping density to replace scatter amplitudes during visualization.
        mask@logical                    = logical([]) ;                                             % Control which scatterers to image.
    end
    
    
    %% METHODS
    methods
        
        
        %% BASIC CONSTRUCTOR METHOD 
        function self = PhantomImageParameter(type, noScatterers, varargin) 
            
            % Initialize upperclass.
            self@ImageParameter(type) ;
            
            if nargin < 2 ;
                noScatterers = 0 ;
            end            
            if nargin < 1
                type = 'none' ;
            end
            if ~isa(type, 'char') 
                type = class(type) ;
            end
            switch lower(type)  
                case 'cystphantom'                  
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 15 ;                                                      % Size of ScatterPoint
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 500 ;
                    self.cmap           = parula(256) ;
                    self.format         = 'png' ;
                    self.data           = 'scatterers' ;
                    self.check          = true ;  
                    self.fixed          = false ;
                    
                    self.rendering      = ScatterSetting.Amplitude3D ;
                    self.addedName      = '' ;
                    self.density        = ones(noScatterers, 1) ;
                    self.mask           = true(noScatterers, 1) ;
                case 'gridphantom'
                    mask            = false(noScatterers,1) ;
                    mask(1:8:end)   = true ;
                    
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 15 ;                                                      % Size of ScatterPoint
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 600 ;
                    self.cmap           = parula(256) ;
                    self.format         = 'png' ;
                    self.data           = 'scatterers' ;
                    self.check          = true ;  
                    self.fixed          = false ;
                    
                    self.rendering      = ScatterSetting.Amplitude2D ;
                    self.addedName      = '' ;
                    self.density        = ones(noScatterers, 1) ;
                    self.mask           = mask ;
              
                otherwise
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 15 ;                                                      % Size of ScatterPoint
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 600 ;
                    self.cmap           = parula(256) ;
                    self.format         = 'png' ;
                    self.data           = 'scatterers' ;
                    self.check          = true ;  
                    self.fixed          = false ;
                    
                    self.rendering      = ScatterSetting.Amplitude2D ;
                    self.addedName      = '' ;
                    self.density        = ones(noScatterers, 1) ;
                    self.mask           = true(noScatterers, 1);
            end
            
            % Set given properties with non standard-values.
            self.set(varargin{:}) ;
        end           
    end
end