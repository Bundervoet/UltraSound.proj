%% Matrix TYpe

% Class containing Matrix NameType en TranposeType to use.

% Created by Shaun Bundervoet on 23 Nov 16.
% Latest version 23 Nov 16.

classdef MaT
    
    %% PROPERTIES
    properties
        N@char      = '' ;
        T@logical   = false ;
    end   
    
    %% METHODS
    methods
        
        
        %% BASIC CONSTRUCTOR METHOD 
        function self = MaT(N, T) 
            if nargin == 2
                self.N = N ;
                self.T = T ;
            elseif isa(N, 'char')
                self.N = N ;
                self.T = false ;
            elseif isa(N, 'logical')
                self.N = sensing ;
                self.T = N ; 
            elseif isa(N, 'MaT')
                self = N ;
            end
        end    
        function output = str(self)
            output = self.N ;
        end
        function output = strT(self)
            if self.T ; T = 'T' ; else, T = '' ; end ; %#ok<*PROP>
            output = [self.N, T] ;
        end
        function output = strD(self)
            % Initialize function variables
            nameINDX    = strfind(self.N, 'sensing') ;
            if ~isempty(nameINDX)
                output  = [self.N(1:(nameINDX-1)), 'dictionary', self.N((nameINDX+7):end)] ;
            else
                output 	= 'dictionary' ;
            end
        end
        function output = strDT(self)
            % Initialize function variables
            nameINDX    = strfind(self.N, 'sensing') ;
            if ~isempty(nameINDX)
                name  = [self.N(1:(nameINDX-1)), 'dictionary', self.N((nameINDX+7):end)] ;
            else
                name 	= 'dictionary' ;
            end
            if self.T ; T = 'T' ; else, T = '' ; end ; %#ok<*PROP>
            output = [name, T] ;
        end
    end
end