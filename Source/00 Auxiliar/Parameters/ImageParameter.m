%% ImageParameter

% Class containing all imaging parameters when using imageNow.

% Created by Shaun Bundervoet on 24 AUG 16.
% Latest version 24 AUG 16.

classdef ImageParameter < handle
    
    %% PROPERTIES
    properties
        dynamic@DynamicSetting          = DynamicSetting.plain ;                                    % Choose further transformation of imaging data.
        scale@double                    = 1 ;                                                       % Inflate/decrease image data.
        contrast@double                 = 1 ;                                                       % Choose to only view a partial segment of [min max].
        minimum@double                  = 0 ;                                                       % Pixel value minimum.
        maximum@double                  = 255 ;                                                     % Pixel value maximum.
        window@double                   = 1 ;                                                       % Matlab figure window inwhich to show image.
        cmap@double                     = parula(256) ;                                             % Colormap chosen to represent data.
        format@char                     = 'png' ;                                                   % Text-format inwhich to export data.
        data@char                       = 'matrixDATA' ;                                            % Select which specific data to view.
        index@double                    = 1 ;                                                       % Specified when multiple elements exist.
        check@logical                   = true ;                                                    % ? unknown @the moment...
        meta@struct                     = struct() ;                                                % This property is used when addional properties need te be defined.
        fixed@logical                   = false ;                                                   % Allow changes to parameters?
    end
    
    
    %% METHODS
    methods
        
        
        %% BASIC CONSTRUCTOR METHOD 
        function self = ImageParameter(type, varargin)  
            if nargin < 1
                type = 'none' ;
            end
            if ~isa(type, 'char') 
                type = class(type) ;
            end
            switch lower(type)
                case 'radiofrequencydata'
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 1 ;
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 100 ;
                    self.cmap           = parula(256) ;
                    self.format         = 'png' ;
                    self.data           = 'signalDATA' ;
                    self.check          = true ;
                    self.fixed          = false ;
                    
                case 'brightnessmodeimage'
                    self.dynamic        = DynamicSetting.none ;
                    self.scale          = 1 ;
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 200 ;
                    self.cmap           = gray(256) ;
                    self.format         = 'png' ;
                    self.data           = 'bmode' ;
                    self.check          = true ;  
                    self.meta           = struct('step', 'result') ; 
                    self.fixed          = false ;
                    
                case 'systemmatrix'
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 1 ;
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 300 ;
                    self.cmap           = parula(256) ;
                    self.format         = 'png' ;
                    self.data           = 'element' ;
                    self.index          = 1 ;
                    self.check          = true ;  
                    self.fixed          = false ;                   
                    self.meta           = struct('type', MaT('dictionary', false)) ;
                    
                case 'samplingmask'
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 1 ;
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 400 ;
                    self.cmap           = parula(256) ;
                    self.format         = 'png' ;
                    self.data           = 'density' ;
                    self.index          = 1 ;
                    self.check          = true ;  
                    self.fixed          = false ;  
            
                case 'reconstructiondata'
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 1 ;
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 100 ;
                    self.cmap           = parula(256) ;
                    self.format         = 'png' ;
                    self.data           = 'reconstructionDATA' ;
                    self.index          = 1 ;
                    self.check          = true ;
                    self.meta           = struct('step', 'result', 'space', 'id', ...
                        'xd', 4, 'yd', 1, 'w', 1) ;
                    self.fixed          = false ;                   
            
                case {'cyst2dgridphantom', 'gridphantom'}
                    self.dynamic        = DynamicSetting.decibel ;
                    self.scale          = 3 ;
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 255 ;
                    self.window         = 100 ;
                    self.cmap           = gray(256) ;
                    self.format         = 'png' ;
                    self.data           = 'sliceDATA' ;
                    self.index          = 1 ;
                    self.check          = true ;
                    self.fixed          = false ;
                    
                otherwise
                    self.dynamic        = DynamicSetting.plain ;
                    self.scale          = 1 ;
                    self.contrast       = 1 ;
                    self.minimum        = 0 ;
                    self.maximum        = 1 ;
                    self.window         = 900 ;
                    self.cmap           = gray(256) ;
                    self.format         = 'png' ;
                    self.data           = 'matrixDATA' ;
                    self.check          = true ; 
                    self.fixed          = false ;
            end
            
            % Set given properties with non standard-values.
            self.set(varargin{:}) ;
        end
        function unlock(self) 
            self.fixed = false ;
        end
        function setOne(par, input, value)
            % --- 
            % This methods allows the user to change one specific property. The method checks to
            % ensure that the properties are unlocked.
            % ---
            if sum(strcmp(properties(par), input))
                if ~par.fixed
                    par.(input) = value ;
                else
                    warning('!!ImageParameter::setOne::Properties have been locked!!') ;
                end
            else
                % Only allow specific parameters to be set to "meta". This ensures that now mistakes
                % are made when setting image parameters with unidentified names.
                if sum(strcmp(input, {'step', 'type', 'result', 'space', 'xd', 'yd', 'w'}))
                    par.meta.(input) = value ;
                else
                    % Throw an error if the given variable name does not belong to the list of
                    % ImageParameters.
                    errorStruct.message = ['Input parameter "', input,'" is not a valid image parameter.'] ;
                    errorStruct.identifier = 'ImageParameter:setOne:InvalidParameterName';
                    error(errorStruct) ;
                end
            end
        end
        function set(par, varargin)
            % ---
            % This methods allows to change the initial imaging parameters. This includes 
            % recognising intuitive input such as 'parula' for the colormapping.
            % ---
            for jj = 1:2:length(varargin)
                input = varargin{jj} ; value = varargin{jj+1} ;
                if isvarname(input)
                    % List of specific recognised inputs. 
                    % ex self.imageNow('h',100) equals self.imageNow('window', 100).
                    switch lower(input)
                        case {'colormap', 'cmap'}           % Specialised input for colormap
                            if isa(value, 'char') 
                                switch lower(varargin{jj+1})
                                    case 'parula'
                                        par.setOne('cmap', parula(256)) ;
                                    case {'gray', 'grey'}
                                        par.setOne('cmap', gray(256)) ;
                                    case 'jet'
                                        par.setOne('cmap', jet(256)) ;
                                end
                            else
                                par.setOne('cmap', value) ;
                            end
                        case 'h'                            % Recognise 'h' as input for the window number
                            par.setOne('window', value) ;
                        case 'point'
                            par.setOne('index', value) ;
                        case {'lock', 'fixed'}
                            par.fixed  = value ;
                        case {'dynamic', 'dynamicsetting'}
                            if isa(value, 'char') 
                                par.setOne('dynamic', DynamicSetting.(value)) ;
                            else
                                par.setOne('dynamic', value) ;
                            end
                        otherwise
                            par.setOne(input, value) ; 
                    end
                else
                    % Throw an error if the given name does not fullfile the requirements of a
                    % variable name for MATLAB.
                    switch class(input)
                        case 'char'
                            errorStruct.message = ['Input argument "', input,'" is not a valid field name.'] ;
                        otherwise
                            errorStruct.message = ['Input argument "', num2str(input),'" is not a valid field name.'] ;
                    end
                    errorStruct.identifier = 'ImageParameter:setPar:InvalidFieldName';
                    error(errorStruct) ;
                end
            
            end
        end
        function setPar(par, varargin) % OLD METHOD NAME
            par.set(varargin{:}) ;
        end
        function outputSignal = transform(par, inputSignal)
            % Relay transformation to dynamicParameter.
            outputSignal = par.dynamic.transform(inputSignal, par.minimum, par.maximum) ;
        end       
    end
end