%% Tbl

% Class used to quickly construct and output tabled information.

% Created by Shaun Bundervoet on 1 Mar 17.
% Latest version 1 Mar 17.

classdef Tbl
    
    %% PROPERTIES
    properties
        info@table                    
    end   
    
    %% METHODS
    methods
        
        
        %% BASIC CONSTRUCTOR METHOD 
        function self = Tbl(colNames, rowNames) 
            colString = '' ;
            for cc = 1:length(colNames)
                cmd = [colNames{cc}, ' = zeros(length(rowNames),1) ;'] ; eval(cmd) ;
                colString = [colString, colNames{cc}, ', '] ;
            end
            colString = colString(1:(end-2)) ;           
            cmd = ['self.info = table(', colString,', ''rownames'', rowNames) ; '] ; eval(cmd) ;
        end           
    end
end