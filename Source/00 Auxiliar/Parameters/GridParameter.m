%% GridParameter

% Class containing all grid parameters for one coordinate

% Created by Shaun Bundervoet on 30 AUG 16.
% Latest version 30 AUG 16.

classdef GridParameter < handle
    
    %% PROPERTIES
    properties
        startingPosition@double         = 0 ;
        jumpSize@double                 = 0 ;
        noJumps@uint16                  = uint16(0) ;
    end
    
    
    %% METHODS
    methods
        
        
        %% BASIC CONSTRUCTOR METHOD 
        function self = GridParameter(start, jump, no) 
            if nargin > 1 
                self.startingPosition   = start ;
                self.jumpSize           = jump ;
                self.noJumps            = uint16(no) ;
            end
        end
        function output = start(par)
            output = par.startingPosition ;
        end
        function output = jump(par)
            output = par.jumpSize ;
        end
        function output = no(par)
            output = double(par.noJumps) ;
        end
        function setPar(par, input, value )
            % ---
            % This methods allows the user to change one specific property.
            % ---
            switch lower(input)
                case {'start', 'startingposition'}
                    par.startingPosition   = value ;
                case {'jump', 'jumpsize'}
                    par.jumpSize           = value ;
                case {'no', 'nojumps', 'd'}
                    par.noJumps            = uint16(value) ;
                otherwise
                    if sum(strcmp(properties(par), input))
                        par.(input) = value ;
                    else
                        % Throw an error if the given variable name does not belong to the list of
                        % ImageParameters.
                        errorStruct.message = ['Input parameter "', input,'" is not a valid image parameter.'] ;
                        errorStruct.identifier = 'GridParameter:setPar:InvalidParameterName';
                        error(errorStruct) ;
                    end
            end
        end
        function output = linearize(par)
            output = linspace(par.startingPosition, par.startingPosition+(par.jumpSize * double(par.noJumps-1)), double(par.noJumps-1)) ;
        end
    end
end